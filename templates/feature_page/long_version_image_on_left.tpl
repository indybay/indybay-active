<div class="story blurbwrapper">
  <div class="storymobileimage">
    <a class="headline-image" title="{{ TPL_LOCAL_ALT_TAG|raw }}" href="{{ TPL_LOCAL_RELATED_URL|raw }}">
      <img loading="lazy" class="{{ TPL_LOCAL_IMAGE_CLASS|raw }}" width="{{ TPL_LOCAL_IMAGE_WIDTH|raw }}" height="{{ TPL_LOCAL_IMAGE_HEIGHT|raw }}" src="{{ TPL_LOCAL_IMAGE_URL|raw }}" alt="{{ TPL_LOCAL_ALT_TAG|raw }}"></a>
  </div>
  <div class="hed">
    <div class="feature-blurb-date">
      {{ TPL_LOCAL_FORMATTED_DATE|raw }}
    </div>
    <span class="headline-text">{{ TPL_LOCAL_TITLE1|raw }}</span>
  </div>
  <div class="feature-blurb-background">
    <div class="feature-blurb-subhead">
      <!-- <div class="feature-blurb-date">
        {{ TPL_LOCAL_FORMATTED_DATE|raw }}
      </div> -->
      <span class="feature-blurb">{{ TPL_LOCAL_TITLE2|raw }}</span>
    </div>
  </div>
  <div class="blurb">
    <div class="storyimage">
      <a class="headline-image" title="{{ TPL_LOCAL_ALT_TAG|raw }}" href="{{ TPL_LOCAL_RELATED_URL|raw }}">
        <img loading="lazy" class="{{ TPL_LOCAL_IMAGE_CLASS|raw }}" width="{{ TPL_LOCAL_IMAGE_WIDTH|raw }}" height="{{ TPL_LOCAL_IMAGE_HEIGHT|raw }}" src="{{ TPL_LOCAL_IMAGE_URL|raw }}" class="blurb-img-left" alt="{{ TPL_LOCAL_ALT_TAG|raw }}"></a>
    </div>
    <div class="blurbtext">
      {{ TPL_LOCAL_BREAKING_NEWS|raw }}
      {{ TPL_LOCAL_TEXT|raw }}
    </div>
  </div>
</div>
<!-- template was long_version_image_on_left -->
