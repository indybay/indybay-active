<div class="story blurbwrapper">
  <div class="hed">
    <div class="feature-blurb-date">
      {{ TPL_LOCAL_FORMATTED_DATE|raw }}
    </div>
    <span class="headline-text">{{ TPL_LOCAL_TITLE1|raw }}</span>
  </div>
  <div class="feature-blurb-background">
    <div class="feature-blurb-subhead">
      <!-- <div class="feature-blurb-date">
        {{ TPL_LOCAL_FORMATTED_DATE|raw }}
      </div> -->
      <span class="feature-blurb">{{ TPL_LOCAL_TITLE2|raw }}</span>
    </div>
  </div>
  <div class="blurb">
    <div class="blurbtext">
      {{ TPL_LOCAL_BREAKING_NEWS|raw }}
      {{ TPL_LOCAL_TEXT|raw }}
    </div>
  </div>
</div>
<!-- template was long_version_no_image -->
