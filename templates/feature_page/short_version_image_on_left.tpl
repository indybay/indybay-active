<div class="storyshort blurbwrapper">
  <div class="storymobileimage">
    <a class="headline-image" title="{{ TPL_LOCAL_ALT_TAG|raw }}" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}">
      <img loading="lazy" class="{{ TPL_LOCAL_IMAGE_CLASS|raw }}" width="{{ TPL_LOCAL_IMAGE_WIDTH|raw }}" height="{{ TPL_LOCAL_IMAGE_HEIGHT|raw }}" src="{{ TPL_LOCAL_IMAGE_URL|raw }}" alt="{{ TPL_LOCAL_ALT_TAG|raw }}"></a>
  </div>
  <div class="hed">
    <div class="feature-blurb-date">
      {{ TPL_LOCAL_FORMATTED_DATE|raw }}
    </div>
    <a class="headline-text" name="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" rel="bookmark" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}" title="Permanent link to this story">{{ TPL_LOCAL_TITLE1|raw }}</a>
  </div>
  <div class="feature-blurb-background">
    <div class="feature-blurb-subhead">
      <!-- <div class="feature-blurb-date">
        {{ TPL_LOCAL_FORMATTED_DATE|raw }}
      </div> -->
      <a class="feature-blurb" rel="bookmark" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}" title="Permanent link to this story">{{ TPL_LOCAL_TITLE2|raw }}</a>
    </div>
  </div>
  <div class="blurb">
    <div class="storyimage">
      <a class="headline-image" title="{{ TPL_LOCAL_ALT_TAG|raw }}" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}">
        <img loading="lazy" class="{{ TPL_LOCAL_IMAGE_CLASS|raw }}" width="{{ TPL_LOCAL_IMAGE_WIDTH|raw }}" height="{{ TPL_LOCAL_IMAGE_HEIGHT|raw }}" src="{{ TPL_LOCAL_IMAGE_URL|raw }}" class="blurb-img-left" alt="{{ TPL_LOCAL_ALT_TAG|raw }}"></a>
    </div>
    <div class="blurbtext">
      {{ TPL_LOCAL_BREAKING_NEWS|raw }}
      {{ TPL_LOCAL_SUMMARY|raw }}
      {{ TPL_LOCAL_READ_MORE_LINKS|raw }}
    </div>
  </div>
</div>
<!-- template was short_version_image_on_left -->
