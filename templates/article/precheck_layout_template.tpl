use Indybay\Cache\ArticleCache;
use Indybay\DB\ArticleDB;
use Indybay\Renderer\ArticleRenderer;

$article_renderer=new ArticleRenderer();
$article_db=new ArticleDB();
$article_cache=new ArticleCache();
$article_info=$article_db->getArticleInfoFromNewsItemId({{ TPL_LOCAL_NEWS_ITEM_ID }});
$directory=NEWS_PATH."/".$article_cache->createDateBasedRelativeDirectory(NEWS_PATH."/",$article_info["creation_timestamp"]);
$relative_url=$article_renderer->findRecentDuplicateNonhiddenUrlsByTitle(htmlspecialchars_decode('{{ TPL_LOCAL_TITLE1 }}'), {{ TPL_LOCAL_NEWS_ITEM_ID }});

if ($relative_url!=""){
	$new_html="<?php \nheader(\"HTTP/1.0 301 Moved Permanently\");\n";
	$new_html.="header(\"Location: ".SERVER_URL.$relative_url."\");\n";
	$new_html.="?>";
	$article_cache->cacheFile($directory."/"."{{ TPL_LOCAL_NEWS_ITEM_ID }}.php", $new_html);
	header("HTTP/1.0 301 Moved Permanently");
	header("Location: ".SERVER_URL.$relative_url."");
	exit;
}else{
	$article_info["not_duplicate"]="1";
	$article_cache->cacheMainIncludeForArticle($directory, $article_info);
	echo "<HTML><HEAD><meta http-equiv=\"refresh\" content=\"0\"></HEAD></HTML>";
	exit;
}
