
readfile('{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_content.html');

include '{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_attachments.inc';
?>

<div class="comment_box_longversion">

	<?php
	if (!array_key_exists("printable",$_GET) || $_GET["printable"]==""){
		?>

		<div class="addcomment unknown-addcomment">
			<a href="/comment.php?top_id={{ TPL_LOCAL_NEWS_ITEM_ID }}">Add Your Comments</a>
		</div>
		
		<?php
		include '{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_comments.inc';

	}
	?>

	</div>
	<!-- END .comment_box_longversion -->

<?php
