<!-- hidden TEMPLATE -->
<div class="hidden">

	<code>
		<big>The following post has status <i>hidden</i>:</big>
	</code>

	<div class="heading">
		<a name="C_ANCHOR">
			<h1>H_EADING</h1>
		</a>
		<br>
		by A_UTHOR <a href="/search/?author=U_RLAUTH&amp;comments=yes">&#8226;</a>
		<em>C_DATE</em><br>
		<a href="mailto:C_ONTACT">C_ONTACT</a> P_HONE A_DDRESS
	</div>

	<div class="summary">S_UMMARY</div>

	<p class="media">M_EDIA</p>

	<p class="article article-hidden">A_RTICLE</p>

{% if L_INK|length %}
	<p class="link"><a href="L_INK">C_ROPURL</a></p>
{% endif %}

	<p class="addcomment hidden-addcomment">
		<a href="/comment.php?top_id=A_RTID">add your comments</a>
	</p>

</div>
<br>

<!-- hidden /TEMPLATE -->
