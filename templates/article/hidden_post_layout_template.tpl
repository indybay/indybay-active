?>
<h2>HIDDEN</h2>

<i>The following post may have been a test post, a duplicate,
 or could have been hidden if it violated this site's Points of Unity.
If you think this item should not have been hidden, first make sure
it wasn't posted twice and you were just unable to find the other copy of the post.
If that was not the case, you can contact the editorial collective by e-mailing 
<a href="mailto:indybay@lists.riseup.net">indybay@lists.riseup.net</a>.
 </i>
 <br><br>
<?php
readfile('{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_content.html');
include '{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_attachments.inc';
?>

<?php
