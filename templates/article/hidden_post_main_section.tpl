<!-- hidden_post_main_section TEMPLATE -->
<div class="webcast">
  
  <div class="headers">

    <div class="heading">
      <h1><strike>{{ TPL_LOCAL_TITLE1|raw }}</strike></h1>
    </div>

    <div class="author">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }} <em>{{ TPL_LOCAL_CREATED|raw }}</em>
    </div>

    <div class="summary">
      <strike>{{ TPL_LOCAL_SUMMARY|raw }}</strike>
    </div>

    {% if TPL_LOCAL_SHORTENED_RELATED_LINK|length %}
    <div class="more-link">
      <strike>{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</strike>
    </div>
    {% endif %}

  </div>
  <!-- END .headers -->

</div>
<!-- END .webcast -->

<!-- hidden_post_main_section /TEMPLATE -->
