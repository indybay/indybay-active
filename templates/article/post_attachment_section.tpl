<!-- TEMPLATE -->
<a name="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"></a>
<div class="webcast">
  <div class="headers-attachment">
    <span class="heading-attachment"><a class="child" href="#{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"><span class="permalink">§</span>{{ TPL_LOCAL_TITLE1|raw }}</a></span>
    <div class="author-attachment">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }} 
      <span class="author-email nowrap">{{ TPL_LOCAL_EMAIL|raw }}</span>
      <div class="author-date nowrap">{{ TPL_LOCAL_CREATED|raw }}</div>
    </div>
  </div>
  {{ TPL_LOCAL_NOMEDIA1|raw }}
  <div class="media">
    {{ TPL_LOCAL_MEDIA|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA2|raw }}
  <div class="article-attachment">
    {{ TPL_LOCAL_TEXT|raw }}
  </div>
  <!-- END .article -->
  {% if TPL_LOCAL_RELATED_URL|length %}
  <div class="more-link">
    <a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
  </div>
  {% endif %}
</div>
<!-- /TEMPLATE -->
