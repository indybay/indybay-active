<!-- TEMPLATE  -->
<a name="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"></a>
<div class="webcast">
  <div class="headers-comment">
    <div class="heading-comment">
      <a class="child" href="#{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"><span class="permalink">§</span>{{ TPL_LOCAL_TITLE1|raw }}</a>
    </div>
    <div class="author-comment">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}
      <span class="author-email nowrap">{{ TPL_LOCAL_EMAIL|raw }}</span>
      <div class="author-date nowrap">{{ TPL_LOCAL_CREATED|raw }}</div>
    </div>
  </div>
  {{ TPL_LOCAL_NOMEDIA1|raw }}
  <div class="media">
    {{ TPL_LOCAL_MEDIA|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA2|raw }}
  <div class="article-comment">
    {{ TPL_LOCAL_TEXT|raw }}
  </div>

  {% if TPL_LOCAL_RELATED_URL|length %}
  <div class="more-link">
		{{ TPL_LOCAL_MORE_INFO_LABEL|raw }} 
    <a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
  </div>
  {% endif %}

</div>
<!-- END .webcast -->
<!-- /TEMPLATE -->

<div class="addcomment comment-addcomment">
  <a href="/comment.php?top_id={{ TPL_LOCAL_PARENT_ITEM_ID|raw }}">Add a Comment</a>
</div>
