<!-- comment_box template -->
<div class="comments_list">
  <div class="comments_title">
    <a href="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}.php?show_comments=1#comments">
      Latest Comments
    </a>
  </div>
  <div class="comments-text">
    <div>Listed below are the latest comments about this post.</div>
    <div>These comments are submitted anonymously by website visitors.</div>
  </div>

  <div class="grid grid--3-cols">
    <div class="comment_list_item comment_list_head">TITLE</div>
    <div class="comment_list_item comment_list_head">AUTHOR</div>
    <div class="comment_list_item comment_list_head">DATE</div>
    {{ TPL_LOCAL_LATEST_COMMENT_LINKS|raw }}
  </div>

</div>
<!-- END comment_box template -->
