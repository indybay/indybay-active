<!-- TEMPLATE -->
<div class="breaking">
  <a class="child" name="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" href="#{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"><b class="date">{{ TPL_LOCAL_CREATED|raw }}:</strong></a>
  <span class="text">{{ TPL_LOCAL_TITLE1|raw }}</span> <span class="html">{{ TPL_LOCAL_SUMMARY|raw }}</span>
</div>
<!-- /TEMPLATE -->
