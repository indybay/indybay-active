<div class="feature-categories">
  <!-- <span class="feature-categories-label">Related Categories:</span> -->
  {% for page in pages %}
    <span class="categories-link">
      <a href="/{{ page.relative_path }}">{{ page.long_display_name }}</a>
    </span>
    <!-- {% if loop.last == false %} | {% endif %} -->
  {% endfor %}
</div>
