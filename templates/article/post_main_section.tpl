<!-- TEMPLATE -->

{{ TPL_LOCAL_PAGE_LINKS|raw }}

<div class="webcast">
  <div class="headers">
    <div class="heading">
      <h1>{{ TPL_LOCAL_TITLE1|raw }}</h1>
    </div>
    <div class="author">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}
      <span class="author-email nowrap">{{ TPL_LOCAL_EMAIL|raw }}</span>
      <div class="author-date nowrap">{{ TPL_LOCAL_CREATED|raw }}</div>
    </div>
  </div>
  <div class="summary">
    {{ TPL_LOCAL_SUMMARY|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA1|raw }}
  <div class="media">
    {{ TPL_LOCAL_MEDIA|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA2|raw }}
  <div class="article">
    {{ TPL_LOCAL_TEXT|raw }}
  </div>
  <!-- END .article -->

  {% if TPL_LOCAL_RELATED_URL|length %}
  <div class="more-link">
		{{ TPL_LOCAL_MORE_INFO_LABEL|raw }} 
    <a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
  </div>
  {% endif %}

</div>
<!-- END .webcast -->
<!-- /TEMPLATE -->
