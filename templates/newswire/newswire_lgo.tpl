<?php
use Indybay\Cache\NewswireCache;
$newswire_cache_class= new NewswireCache();
?>
<div class="newswirewrap newswirewrap-local">

<div class="newswirehead" >
	<span class="newswirehead-text">
		<a class="publink" name="local" href="/search/?news_item_status_restriction=3&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}">Local Newswire</a>
	</span>
</div>

<div class="localglobal"><a class="localgloballinks"
href="#global">Global</a> | <a
class="localgloballinks" href="#breaking">Breaking</a></div>

<?php
echo $newswire_cache_class->loadLocalNewswireForPage({{ TPL_LOCAL_PAGE_ID }});
?>
<span class="newswire-more"><small><a class="publink" href="/search/?news_item_status_restriction=3&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}" >More Local News...</a></small></span>

<!-- <div class="closenav" title="close">[ <span>&times;</span> hidden1 ]</div> -->
</div>


<div class="newswirewrap newswirewrap-global">

<div class="newswirehead" >
	<span class="newswirehead-text">
		<a class="publink"
href="/search/?news_item_status_restriction=5&amp;include_events=0&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}" name="global">Global Newswire</a>
	</span>
</div>

<div class="localglobal">
<a class="localgloballinks" href="#local">Local</a> |
<a class="localgloballinks" href="#breaking">Breaking</a>
</div>

<?php
echo $newswire_cache_class->loadNonlocalNewswireForPage({{ TPL_LOCAL_PAGE_ID }});
?>
<span class="newswire-more"><small><a class="publink" href="/search/?news_item_status_restriction=5&amp;include_events=0&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}">More Global News...</a></small></span>

<!-- <div class="closenav" title="close">[ <span>&times;</span> hidden2 ]</div> -->
</div>


<div class="newswirewrap newswirewrap-other">

<div class="newswirehead" >
	<span class="newswirehead-text">
		<a class="publink" href="/search/?news_item_status_restriction=<?php
echo NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;
?>&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}" name="breaking">Other/Breaking News</a>
	</span>
</div>

<div class="localglobal">
<a class="localgloballinks" href="#local">Local</a> |
<a class="localgloballinks" href="#global">Global</a>
</div>

<?php
echo $newswire_cache_class->loadOtherNewswireForPage({{ TPL_LOCAL_PAGE_ID }});
?>
<span class="newswire-more"><small><a class="publink" href="/search/?news_item_status_restriction=<?php
echo NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;

?>&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}">Open Newswire...</a></small></span>

<div class="closenav" title="close">[ <span>&times;</span> close ]</div>
</div>
