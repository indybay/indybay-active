<div class="nooz">
  <img src="{{ TPL_LOCAL_IMAGE_ICON_LINK }}" alt="{{ TPL_LOCAL_IMAGE_ICON_ALT }}" width="12" height="12">
  <a class="newswire" rel="bookmark" href="{{ TPL_LOCAL_NEWS_ITEM_LINK }}">{{ TPL_LOCAL_TITLE1 }}</a>
  <span class="newsauthor">{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME }}</span>
  <span class="newscomments">{{ TPL_LOCAL_NUMCOMMENTS_SECTION }}</span>
  <span class="date">{{ TPL_LOCAL_DATE }}</span> 
</div>
