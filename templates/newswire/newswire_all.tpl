<div class="newswirewrap newswirewrap-local">

  <div class="newswirehead">
    <span class="newswirehead-text">
        <a class="publink" name="local"
  href="/search/?news_item_status_restriction=478170&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}">Latest News</a>
    </span>
  </div>

</div>

<?php
use Indybay\Cache\NewswireCache;
$newswire_cache_class= new NewswireCache();
echo $newswire_cache_class->loadAllNewswireForPage({{ TPL_LOCAL_PAGE_ID }});
?>
<small><a class="publink" href="/search/?news_item_status_restriction=3&amp;include_events=1&amp;include_posts=1&amp;page_id={{ TPL_LOCAL_PAGE_ID }}" >More News...</a></small>
