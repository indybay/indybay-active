<!-- breaking news template -->
<a href="../index.php">{{ TPL_ADMIN_INDEX|raw }}</a> : 
<a href="list.php">List Breaking News</a> | 
<a href="create.php">Create Breaking News</a>
| <a href="/breaking/">Public Breaking News Page</a>

<h3><i>{{ TPL_CREATE_RESULT|raw }}</i></h3>

<form action="create.php" method="post">
<input type="hidden" name="method" value="create">
<p><label for="txt">News Text:<br>
<input type="text" maxlength="110" size="80" name="txt" id="txt">
<br>110 characters max; no HTML!<br>
WARNING: Txtmob apparently will not send messages containing a double quotation mark (")... single-quotes (') are OK.</label></p>
<p><label for="htm">Additional HTML (links etc.):<br>
<textarea rows="5" cols="80" name="htm" id="htm"></textarea></label></p>
<p>
<label for="t"><input type="radio" name="display" value="t" id="t"> Publish</label>
<br>
<label for="f"><input type="radio" name="display" value="f" id="f" checked="checked"> Keep hidden</label></p>

<p><label for="s"><input type="radio" name="send" value="t" id="s"> Send Text message now</label>
<br>
<label for="d"><input type="radio" name="send" value="f" id="d" checked="checked"> Do not send Text message 
now</label></p>

<p><input type="submit" value="Submit"></p>

</form>

<!-- / breaking news template -->
