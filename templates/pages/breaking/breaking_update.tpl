<!-- breaking news template -->
<a href="../index.php">{{ TPL_ADMIN_INDEX|raw }}</a> : 
<a href="list.php">List Breaking News</a> | 
<a href="create.php">Create Breaking News</a> | 
<a href="/breaking/">Public Breaking News Page</a>

<h3><i>{{ TPL_CREATE_RESULT|raw }}</i></h3>

<form action="update.php" method="post">
<input type="hidden" name="method" value="create">
<p><label for="txt">News Text:<br>
<input type="text" maxlength="110" size="80" name="txt" id="txt" value="{{ TPL_TXT|raw }}">
<br>110 characters max; no HTML!<br>
WARNING: Txtmob apparently will not send messages containing a double quotation mark (")... single-quotes (') are OK.</label></p>
<p><label for="htm">Additional HTML (links etc.):<br>
<textarea rows="5" cols="80" name="htm" id="htm">{{ TPL_HTM|raw }}</textarea></label></p>
<p>
<label for="t"><input type="radio" name="display" value="t" id="t" {{ TPL_CHECKEDPUBLISH|raw }}> Publish</label>
<br>
<label for="f"><input type="radio" name="display" value="f" id="f" {{ TPL_CHECKEDHIDDEN|raw }}> Hidden</label></p>

<p><label for="s"><input type="radio" name="send" value="t" id="s"> Send Text message now</label>
<br>
<label for="d"><input type="radio" name="send" value="f" id="d" checked="checked"> Do not send Text message 
now</label></p>

<p><label for="created">Date: <input type="text" name="created" value="{{ TPL_CREATED|raw }}" id="created"></label></p>

<input type="hidden" name="newsitem" value="{{ TPL_NEWSITEM|raw }}">

<p><input type="submit" value="Submit"></p>
</form>
<h3>Version history of this news item:</h3>
{{ TPL_ROWLIST|raw }}

<!-- / breaking news template -->
