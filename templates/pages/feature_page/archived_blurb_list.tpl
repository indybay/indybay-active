{{ TPL_STATUS_MESSAGES|raw }}

<h1>{{ TPL_FEATURE_PAGE_ARCHIVE_HEADER|raw }}</h1>

<div class="archive-pagination" >
	<span class="archive-featurename">
		<a href="{{ TPL_LOCAL_FEATURE_PAGE_URL|raw }}">{{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</a>: 
	</span>
	<span class="archive-nav">
		{{ TPL_NAV|raw }}
	</span>
	<span class="archive-dividerbar">
		|
	</span>
	<span class="archive-searchlink">
		<a href="/search/advanced_search.php?include_posts=0&amp;include_events=0&amp;include_blurbs=1&amp;page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_SEARCH|raw }}</a>
	</span>
</div>

<div class="archivedisplay">
{{ TPL_LOCAL_HTML|raw }}
</div>

<div class="archive-pagination" >
	<span class="archive-featurename">
		<a href="{{ TPL_LOCAL_FEATURE_PAGE_URL|raw }}">{{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</a>:
	</span>
	<span class="archive-pagination">
		{{ TPL_NAV|raw }}
	</span>
</div>
