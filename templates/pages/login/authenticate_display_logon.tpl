<form name="logon_form" action="/admin/authentication/authenticate.php" method="post">
    
  <div class="grid grid--2-cols-form logon">

    <div class="grid--row-full">

      <h3>{{ TPL_ADMIN_ENTER|raw }} {{ TPL_SITENICK|raw }}</h3>

      {{ TPL_LOCAL_ERROR|raw }}

      <p><a href="/">{{ TPL_RETURN_TO_SITE|raw }}</a></p>

    </div>

    <div>
      <label for="username1">{{ TPL_USERNAME|raw }}:</label>
    </div>
    <div>
      <input type="text" name="username1" id="username1" value="{{ TPL_LOCAL_USERNAME|raw }}" tabindex="1">
    </div>

    <div>
      <label for="password">{{ TPL_PASSWORD|raw }}:</label>
    </div>
    <div>
      <input type="password" name="password" id="password" value="" tabindex="2">
    </div>

    <div></div>

    <div>
      <input type="submit" name="logon" value="Log On" tabindex="3">

      <input type="hidden" name="goto" value="{{ TPL_GOTO|raw }}">
    </div>

  </div>

</form>
