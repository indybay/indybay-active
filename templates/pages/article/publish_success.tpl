{{ TPL_LOCAL_PUBLISH_RESULT|raw }}

<blockquote>Your post can be found at
  <br>
  <span class="strong">
    <a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}">{{ TPL_LOCAL_PUBLISH_LINK|raw }}</a>
  </span>
</blockquote>

{{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}
