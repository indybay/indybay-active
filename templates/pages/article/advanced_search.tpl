<!-- advanced_search template -->
<div class="inner-wrap">

  <div><h1 class="headline-text">{{ TPL_SEARCH_ADVANCED|raw }}</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>
    
<div class="list-search">

  <form name="search" action="/search/search_results.php">

    <div class="grid grid--2-cols-form">

      <div class="search-element">
        <label for="search">{{ TPL_KEYWORD|raw }}:</label>
      </div>
      <div>
        <input id="search" name="search" type="text" size="20" value="{{ TPL_LOCAL_SEARCH|raw }}" placeholder="Enter keyword(s)">
      </div>

      <div class="grid--row-full">

        <div class="search-explanation">

          <div class="clicktip">

            <p class="trigger" title="click to learn more">How keyword search logic works</p>

            <div class="target">

              Multiple search words imply "<em>or</em>" logic. A leading plus sign indicates that the word <em>must</em> be present.
              To search for an exact phrase, enclose it in double quotes.

              <dl>
                <dt><p><em>apple banana</em></p></dt>
                <dd><p>Find articles that contain at least one of the two words.</p></dd>
                <dt><p><em>+apple +juice</em></p></dt>
                <dd><p>Find articles that contain both words.</p></dd>
                <dt><p><em>+apple -macintosh</em></p></dt>
                <dd><p>Find articles that contain the word <q>apple</q> but not <q>macintosh</q>.</p></dd>
                <dt><p><em>apple*</em></p></dt>
                <dd><p>Find articles that contain words such as <q>apple</q>, <q>apples</q>, <q>applesauce</q>, or <q>applet</q>.</p></dd>
                <dt><p><em>"some words"</em></p></dt>
                <dd><p>Find articles that contain the exact phrase <q>some words</q> (for example, <q>some words of wisdom</q> but not <q>some noise words</q>).</p></dd>
              </dl>

            </div><!-- END .target -->

          </div><!-- END .clicktip -->

        </div><!-- END search-explanation -->

      </div>

      <div class="grid--row-full">
        <hr>
      </div>

      <div class="search-element">
        <label for="media_type_grouping_id">{{ TPL_MEDIA_TYPE|raw }}:</label>
      </div>
      <div>
        {{ TPL_SEARCH_MEDIUM|raw }}
      </div>

      <div class="search-element">
        <label for="news_item_status_restriction">{{ TPL_DISPLAY_OPTIONS|raw }}:</label>
      </div>
      <div>
        {{ TPL_SEARCH_DISPLAY|raw }}
      </div>

      <div class="search-element">
        <label for="topic_id">{{ TPL_TOPIC|raw }}:</label>
      </div>
      <div>
        {{ TPL_CAT_TOPIC_SELECT|raw }}
      </div>

      <div class="search-element">
        <label for="region_id">{{ TPL_REGION|raw }}:</label>
      </div>
      <div>
        {{ TPL_CAT_REGION_SELECT|raw }}
      </div>

      <div class="grid--row-full">
        <hr>
      </div>

      <div class="search-element">
        <label for="search_date_type">{{ TPL_SEARCH_DATE_RESTRICTION|raw }}:</label>
      </div>
      <div>
        {{ TPL_SEARCH_DATE_TYPE_SELECT|raw }}
      </div>

      <div class="grid--row-full search-element">
        <span class="search-range disabled">
          <span class="datetext">{{ TPL_SEARCH_BETWEEN|raw }} </span>
          <span class="dateset nowrap">{{ TPL_DATE_BETWEEN_START|raw }} </span>
          <span class="datetext">{{ TPL_SEARCH_AND|raw }} </span>
          <span class="dateset nowrap">{{ TPL_DATE_BETWEEN_END|raw }}</span>
        </span>
      </div>

      <div class="grid--row-full">
        <hr>
      </div>

      <div class="grid--row-full flex flex--align-center flex--wrap search-includes">

        <span class="strong">Include in Search:</span> 

        <span class="nowrap">
          <label for="include_comments">{{ TPL_INCLUDE_COMMENTS }}</label>
          {{ TPL_LOCAL_CHECKBOX_COMMENTS|raw }}
        </span>

        <span class="nowrap">
          <label for="include_attachments">{{ TPL_INCLUDE_ATTACHMENTS }}</label>
          {{ TPL_LOCAL_CHECKBOX_ATTACHMENTS|raw }}
        </span>

        <span class="nowrap">
          <label for="include_events">{{ TPL_INCLUDE_EVENTS }}</label>
          {{ TPL_LOCAL_CHECKBOX_EVENTS|raw }}
        </span>

        <span class="nowrap">
          <label for="include_posts">{{ TPL_INCLUDE_POSTS }}</label>
          {{ TPL_LOCAL_CHECKBOX_POSTS|raw }}
        </span>

        <span class="nowrap">
          <label for="include_blurbs">{{ TPL_INCLUDE_BLURBS }}</label>
          {{ TPL_LOCAL_CHECKBOX_BLURBS|raw }}
        </span>

      </div>

      <div class="grid--row-full">
        <hr>
      </div>

      <div class="grid--row-full">
        <input type="submit" value="{{ TPL_GO|raw }}" name="submitted_search">
      </div>

    </div><!-- END grid--2-cols-form -->

    <br><br>

  </form>

</div><!-- end list-search -->
<!-- end advanced_search template -->
