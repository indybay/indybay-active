<!-- newswire list template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div><h1 class="headline-text">{{ TPL_NEWSITEM_LIST|raw }}</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>
    
<div class="list-search">

  <form method="GET" action="index.php">

    {{ TPL_TOP_SEARCH_FORM|raw }}

  </form>

</div><!-- END list-search -->

<hr>

{{ TPL_NAV|raw }}

{{ TPL_TABLE_MIDDLE|raw }}

{{ TPL_NAV|raw }}

<hr>

<div class="list-search list-search-bottom">

  <form method="get" action="index.php">

    {{ TPL_TOP_SEARCH_FORM|raw }}

  </form>

</div><!-- END list-search list-search-bottom -->

<br><br>

<!-- end newswire list template -->
