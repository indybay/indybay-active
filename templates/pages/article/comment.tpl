<h1 class="headline-text">Add Comment on:</h1>

<div class="comment_articlepreview">
  <span class="strong">{{ TPL_LOCAL_PARENT_TITLE1|raw }}</span>
  <br>
  <span class="author">by {{ TPL_LOCAL_PARENT_DISPLAYED_AUTHOR_NAME|raw }}</span>
  <br>
  {{ TPL_LOCAL_PARENT_SUMMARY|raw }}
</div>
<br>
<div class="subtext">
  {{ TPL_HIDE1|raw }}
  {{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}
  {{ TPL_HIDE2|raw }}
</div>

<div class="comment-guidlines">

  <h2>Guidelines for commenting on news articles:</h2>

  <p>Thanks for contributing to Indybay's open publishing newswire. You may use any format for your response, from traditional academic discourse to subjective personal account. Please, keep it on topic and concise. Read our <a href="/newsitems/2002/08/04/1395001.php">editorial policy</a>, <a href="/newsitems/2003/12/15/16659061.php">privacy</a>, and <a href="/newsitems/2003/12/15/16659051.php">legal</a> statements before continuing. Or <a href="javascript:history.back()">go back to the article</a>.</p>

</div>


<form enctype="multipart/form-data" method="post" accept-charset="UTF-8" action="/comment.php?top_id={{ TPL_LOCAL_PARENT_ITEM_ID|raw }}" id="publish-form">

  <input type="hidden" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID|raw }}">

  {{ TPL_VALIDATION_MESSAGES|raw }}
  {{ TPL_STATUS_MESSAGES|raw }}

  <div class="module module-unified module-unified-grey module-support">
    <div class="module-header">
      <span class="extraspaceleftsm">
        <span class="strong">{{ TPL_PUB_STEPONE|raw }}</span>
      </span>
    </div>
    <div class="module-inner grid grid--2-cols-form">
      
      <div class="first-col">
        <label for="title1">{{ TPL_TITLE3|raw }}</label>
        <div class="small">({{ TPL_REQUIRED|raw }})</div>
      </div>           
      <div>
        <textarea name="title1" id="title1" placeholder="Use Title Case for Titles" rows="2" maxlength="90">{{ TPL_LOCAL_TITLE1|raw }}</textarea>
        <!-- <input type="text" name="title1_OLD" id="title1_OLD" placeholder="Use Title Case for Titles" maxlength="90" value="{{ TPL_LOCAL_TITLE1|raw }}"> -->
      </div>

      <div class="first-col">
        <label for="displayed_author_name">{{ TPL_PUB_AUTHOR|raw }}</label>
        <div class="small">({{ TPL_REQUIRED|raw }})</div>
      </div>
      <div>
        <input type="text" id="displayed_author_name" name="displayed_author_name" size="25" maxlength="45" value="{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}">
      </div>

      <!--             
      <div class="grid--row-full first-col">
        <span class="subheader">Contact Info</span>
      </div>
      -->

      <div class="first-col">
        <label for="email">{{ TPL_EMAIL|raw }}</label>
      </div>
      <div>
        <input type="email" name="email" id="email" size="25" maxlength="45" value="{{ TPL_LOCAL_EMAIL|raw }}">
        <br>
        <div class="subtext">
          <span class="nowrap">
            <label for="display_contact_info" class="sub-input">{{ TPL_DISPLAY_CONTACT_INFO|raw }}</label>
            {{ TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO|raw }}
          </span>
          <br>
          <span class="small">
            {{ TPL_DISPLAY_ADDITIONAL_ADDITIONAL_TEXT|raw }}
          </span>
        </div>
      </div>
      
      <div class="first-col">
        <label for="related_url">{{ TPL_PUB_URL|raw }}</label>
      </div>
      <div>
        <input type="text" name="related_url" id="related_url" maxlength="255" value="{{ TPL_LOCAL_RELATED_URL|raw }}">
      </div>

    </div><!-- END module-inner grid grid- -2-cols-form -->
  </div>
  <!-- END publish form part 1 -->

  <!-- publish form part 2 -->
  <div class="module module-unified module-unified-grey module-support">
    <div class="module-header">
      <span class="extraspaceleftsm">
        <span class="strong">{{ TPL_PUB_STEPTWO|raw }}</span>
      </span>
    </div>
    <div class="module-inner grid grid--2-cols-form">

      <div class="grid--row-full">
        <label for="text">TEXT/HTML</label>
        <div class="small">
          <span class="rednote strong">Tip:</span> 
          <a href="/notice/legibility-note.html" title="Click to see an example" target="blank">use spaces between paragraphs</a>.
        </div>
        <textarea name="text" id="text" rows="10" cols="80">{{ TPL_LOCAL_TEXT|raw }}</textarea>
        <div class="subtext">
          <span class="nowrap">
            <label for="is_text_html" class="sub-input">{{ TPL_IS_TEXT_HTML|raw }}</label> 
            {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
          </span>
          <br>
          <span class="small">{{ TPL_PUB_ART|raw }}</span>
        </div>
      </div>

    </div><!-- END module-inner grid grid- -2-cols-form -->
  </div>
  <!-- END publish form part 2 -->

  <!-- publish form part 3 -->
  <div class="module module-unified module-unified-grey module-support">
    <div class="module-header">
      <span class="extraspaceleftsm">
        <span class="strong">{{ TPL_PUB_STEPTHREE|raw }}</span>
      </span>
    </div>
    <div class="module-inner grid grid--2-cols-form">
      
      <div class="first-col">
        <label for="file_count">{{ TPL_PUB_CUANTOFILES|raw }}</label>
      </div>
      <div>
        <span>{{ TPL_SELECT_FILECOUNT|raw }}</span>
        <div id="upload_warning">
          <span class="small">(Required fields must be entered before you can choose uploads)</span>
        </div>
        <input type="submit" name="action" value="Enter" id="upload_submit_button">

        <div class="clicktip">
          <p class="trigger" title="click to learn more">Please note the allowed file types and file sizes!</p>
          <div class="target">
            <p>
              {{ TPL_MAX_UPLOAD|raw }} {{ TPL_UPLOAD_MAX_FILESIZE|raw }}; {{ TPL_TOTAL_LIMIT|raw }} {{ TPL_POST_MAX_SIZE|raw }}; {{ TPL_MAX_EXECUTION_TIME|raw }} {{ TPL_HOURS|raw }}
            </p>
            <p>
              {{ TPL_ACCEPTED_TYPES|raw }}
            </p>
            <p>
              <span class="strong">For playability of audio files on mobile devices</span>, upload MP3s.
            </p>
          </div>
        </div><!-- END .clicktip -->

      </div>

      <!-- uploads section -->
      <div id="file_boxes2" class="grid--row-full"></div>
      <!-- END uploads section -->
          
    </div><!-- END module-inner grid grid- -2-cols-form -->
  </div>
  <!-- END publish form part 3  -->

  <!-- publish form nonjscript -->
  <div id="nonjscript_file_boxes">
  {{ TPL_FILE_BOXES|raw }}
  </div>

  <!-- publish form part 4 -->
  <div class="module module-unified module-unified-grey module-support">
    <div class="module-header">
      <span class="extraspaceleftsm">
        <span class="strong">CAPTCHA</span>
      </span>
    </div>
    <div class="module-inner grid grid--2-cols-form">
      
      <div class="first-col">
        <label for="captcha_math">Anti-spam question</label>
        <!-- <div class="small">({{ TPL_REQUIRED|raw }})</div> -->
      </div>
      <div>
        {{ TPL_CAPTCHA_FORM|raw }}
      </div>

    </div><!-- END module-inner grid grid- -2-cols-form -->
  </div>
  <!-- END publish form part 4 -->

  <!-- publish buttons -->
  <div class="publish-buttons">
    <hr>
    <div id="preview_button">{{ TPL_LOCAL_PREVIEW|raw }}</div>
    <input type="submit" name="publish" value="{{ TPL_BUTTON_PUBLISH|raw }}">
  </div>

  <!-- TPL_HIDE2|raw -->
  {{ TPL_HIDE2|raw }}
  <!-- end TPL_HIDE2|raw -->

</form>

<!-- DO WE NEED THIS SECTION BECAUSE ONLY ONE UPLOAD ALLOWED WITH COMMENTS? -->
<!-- Select files template -->
<!-- <div id="files_select_template" class="grid grid--2-cols-form">

  <div class="grid--row-full">
    <hr>
  </div>

  <div class="first-col">
    <label for="linked_file_title_::uploadnum::">
      Title #::uploadnum::
    </label>
  </div>
  <div>
    <input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" id="linked_file_title_::uploadnum::" value="">
  </div>
  
  <div class="first-col">
    <label for="linked_file_::uploadnum::" title="Additional upload">
      Upload #::uploadnum::
    </label>
  </div>
  <div class="fileinputwrapper">
    <input class="fileinput" type="file"  
      name="linked_file_::uploadnum::" id="linked_file_::uploadnum::" {{ TPL_FILE_ACCEPT|raw }}>
    
    <div class="previewimagewrapper">
      <div class="clear-file-upload" title="Clear this attachment">×</div>
      <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
      <img class="previewimage" src="" alt="preview image">
    </div>
  </div>
  
  <div class="first-col alt-tag">
    <label for="linked_file_alt_tag_::uploadnum::" title="Alt text">Image Description #::uploadnum::</label>
  </div>
  <div class="alt-tag">
    <input name="linked_file_alt_tag_::uploadnum::" id="linked_file_alt_tag_::uploadnum::" size="25" maxlength="140" type="text" value="">
    <div class="small">Short description of the image used by screen readers.</div>
  </div>

  <div class="first-col">
    <label for="linked_file_comment_::uploadnum::">
      Optional Text #::uploadnum::
    </label>
  </div>
  <div>
    <textarea 
      name="linked_file_comment_::uploadnum::" id="linked_file_comment_::uploadnum::" rows="3" cols="50"></textarea>
  </div>
        
</div> -->
<!-- END #files_select_template -->


<!-- Select file #1 template -->
<div id="files_select_template_1" class="grid grid--2-cols-form">

  <div class="grid--row-full">
    <hr>
  </div>
  
  <div class="first-col">
    <label for="linked_file_1">
      Upload
    </label>
  </div>
  <div class="fileinputwrapper">
    <input class="fileinput" type="file" 
    name="linked_file_1" 
    id="linked_file_1" {{ TPL_FILE_ACCEPT|raw }}>
    
    <div class="previewimagewrapper">
      <div class="clear-file-upload" title="Clear this attachment">×</div>
      <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
      <img class="previewimage" src="" alt="preview image">
    </div>
  </div>

  <div class="first-col alt-tag">
    <label for="linked_file_alt_tag_1" title="Alt text">Image Description</label>
  </div>
  <div class="alt-tag">
    <input name="linked_file_alt_tag_1" id="linked_file_alt_tag_1" size="25" maxlength="140" type="text" value="">
    <div class="small">Short description of the image used by screen readers.</div>
  </div>
  
</div>
<!-- END #files_select_template_1 -->

<!-- end publish template -->
