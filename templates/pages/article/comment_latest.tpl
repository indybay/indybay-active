<div>{{ TPL_LATEST_COMMENT_TEXT|raw }}<a href="/search/?sort=rank">{{ TPL_MOST_COMMENTED|raw }}</a>.</div><br>

<table width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td><strong><small>{{ TPL_THREAD|raw }}</small></strong></td>
    <td><strong><small>{{ TPL_LATEST_AUTHOR|raw }}</small></strong></td>
    <td><strong><small>{{ TPL_POSTED_AT|raw }}</small></strong></td>
  </tr>
{{ TPL_LOCAL_COMMENT_ROWS|raw }}
</table>

<p align="right"><a href="/search/?page=5&amp;sort=modified">{{ TPL_BROWSE_OLDER_THREADS|raw }}</a></p>

<p><small>{{ TPL_LAST_UPDATED|raw }}</small></p>
