<!-- newswire_search template -->
<div class="flex flex--align-center flex--wrap search-options">

  <span class="strong">
    Filter Results:
  </span>

  <span>
    {{ TPL_SEARCH_DISPLAY|raw }}
  </span>

  <span>
    {{ TPL_SEARCH_MEDIUM|raw }}
  </span>

  <span>
    {{ TPL_CAT_TOPIC_SELECT|raw }}
  </span>

  <span>
    {{ TPL_CAT_REGION_SELECT|raw }}
  </span>

</div><!-- END search-options 1 -->

<input type="submit" value="Search">

<div class="flex flex--align-center flex--wrap search-options">

  <span>
    <a href="/search/advanced_search.php" title="Advanced Search">{{ TPL_SEARCH_ADVANCED }}</a>
  </span>
  <span>
    <a href="/syn/generate_rss.php?news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION }}&amp;include_blurbs={{ TPL_LOCAL_INCLUDE_BLURBS }}&amp;include_events={{ TPL_LOCAL_INCLUDE_EVENTS }}&amp;media_type_grouping_id={{ TPL_LOCAL_MEDIA_TYPE_GROUPING_ID }}&amp;include_posts={{ TPL_LOCAL_INCLUDE_POSTS }}&amp;region_id={{ TPL_LOCAL_REGION_ID }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID }}" title="RSS feed"><img src="/im/rss-rounded.svg" class="mediaicon" alt="RSS feed" width="12" height="12"></a>
  </span>

</div><!-- END search-options 2 -->

<!-- end newswire_search template -->
