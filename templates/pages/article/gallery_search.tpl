<!-- gallery_search template -->
<div class="flex flex--align-center flex--wrap search-options">

  <span class="strong">
    Filter Results:
  </span>

  <span>
    {{ TPL_SEARCH_DISPLAY|raw }}
  </span>

  <span>
    {{ TPL_CAT_TOPIC_SELECT|raw }}
  </span>

  <span>
    {{ TPL_CAT_REGION_SELECT|raw }}
  </span>

</div><!-- END search-options 1 -->

<div class="flex flex--align-center flex--wrap search-options search-options-display">

  <span>
    <label for="gallery_display_option_id">Display:</label> 
  </span>

  <span> 
    {{ TPL_GALLERY_DISPLAY_OPTIONS|raw }}
  </span>

  <span>
    <input type="submit" value="{{ TPL_GO }}">
  </span>

</div><!-- END search-options 2 -->

<a class="flex flex--align-center flex--wrap rss-feed" href="/syn/generate_rss.php?news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID }}&amp;region_id={{ TPL_LOCAL_REGION_ID }}&amp;media_type_grouping_id={{ TPL_LOCAL_MEDIA_TYPE_GROUPING_ID }}&amp;include_comments={{ TPL_LOCAL_INCLUDE_COMMENTS }}&amp;date_range_start_day={{ TPL_LOCAL_DATE_RANGE_START_DAY }}&amp;date_range_start_month={{ TPL_LOCAL_DATE_RANGE_START_MONTH }}&amp;date_range_start_year={{ TPL_LOCAL_DATE_RANGE_START_YEAR }}&amp;date_range_end_day={{ TPL_LOCAL_DATE_RANGE_END_DAY }}&amp;date_range_end_month={{ TPL_LOCAL_DATE_RANGE_END_MONTH }}&amp;date_range_end_year={{ TPL_LOCAL_DATE_RANGE_END_YEAR }}&amp;search_date_type={{ TPL_LOCAL_DATE_TYPE }}&amp;search={{ TPL_LOCAL_SEARCH }}&amp;include_posts={{ TPL_LOCAL_INCLUDE_POSTS }}&amp;include_attachments={{ TPL_LOCAL_INCLUDE_ATTACHMENTS }}&amp;include_blurbs={{ TPL_LOCAL_INCLUDE_BLURBS }}&amp;include_events={{ TPL_LOCAL_INCLUDE_EVENTS }}" title="RSS feed"><img src="/im/rss-rounded.svg" class="mediaicon" alt="RSS feed" width="12" height="12"> <span>RSS Feed</span></a>

<!-- end gallery_search template -->


