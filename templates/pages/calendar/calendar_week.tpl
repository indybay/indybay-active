<div class="grid grid--3-cols calendar-main-header calendar-header">
  <div class="calendar-month calendar-month-prev">
    {{ TPL_CAL_EVENT_MONTH_VIEW_PREV|raw }}
  </div>
  <div class="event-navs">
    <div class="event-nav">
      <span class="week-nav week-nav-prev">
        <a
          href="calendar_week.php?day={{ TPL_LOCAL_LAST_WEEK_DAY|raw }}&amp;month={{ TPL_LOCAL_LAST_WEEK_MONTH|raw }}&amp;year={{ TPL_LOCAL_LAST_WEEK_YEAR|raw }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION|raw }}">
          {{ TPL_PREV_WEEK|raw }}
        </a>
      </span>
      <span class="headline-text weekof-text">{{ TPL_WEEK_OF|raw }} {{ TPL_CAL_CUR_DATE|raw }}</span>
      <span class="week-nav week-nav-next">
        <a
          href="calendar_week.php?day={{ TPL_LOCAL_NEXT_WEEK_DAY|raw }}&amp;month={{ TPL_LOCAL_NEXT_WEEK_MONTH|raw }}&amp;year={{ TPL_LOCAL_NEXT_WEEK_YEAR|raw }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION|raw }}">
          {{ TPL_NEXT_WEEK|raw }}
        </a>
      </span>
    </div>
    <div class="event-nav2">
      <span class="nowrap">
        <a
          class="calendar-addevent-link"
          href="/calendar/event_add.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}">
          {{ TPL_CAL_ADD_AN_EVENT|raw }}
        </a>
      </span>
      <span>
      <a href="#ical" title="Click for iCal feeds">
        <img
          src="/im/rss-rounded.svg"
          class="mediaicon"
          alt="iCal feed icon"
          width="12" 
          height="12">
      </a>
      </span>
      <span class="nowrap">
        <a
          href="/search/search_results.php?news_item_status_restriction=690690&amp;include_events=1&amp;search_date_type=displayed_date&amp;submitted_search=1&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}">
          {{ TPL_CAL_LIST_FUTURE_EVENTS|raw }}
        </a>
      </span>
    </div>
    <div class="event-form">
      <form action="calendar_week.php" method="GET" >
        <div class="event-nav3">
          <label for="topic_id">{{ TPL_TOPIC|raw }}:</label> 
          {{ TPL_CAL_EVENT_TOPIC_DROPDOWN|raw }}
        </div>
        <div class="event-nav3">
          <label for="region_id">{{ TPL_REGION|raw }}:</label> 
          {{ TPL_CAL_EVENT_LOCATION_DROPDOWN|raw }}
        </div>
        <input type="hidden" name="day" value="{{ TPL_LOCAL_DAY|raw }}">
        <input type="hidden" name="month" value="{{ TPL_LOCAL_MONTH|raw }}">
        <input type="hidden" name="year" value="{{ TPL_LOCAL_YEAR|raw }}">
        <input
          type="submit"
          name="Filter"
          value="{{ TPL_FILTER|raw }}"
       >
      </form>
    </div>
  </div>
  <div class="calendar-month calendar-month-next">

    {{ TPL_CAL_EVENT_MONTH_VIEW_NEXT|raw }}
    
  </div>
</div>


<div class="grid grid--7-cols cal-main">
  
    {{ TPL_CAL_EVENT_MONTH_DAYTITLE|raw }}
    
  	{{ TPL_CAL_EVENT_MONTH_VIEW_FULL|raw }}

</div>


<div id="ical" class="calendar-links">
  
  <div>
    <span>
      <a href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}"
        title="iCal feed">
        <img
          src="/im/rss-rounded.svg"
          class="mediaicon"
          alt="iCal feed" 
          width="12" 
          height="12">
      </a>
      Subscribe to this calendar:
    </span>
    <span>
      <a href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}"
        title="Add to iCal or compatible calendar application">All events</a> | 
    </span>
    <span>
      <a href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction=105"
        title="Add featured events to calendar application">Featured events</a>
    </span>
  </div>
  
  <div>
    <span>Add this calendar to your Google Calendar:</span>
    <span>
      <a href="https://www.google.com/calendar/render?cid=https%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D{{ TPL_LOCAL_TOPIC_ID|raw }}%26region_id%3D{{ TPL_LOCAL_REGION_ID|raw }}"
        title="Add this calendar to your Google calendar">All events</a> | 
    </span>
    <span>
      <a href="https://www.google.com/calendar/render?cid=https%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D{{ TPL_LOCAL_TOPIC_ID|raw }}%26region_id%3D{{ TPL_LOCAL_REGION_ID|raw }}%26news_item_status_restriction%3D105"
        title="Add featured events to your Google calendar">Featured events</a>
    </span>
  </div>
    
</div>
