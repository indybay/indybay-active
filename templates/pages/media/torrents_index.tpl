<table width="90%"><tr><td>

  <div align="left">

    <img src="/im/bittorrent.white.png" width="16" height="16">
    <big><strong> indybay torrents </strong></big>
    <br>
    <small>
        <!--
        Seed our torrents! We will try to keep many
        of the files uploaded to Indybay seeded, but you're
        encouraged to help out by making use of our 
        <a href="/syn/torrents.rss">Torrent RSS feed</a>.
        -->
      If you upload a large file, you can help seed it by
      downloading the associated torrent, opening it in your <a
      href="http://en.wikipedia.org/wiki/BitTorrent">BitTorrent</a> client,
      and selecting the existing media file on
      your hard drive as the download location.
      At this point you will connect to a "tracker" and
      help seed the file for as long as you remain connected.
      <a href="http://azureus.sourceforge.net/">Some clients</a>
      allow you to easily seed a large number of
      files. <strong><a
      href="http://docs.indymedia.org/view/Global/BitTorrent">
      What the heck is BitTorrent?</a></strong>
    </small>

    <!--
    <a href="/syn/torrents.rss"><img
    src="http://www.indybay.org/images/iconRSS.gif"></a>
    -->

  </div>

</tr></tr></table>

<div class="list-search">

  <form method="get" action="index.php">

    {{ TPL_TOP_SEARCH_FORM|raw }}

  </form>

</div>

<hr>

{{ TPL_NAV|raw }}

<table class="torrent" width="80%">

  {{ TPL_TABLE_MIDDLE|raw }}

</table>

{{ TPL_NAV|raw }}

<hr>

<div class="list-search list-search-bottom">

  <form method="get" action="index.php">

    {{ TPL_NAV|raw }}

    {{ TPL_BOTTOM_SEARCH_FORM|raw }}

  </form>

</div>
