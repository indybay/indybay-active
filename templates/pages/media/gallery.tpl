<!-- gallery template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div><h1 class="headline-text">Photo Gallery</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<div class="list-search">

  <form method="get" action="/gallery/index.php">
    
    {{ TPL_TOP_SEARCH_FORM|raw }}

  </form>

</div>

<hr>

{{ TPL_NAV|raw }}

<div class="photo-gallery-wrapper">

  {{ TPL_TABLE_MIDDLE|raw }}

</div>

{{ TPL_NAV|raw }}

<hr>

<div class="list-search list-search-bottom">

  <form method="get" action="index.php">

    {{ TPL_BOTTOM_SEARCH_FORM|raw }}

  </form>

</div>

<br><br>

<!-- end gallery template -->
