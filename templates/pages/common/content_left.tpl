
						<!-- main table content area -->
						<tr>
							<td class="bg3">

								<!-- Nested Table to split content -->
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>

				<!-- Left Col -->

				<td width="100" valign="top" class="bgleftcol">
					<!-- Naviagtion -->
					<table width="100%" cellspacing="0" cellpadding="3">
						<tr>
							<td>
								<div class="left-trash">
									<small>
										<a class="left" href="/features/"><strong>features</strong></a><br>
										<span class="featurelist">
											&#8226; <a class="left" href="/search/">latest news</a><br>
											&#8226; <a class="left" href="/comment_latest.php">commentary</a>
										</span>
									</small>
								</div>
								<div class="left-trash">
									<small><a class="left" href="/process/resources/"><strong>make media</strong></a></small>
								</div>
								<div class="left-trash">
									<small><a class="left" href="/process/"><strong>participate</strong></a></small>
								</div>
								<div class="left-trash">
									<small><strong><a class="left" href="/archives/">archives</a></strong></small>
								</div>
								<div class="left-trash">
									<small><strong><a class="left" href="/irc/">chat</a></strong></small>
								</div>
								<div class="left-trash">
									{{ TPL_SEARCH|raw }}
								</div>
								<div class="left-trash">
									{{ TPL_TRANSLATE_FORM|raw }}
								</div>
								<div class="left-trash">
									<small><a class="left" href="/process/links.php"><strong>links</strong></a></small>
								</div>
								<div class="left-trash">
									<small><a class="left" href="/process/donate.php"><strong>donate</strong></a></small>
								</div>
								<div class="left-trash">
									<small><strong><a class="left" href="http://sfactive.indymedia.org/">sf-active</a></strong></small>
								</div>
								<div class="left-trash">
									<small><strong><a class="left" href="/network.php">imc network</a></strong></small>
								</div>
								{{ TPL_LEFT|raw }}
							</td>
						</tr>
					</table>
					<!-- End Naviagtion -->
				</td>

				<!-- Spacer -->
				<td class="bgult" width="1">
					<img src="/im/bump.gif" width="1"  height="1" alt="">
				</td>

				<!-- Middle -->
				<td valign="top" class="bgcentercol">
					<table cellspacing="0" cellpadding="4" width="100%">
						<tr>
							<td width="100%" valign="top">
								<table cellspacing="0" cellpadding="3">
									<tr>
										<td>
