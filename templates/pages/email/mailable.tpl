<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>
			{{ TPL_PRN_TITLE|raw }}
        </title>
        <meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOARCHIVE">
        <style type="text/css">
        /*<![CDATA[*/
        <!--
                .heading { font-family: sans-serif }
                .summary { font-family: sans-serif }
                .media { font-family: sans-serif }
                .article { font-family: serif }
                .link { font-family: sans-serif }
                .addcomment { visibility: hidden; display: none; }
        // -->
        /*]]>*/
        </style>
    </head>
    <body>

        <h3>
            {{ TPL_LANG_PRN_SITE_NAME|raw }}
        </h3>

        <hr>

        <strong>{{ TPL_ERROR_MSG|raw }}</strong>

        <p>
            <form method="post" name="direction">
            <strong>{{ TPL_FROMADDRESS|raw }}</strong>: 
            <input type="text" name="fromaddress" value="{{ TPL_FROMVALUE|raw }}">
            <br>
            <strong>{{ TPL_TOADDRESS|raw }}</strong>: 
            <input type="text" name="toaddress" value="{{ TPL_TOVALUE|raw }}">
            <br>
            <strong>{{ TPL_SUBJECT|raw }}</strong>: 
            <input type="text" name="subject" value="{{ TPL_SUBJECTVALUE|raw }}">
            <br>
            <strong>{{ TPL_COMMENTMAIL|raw }}</strong>: 
            <br>
            <textarea name="commentmail" cols="50" rows="5">{{ TPL_COMMENTMAIL_VALUE|raw }}</textarea>
            <br>
            <input type="hidden" value="{{ TPL_IDVALUE|raw }}" name="id">
            <input type="hidden" value="{{ TPL_COMMENTSVALUE|raw }}" name="comments">
            <input type="submit" name="email_it" value="{{ TPL_SUBMIT_EMAIL|raw }}">
            <br>
            </form>
        </p>

        <p>
            {{ TPL_LANG_PRN_ORGINAL_ARTICLE|raw }} 
            <a href="{{ TPL_PRN_ARTICLE_URL|raw }}">{{ TPL_PRN_ARTICLE_URL|raw }}</a> 
            <a href="{{ TPL_PRN_COMMENT_LINK|raw }}">{{ TPL_PRN_COMMENT_TEXT|raw }}</a>
        </p>

        {{ TPL_PRN_ARTICLE_HTML|raw }} 

        <hr>

        {{ TPL_PRN_COMMENTS_HTML|raw }}

    </body>
</html>

