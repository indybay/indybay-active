<!-- Mailing List Template -->

<div class="inner-wrap">

  <div>
    <h1>Subscribe to Off-Site Updates</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<p>
  <span class="strong">News & Events Emails</span>
  <br>
  <span class="subtext"><a href="https://lists.riseup.net/www/subscribe/indybay-news">Subscribe to our low-volume email list</a> or <a href="https://lists.riseup.net/www/info/indybay-news">manage your subscription</a>.</span>
</p>

<hr>

<p>
  <span class="strong">Indybay Event Calendar</span>
  <br>
  <span class="subtext">Browse the <img src="/im/imc_event.svg" alt="event" width="12" height="12"> <a href="/calendar/">calendar</a> for iCal feeds filtered by topic and/or region.  These iCal feeds work with a variety of calendar applications for your computer or mobile device as well as online calendar apps. Here's a <a href="https://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D0%26region_id%3D36" title="Add a calendar feed to your Google calendar">sample link</a> to add the City of San Francisco calendar to your Google Calendar.</span>
</p>

<hr>

<p>
  <span class="strong">Bluesky</span>
  <br>
  <span class="subtext">Follow <a href="https://bsky.app/profile/indybay.org" rel="noreferrer" target="_blank">@indybay.org</a> and checkout our <a href="https://go.bsky.app/P3S3VRK" rel="noreferrer" target="_blank">starter pack</a></span>
</p>

<p>
  <span class="strong">Mastodon</span>
  <br>
  <span class="subtext">Follow <a href="https://kolektiva.social/@indybay" rel="noreferrer" target="_blank">@indybay</a></span>
</p>

<p>
  <span class="strong">Twitter</span>
  <br>
  <span class="subtext">Follow <a href="https://twitter.com/intent/follow?screen_name=indybay" rel="noreferrer" target="_blank">@indybay</a> 
  and 
  <a href="https://twitter.com/intent/follow?screen_name=scimc" rel="noreferrer" target="_blank">@scimc</a></span>
</p>

<p>
  <span class="strong">Facebook</span>
  <br>
  <span class="subtext">Follow <a href="https://www.facebook.com/indybay.org/" rel="noreferrer" target="_blank">indybay.org</a> 
  and 
  <a href="https://www.facebook.com/SantaCruzIMC/" rel="noreferrer" target="_blank">SantaCruzIMC</a></span>
</p>

<p>
  <span class="strong">Instagram</span>
  <br>
  <span class="subtext">Follow <a href="https://www.instagram.com/indymedia/" rel="noreferrer" target="_blank">indymedia</a></span>
</p>

<hr>

<p>
  <span class="strong">Audio and Video Feeds ("Podcasts")</span>
</p>

<div class="flex flex--colgap-2 flex--wrap">
  <div>
    iTunes 4.9+
  </div>
  <div>
    <a href="/syn/audio.pcast"><img src="/im/pcast.png" width="45" height="15" alt="pcast"> Subscribe to Indybay's audio channel</a>
    <br>
    <a href="/syn/video.pcast"><img src="/im/pcast.png" width="45" height="15" alt="pcast"> Subscribe to Indybay's video channel</a>
  </div>
</div>

<hr>

<p>
  <span class="strong">Javascript Newsfeed</span>
  <br>
  <span class="subtext">Indymedia is on the FBI's watch list &#8212; put it on yours too!
  Simply include this HTML in your webpage to add our headlines to
  your site:
  <br>
  <code>&lt;script type="text/javascript" src="https://www.indybay.org/syn/jscript.php"&gt;&lt;/script&gt;</code></span>
</p>

<hr>

<div class="subscribe_container">
  <div class="subscribe_header">
    RSS/XML Newsfeeds:
  </div>
  <div class="subscribe_item">
    <a class="subscribe_link" href="/syn/generate_rss.php?include_posts=0&amp;include_blurbs=1">Feature Stories <span>(rss)</span></a>
  </div>
  <div class="subscribe_item">
    <a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155">Newswire <span>(rss)</span></a>
  </div>
  <div class="subscribe_item">
    <a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155&amp;rss_version=2">Newswire with enclosures <span>(rss)</span></a>
  </div>
  <div class="subscribe_item">
    <a class="subscribe_link" href="/syn/">Complete Syndication Index <span>includes topical and regional feeds!</span></a>
  </div>
</div>

<p>
  <span class="subtext">You can add the above URLs to your blog, feed reader app or browser extension. Feel free to <a href="mailto:indybay@lists.riseup.net">let us know</a> if you find it useful or if there's a problem.</span>
</p>

<div class="subscribe_container">
  <div class="subscribe_header">
    3rd party syndication services:
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/frontpage">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/frontpage">Frontpage featured stories</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/media">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/media">Newswire with media enclosures</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/features">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/features">Featured stories for all sections of the site</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/newswire">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/newswire">Open-publishing newswire</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/video">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/video">Video</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/radio">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/radio">Radio</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/photos">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/photos">Photo</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/torrents">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/torrents">BitTorrent torrents</a>
  </div>
</div>
<br>
