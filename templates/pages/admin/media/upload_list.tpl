<div class="inner-wrap">

  <div>
    <h1>{{ TPL_UPLOAD_LIST|raw }}</h1>
  </div>

  <p><a href="upload_add.php">{{ TPL_ADD_NEW_UPLOAD|raw }}</a></p>

</div><!-- END .inner-wrap -->

<hr>

<h2>Search For Uploads:</h2>
    
<form name="upload_list" type="GET">

  <div class="grid grid--2-cols-form upload upload-options">

    <div class="bg-grey">
      <label for="">{{ TPL_UPLOAD_TYPE|raw }}:</label>
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_SELECT_UPLOAD_TYPES|raw }}
    </div>

    <div class="bg-grey">
      <label for="">{{ TPL_MEDIA_TYPE_GROUPING|raw }}:</label>
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_SELECT_GROUPING_IDS|raw }}
    </div>
      
    <div class="bg-grey">
      <label for="">File Name:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="keyword" name="keyword" value="{{ TPL_LOCAL_KEYWORD|raw }}"> 
    </div>

    <div class="grid--row-full">
      <input type="submit" name="filter" value="Filter">
    </div>

  </div>

  <hr>

  {{ TPL_NAV|raw }}

  <div class="grid grid--3-cols upload-list">

    {{ TPL_UPLOAD_ROWS|raw }}
  
  </grid>

  {{ TPL_NAV|raw }}

</form>
