<div class="inner-wrap">

  <div>
    <h1>Upload a New Media File</h1>
  </div>

  <p>
    <a href="/admin/media/upload_list.php">Return to Upload List</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form action="upload_add.php" enctype="multipart/form-data" method="post">

  <div class="grid grid--2-cols-form upload upload-add">

    <div class="bg-grey">
      <label for="max_width">{{ TPL_RESTRICT_IMAGE_SIZE|raw }}:</label> 
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_SELECT_MAX_SIZE|raw }}
    </div>

    <div class="bg-grey">
      <label for="restrict_dimensions">Restrict Dimensions:</label> 
    </div>
    <div class="bg-grey">
      <input type="checkbox" id="restrict_dimensions" name="restrict_dimensions" value="1">
    </div class="bg-grey">

    <!-- Not sure if this works.  It's been commented out forever. -->
    <!-- 
    <div>
      {{ TPL_IMAGE_NAME|raw }}
      <input type="text" name="image_name" >
    </div>
    <div>
      {{ TPL_IMAGE_ALT_TEXT|raw }}
      <input type="text" name="alt_text">
    </div>
    -->

    <div class="bg-grey">
      <input type="hidden" name="MAX_FILE_SIZE" value="67108864">
      <label for="upload_file">{{ TPL_FILE_TO_UPLOAD|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="file" id="upload_file" name="upload_file">
    </div>

    <div class="grid--row-full">
      <input type="submit" name="upload" value="upload">
    </div>

  </div><!-- END  -->

</form>
