<div class="inner-wrap">

  <div>
    <h1>Uploaded Media File</div>

  <p>
    <a href="/admin/media/upload_list.php">Return to Upload List</a> 
    {{ TPL_DELETE_LINK|raw }}
  </p>

</div><!-- END .inner-wrap -->

<hr>
    
<div class="grid grid--2-cols-form upload upload-edit">

  <div class="bg-grey">{{ TPL_MEDIA_ATTACHMENT_ID|raw }}: </div>
  <div class="bg-grey">{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}</div>
    
  <div class="bg-grey">{{ TPL_FILE_LOCATION|raw }}: </div>
  <div class="file-path bg-grey">{{ TPL_LOCAL_IMAGE_LOCATION|raw }}</div>

  <div class="bg-grey">{{ TPL_UPLOAD_TYPE|raw }}: </div>
  <div class="bg-grey">{{ TPL_LOCAL_UPLOAD_TYPE_NAME|raw }}</div>

  <div class="bg-grey">{{ TPL_UPLOAD_SIZE|raw }}: </div>
  <div class="bg-grey">{{ TPL_LOCAL_UPLOAD_SIZE|raw }}</div>

  <div class="grid--row-full mobile-only"></div>
  <div class="grid--row-full upload-edit-image bg-grey">
    {{ TPL_LOCAL_THUMBNAIL|raw }}
  </div>

  <div class="bg-grey">{{ TPL_ORIGINAL_FILE_NAME|raw }}: </div>
  <div class="file-path bg-grey">{{ TPL_LOCAL_ORIGINAL_FILE_NAME|raw }}</div>

  <div class="bg-grey"><label for="alt_tag">Image Description:</label></div>
  <div class="bg-grey"><form method="post">
    <input type="text" name="alt_tag" value="{{ TPL_LOCAL_ALT_TAG }}" id="alt_tag" max_length="200">
    <input type="submit" value="Save">
  </form></div>

  <div class="grid--row-full file-associations bg-grey"><!-- collapsed content to eliminate white space so CSS :empty works -->{{ TPL_CURRENT_RELATED_NEWS_ITEM_LINKS|raw }}{{ TPL_PREVIOUS_RELATED_NEWS_ITEM_LINKS|raw }}{{ TPL_PARENT_IMAGE_LINK|raw }}{{ TPL_CHILD_IMAGE_LINKS|raw }}</div>

</table>
