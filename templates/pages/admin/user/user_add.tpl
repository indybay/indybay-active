
<div class="inner-wrap">
  
  <span class="strong">
  
  {{ TPL_VALIDATION_MESSAGES|raw }}

  {{ TPL_STATUS_MESSAGES|raw }}
  
  </span>

<div><h1>User Add</h1>
  </div>

<p>
  <a href="/admin/user/user_list.php">Return to User List</a>
</p>

</div><!-- END .inner-wrap -->

<hr>

<form name="user_edit" method="post" action="user_add.php">
  <input type="hidden" name="is_posted" value="1">

  <div class="grid grid--2-cols-form user user-add">

    <div class="bg-header">
      <label for="username">{{ TPL_USERNAME|raw }}:</label>
    </div>
    <div class="bg-header">
      <input type="text" id="username" name="username" value="{{ TPL_LOCAL_USERNAME|raw }}">
    </div>

    <div class="bg-grey">
      <label for="has_login_rights">{{ TPL_HAS_LOGIN_RIGHTS|raw }}</label>
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_CHECKBOX_HAS_LOGIN_RIGHTS|raw }}
    </div>

    <div class="bg-grey">
      <label for="email">{{ TPL_EMAIL|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="email" name="email" value="{{ TPL_LOCAL_EMAIL|raw }}">
    </div>

    <div class="bg-grey">
      <label for="phone">{{ TPL_PHONE|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="phone" name="phone" value="{{ TPL_LOCAL_PHONE|raw }}">
    </div>

    <div class="bg-grey">
      <label for="first_name">{{ TPL_FIRST_NAME|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="first_name" name="first_name" value="{{ TPL_LOCAL_FIRST_NAME|raw }}">
    </div>

    <div class="bg-grey">
      <label for="last_name">{{ TPL_LAST_NAME|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="last_name" name="last_name" value="{{ TPL_LOCAL_LAST_NAME|raw }}">
    </div>

    <div class="bg-grey">
      <label for="new_password1"><!-- {{ TPL_PASSWORD1|raw }} -->{{ TPL_NEW_PASSWORD|raw }}:</label>
    </div>
    <div class="bg-grey">
    <input type="password" id="new_password1" name="new_password1" >
    </div>

    <div class="bg-grey">
      <label for="new_password2"><!-- {{ TPL_PASSWORD2|raw }} -->{{ TPL_REPEAT_NEW_PASSWORD|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="password" id="new_password2" name="new_password2">
    </div>
      
    <div class="grid--row-full">
      <input type="submit" id="add" name="add" value="Add">
    </div>

  </div><!-- END grid--2-cols-form user user-add -->

</form>
