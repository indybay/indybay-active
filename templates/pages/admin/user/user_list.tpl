<div class="inner-wrap">

  <div>
    <h1>Users</h1>
  </div>

  <p>
    <a href="user_edit.php?user_id={{ TPL_LOCAL_SESSION_USER_ID|raw }}">{{ TPL_USER_EDIT_OWN|raw }}</a> | 
    <a href="user_change_password.php?user_id={{ TPL_LOCAL_SESSION_USER_ID|raw }}">{{ TPL_CHANGE_OWN_PASSWORD|raw }}</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<div class="grid grid--7-cols user user-list">

  <div class="grid--row-full">
    <h2>Enabled Users:</h2>
    <p>Users Who Have Logged In In The Past 15 Days</p>
  </div>

  <div class="bg-header">Username</div>
  <div class="bg-header">First</div>
  <div class="bg-header">Last</div>
  <div class="bg-header">Contact</div>
  <div class="bg-header">ID</div>
  <div class="bg-header">Login</div>
  <div class="bg-header">Activity</div>

  {{ TPL_ENABLED_USERS_RECENT|raw }}

  <div class="grid--row-full">
    <p>Users Who Have Not Logged In In The Past 15 Days</p>
  </div>
  
  <div class="bg-header">Username</div>
  <div class="bg-header">First</div>
  <div class="bg-header">Last</div>
  <div class="bg-header">Contact</div>
  <div class="bg-header">ID</div>
  <div class="bg-header">Login</div>
  <div class="bg-header">Activity</div>

  {{ TPL_ENABLED_USERS_NONRECENT|raw }}

  <div class="grid--row-full">
    <hr>
    <h2>Users Without Login Rights:</h2>
  </div>

  <div class="bg-header">Username</div>
  <div class="bg-header">First</div>
  <div class="bg-header">Last</div>
  <div class="bg-header">Contact</div>
  <div class="bg-header">ID</div>
  <div class="grid--item-span-2 bg-header">Last Login</div>

  {{ TPL_DISABLED_USERS|raw }}
  
</div><!-- END grid--7-cols users -->

<p><a href="user_add.php">{{ TPL_USER_ADD|raw }}</a></p>
