<div class="inner-wrap">
  
  <span class="strong">
  
    {{ TPL_VALIDATION_MESSAGES|raw }}
  
    {{ TPL_STATUS_MESSAGES|raw }}
  
  </span>

  <div>
    <h1>User Edit</h1>
  </div>

  <p>
    <a href="/admin/user/user_list.php">Return to User List</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form name="user_edit" method="post" action="user_edit.php">
  <input type="hidden" name="user_id" value="{{ TPL_LOCAL_USER_ID|raw }}">

  <div class="grid grid--2-cols-form user user-edit">

    <div class="bg-header">
      {{ TPL_USERNAME|raw }}:
    </div>
    <div class="bg-header">
      {{ TPL_LOCAL_USERNAME|raw }}
    </div>

    <div class="bg-grey">
      <label for="has_login_rights">{{ TPL_HAS_LOGIN_RIGHTS|raw }}</label>
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_CHECKBOX_HAS_LOGIN_RIGHTS|raw }}
    </div>

    <div class="bg-grey">
      <label for="email">{{ TPL_EMAIL|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="email" name="email" value="{{ TPL_LOCAL_EMAIL|raw }}">
    </div>

    <div class="bg-grey">
      <label for="phone">{{ TPL_PHONE|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="phone" name="phone" value="{{ TPL_LOCAL_PHONE|raw }}">
    </div>

    <div class="bg-grey">
      <label for="first_name">{{ TPL_FIRST_NAME|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="first_name" name="first_name" value="{{ TPL_LOCAL_FIRST_NAME|raw }}">
    </div>

    <div class="bg-grey">
      <label for="last_name">{{ TPL_LAST_NAME|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="text" id="last_name" name="last_name" value="{{ TPL_LOCAL_LAST_NAME|raw }}">
    </div>

    <div class="bg-grey">
      {{ TPL_LAST_LOGIN|raw }}:
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_LAST_LOGIN|raw }}
    </div class="bg-grey">

    <div class="grid--row-full">
      <span class="small">{{ TPL_LOCAL_CREATION_INFO|raw }}</span>
    </div>
    
    <div class="grid--row-full">
      <input type="submit" id="save" name="save" value="{{ TPL_SAVE|raw }}"> | 
      <a href="user_change_password.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_CHANGE_PASSWORD|raw }}</a>
    </div>

  </div><!-- END grid--2-cols-form user user-edit -->

</form>
