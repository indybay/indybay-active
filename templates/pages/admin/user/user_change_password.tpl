<div class="inner-wrap">
  
  <span class="strong">
  
    {{ TPL_STATUS_MESSAGES|raw }}
    
    {{ TPL_VALIDATION_MESSAGES|raw }}
  
  </span>

  <div>
    <h1>User Password</h1>
  </div>

  <p>
    <a href="/admin/user/user_list.php">Return to User List</a> | 
    <a href="user_edit.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_USER_EDIT_DETAIL|raw }}</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form name="user_change_password" method="post" action="user_change_password.php">
  <input type="hidden" name="user_id" value="{{ TPL_LOCAL_USER_ID|raw }}">

  <div class="grid grid--2-cols-form user user-password">

    <div class="bg-header">
      {{ TPL_USERNAME|raw }}: 
    </div>
    <div class="bg-header">
      {{ TPL_LOCAL_USERNAME|raw }}
    </div>

    <div class="bg-grey">
      {{ TPL_EMAIL|raw }}: 
    </div>
    <div class="bg-grey">
      {{ TPL_LOCAL_EMAIL|raw }}
    </div>

    <div class="bg-grey">
      <label for="old_password">{{ TPL_OLD_PASSWORD|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="password" id="old_password" name="old_password" value="">
    </div>

    <div class="bg-grey" class="bg-grey">
      <label for="new_password1">{{ TPL_NEW_PASSWORD|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="password" id="new_password1" name="new_password1" value="">
    </div>

    <div class="bg-grey">
      <label for="new_password2">{{ TPL_REPEAT_NEW_PASSWORD|raw }}:</label>
    </div>
    <div class="bg-grey">
      <input type="password" id="new_password2" name="new_password2" value="">
    </div>
    
    <div class="grid--row-full">
      <input type="submit" id="change_password" name="change_password" value="{{ TPL_CHANGE_PASSWORD|raw }}">
    </div>

  </div><!-- END grid--2-cols-form user user-password -->

</form>
