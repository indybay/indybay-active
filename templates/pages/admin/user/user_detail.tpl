<div class="inner-wrap">

  <div>
    <h1>User Detail</h1>
  </div>

  <p>
    <a href="/admin/user/user_list.php">Return to User List</a> | 
    <a href="user_edit.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_EDIT|raw }} User Contact Info</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<div class="grid grid--2-cols-form user user-detail">

  <div class="bg-header">
    {{ TPL_USERNAME|raw }}:
  </div>
  <div class="bg-header">
    {{ TPL_LOCAL_USERNAME|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_HAS_LOGIN_RIGHTS|raw }}
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_HAS_LOGIN_RIGHTS|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_EMAIL|raw }}:
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_EMAIL|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_PHONE|raw }}:
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_PHONE|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_FIRST_NAME|raw }}:
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_FIRST_NAME|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_LAST_NAME|raw }}:
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_LAST_NAME|raw }}
  </div>

  <div class="bg-grey">
    {{ TPL_LAST_LOGIN|raw }}:
  </div>
  <div class="bg-grey">
    {{ TPL_LOCAL_LAST_LOGIN|raw }}
  </div>

  <div class="grid--row-full">
    <span class="small">{{ TPL_LOCAL_CREATION_INFO|raw }}</span>
  </div>

</div>
