<!-- breaking news template -->
<a href="../index.php">{{ TPL_ADMIN_INDEX|raw }}</a> : 
<a href="../article/index.php?news_item_status_restriction=0&amp;submitted_search=Go">List Breaking News</a> |
<a href="create.php?parent_item_id={{ TPL_PARENT_ITEM_ID|raw }}">Create Breaking News</a>

<h3><i>{{ TPL_CREATE_RESULT|raw }}</i></h3>

<form action="update.php" method="post">
<input type="hidden" name="method" value="create">
<p><label for="title1">News Text:<br>
<!-- <input type="text" maxlength="140" size="140" name="title1" id="title1" value="{{ TPL_TXT|raw }}"> -->
<textarea name="title1" id="title1" value="{{ TPL_TXT|raw }}" placeholder="Use Title Case for Titles" rows="2" cols="80" maxlength="140"></textarea>
<br>140 characters max; no HTML!<br>
Include appropriate <a target="_blank" href="http://en.wikipedia.org/wiki/Tag_%28metadata%29#Hashtags">hash tags</a>.</label></p>
<p><label for="htm">Additional HTML (links etc.):<br>
<textarea rows="5" cols="80" name="htm" id="htm">{{ TPL_HTM|raw }}</textarea></label></p>
<p>
<label for="t"><input type="radio" name="display" value="t" id="t" {{ TPL_CHECKEDPUBLISH|raw }}> Publish</label>
<br>
<label for="f"><input type="radio" name="display" value="f" id="f" {{ TPL_CHECKEDHIDDEN|raw }}> Hidden</label></p>

<p><label for="s"><input type="radio" name="send" value="t" id="s"> Send Text message now</label>
<br>
<label for="d"><input type="radio" name="send" value="f" id="d" checked="checked"> Do not send Text message 
now</label></p>

<p><label for="created">Date: <input type="text" name="created" value="{{ TPL_CREATED|raw }}" id="created"></label></p>

<input type="hidden" name="newsitem" value="{{ TPL_NEWSITEM|raw }}">

<input type="hidden" name="parent_item_id" value="{{ TPL_PARENT_ITEM_ID|raw }}">

<p><input type="submit" value="Submit"></p>
</form>
<h3>Version history of this news item:</h3>
{{ TPL_ROWLIST|raw }}

<!-- / breaking news template -->
