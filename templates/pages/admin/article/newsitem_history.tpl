<!-- newsitem_history template -->
<div class="inner-wrap">

  <div>
    <h1>Newsitem History</h1>
  </div>

  <p>
    <a href="/admin/article/article_edit.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
      {{ TPL_RETURN_TO_NEWSITEM|raw }}
    </a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<h2>{{ TPL_NEWS_ITEM_HISTORY|raw }}</h2>

{{ TPL_LOCAL_PREVIOUS_VERSION_LIST|raw }}

<hr>

<p>
  <a href="/admin/article/article_edit.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
    {{ TPL_RETURN_TO_NEWSITEM|raw }}
  </a>
</p>
<br>

<!-- newsitem_history template -->