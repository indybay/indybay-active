<!-- we cant open or close the form since it's needed for paging | we could use hidden fields but it's
safer to only have thing in one place -->

<div class="flex flex--align-center flex--wrap search-options"> 

  <span>
    <label for="search">{{ TPL_SEARCH|raw }}</label>:
  </span>

  <span>
    <input id="search" name="search" type="text" size="14"  value="{{ TPL_LOCAL_SEARCH|raw }}">
  </span>

  <span>
    {{ TPL_SEARCH_DISPLAY|raw }}
  </span>

  <span>
    {{ TPL_SEARCH_MEDIUM|raw }}
  </span>

  <span>
    {{ TPL_CAT_TOPIC_SELECT|raw }}
  </span>

  <span>
    {{ TPL_CAT_REGION_SELECT|raw }}
  </span>

</div>

<div class="flex flex--align-center flex--wrap search-includes">

  <span>Include in Search:</span> 

  <span class="nowrap">
    <label for="include_comments">{{ TPL_INCLUDE_COMMENTS }}</label>
    {{ TPL_LOCAL_CHECKBOX_COMMENTS|raw }}
  </span>

  <span class="nowrap">
    <label for="include_attachments">{{ TPL_INCLUDE_ATTACHMENTS }}</label>
    {{ TPL_LOCAL_CHECKBOX_ATTACHMENTS|raw }}
  </span>

  <span class="nowrap">
    <label for="include_events">{{ TPL_INCLUDE_EVENTS }}</label>
    {{ TPL_LOCAL_CHECKBOX_EVENTS|raw }}
  </span>

  <span class="nowrap">
    <label for="include_posts">{{ TPL_INCLUDE_POSTS }}</label>
    {{ TPL_LOCAL_CHECKBOX_POSTS|raw }}
  </span>

  <span class="nowrap">
    <label for="include_blurbs">{{ TPL_INCLUDE_BLURBS }}</label>
    {{ TPL_LOCAL_CHECKBOX_BLURBS|raw }}
  </span>

</div>

<input type="hidden" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID }}">
<input type="submit" name="submitted_search" value="{{ TPL_GO }}">
