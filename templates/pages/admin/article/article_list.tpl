<!-- article_list template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_VALIDATION_MESSAGES|raw }}

    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div>
    <h1>{{ TPL_ARTICLES_ADMIN|raw }}</h1>
  </div>

  <div class="flex flex--align-center flex--justify-both flex--wrap inner-wrap-extras">

    <span>
      <a href="#aboutclass">Read About Classification Choices</a>
    </span>

    <span>
      <a href="/admin/feature_page/blurb_add.php">{{ TPL_BLURB_ADD|raw }}</a>
    </span>

  </div>

</div><!-- END .inner-wrap -->

<hr>

<div class="flex flex--align-center flex--wrap find-by-id">

  <div>
    <form action="/admin/article/article_edit.php">
      <label for="id">{{ TPL_ENTER_ARTICLE_ID|raw }}</label>
      <br>
      <input type="text" id="id" name="id" value="">
      <input type="submit" name="{{ TPL_ARTICLE_EDIT|raw }}" value="Find">
    </form>
  </div>
  <div>
    <form action="/admin/article/index.php">
    <label for="parent_item_id">{{ TPL_ENTER_COMMENT_ID|raw }}</label>
      <br>
      <input type="text" id="parent_item_id" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID|raw }}">
      <input type="hidden" name="comments" value="yes">
      <input type="submit" name="force_parent_id_search" value="Find">
    </form>
  </div>

</div>

<hr>

<div class="list-search">

  <form method="get" action="/admin/article/index.php">

    {{ TPL_TOP_SEARCH_FORM|raw }}

    <hr>

    {{ TPL_NAV|raw }}

  </form>

</div>

<form method="post" id="order_form" name="order_form">

  <input type="submit" name="classify" value="{{ TPL_SAVE|raw }}">
  <input type="hidden" name="num_rows" value="{{ TPL_NUM_ROWS|raw }}">
  <input type="hidden" name="bulk_classify" value="1">
  <br>

  {{ TPL_TABLE_MIDDLE|raw }}

  <input type="submit" name="classify" value="{{ TPL_SAVE|raw }}">

  {{ TPL_NAV|raw }}

  <p>
    <span class="setAllHide linklike">Set All Dropdowns to Hidden</span> | 
    <span class="resetAll linklike">Reset</span>
  </p>

</form>

<hr>

<br>

<p><span id="aboutclass" class="strong">ABOUT CLASSIFICATIONS</span></p>

<p>The new article admin system gives you a choice of more ways to classify posts than the old system.</p>

<p>NEW is the status when anything new comes in.</p>

<p>The HIGHLIGHTED statuses are used to make posts what we call "local" and "global"
in the right column. If you make an event HIGHLIGHTED (local or nonlocal) it will appear in the eventlinks.</p>

<p>OTHER acts the same as NEW but is a way to mark something as having been examined.
The corporate reports choices are similar to OTHER on most pages
but they make posts appear in the right column on international pages.</p>

<p>HIDDEN hides a post or comment</p>

<p>QUESTIONABLE means you are not sure if something should be hidden (QUESTIONABLE/HIDE will hide it and QUESTIONABLE/DONTHIDE will not hide it)</p>

<br>

<p><span class="strong">Classifying Comments</span></p>

<p>To make it easier for editors to know if comments have been reviewed it is worthwhile to classify comments. 
The "Needs Attention" page only shows NEW and QUESTIONABLE comments so you will want to classify any comment that shouldn't be hidden as OTHER. 
Classifying a comment as hidden will hide it but the other statuses will not have an effect on how the comment is seen.</p>

<br ><br>
<!-- end article_list template -->

