
<a href="/admin/article/article_edit.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
  {{ TPL_RETURN_TO_NEWSITEM|raw }}
</a>

<br>

{{ TPL_LOCAL_ITEM_PREVIEW|raw }}
<h3>Make Post An Attachment</h3>
<span class="strong">
{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}
</span>
<form method="Post">
Enter Desired Parent Id if you Know It:<br><input type="text" name ="parent_item_id">

<br>
Or Choose A Recent News Item From This List:
<br>
{{ TPL_LOCAL_POSSIBLE_PARENT_LIST|raw }}
<input type="hidden" name="item_id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
<p>
<input type="submit" name="make_attachment" value="Make This Post An Attachment">
</form>