<div class="inner-wrap">

<div><h1><code>www.indybay.org server status</code></h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<div class="server-status">

<h2><code>web server</code></h2>

<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/apache_accesses.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/apache_accesses-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/apache_processes.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/apache_processes-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/apache_volume.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/apache_volume-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>

<hr>

<h2><code>system</code></h2>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/cpu.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/cpu-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/load.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/load-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/memory.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/memory-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/swap.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/swap-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/processes.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/processes-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/ipmi_temp.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/ipmi_temp-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>

<hr>

<h2><code>disks</code></h2>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/df.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/df-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/zpool_chksum.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/zpool_chksum-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/smart_ada0.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/smart_ada0-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/smart_ada1.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/smart_ada1-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/smart_ada2.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/smart_ada2-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/smart_ada3.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/smart_ada3-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>

<hr>

<h2><code>network</code></h2>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/if_igb0.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/if_igb0-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/if_errcoll_igb0.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/if_errcoll_igb0-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/netstat.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/netstat-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>

<hr>

<h2><code>database</code></h2>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/mysql_network_traffic.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/mysql_network_traffic-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/mysql_commands.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/mysql_commands-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/mysql_slow.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/mysql_slow-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>
<br>
<a href="https://www.indybay.org/munin/indymedia.org/ramona.indymedia.org/mysql_innodb_bpool_act.html"><img src="https://www.indybay.org/munin-cgi/munin-cgi-graph/indymedia.org/ramona.indymedia.org/mysql_innodb_bpool_act-pinpoint={{ TPL_START|raw }},{{ TPL_END|raw }}.png?&size_x=927&size_y=272"></a>

</div>
