<script defer src="{{ TPL_LUMEN_SCRIPT_SRC }}"></script>
<div id="lumen-widget"></div>
<script>
  var lumen_submitter = LumenSubmitterWidget({
    element_selector: '#lumen-widget',
    public_key: '{{ TPL_LUMEN_PUBLIC_KEY }}'
  });
</script>
