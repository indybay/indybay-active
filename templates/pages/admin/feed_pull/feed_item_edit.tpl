<h3>Story Edit Page For "{{ TPL_LOCAL_TITLE|raw }}"</h3>
<strong>
<a href="/admin/feed_pull/feed_item_list.php?feed_id={{ TPL_LOCAL_FEED_ID|raw }}">Return to Story List for Feed "{{ TPL_LOCAL_FEED_NAME|raw }}"</a>
</strong>
{{ TPL_LOCAL_PUBLISH_INFO|raw }}
<p>
<form method="post" action="/admin/feed_pull/feed_item_edit.php">
<input type="hidden" name="rss_item_id" value="{{ TPL_LOCAL_RSS_ITEM_ID|raw }}">
<table border=1>
<tr>
<td>Creation Date:</td>
<td> {{ TPL_LOCAL_CREATION_DATE|raw }}</td>
</tr>
<tr>
<tr>
<td>Pull Date:</td>
<td>{{ TPL_LOCAL_PULL_DATE|raw }}</td>
</tr>
<tr>
<tr>
<td>Title:</td>
<td> <input type="text"  size=80 name="title" value="{{ TPL_LOCAL_TITLE|raw }}"></td>
</tr>
<tr>
<tr>
<td>Author:</td>
<td> <input type="text"  size=80 name="author" value="{{ TPL_LOCAL_AUTHOR|raw }}"></td>
</tr>
<tr>
<td>Related URL:</td>
<td><input type="text" size=80  name="related_url" value="{{ TPL_LOCAL_RELATED_URL|raw }}"></td>
</tr>
<tr>
<td>Summary:</td>
<td><textarea name="summary" cols=60 rows=10>{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
</td>
</tr>
<tr>
<td>Text:</td>
<td><textarea name="text" cols=60 rows=20>{{ TPL_LOCAL_TEXT|raw }}</textarea>
</td>
</tr>
<tr><td colspan="2">&#160;</td></tr>
<tr>
<td>Display Status:</td>
<td>{{ TPL_STATUS_SELECT|raw }}
</td>
</tr>
<tr><td colspan="2">&#160;</td></tr>
<tr>
<td>Categories:</td>
<td>
{{ TPL_LOCAL_CHECKBOX_REGION|raw }}
<br>
{{ TPL_LOCAL_CHECKBOX_TOPIC|raw }}
<br>
{{ TPL_LOCAL_CHECKBOX_INT|raw }}
</td></tr>
<tr><td colspan="2">&#160;</td></tr>
<tr>
<td colspan="2">
<input type="submit" name="update" value="update">
&#160;
<input type="submit" name="delete" value="delete">
&#160;
<input type="submit" name="publish" value="publish">
</td>
</tr>
</table>
</form>
<hr>
<h2>PREVIEW:</h2>
<hr>
<p>
{{ TPL_LOCAL_TITLE|raw }}
<br>
by {{ TPL_LOCAL_AUTHOR|raw }}
<p>
<blockquote>
{{ TPL_LOCAL_SUMMARY|raw }}
</blockquote>
<p>
{{ TPL_LOCAL_TEXT|raw }}