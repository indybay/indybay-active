<!-- event_edit template -->

<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_VALIDATION_MESSAGES|raw }}
    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div>
    <h1>Event Edit</h1>
  </div>

  <p>
    <span class="newsitem-ids">
      <span class="nowrap">
        {{ TPL_ID|raw }}: 
        {{ TPL_LOCAL_NEWS_ITEM_ID|raw }} 
      </span>
      <span class="nowrap">
        {{ TPL_VERSION_ID|raw }}: 
        <a href="#version-history" title="View version history below">{{ TPL_LOCAL_NEWS_ITEM_VERSION_ID|raw }}</a> &#8595;
      </span>
      <span class="nowrap"> 
        <a href="#page-list-categories" title="Select categories from page list">Select Page Categories</a> &#8595;
      </span>
      <span class="nowrap">
        <a href="#view" title="View saved article in admin below">Admin View</a> &#8595;
      </span>
    </span><br>
  </p>

  <p>
    Current Post: <a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}" title="View live version of article">{{ TPL_LOCAL_PUBLISH_LINK|raw }}</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form action="/admin/calendar/event_edit.php" method="post">
  <input type="hidden" name="editswitch" value="1">
  <input type="hidden" name="news_item_id" value="{{ TPL_LOCAL_ID|raw }}">
  <input type="hidden" name="timestamp" value="{{ TPL_LOCAL_MICROTIME|raw }}">
  <input type="hidden" name="old_display" value="{{ TPL_LOCAL_OLD_DISPLAY|raw }}">

  <div class="grid grid--2-cols-form article-edit">

    <div class="grid--row-full">

      <!-- <input type="submit" name="save" value="Save"> -->

      <input type="submit" name="save_and_regenerate_all" value="Save">
      
      <input type="submit" name="blurbify" value="Create Blurb From Post">

      {{ TPL_PUBLISH_RESULT|raw }}

    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div>
      <span class="small">
        {{ TPL_CREATED|raw }}:
        <br>
        {{ TPL_LOCAL_CREATION_DATE|raw }}
        <!-- Is this ever used for events?  It is for blurbs. -->
        {{ TPL_LOCAL_CREATED_BY_USER_INFO|raw }}
      </span>
    </div>
    <div>
      <span class="small">
        {{ TPL_MODIFIED|raw }}:
        <br>
        {{ TPL_LOCAL_VERSION_CREATION_DATE|raw }}
        <!-- This is definitely used. -->
        {{ TPL_LOCAL_LAST_UPDATED_BY_USER_INFO|raw }}
      </span>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div>
      <label for="title1">{{ TPL_TITLE1|raw }}:</label>
    </div>
    <div>
      <textarea id="title1" name="title1" rows="2" maxlength="100">{{ TPL_LOCAL_TITLE1|raw }}</textarea>
      <!-- <input type="text" name="title1" size="70" maxlength="100" value="{{ TPL_LOCAL_TITLE1|raw }}"> -->
    </div>

    <div>
      <label for="displayed_date_month">{{ TPL_DISPLAYED_DATE|raw }}:</label>
    </div>
    <div class="event-displayed-date">
      {{ TPL_LOCAL_SELECT_DATE_DISPLAYED_DATE|raw }}
    </div>

    <div>
      <label for="event_duration">{{ TPL_EVENT_DURATION|raw }}:</label>
    </div>
    <div class="event-duration">
      <input type="text" id="event_duration" name="event_duration" size="15" value="{{ TPL_LOCAL_EVENT_DURATION|raw }}">
    </div>

    <div>
      <label for="event_type_id">{{ TPL_EVENT_TYPE|raw }}:</label>
    </div>
    <div>
      {{ TPL_CAT_TYPE_SELECT|raw }}
    </div>

    <div>
      <label for="displayed_author_name">{{ TPL_CONTACT_NAME|raw }}:</label>
    </div>
    <div>
      <input type="text" id="displayed_author_name" name="displayed_author_name" size="70" value="{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}">
    </div>

    <div>
      <label for="email">{{ TPL_EMAIL|raw }}:</label>
    </div>
    <div>
      <input type="text" id="email" name="email" size="70" value="{{ TPL_LOCAL_EMAIL|raw }}">
    </div>

    <div>
      <label for="phone">{{ TPL_PHONE|raw }}:</label>
    </div>
    <div>
      <input type="text" id="phone" name="phone" size="70" value="{{ TPL_LOCAL_PHONE|raw }}">
    </div>

    <div>
      <label for="address">{{ TPL_ADDRESS|raw }}:</label>
    </div>
    <div>
      <input type="text" id="address" name="address" size="70" placeholder="(contact address only used in old events)" value="{{ TPL_LOCAL_ADDRESS|raw }}">
      <br>
      <label for="display_contact_info">{{ TPL_SHOW_CONTACT_INFO|raw }}</label> {{ TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO|raw }}
    </div>

    <div>
      <label for="news_item_status_id">
        {{ TPL_DISPLAYSTAT|raw }}:
      </label>
    </div>
    <div>
      {{ TPL_SELECT_DISPLAY|raw }} 
    </div>

    <div>
      <label for="news_item_type_id">{{ TPL_ARTTYPE|raw }}:</label> 
    </div>
    <div>
      {{ TPL_SELECT_NEWS_ITEM_TYPE_ID|raw }}
    </div>

    <div class="grid--row-full">
      <label for="summary">{{ TPL_SUMMARY|raw }}:</label>
      <textarea id="summary" name="summary" rows="5">{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
      <label for="is_summary_html">{{ TPL_IS_SUMMARY_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
    </div>

    <div class="grid--row-full">
      <label for="text">{{ TPL_DESCRIPTION|raw }}:</label>
      <textarea id="text" name="text" rows="20">{{ TPL_LOCAL_TEXT|raw }}</textarea>
      <label for="is_text_html">{{ TPL_IS_TEXT_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
    </div>

    <div>
      <label for="related_url">{{ TPL_LINK|raw }}:</label>
    </div>
    <div>
      <input type="text" id="related_url" name="related_url" size="70" value="{{ TPL_LOCAL_RELATED_URL|raw }}">
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div id="page-list-categories" class="grid--row-full page-list">
      <h2>{{ TPL_PAGE_LIST|raw }}</h2>
      <!-- start checkboxes -->
      {{ TPL_LOCAL_CHECKBOX_REGION|raw }}
      <!-- end CHECKBOX_REGION -->
      {{ TPL_LOCAL_CHECKBOX_TOPIC|raw }}
      <!-- end CHECKBOX_TOPIC -->
      {{ TPL_LOCAL_CHECKBOX_INT|raw }}
      <!-- end CHECKBOX_INT -->
      {{ TPL_LOCAL_CHECKBOX_OTHER|raw }}
      <!-- end CHECKBOX_OTHER -->
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <h2 class="nomarg">Attachment Options</h2>
    </div>

    <div>
      <label for="parent_item_id">{{ TPL_PARENT_ID|raw }}:</label>
    </div>
    <div>
      <input type="text" id="parent_item_id" name="parent_item_id" size="10" value="{{ TPL_LOCAL_PARENT_ID|raw }}">
    </div>

    <div>
      <label for="media_attachment_id">{{ TPL_MEDIA_ATTACHMENT_ID|raw }}:</label>
    </div>
    <div>
      <input type="text" size="10" id="media_attachment_id" name="media_attachment_id" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}">
    </div>

    <div>
      <label for="thumbnail_media_attachment_id">{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}:</label>
    </div>
    <div>
      <input type="text" id="thumbnail_media_attachment_id" name="thumbnail_media_attachment_id" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}">
    </div>

    <!-- <div>
      <label for="replace_upload">{{ TPL_REPLACE_MEDIA_ATTACHMENT|raw }}:</label>
    </div>
    <div>
      <input type="file" id="replace_upload" name="replace_upload">
    </div> -->

    {{ TPL_LOCAL_IMAGE_OPTIONS|raw }}

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <input type="submit" id="save2" name="save2" value="Save">
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div id="version-history" class="grid--row-full version-ids">
      <h2>Version History</h2>
      {{ TPL_LOCAL_PREVIOUS_VERSION_LIST|raw }}
      <p>
        <a href="/admin/article/newsitem_history.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_NEWSITEM_HISTORY|raw }}</a>
      </p>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

  </div><!-- END grid--2-cols-form article-edit -->

</form>

<h2 id="view">Admin View</h2>

<p>
  Current Post: <a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}" title="View live version of article">{{ TPL_LOCAL_PUBLISH_LINK|raw }}</a>
</p>

<div id="preview">
  {{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}
</div><!-- END admin #preview -->

<!-- end event_edit template -->
