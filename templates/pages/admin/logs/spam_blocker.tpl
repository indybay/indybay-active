<div class="inner-wrap">

  <div>
    <h1>{{ TPL_SPAM_BLOCKS|raw }}</h1>
  </div>

  <p>WARNING: This spam blocking system has largely been replaced by a new system <a href="spam_identification.php">available here</a>.
  </p>
  <p>Blocks work by taking all the parameters you define and only block where all of the conditions are met. POST means HTTP post which only applies on the site for previews and posts (searches are done via HTTP GETs).
  </p>
  <p>This section will be changed in a few months to make it easier to use (mainly through links in from logs and edit pages), but if you need to get more details about how to use blocks now you can email zogren@yahoo.com.
  </p>

</div><!-- END .inner-wrap -->

<hr>

<h2>Add Blocks</h2>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-blocker">

    <div class="grid--row-full bg-grey">
      Block specific IP: <input type="text" name="ip" value="{{ TPL_LOCAL_IP|raw }}">
    </div>

    <div class="grid--row-full bg-grey">
      Block whole subnets:<br>
      (
      <input type="text" name="ip_part1" value="{{ TPL_LOCAL_IP1|raw }}">
      .
      <input type="text" name="ip_part2" value="{{ TPL_LOCAL_IP2|raw }}">
      .
      <input type="text" name="ip_part3" value="{{ TPL_LOCAL_IP3|raw }}">
      .
      *
      )
    </div>

    <div class="grid--row-full bg-grey">
      Restrict block to specific parent id: <input type="text" name="parent_news_item_id" value="{{ TPL_LOCAL_PARENT_NEWS_ITEM_ID|raw }}">
    </div>

    <div class="grid--row-full bg-grey">
      Restrict to {{ TPL_LOCAL_METHOD_SELECT|raw }} <br>
      <span class="small">(blocking POST will disable to publish button and publish page, wheras blocking "DB Post" will post but make post or attachment save as hidden)</span>
      <br>
      Where requested url contains
      <input type="text" name="url" value="{{ TPL_LOCAL_URL|raw }}"> and referring url contains <input type="text" name="referring_url" value="{{ TPL_LOCAL_REFERRING_URL|raw }}">
      <br>
      Where HTTP headers, title, author, email or summary contains:
      <input type="text" name="keyword" value="{{ TPL_LOCAL_KEYWORD|raw }}">
      <br>
      When blocked redirect user to the following url:
      <input type="text" name="redirect_url" value="{{ TPL_LOCAL_REDIRECT_URL|raw }}">
      <br>
      Note (for other admins) :
      <input type="text" name="note" value="{{ TPL_LOCAL_NOTE|raw }}">
    </div>

    <div class="grid--row-full">
      <input type="submit" name="add block" value="Add Block">
    </div>

  </div>

</form>

<hr>

<h2>Blocks List</h2>

<p class="small">[This wide table scrolls horizontally &#8596;]</p>

<form method="post">

  <div class="overflow-spam-blocker2">

    <div class="overflow-content overflowed-content">

      <div class="grid grid--13-cols-form spam spam-blocker2">

        <div class="bg-header"></div>
        <div class="bg-header">ip</div>
        <div class="bg-header">ip_part1</div>
        <div class="bg-header">ip_part2</div>
        <div class="bg-header">ip_part3</div>
        <div class="bg-header">method</div>
        <div class="bg-header">url</div>
        <div class="bg-header">ref url</div>
        <div class="bg-header">parent id</div>
        <div class="bg-header">more</div>
        <div class="bg-header">block_destination</div>
        <div class="bg-header">note</div>
        <div class="bg-header">added info</div>

        {{ TPL_LOCAL_BLOCK_LIST|raw }}

        <div class="grid--row-full">
          <input type="submit" name="clear_all_blocks" value="Clear Blocks">
        </div>

      </div><!-- END grid--13-cols-form spam-blocker2 -->

    </div><!-- END overflow-content overflowed-content

  </div><!-- END overflow-spam-blocker2 -->

</form>