<h3>{{ TPL_LOG_LIST|raw }}</h3>


<form>
<strong>Filter:</strong>

<br>
ip <input name="ip" value="{{ TPL_LOCAL_IP|raw }}">
<br>
ip search by parts (
<input name="ip1" value="{{ TPL_LOCAL_IP1|raw }}">
.<input name="ip2" value="{{ TPL_LOCAL_IP2|raw }}">
.<input name="ip3" value="{{ TPL_LOCAL_IP3|raw }}">
. *
)
<br>
method 

{{ TPL_LOCAL_METHOD_SELECT|raw }}

url <input name="url" value="{{ TPL_LOCAL_URL|raw }}">
ref url <input name="ref_url" value="{{ TPL_LOCAL_REF_URL|raw }}">
<br>
parent id <input name="parent_news_item_id" value="{{ TPL_LOCAL_PARENT_NEWS_ITEM_ID|raw }}">
posted id <input name="news_item_id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
<br>
keyword <input name="keyword" value="{{ TPL_LOCAL_KEYWORD|raw }}">
sort by
{{ TPL_LOCAL_SORT_BY_SELECT|raw }}
<br>
<input type="submit" name="submit" value="FILTER">
<br>
{{ TPL_NAV|raw }}

<table width=100%>
<tr bgcolor="#000000">
<td valign="top"><font color="#ffffff"><small>ip</small></font></td>
<td valign="top"><font color="#ffffff"><small>start time & duration</small></font></td>
<td valign="top"><font color="#ffffff"><small>method</small></font></td>
<td valign="top"><font color="#ffffff"><small>url</small></font></td>
<td valign="top"><font color="#ffffff"><small>ref url</small></font></td>
<td valign="top"><font color="#ffffff"><small>parent id</small></font></td>
<td valign="top"><font color="#ffffff"><small>id</small></font></td>
<td valign="top"><font color="#ffffff"><small>more</small></font></td>
</tr>
{{ TPL_LOCAL_LOG_LIST|raw }}
</table>
{{ TPL_NAV|raw }}
</form>
<hr>
<form method="post">
<input type="submit" name="clear_get_logs" value="Clear Get Logs">
<br>
<input type="submit" name="clear_all_logs" value="Clear All Logs">
<br>
<input type="submit" name="clear_ips" value="Clear IPs From Logs">
</form>
<hr>
<table border=1><tr><td>
<form method="post">
{{ TPL_LOG_GET|raw }}? <input type=checkbox name="log_get" value="true" {{ TPL_LOCAL_GET_CHECKED|raw }}>
{{ TPL_LOG_IPS_FOR_GET|raw }}? <input type=checkbox name="log_ip_get" value="true" {{ TPL_LOCAL_GETIP_CHECKED|raw }}>
<br>
{{ TPL_LOG_SEARCH|raw }}? <input type=checkbox name="log_search" value="true" {{ TPL_LOCAL_SEARCH_CHECKED|raw }}>
{{ TPL_LOG_IPS_FOR_SEARCH|raw }}? <input type=checkbox name="log_ip_search" value="true" {{ TPL_LOCAL_SEARCHIP_CHECKED|raw }}>
<br>
{{ TPL_LOG_POST|raw }}? <input type=checkbox name="log_post" value="true" {{ TPL_LOCAL_POST_CHECKED|raw }}>
{{ TPL_LOG_IPS_FOR_POST|raw }}? <input type=checkbox name="log_ip_post" value="true" {{ TPL_LOCAL_POSTIP_CHECKED|raw }}>
<br>
{{ TPL_LOG_DB|raw }}? <input type=checkbox name="log_db" value="true" {{ TPL_LOCAL_DB_CHECKED|raw }}>
{{ TPL_LOG_IPS_FOR_DB|raw }}? <input type=checkbox name="log_ip_db" value="true" {{ TPL_LOCAL_DBIP_CHECKED|raw }}>
<br>
<input type="submit" name="save_log_status_changes" value="{{ TPL_SAVE_LOG_STATUS_CHANGES|raw }}">
</form>
</td></tr></table>
<hr>



