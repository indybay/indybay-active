<div class="inner-wrap">

  <div>
    <h1>Spam Identification</h1>
  </div>

  <p>This system is intended to replace the old block system, but since that did provide some additional options (like redirecting based off referring and desired url), it is still <a href="spam_blocker.php">available here</a>.
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-id">

    <div class="grid--row-full bg-grey">
      <p>IPs to block from site (only use this for spam engines and spam attacks not trolls or those manually reposting right after you rehide):</p>
      </div>

    <div class="grid--row-full bg-grey">
      <textarea name="ip_block_list" cols="50" rows="10">{{ TPL_LOCAL_BLOCK_IPS|raw }}</textarea>
    </div>

    <div class="grid--row-full">
      <input type="submit" name="save_ip_block_list" value="Save">
    </div>

  </div>

</form>

<hr>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-id">

    <div class="grid--row-full bg-grey">
      IPs & Subnets That May Be Spam (posts may get hidden or prevented from posting based off other factors):</p>
        </div>

    <div class="grid--row-full bg-grey">
      <textarea name="ip_spam_list" cols="50" rows="10">{{ TPL_LOCAL_SPAM_IPS|raw }}</textarea>
        </div>

    <div class="grid--row-full">
      <input type="submit" name="save_ip_spam_list" value="Save">
    </div>

  </div>

</form>

<hr>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-id">

    <div class="grid--row-full bg-grey">
      Title Keywords That Could Indicate Spam (posts may get hidden or prevented from posting based off other factors):</p>
        </div>

    <div class="grid--row-full bg-grey">
      <textarea name="title_spam_list" cols="50" rows="10">{{ TPL_LOCAL_SPAM_TITLE|raw }}</textarea>
        </div>

    <div class="grid--row-full">
      <input type="submit" name="save_title_spam_list" value="Save">
    </div>

  </div>

</form>

<hr>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-id">

    <div class="grid--row-full bg-grey">
      Subject Or Text Keywords That Could Indicate Spam (posts may get hidden or prevented from posting based off other factors):</p>
        </div>

    <div class="grid--row-full bg-grey">
      <textarea name="text_spam_list" cols="50" rows="10">{{ TPL_LOCAL_SPAM_TEXT|raw }}</textarea>
        </div>

    <div class="grid--row-full">
      <input type="submit" name="save_text_spam_list" value="Save">
    </div>

  </div>

</form>

<p>

<form method="post">

  <div class="grid grid--2-cols-form spam spam-id">

    <div class="grid--row-full bg-grey">
      Specific Strings In Text That Should Be Blocked During Validation:</p>
      </div>

    <div class="grid--row-full bg-grey">
      <textarea name="validation_string_list" cols="50" rows="10">{{ TPL_LOCAL_VALIDATION_STRINGS|raw }}</textarea>
      </div>

    <div class="grid--row-full">
      <input type="submit" name="save_validation_string_list" value="Save">
    </div>

  </div>

</form>