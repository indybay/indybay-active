<!-- blurb_edit template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_VALIDATION_MESSAGES|raw }}
    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div>
    <h1>Feature Edit</h1>
  </div>

  <p>
    <span class="newsitem-ids">
      <span class="nowrap">
        {{ TPL_ID|raw }}: 
        {{ TPL_LOCAL_NEWS_ITEM_ID|raw }} 
      </span>
      <span class="nowrap">
        {{ TPL_VERSION_ID|raw }}: 
        <a href="#version-history" title="View version history below">{{ TPL_LOCAL_NEWS_ITEM_VERSION_ID|raw }}</a> &#8595;
      </span>
      <span class="nowrap">
        <a href="#view" title="View saved article in admin below">Admin View</a> &#8595;
      </span>
    </span><br>
  </p>

  <p>
    <span class="newsitem-associations">
      {{ TPL_LOCAL_PAGE_ASSOCIATIONS|raw }}
    </span>
  </p>

  <p>
    <a href="blurb_recategorize.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_RECATEGORIZE|raw }}</a>
    <!-- | <a href="/admin/breaking/create.php?parent_item_id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">Add breaking news item for this blurb</a> -->
  </p>

  <!-- <p>
    Current Feature: <a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}" title="View live version of article">{{ TPL_LOCAL_PUBLISH_LINK|raw }}</a>
  </p> -->

</div><!-- END .inner-wrap -->

<hr>

<form enctype="multipart/form-data" method="post" action="blurb_edit.php">
  <input type="hidden" name="editswitch" value="1">
  <input type="hidden" name="news_item_id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
  <input type="hidden" name="timestamp" value="{{ TPL_LOCAL_MICROTIME|raw }}">

  <div class="grid grid--2-cols-form article-edit">

    <div class="grid--row-full">

      <input type="submit" name="save" value="Save"> 

    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div>
      <span class="small">
        {{ TPL_CREATED|raw }}:
        <br>
        {{ TPL_LOCAL_CREATION_DATE|raw }}
        
        {{ TPL_LOCAL_CREATED_BY_USER_INFO|raw }}
      </span>
    </div>
    <div>
      <span class="small">
        {{ TPL_MODIFIED|raw }}:
        <br>
        {{ TPL_LOCAL_VERSION_CREATION_DATE|raw }}
        
        {{ TPL_LOCAL_LAST_UPDATED_BY_USER_INFO|raw }}
      </span>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div>
      <label for="title1">{{ TPL_TITLE1|raw }}:</label>
    </div>
    <div>
      <textarea id="title1" name="title1" rows="2" maxlength="100">{{ TPL_LOCAL_TITLE1|raw }}</textarea>
      <!-- <input type="text" name="title1" size="70" maxlength="100" value="{{ TPL_LOCAL_TITLE1|raw }}"> -->
    </div>

    <div>
      <label for="title2">{{ TPL_TITLE2|raw }}:</label>
    </div>
    <div>
      <textarea id="title2" name="title2" rows="2" maxlength="100">{{ TPL_LOCAL_TITLE2|raw }}</textarea>
      <!-- <input type="text" name="title2" maxlength="90" value="{{ TPL_LOCAL_TITLE2|raw }}"> -->
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <h2>{{ TPL_SHORT_VERSION|raw }}:</h2>
      <textarea id="summary" name="summary" rows="5">{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
      <label for="is_summary_html">{{ TPL_IS_SHORT_VERSION_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
    </div>

    <!-- <div class="grid--row-full">
      <hr>
    </div> -->

    <div class="grid--row-full">
      <h3>{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT|raw }}</h3>
      <p class="small">
        {{ TPL_IMAGE_SELECT_DESC|raw }}
      </p>
    </div>

    <div>
      <label for="thumbnail_media_attachment_id">{{ TPL_ENTER_ID|raw }}:</label>
    </div>
    <div>
      <input type="text" id="thumbnail_media_attachment_id" name="thumbnail_media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}"> 
      <span class="small">{{ TPL_LOCAL_THUMBNAIL_EDIT_LINK|raw }}</span>
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_media_attachment_select_id">{{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_THUMBNAIL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_upload">{{ TPL_UPLOAD_A_FILE|raw }}:</label>
      <br>
      <span class="small">(480w x 320h)</small>
    </div>
    <div>
      <div class="fileinputwrapper">
        <input type="file" id="thumbnail_upload" name="thumbnail_upload" class="fileinput" accept=".gif,.jpeg,.jpg,.png">
      </div>
      <div class="previewimagewrapper">
        <div class="clear-file-upload" title="Clear this attachment">×</div>
        <img class="previewimage" src="" alt="preview image">
      </div>
      <div class="imagedimensions">
        Image is <span class="filedimension">width x height</span>.
        <span class="filedimensioncorrect">Perfect!</span>
        <span class="filedimensionwarning">It should be 480 x 320.</span>
      </div>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <h2>{{ TPL_LONG_VERSION|raw }}</h2>
      <textarea id="text" name="text" rows="30" wrap="virtual">{{ TPL_LOCAL_TEXT|raw }}</textarea>
      <br>
      <label for="is_text_html">{{ TPL_IS_LONG_VERSION_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
    </div>

    <div>
      <label for="related_url">{{ TPL_PUB_URL|raw }}:</label>
    </div>
    <div>
      <input type="text" id="related_url" name="related_url" size="70" value="{{ TPL_LOCAL_RELATED_URL|raw }}">
    </div>

    <!-- <div class="grid--row-full">
      <hr>
    </div> -->

    <div class="grid--row-full">
      <h3>{{ TPL_MEDIA_ATTACHMENT|raw }}</h3>
      <p class="small">
        {{ TPL_IMAGE_SELECT_DESC|raw }}
      </p>
    </div>

    <div>
      <label for="media_attachment_id">{{ TPL_ENTER_ID|raw }}:</label>
    </div>
    <div>
    <input type="text" id="media_attachment_id" name="media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}">
      <span class="small">{{ TPL_LOCAL_ATTACHMENT_EDIT_LINK|raw }}</span>
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="media_attachment_select_id">{{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_upload">{{ TPL_UPLOAD_A_FILE|raw }}:</label>
      <br>
      <span class="small">(480w x 320h)</small>
    </div>
    <div>
    <div class="fileinputwrapper">
        <input class="fileinput" type="file" name="main_upload" accept=".gif,.jpeg,.jpg,.png">
      </div>
      <div class="previewimagewrapper">
        <div class="clear-file-upload" title="Clear this attachment">×</div>
        <img class="previewimage" src="" alt="preview image">
      </div>
      <div class="imagedimensions">
        Image is <span class="filedimension">width x height</span>.
        <span class="filedimensioncorrect">Perfect!</span>
        <span class="filedimensionwarning">It should be 480 x 320.</span>
      </div>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <input type="submit" id="save2" name="save2" value="Save">
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div id="version-history" class="grid--row-full version-ids">
      <h2>{{ TPL_RECENT_PREVIOUS_VERSIONS|raw }}</h2>
      {{ TPL_LOCAL_PREVIOUS_VERSION_LIST|raw }}
      <p>
        <a href="/admin/article/newsitem_history.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_BLURB_HISTORY|raw }}</a>
      </p>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

  </div><!-- END grid--2-cols-form article-edit -->

</form>

<h2 id="view">Admin View</h2>

<!-- <p>
  Current Post: <a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}" title="View live version of article">{{ TPL_LOCAL_PUBLISH_LINK|raw }}</a>
</p> -->

<div id="preview">

  <p class="rednote">Long and short versions of blurbs below appear as they will on stand-alone and feature blurb pages...</p>

  <hr>

  <div>
    <h3>{{ TPL_LONG_VERSION|raw }}:</h3>
    {{ TPL_LOCAL_DISPLAY_LONG_PREVIEW|raw }}
  </div>

  <hr>
  
  <div class="page preview-page-feature">
    <h3>{{ TPL_SHORT_VERSION|raw }}:</h3>
    {{ TPL_LOCAL_DISPLAY_SHORT_PREVIEW|raw }}
  </div>

  <p class="rednote">Keep titles short enough so they do not break to two lines.  Don't let short version text hang below blurb image.</p>

</div><!-- END admin #preview -->

<!-- end blurb_edit template -->
