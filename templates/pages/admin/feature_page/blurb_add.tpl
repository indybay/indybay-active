<!-- blurb_add template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_VALIDATION_MESSAGES|raw }}
    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div>
    <h1>Create New Feature</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<form enctype="multipart/form-data" method="post" action="/admin/feature_page/blurb_add.php">

  <div class="grid grid--2-cols-form article-edit">

    <div>
      <label for="title1">{{ TPL_TITLE1|raw }}:</label>
    </div>
    <div>
      <textarea id="title1" name="title1" rows="2" maxlength="100">{{ TPL_LOCAL_TITLE1 }}</textarea>
      <!-- <input type="text" id="title1" name="title1" maxlength="100" value="{{ TPL_LOCAL_TITLE1 }}"> -->
    </div>

    <div>
      <label for="title2">{{ TPL_TITLE2|raw }}:</label>
    </div>
    <div>
      <textarea id="title2" name="title2" rows="2" maxlength="100">{{ TPL_LOCAL_TITLE2 }}</textarea>
      <!-- <input type="text" id="title2" name="title2" maxlength="100" value="{{ TPL_LOCAL_TITLE2 }}"> -->
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <h2>{{ TPL_SHORT_VERSION|raw }}:</h2>
      <textarea id="summary" name="summary" rows="5">{{ TPL_LOCAL_SUMMARY }}</textarea>
      <label for="is_summary_html">{{ TPL_IS_SHORT_VERSION_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
    </div>

    <!-- <div class="grid--row-full">
      <hr>
    </div> -->

    <div class="grid--row-full">
      <h3>{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT|raw }}</h3>
      <p class="small">
        {{ TPL_IMAGE_SELECT_DESC|raw }}
      </p>
    </div>

    <div>
      <label for="thumbnail_media_attachment_id">{{ TPL_ENTER_ID|raw }}:</label>
    </div>
    <div>
      <input type="text" id="thumbnail_media_attachment_id" name="thumbnail_media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID }}">
      <span class="small">{{ TPL_LOCAL_THUMBNAIL_EDIT_LINK|raw }}</span>
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_media_attachment_select_id">{{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_THUMBNAIL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_upload">{{ TPL_UPLOAD_A_FILE|raw }}:</label>
      <br>
      <span class="small">(480w x 320h)</small>
    </div>
    <div>
      <div class="fileinputwrapper">
        <input type="file" id="thumbnail_upload" name="thumbnail_upload" class="fileinput" accept=".gif,.jpeg,.jpg,.png">
      </div>
      <div class="previewimagewrapper">
        <div class="clear-file-upload" title="Clear this attachment">×</div>
        <img class="previewimage" src="" alt="preview image">
      </div>
      <div class="imagedimensions">
        Image is <span class="filedimension">width x height</span>.
        <span class="filedimensioncorrect">Perfect!</span>
        <span class="filedimensionwarning">It should be 480 x 320.</span>
      </div>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <h2>{{ TPL_LONG_VERSION|raw }}</h2>
      <textarea id="text" name="text" rows="30" wrap="virtual">{{ TPL_LOCAL_TEXT }}</textarea>
      <label for="is_text_html">{{ TPL_IS_LONG_VERSION_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
    </div>

    <div>
      <label for="related_url">{{ TPL_PUB_URL|raw }}:</label>
    </div>
    <div>
      <input type="text" id="related_url" name="related_url" size="70" value="{{ TPL_LOCAL_RELATED_URL }}">
    </div>

    <!-- <div class="grid--row-full">
      <hr>
    </div> -->

    <div class="grid--row-full">
      <h3>{{ TPL_MEDIA_ATTACHMENT|raw }}</h3>
      <p class="small">
        {{ TPL_IMAGE_SELECT_DESC|raw }}
      </p>
    </div>

    <div>
      <label for="media_attachment_id">{{ TPL_ENTER_ID|raw }}:</label>
    </div>
    <div>
    <input type="text" id="media_attachment_id" name="media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID }}">
      <span class="small">{{ TPL_LOCAL_ATTACHMENT_EDIT_LINK|raw }}</span>
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="media_attachment_select_id">{{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
    </div>

    <div class="grid--row-full">
      {{ TPL_OR|raw }}
    </div>

    <div>
      <label for="thumbnail_upload">{{ TPL_UPLOAD_A_FILE|raw }}:</label>
      <br>
      <span class="small">(480w x 320h)</small>
    </div>
    <div>
      <div class="fileinputwrapper">
        <input class="fileinput" type="file" name="main_upload" accept=".gif,.jpeg,.jpg,.png">
      </div>
      <div class="previewimagewrapper">
        <div class="clear-file-upload" title="Clear this attachment">×</div>
        <img class="previewimage" src="" alt="preview image">
      </div>
      <div class="imagedimensions">
        Image is <span class="filedimension">width x height</span>.
        <span class="filedimensioncorrect">Perfect!</span>
        <span class="filedimensionwarning">It should be 480 x 320.</span>
      </div>
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <input type="submit" name="publish" value="{{ TPL_BUTTON_PUBLISH|raw }}">
    </div>

  </div><!-- END grid--2-cols-form article-edit -->

</form>

<!-- end blurb_add template -->
