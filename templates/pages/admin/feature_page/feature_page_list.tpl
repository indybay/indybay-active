<div class="inner-wrap">
  
  <span class="strong">
    
    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div><h1 class="headline-text">{{ TPL_PAGES|raw }}</h1>
  </div>

  <p><a href="/admin/feature_page/blurb_add.php">{{ TPL_BLURB_ADD|raw }}</a></p>

</div><!-- END .inner-wrap -->

<hr>
    
<div class="grid grid--5-cols feature-pages">
  
  <div class="grid--row-full">
    <h2>{{ TPL_OTHER_PAGES|raw }}</h2>
  </div>
    
  {{ TPL_OTHER_FEATUREPAGE_GRIDROWS|raw }}
  
  <div class="grid--row-full">
    <h2>{{ TPL_REGION_PAGES|raw }}</h2>
  </div>
    
  {{ TPL_REGION_FEATUREPAGE_GRIDROWS|raw }}
  
  <div class="grid--row-full">
    <h2>{{ TPL_TOPIC_PAGES|raw }}</h2>
  </div>
    
  {{ TPL_TOPIC_FEATUREPAGE_GRIDROWS|raw }}
  
  <div class="grid--row-full">
    <h2>{{ TPL_INTERNATIONAL_PAGES|raw }}</h2>
  </div>
    
  {{ TPL_INTERNATIONAL_FEATUREPAGE_GRIDROWS|raw }}

</div><!-- END grid grid--5-cols feature-pages -->

<hr>

<p>
  <a href="/admin/feature_page/feature_page_list.php?force_all_pages_live=1">Push All Center Columns Live</a>
</p>

<p>
  <a href="/admin/feature_page/feature_page_list.php?force_all_nonfp_pages_live=1">Push All Center Columns Live (Except For Front Page)</a>
</p>

<p>
  <a href="/admin/feature_page/feature_page_list.php?force_full_newswire_regeneration=1">Regenerate All Newswires</a>
</p>


<br><br><br>

