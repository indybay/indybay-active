<!-- feature-page-archived-blurb-list template -->
<div class="inner-wrap">

  <div>
    <h1>{{ TPL_ARCHIVED_BLURB_LIST_FOR|raw }} <em>{{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</em></h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<h2>
  {{ TPL_LOCAL_CURRENT_LINK|raw }} | 
  {{ TPL_LOCAL_ARCHIVED_LINK|raw }} | 
  {{ TPL_LOCAL_HIDDEN_LINK|raw }}
</h2>

<hr>

{{ TPL_NAV|raw }}

<hr>

<div class="grid grid--6-cols feature-page-list feature-page-list-archived">
 
  <div class="bg-header">
    {{ TPL_TITLE2|raw }}
  </div>
  <div class="bg-header">
    {{ TPL_NEWS_ITEM_ID|raw }}
  </div>
  <div class="bg-header">
    {{ TPL_CREATED|raw }}
  </div>
  <div class="bg-header">
    {{ TPL_MODIFIED|raw }}
  </div>
  <div class="bg-header">
    <!-- {{ TPL_UNARCHIVE|raw }} -->
  </div>
  <div class="bg-header">
    <!-- {{ TPL_STATUS|raw }} -->
  </div>

  {{ TPL_LOCAL_TABLE_ROWS|raw }}

</div><!-- END grid--7-cols feature-page-list -->

<hr>

{{ TPL_NAV|raw }}

<!-- END feature-page-archived-blurb-list template -->
