<div class="inner-wrap">

  <div>
    <h1 class="headline-text">{{ TPL_EDIT_PAGE_INFO|raw }}</h1>
  </div>

  <p><a href="/admin/feature_page/blurb_add.php">Return to {{ TPL_PAGES|raw }}</a></p>

</div><!-- END .inner-wrap -->

<hr>

<form name="category_update_form" method="post">

  <input type="hidden" name="page_id" value="{{ TPL_LOCAL_PAGE_ID|raw }}">

  <div class="grid grid--2-cols-form feature-page-def">

    <div>
      <label for="short_display_name">{{ TPL_PAGE_SHORT_NAME|raw }}</label>
    </div>
    <div>
      <input type="text" id="short_display_name" name="short_display_name" value="{{ TPL_LOCAL_SHORT_DISPLAY_NAME|raw }}">
    </div>
    
	  <div>
      <label for="long_display_name">{{ TPL_PAGE_LONG_NAME|raw }}</label>
    </div>
    <div>
   	  <input type="text" id="long_display_name" name="long_display_name" size="40" value="{{ TPL_LOCAL_LONG_DISPLAY_NAME|raw }}">
   	</div>
    
	  <div>
      <label for="items_per_newswire_section">{{ TPL_PAGE_SUMMARY_LENGTH|raw }}</label>
    </div>
    <div>
        <input type="text" id="items_per_newswire_section" name="items_per_newswire_section" value="{{ TPL_LOCAL_ITEMS_PER_NEWSWIRE_SECTION|raw }}">
    </div>
    
	  <div>
      <label for="relative_path">{{ TPL_PAGE_RELATIVE_PATH|raw }}</label>
    </div>
    <div>
      <input type="text" id="relative_path" name="relative_path" value="{{ TPL_LOCAL_RELATIVE_PATH|raw }}">
    </div>
    
    <div class="grid--row-full">
      <label for="include_in_readmore_links">{{ TPL_INCLUDE_IN_READMORE_LINKS|raw }}</label> 
      <input type="checkbox" id="include_in_readmore_links" name="include_in_readmore_links" value="1" {{ TPL_LOCAL_INCLUDE_IN_READMORE_LINKS|raw }}>
    </div>
    
	  <div>
      <label for="newswire_type_id">{{ TPL_NEWSWIRE_TYPE|raw }}</label>
    </div>
    <div>
      {{ TPL_LOCAL_SELECT_NEWSWIRE|raw }}
    </div>
    
	  <div>
      <label for="center_column_type_id">{{ TPL_CENTERCOLUMN_TYPE|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_SELECT_CENTERCOLUMN|raw }}
    </div>

    <div class="grid--row-full">
      <hr>
    </div>
    
	  <div class="grid--row-full">
      {{ TPL_EVENT_LIST_PREFERENCES|raw }}
    </div>
    
	  <div>
      <label for="event_list_type_id">{{ TPL_EVENTLIST_TYPE|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_SELECT_EVENTLIST|raw }}
    </div>
    
	  <div>
      <label for="max_events_in_highlighted_list">{{ TPL_MAX_EVENTS_IN_HIGHLIGHTED_LIST|raw }}:</label>
    </div>
    <div>
      <input type="text" id="max_events_in_highlighted_list" name="max_events_in_highlighted_list" value="{{ TPL_LOCAL_MAX_EVENTS_IN_HIGHLIGHTED_LIST|raw }}">
    </div>
    
	  <div>
      <label for="max_timespan_in_days_for_highlighted_event_list">{{ TPL_MAX_TIMESPAN_FOR_HIGHLIGHTED_EVENT_LIST|raw }}:</label>
    </div>
    <div>
      <input type="text" id="max_timespan_in_days_for_highlighted_event_list" name="max_timespan_in_days_for_highlighted_event_list" value="{{ TPL_LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_HIGHLIGHTED_EVENT_LIST|raw }}">
    </div>
    
	  <div>
      <label for="max_events_in_full_event_list">{{ TPL_MAX_EVENTS_IN_FULL_EVENT_LIST|raw }}:</label>
    </div>
    <div>
      <input type="text" id="max_events_in_full_event_list" name="max_events_in_full_event_list" value="{{ TPL_LOCAL_MAX_EVENTS_IN_FULL_EVENT_LIST|raw }}">
    </div>
    
	  <div>
      <label for="max_timespan_in_days_for_full_event_list">{{ TPL_MAX_TIMESPAN_FOR_FULL_EVENT_LIST|raw }}:</label>
    </div>
    <div>
      <input type="text" id="max_timespan_in_days_for_full_event_list" name="max_timespan_in_days_for_full_event_list" value="{{ TPL_LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_FULL_EVENT_LIST|raw }}">
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div class="grid--row-full">
      <input type="submit" name="save" value="{{ TPL_SAVE|raw }}">
    </div>

  </div><!-- END grid--2-cols-form mailing-list -->

</form>