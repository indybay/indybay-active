<!-- blurb_recategorize template -->
<div class="inner-wrap">

  <div>
    <h1>Feature Page Categories</h1>
  </div>

  <p>
    <a href="blurb_edit.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_BACK_TO_BLURB_EDIT|raw }}</a>
  </p>

  <p>
    <span class="newsitem-associations">
      {{ TPL_LOCAL_PAGE_ASSOCIATIONS|raw }}
    </span>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<h2>Update Feature Associations</h2>

<div class="grid grid--2-cols-form article-edit blurb-recategorize">

  <div>
    <label for="add_page_id">{{ TPL_ADD_AS_CURRENT_TO_PAGE|raw }}:</label>
  </div>
  <div>
    <form method="GET">
      {{ TPL_LOCAL_SELECT_NONASSOCIATED_PAGES|raw }} 
      <input name="add_current" value="{{ TPL_ADD_SUBMIT_TEXT|raw }}" type="submit"> 
      <input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
    </form>
  </div>

  <div>
    <label for="add_page_id">{{ TPL_ADD_AS_ARCHIVED_TO_PAGE|raw }}:</label>
  </div>
  <div>
    <form method="GET">
      {{ TPL_LOCAL_SELECT_NONASSOCIATED_PAGES|raw }} 
      <input name="add_archived" value="{{ TPL_ADD_SUBMIT_TEXT|raw }}" type="submit"> 
      <input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
    </form>
  </div>

  <div>
    <label for="add_page_id">{{ TPL_REMOVE_CATEGORY_ASSOCIATION|raw }}:</label>
  </div>
  <div>
    <form method="GET">
      {{ TPL_LOCAL_SELECT_ASSOCIATED_PAGES|raw }}  
      <input name="remove_association" value="{{ TPL_REMOVE_SUBMIT_TEXT|raw }}" type="submit"> 
      <input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
    </form>
  </div>

</div><!-- END grid--2-cols-form article-edit -->

<hr>

<p>
  {{ TPL_RECATEGORIZE_FEATURE_PAGE_EXPLANATIONS|raw }}
</p>

<!-- END blurb_recategorize template -->

