<!-- feature-page-blurb-list template -->
<div class="inner-wrap">
  
  <span class="strong">

    {{ TPL_VALIDATION_MESSAGES|raw }}
    {{ TPL_STATUS_MESSAGES|raw }}
    
  </span>

  <div>
    <h1>{{ TPL_CURRENT_BLURB_LIST_FOR|raw }} <em>{{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</em></h1>
  </div>

  <p>
    <span class="small">{{ TPL_LAST_PUSHED_DATE_TEXT|raw }} {{ TPL_LOCAL_LAST_PUSHED_DATE|raw }}</span>
  </p>

  <p>
    <a href="blurb_add.php?preselect_page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_BLURB_ADD|raw }}</a> | 
    <a href="feature_page_preview.php?page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_PREVIEW_FEATURE_PAGE|raw }}</a> | 
    <a href="/?preview=1&page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_TEST_PAGE|raw }}</a>
  </p>

</div><!-- END .inner-wrap -->

<hr>

<form name="order_form" action="feature_page_blurb_list.php" method="post">

  <h2>
    {{ TPL_LOCAL_CURRENT_LINK|raw }} | 
    {{ TPL_LOCAL_ARCHIVED_LINK|raw }} | 
    {{ TPL_LOCAL_HIDDEN_LINK|raw }}
  </h2>

  <hr>

  <INPUT type="submit" name="reorder" value="Reorder, Change Templates, and Archive">

  <hr>

  <div class="grid grid--6-cols feature-page-list feature-page-list-current">
   
    <div class="bg-header">
      {{ TPL_TITLE2|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_NEWS_ITEM_ID|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_CREATED|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_MODIFIED|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_ORDER_NUM|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_TEMPLATE|raw }}
    </div>
    <div class="bg-header">
      {{ TPL_STATUS|raw }}
    </div>

    {{ TPL_LOCAL_TABLE_ROWS|raw }}

  </div><!-- END grid--7-cols feature-page-list -->

  <hr>

  <INPUT type="submit" name="reorder" value="Reorder, Change Templates, and Archive">
  <input type="hidden" name="page_id" value="{{ TPL_LOCAL_PAGE_ID|raw }}">

</form>
<!-- END feature-page-blurb-list template -->
