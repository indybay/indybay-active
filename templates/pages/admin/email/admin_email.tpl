<div class="inner-wrap">
  
  <span class="strong">
    
    {{ TPL_LOCAL_SUCCESS|raw }}
    
  </span>

  <div>
    <h1>{{ TPL_MAILING_LIST_ADMIN|raw }}</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

<form name="mailinglist" method="post" action="admin_email.php">

  <div class="grid grid--2-cols-form mailing-list">

    <div class="grid--row-full">
      <input type="submit" name="generate" class="bttn--generate-email" value="Generate Indybay Email"> 
      <input type="submit" name="generate_sc" class="bttn--generate-email" value="Generate SC Email">
    </div>

    <div class="grid--row-full">
      <hr>
    </div>

    <div>
      <label for="to">{{ TPL_TO|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_TO_DROPDOWN|raw }}
    </div>

    <div>
      <label for="from">{{ TPL_FROM|raw }}:</label>
    </div>
    <div>
      {{ TPL_LOCAL_FROM_DROPDOWN|raw }}
    </div>

    <div>
      <label for="msubject">{{ TPL_SUBJECT|raw }}:</label>
    </div>
    <div>
      <input type="hidden" name="action" value="send">
      <input type="text" id="msubject" name="msubject" value="{{ TPL_LOCAL_MSUBJECT|raw }}" size="40">
    </div>

    <div class="grid--row-full">
      <label for="mbody">{{ TPL_MESSAGE_BODY|raw }}:</label>
    </div>

    <div class="grid--row-full">
      <textarea id="mbody" name="mbody" rows="16" cols="100">{{ TPL_LOCAL_TEXTAREA|raw }}</textarea>
    </div>

    <div class="grid--row-full">
      {{ TPL_CHECK_HERE|raw }}: <input type="checkbox" id="failsafe" name="failsafe" value="1">
    </div>

    <div class="grid--row-full">
      <input type="submit" name="Submit" value="{{ TPL_SEND|raw }}">
    </div>

  </div><!-- END grid--2-cols-form mailing-list -->

</form>
