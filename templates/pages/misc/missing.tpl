<!-- 404 Template -->

<div class="inner-wrap">

  <div>
    <h1>404 Error</h1>
  </div>

</div><!-- END .inner-wrap -->

<hr>

  <p><span class="strong">Indybay was unable to find the page you requested.</span>
  <br>
  Try our <a href="/">home page</a> or <a href="/search/">search</a> for what you want.</p>

<hr>

<div class="error404">
  <img src="/im/unembedded-800.jpg">
</div>

<br>
