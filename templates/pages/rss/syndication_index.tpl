<table cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <h1 class="headline-text">{{ TPL_SYNDICATION_PAGE|raw }}</h1>
    </td>
  </tr>

  <tr>
    <td><strong>Front Page</strong></td>
  </tr>
  <tr>
    <td class="bg-greyold">
      <a href="/syn/generate_rss.php?include_blurbs=1&amp;rss_version=1">{{ TPL_FEATURES|raw }}</a> (simple)
       /
      <a href="/syn/generate_rss.php?include_blurbs=1&amp;rss_version=1&amp;include_full_text=1">{{ TPL_FEATURES|raw }}</a> (with content)
      <br>
      <a href="/syn/generate_rss.php?include_posts=1&amp;news_item_status_restriction=3&amp;rss_version=1">{{ TPL_NEWSWIRE|raw }} (rdf)</a>
       /
       <a href="/syn/generate_rss.php?include_posts=1&amp;news_item_status_restriction=3">{{ TPL_NEWSWIRE|raw }} (xml)</a>
       / <a href="/syn/generate_rss.php?include_events=1">{{ TPL_CALENDAR|raw }} (rss)</a> </td>
  </tr>
  <tr>
    <td>&#160;</td>
  </tr>

  <tr>
    <td>
      <h4>{{ TPL_REGION_PAGES|raw }}</h4>
    </td>
  </tr>

  {{ TPL_LOCAL_REGION_SYN_LIST|raw }}

  <tr>
    <td>
      <h4>{{ TPL_TOPIC_PAGES|raw }}</h4>
    </td>
  </tr>

  {{ TPL_LOCAL_TOPIC_SYN_LIST|raw }}

  <tr>
    <td>
      <h4>{{ TPL_INTERNATIONAL_PAGES|raw }}</h4>
    </td>
  </tr>

  {{ TPL_LOCAL_INT_SYN_LIST|raw }}

</table>
