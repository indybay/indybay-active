readfile('{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_content.html');
include '{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_attachments.inc';
?>

<?php
if (!array_key_exists("printable",$_GET) || $_GET["printable"]==""){
	?><div class="addcomment calendar-addcomment">
		<a href="/comment.php?top_id={{ TPL_LOCAL_NEWS_ITEM_ID }}">Add Your Comments</a>
	</div>

	<div class="comment_box_longversion">

		<?php
		if (!array_key_exists("show_comments",$_GET) || $_GET["show_comments"]==""){
                  readfile('{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_commentbox.html');
		}else{
			?>
			<hr>

			<div>
				<span id="comments" name="comments">Comments</span>  
				<span class="small">(<a href="{{ TPL_LOCAL_NEWS_ITEM_ID }}.php">Hide Comments</a>)</span>
			</div>

			<?php
				include '{{ TPL_LOCAL_PATH }}{{ TPL_LOCAL_NEWS_ITEM_ID }}_comments.inc';
		}
		?>
	<?php
	}
	?>

	</div>
	<!-- END .comment_box_longversion -->

<?php
