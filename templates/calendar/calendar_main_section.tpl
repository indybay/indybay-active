<div class="grid grid--2-cols-form grid--2-cols-event">

	{{ TPL_LOCAL_HIDE_LINK_FOR_PREVIEW1|raw }}
	<div class="grid--row-full flex flex--align-center flex--justify-right small">
			<a class="small" href="/calendar/?year={{ TPL_LOCAL_DISPLAYED_YEAR|raw }}&month={{ TPL_LOCAL_DISPLAYED_MONTH|raw }}&day={{ TPL_LOCAL_DISPLAYED_DAY|raw }}" title="View other events happening this week">View events for the week of <span class="nowrap">{{ TPL_LOCAL_DISPLAYED_MONTH|raw }}/{{ TPL_LOCAL_DISPLAYED_DAY|raw }}/{{ TPL_LOCAL_DISPLAYED_YEAR|raw }}</span></a>
	</div>
	{{ TPL_LOCAL_HIDE_LINK_FOR_PREVIEW2|raw }}
      
	<div class="grid--row-full">
		{{ TPL_LOCAL_PAGE_LINKS|raw }}
	</div>

  <div class="grid--row-full heading">
		<h1>
			{{ TPL_LOCAL_TITLE1|raw }}
		</h1>
	</div>

	<div class="grid--row-full media-row">
			<div class="media">
				{{ TPL_LOCAL_MEDIA|raw }}
			</div>
	</div>

	{{ TPL_LOCAL_HIDE_LINK_FOR_PREVIEW1|raw }}
	<div class="grid--row-full flex flex--justify-right">
			<a class="flex flex--align-center small" title="iCal feed: You must have a program on your computer that supports iCal for this to work" href="/calendar/ical_single_item.php?news_item_id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">
				<img src="/im/rss-rounded.svg" class="mediaicon" alt="iCal feed icon" width="12" height="12"> 
				Import event into your personal calendar
			</a>
	</div>
	{{ TPL_LOCAL_HIDE_LINK_FOR_PREVIEW2|raw }}

	<div class="first-col">
		Date:
	</div>
	<div>
		{{ TPL_LOCAL_FORMATTED_DISPLAYED_DATE|raw }}
	</div>
			
	<div class="first-col">
		Time:
	</div>
	<div>
		<span>
			<span itemprop="startDate" content="{{ TPL_LOCAL_ISO_START_TIME|raw }}">
				{{ TPL_LOCAL_FORMATTED_DISPLAYED_START_TIME|raw }}
			</span>
			- 
			<span itemprop="endDate" content="{{ TPL_LOCAL_ISO_END_TIME|raw }}">
				{{ TPL_LOCAL_FORMATTED_DISPLAYED_END_TIME|raw }}
			</span>
		</span>
	</div>

	<div class="first-col">
		Event Type:
	</div>
	<div>
		{{ TPL_LOCAL_KEYWORD_NAME|raw }}
	</div>
		
	{{ TPL_LOCAL_CONTACT_INFO_ROWS|raw }}

	<div class="first-col location-label">
		Location Details:
	</div>
	<div class="location" itemprop="location" itemscope itemtype="http://schema.org/Place">
		<span class="event-location-summary" colspan="2" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			{{ TPL_LOCAL_SUMMARY|raw }}
		</span>
	</div>

	<div class="grid--row-full first-col event-details-label">
		<hr>
		<!-- <span>Event Details:</span> -->
	</div>
	<div class="grid--row-full">
			<div class="event-details">
				{{ TPL_LOCAL_TEXT|raw }}
			</div>
	</div>

	{% if TPL_LOCAL_RELATED_URL|length %}
		<div class="grid--row-full more-link">
			{{ TPL_LOCAL_MORE_INFO_LABEL|raw }} 
			<a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
		</div>
	{% endif %}

	<div class="grid--row-full addedtocalendar">
		<span class="small">
			Added to the calendar on <em>{{ TPL_LOCAL_CREATED|raw }}</em>
			<meta itemprop="url" content="{{ TPL_LOCAL_ARTICLE_URL|raw }}">
		</span>
	</div>

</div><!-- END grid grid--2-cols-form grid grid--2-cols-event -->

<!-- /TEMPLATE -->
