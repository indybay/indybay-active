(function ($) {
  // $(document).ready(function () {
  jQuery(function () {

    // Clears file inputs on click.
    $('body').on('click', '.clear-file-upload', function () {
      var thisInput = $(this).parent('div').siblings('.fileinput')[0];
      $(thisInput).val('');
      $(this).siblings('.previewimage').removeAttr('src');
      $(this).parent('.previewimagewrapper').css('display', 'none');
    });

    // Loads preview images or custom alternate message.
    $('body').on('change', '.fileinput', function () {
      var thisEl = $(this);
      var reader = new FileReader();
      reader.onload = function (e) {

        var dataType = reader.result.split(';')[0].split(':')[1];
        var fileType = dataType.split('/')[1].toUpperCase();

        // Conditional for MIME type.
        if (dataType.includes('image/')) {
          // Get loaded data and render thumbnail.
          $(thisEl).siblings('div').children('.previewimage').show();
          $(thisEl).siblings('div').children('.previewimage').attr('src',e.target.result);
          $(thisEl).siblings('div').find('.nopreview').css('display', 'none');
          $(thisEl).siblings('.previewimagewrapper').css('display', 'inline-block');
        } else {
          // Load non-image message.
          $(thisEl).siblings('div').children('.previewimage').hide();
          $(thisEl).siblings('div').find('.filetype').text(fileType);
          $(thisEl).siblings('div').find('.nopreview').css('display', 'inline-block');
          $(thisEl).siblings('.previewimagewrapper').css('display', 'inline-block');
        }
        if (dataType.includes('image/') || dataType === 'application/pdf') {
          $(thisEl).parent('div').siblings('.alt-tag').css('display', 'block');
        }
        else {
          $(thisEl).parent('div').siblings('.alt-tag').css('display', 'none');
          // @todo Users should be prompted to upload transcript/captions for
          // audio and video.
        }

      };
      // Read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);
    });

    // Controls publish form inputs display.
    $("#files_select_template").hide();
    $("#upload_submit_button").hide();
    $("#nonjscript_file_boxes").hide();
    $("#upload_warning").hide();
    if ($('input[name=captcha_math]').attr('type') != 'hidden') {
      $('input[name=publish]').attr('disabled', 'disabled');
      $('#file_count').attr('disabled', 'disabled');
      $('#file_count').after(' <div class="subtext"><span class="small">Select files to upload after completing the CAPTCHA question and clicking Preview below.</span></div>');
    }
    $("#file_count").on('change',function () {
          var numSelected = parseInt($("#file_count option:selected").val());
          $("nonjscript_file_boxes").hide();

                if (numSelected > 0) {
                    $("#preview_button").hide();
                } else {
                    $("#preview_button").show();
                }
                if (numSelected > 1) {
                    var filebox = $("#files_select_template").clone();
                }
                for (var i = 1; i <= 20; i++){
                    if (i <= numSelected){

                        if ($("#files_select_" + i).length == 0) {
                            var fileboxstr = "";
                              if (i == 1){var k = fileboxstr.indexOf("<!--endoffirstupload-->");
                                fileboxstr = $("#files_select_template_1").html();
                              } else {
                                  fileboxstr = filebox.html().replace(/::uploadnum::/g,i);
                              }
                              fileboxstr = "<div id='files_select_" + i + "' class='file-input-element grid grid--2-cols-form'>" + fileboxstr + "</div>";
                              $("#file_boxes2").append(fileboxstr);
                        }
                    } else {
                        if ($("#files_select_" + i).length > 0) {
                            $("#files_select_" + i).remove();
                        }
                    }
                }
          }

      );
    var numSelected = $("#file_count option:selected").val();
    if (numSelected > 0) { $("#file_count").trigger("change"); }

  });
})(jQuery);
