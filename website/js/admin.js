$(function () {
  // Article list admin - used to reset all selects or set all to "hide".
  $('.resetAll').on('click', function () {
    document.forms['order_form'].reset();
  });
  $('.setAllHide').on('click', function () {
    var i = 0;
    for (
      i = 0; i < 200 && i < document.forms['order_form'].elements.length; i++
    ) {
      var nextelement = document.forms['order_form'].elements[i];
      if (nextelement.type == 'select-one') {
        for (j = 0; j < 200 && j < nextelement.options.length; j++) {
          if (nextelement.options[j].text == 'Hidden') {
            nextelement.selectedIndex = j;
          }
        }
      }
    }
  });

  // Admin text links to set individual select's value.
  $('.admin-select-control').on('click', function () {
    targetSelect = $(this).data('select-id');
    targetValue = $(this).data('select-set');
    document.getElementById(targetSelect).value = targetValue;
  });

  // Loads blurb preview image and related messages.
  $('body').on('change', '.fileinput', function () {
    $(this)
      .parent()
      .siblings('.imagedimensions')
      .find('.filedimensioncorrect,.filedimensionwarning')
      .css('display', 'none');
    var thisEl = $(this);
    var reader = new FileReader();
    reader.onload = function (e) {
      // Get image dimensions and display related messages.
      var img = new Image();
      img.onload = function (e) {
        var imageWidth = this.width;
        var imageHeight = this.height;
        fileDimensions = imageWidth + ' x ' + imageHeight;
        $(thisEl)
          .parent()
          .siblings('.imagedimensions')
          .find('.filedimension')
          .text(fileDimensions);
        if (imageWidth != 480 || imageHeight != 320) {
          $(thisEl)
            .parent()
            .siblings('.imagedimensions')
            .find('.filedimensionwarning')
            .css('display', 'block');
        } else {
          $(thisEl)
            .parent()
            .siblings('.imagedimensions')
            .find('.filedimensioncorrect')
            .css('display', 'block');
        }
        $(thisEl).parent().siblings('.imagedimensions').css('display', 'block');
      };
      img.src = reader.result;

      // Get loaded data and display thumbnail.
      $(thisEl)
        .parent()
        .siblings('.previewimagewrapper')
        .find('.previewimage')
        .attr('src', e.target.result);
      $(thisEl)
        .parent()
        .siblings('.previewimagewrapper')
        .find('.previewimage')
        .css('display', 'block');
      $(thisEl)
        .parent()
        .siblings('.previewimagewrapper')
        .css('display', 'inline-block');
    };
    // Read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
  // Clears file input, preview, and related messages on click.
  $('body').on('click', '.clear-file-upload', function () {
    var thisInput = $(this)
      .parent()
      .siblings('.imagedimensions')
      .find('.fileinputwrapper')
      .children('.fileinput')[0];
    $(thisInput).replaceWith($(thisInput).val('').clone(true));
    $(this).siblings('.previewimage').removeAttr('src');
    $(this).parent('.previewimagewrapper').css('display', 'none');
    $(this).closest('div').find('.filedimension').text('width x height');
    $(this)
      .parent()
      .siblings('.imagedimensions,.filedimensioncorrect,.filedimensionwarning')
      .css('display', 'none');
  });

  // Confirms before leaving blurb edit page.
  if (
    $('body').hasClass('page-admin') &&
    $('.page-admin:contains("Feature Edit")').length &&
    $('.page-admin:not(:contains("Feature Page Categories"))').length
  ) {
    var clicked = false;
    $("input[type='submit']").on('click', function () {
      clicked = true;
    });
    window.onbeforeunload = function () {
      if (!clicked) {
        return 'Are you sure you want to leave this page? Are updates saved?';
      }
    };
  }

  // Disable data entry for event contact address if existing value="".
  if ($('#address').length) {
    addressVal = $('#address').val();
    if (!addressVal) {
      $('#address').prop('disabled', true);
    }
  }

  // Unknown, perhaps unused, originally in admin-header.inc.
  // function forceReload() {
  //   location.href = location.href + "?" + new Date().getTime();
  //   var lastTime = location.search.substring(1) - 0;
  //   if (new Date().getTime() - lastTime > 1000) {
  //     forceReload();
  //   }
  // }
});
