$(function () {
  // Breaks site out of iframes of other sites.
  if (top.location != location) {
    top.location.href = document.location.href;
  }

  // Dark mode.
  if (
    window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches
  ) {
    thisMode = 'dark';
  } else {
    thisMode = 'light';
  }
  // Safari bug with SVG prefers-color-scheme: https://bugs.webkit.org/show_bug.cgi?id=199134.
  isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  if (isSafari) {
    // Dark mode changes require refresh for SVG file CSS.
    document.querySelectorAll('img[src$=".svg"]').forEach(img => {
      svgSrc = img.src;
      if (thisMode == 'dark') {
        newImgSrc = svgSrc.replace('.svg', '-dark.svg');
      } else if (thisMode == 'light') {
        newImgSrc = svgSrc.replace('-dark.svg', '.svg');
      }
      img.src = newImgSrc;
    });
  }
  // Shim for Safari < 14 where addEventListener() doesn't exist.
  var prefersColorSchemeEvent = window.matchMedia(
    '(prefers-color-scheme: ' + thisMode + ')',
  );
  if (typeof prefersColorSchemeEvent.addEventListener === 'function') {
    prefersColorSchemeEvent.addEventListener('change', e => {
      location.reload();
    });
  }
  // End dark mode behaviors.

  // Calculates window dimensions.
  calcWindow = function () {
    //windowWidth = $(window).width();
    windowWidth = document.body.parentNode.clientWidth;
    windowHeight = $(window).height();
    fullHeight = document.body.clientHeight;
    $('#shadow-content-main').height(fullHeight);
    DocPosTop = $(document).scrollTop();
  };
  calcWindow();

  // Turns header nav items on and off depending on page width/resize.
  navSet = function () {
    calcWindow();
    $('div,td').removeClass('active');
    $('#headernav, .tabmenu, #shadow-content-main,#shadow-headertabs').hide();
    $('#headertab-feat').addClass('active');
    if (windowWidth > 1040) {
      $('#headernav, .rightcol, .newswire, .newswirewrap').show();
      closeCities();
    }
  };

  // Sets "back to top" box on or off according to page width/scroll.
  backTopSet = function () {
    calcWindow();
    if (windowWidth < 1040 && DocPosTop > 1000) {
      $('#backtotopbox').css('opacity', '1');
      $('#backtotopbox').fadeIn(1000);
    } else {
      $('#backtotopbox').fadeOut(500);
    }
  };

  // Window resize behavior.
  var saveWindowWidth = true;
  var savedWindowWidth;
  if ((saveWindowWidth = true)) {
    savedWindowWidth = windowWidth;
    saveWindowWidth = false;
  }
  $(window).on('resize', function () {
    windowWidth = window.innerWidth;
    if (savedWindowWidth != windowWidth) {
      navSet();
      backTopSet();
      savedWindowWidth = windowWidth;
    }
  });

  // Copies feature page center column #calendarwrapper to cell for calendar tab menu overlay.
  copyCenterCal = function () {
    $('.navbarcenter-calendar,.calendarwrapper-1')
      .clone()
      .appendTo('#calendarwrap');
    // check whether highlighted events exist for feature page.
    $('#calendarwrap:empty').html(
      '<span class="noevents">No highlighted upcoming events for this topic or region.</span>',
    );
  };
  if ($('body').hasClass('page-front') || $('body').hasClass('page-feature')) {
    copyCenterCal();
  }

  // Mobile header button/tab behavior.
  $('.closenav').on('click', function () {
    closeNav();
  });
  closeNav = function () {
    // Only allows behavior if not actively scrolling.
    if (scrollActiveFlag == 0) {
      $(
        '#headernav,.tabmenu,.newswirewrap,#shadow-content-main,#shadow-headertabs',
      ).hide();
      if (windowWidth > 1040) {
        $('#headernav, .rightcol, .newswire, .newswirewrap').show();
        closeCities();
      }
      $('div,td').removeClass('active');
      $('#headertab-feat').addClass('active');
    }
  };

  // Header buttons behavior.
  $('.headerbutton').on('click', function () {
    if ($(this).hasClass('active')) {
      closeNav();
    } else {
      closeNav();
      $(this).addClass('active');
      thisID = $(this).attr('id');
      thisMenu = '#headermenu-' + thisID.replace('headerbutton-', '');
      $(thisMenu).addClass('active');
      if (windowWidth > 1040) {
        $('#shadow-content-main,#shadow-headertabs').show();
      } else {
        $('#headernav,.tabmenu').hide();
        $('#shadow-content-main,#shadow-headertabs').show();
      }
    }
  });

  // Header menus population.
  $('#headernav .navbar').clone().appendTo('#headermenu-about');
  // $("#headermenu-about a").removeClass("navbarlink").unwrap();
  $('#headermenu-about a').unwrap();
  if ($('.module-regions').length && $('.module-topics').length) {
    $('.module-regions > div').clone().appendTo('.headermenu-category-regions');
    $('.module-topics > div').clone().appendTo('.headermenu-category-topics');
  } else {
    $('#headerbutton-category').css('display', 'none');
  }
  $('.headermenus .module-header-top, .headermenus .module-header').remove();
  $('.headermenus .thisfeaturelistitem a').each(function (i) {
    $(this).addClass('thisfeatured');
  });
  $('.headermenus .thisfeaturelistitem a, .headermenus .featurelistitem a')
    .removeClass('bottomf')
    .unwrap();

  // Header tabs behavior.
  $('.headertab-bttn').on('click', function (e) {
    $(
      '#headernav,#calendarwrap,#shadow-content-main,#shadow-headertabs',
    ).hide();
    $('.rightcol,.newswirewrap.newswire,.newswirewrap').hide();
    if ($(this).hasClass('active')) {
      $('#headernav,#calendarwrap').hide();
      $('.headertab-bttn').removeClass('active');
      $('#headertab-feat').addClass('active');
    } else {
      $('.headertab-bttn').removeClass('active');
      $(this).addClass('active');
      var thisTab = this.id;
      if (thisTab == 'headertab-feat') {
        // $('#shadow-content-main').hide();
      } else if (thisTab == 'headertab-cal') {
        $('#calendarwrap,#shadow-content-main').show();
      } else if (thisTab == 'headertab-newswire') {
        $(
          '.rightcol,.newswire,.newswirewrap-local,.newswirewrap-global,.newswirewrap-other,#shadow-content-main',
        ).show();
      }
    }
    e.preventDefault();
  });

  // Load cities list.
  $('.indy-link').on('click', function () {
    $('.cities-list').load('/cities.html');
  });

  // Toggles cities list.
  $('.indy-link').on('click', function () {
    $('.indy-link span').toggle();
    $('.cities-list').slideToggle();
  });

  closeCities = function () {
    $('.indy-link span.open').hide();
    $('.cities-list').hide();
    $('.indy-link span.closed').show();
  };

  // Adjusts sizing for media icons in blurbs/articles/events/comments.
  iconFix = function () {
    $('img[src*="/im/imc_"]').each(function () {
      $(this).addClass('mediaicon');
    });
  };
  if (
    $('body').is('.page-article, .page-event') ||
    document.title.indexOf('comment')
  ) {
    iconFix();
  }

  // Show/hide media embed code.
  $('.embed').on('click', function () {
    $(this).parents('.media-options').children('.media-embed').toggle();
  });

  // Inserts top local newswire items into blurb/features.
  insertNewswire = function () {
    if ($('body').hasClass('page-front')) {
      // places insert after first blurb on front page.
      var afterBlurbNum = 3;
    } else {
      // places insert after first blurb on feature pages.
      var afterBlurbNum = 2;
    }
    var insertWrapper =
      '<div class="module-wrappers insert-wrapper"><div class="module module-unified"></div></div>';
    var titleAdd =
      '<div class="module-header"><a href="/search/?news_item_status_restriction=3&include_events=1&include_posts=1&page_id=12" class="bottommf">Latest from the Newswire</a></div>';
    $('.pagecontent .blurbwrapper:nth-child(' + afterBlurbNum + ')').after(
      insertWrapper,
    );
    var newswireLink =
      '<div class="insert-newslink">More Newswire Stories</div>';
    // checks if local newswire exists, doesn't for international pages with newswires
    if ($('.newswirewrap-local').length) {
      $(
        '.newswirewrap-local .nooz:nth-child(3), .newswirewrap-local .nooz:nth-child(4), .newswirewrap-local .nooz:nth-child(5)',
      )
        .clone()
        .prependTo('.insert-wrapper .module.module-unified');
    } else {
      $(
        '.newswire .nooz:nth-child(3), .newswire .nooz:nth-child(4), .newswire .nooz:nth-child(5)',
      )
        .clone()
        .prependTo('.insert-wrapper .module.module-unified');
    }
    $(titleAdd).prependTo('.insert-wrapper .module.module-unified');
    $(newswireLink).appendTo('.insert-wrapper .module.module-unified');
  };
  if ($('.rightcol').length) {
    insertNewswire();
  }
  // Inserted newswire link to open local newswire overlay.
  $('.insert-newslink').on('click', function () {
    $('#headernav,#calendarwrap').hide();
    $('.rightcol,.newswire,.newswirewrap').hide();
    $('.headertab-bttn').removeClass('active');
    $('#headertab-newswire').addClass('active');
    $(
      '.rightcol,.newswire,.newswirewrap-local,.newswirewrap-global,' +
        '.newswirewrap-other,#shadow-content-main',
    ).show();
    scrollTop();
  });

  // Handles hard width post content (TABLEs/PREs/CODE) that might break width.
  overflowContentWrap = function () {
    $(
      '.article > table, .article > pre, code, .article-attachment > table, .article-attachment > pre, code, .article-comment > table, .article-comment > pre, code',
    ).wrap('<div class="overflow-content"></div>');
  };
  overflowContentWidths = function () {
    $('.overflow-content').each(function (i) {
      var thisOverflowWidth = $(this).innerWidth();
      var thisOverflowChildWidth = $(this).children()[0].scrollWidth;
      if (thisOverflowChildWidth > thisOverflowWidth) {
        $(this).addClass('overflowed-content');
      }
    });
  };
  if (
    $('body').hasClass('page-article') ||
    $('body').hasClass('page-event') ||
    $('body').hasClass('page-subscribe')
  ) {
    $.when(overflowContentWrap()).then(function (x) {
      overflowContentWidths();
    });
  }

  // Scroll back to top.
  $('#backtotopbox').on('click', function () {
    scrollTop();
  });

  // Scrolls to top of page.
  scrollTop = function () {
    scrollFlag = 1;
    $('html, body').animate(
      {scrollTop: 0},
      {
        duration: 500,
        complete: function () {
          scrollFlag = 0;
          backTopSet();
        },
      },
    );
  };

  // Behavior while scrolling.
  scrollFlag = 0;
  scrollActiveFlag = 0;
  $(window).on('scroll', function () {
    $('#backtotopbox').css('opacity', '0.2');
    setTimeout(function () {
      if (scrollFlag == 0) {
        backTopSet();
      }
    }, 3000);
    scrollActiveFlag = 1;
  });
  // Detect scroll end.
  $.fn.scrollEnd = function (callback, timeout) {
    $(this).on('scroll', function () {
      var $this = $(this);
      if ($this.data('scrollTimeout')) {
        clearTimeout($this.data('scrollTimeout'));
      }
      $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
  };
  $(window).scrollEnd(function () {
    scrollActiveFlag = 0;
  }, 250);

  // Runs after page load or later resize.
  $(window).on('resize', function () {
    backTopSet();
    closeCities();
    closeNav();
  });

  // Disallow line breaks in publish title text areas.
  // First covers typing line break, second pasting one in.
  const formTitle = document.getElementById('title1');
  if (formTitle) {
    formTitle.addEventListener('keypress', function (e) {
      if (e.key === 'Enter') {
        e.preventDefault();
      }
    });
    formTitle.addEventListener('focusout', e => {
      currVal = formTitle.value;
      formTitle.value = currVal.replace(/(\r\n|\n|\r)/gm, ' ');
    });
  }

  // Expands "clicktip" text such as on publish forms and Advanced Search.
  $('.clicktip .trigger').on('click', function () {
    $(this).hide();
    $(this).siblings('.target').addClass('open');
  });

  // Advanced search page date restriction disabling/enabling.
  if ($('body').hasClass('page-search')) {
    $('.search-range .dateset select').prop('disabled', true);
  }
  $('.page-search #search_date_type').on('change', function () {
    var dateRestictVal = $(this).val();
    if (dateRestictVal != 0) {
      $('.search-range').removeClass('disabled');
      $('.search-range .dateset select').prop('disabled', false);
    } else {
      $('.search-range').addClass('disabled');
      $('.search-range .dateset select').prop('disabled', true);
    }
  });
});
