$(function () {
  // jQuery form validation.
  //  $.validator.addMethod("checkurl", function(value, element) {
  //    return /^(www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.test(value);
  //    }, "Please enter a valid URL."
  //  );
  // // connect it to a css class
  //  $.validator.addClassRules({
  //        checkurl : { checkurl : true }
  // });
  $('#publish-form').validate({
    onkeyup: false,
    onfocusout: false,
    wrapper: 'div',
    rules: {
      title1: {
        minlength: 5,
        required: true,
      },
      displayed_author_name: {
        required: true,
      },
      summary: {
        maxlength: 65535,
        minlength: 3,
        required: true,
      },
      text: {
        maxlength: 65535,
        required: function () {
          return $('#file_count').val() < 1;
        },
      },
    },
    messages: {
      title1: 'Please specify a title at least 5 characters long.',
      displayed_author_name: {
        required: 'Attribution is required. Can be pseudonymous.',
      },
      summary: 'This field is required.',
      text: 'Please include some text for this post.',
    },
  });

  // Warning to prevent all-caps titles and authors.
  capitalizationWarning = function () {
    var capDiv = '<div class="caps-warning"></div>';
    $('textarea#title1,input#displayed_author_name').after(capDiv);
    $('#title1,#displayed_author_name').keyup(function () {
      var inputVal = $(this).val().trim();
      var wordsArr = inputVal.split(' ');
      var inputWordCount = inputVal.split(' ').length;
      var inputCharCount = inputVal.replace(/\s/g, '').length;
      var capsCharCount = inputVal.replace(/[^A-Z]/g, '').length;
      var totalCapsPercent = Math.round((capsCharCount / inputCharCount) * 100);
      var allCapsWordCount = 0;
      for (var i = 0; i < wordsArr.length; i++) {
        var thisWord = wordsArr[i];
        var thisWordCharCount = thisWord.length;
        var thisWordCapsCharCount = thisWord.replace(/[^A-Z]/g, '').length;
        if (
          thisWordCharCount > 1 &&
          thisWordCharCount == thisWordCapsCharCount
        ) {
          allCapsWordCount++;
        }
      }
      var word = 'word';
      if (allCapsWordCount > 1) {
        word = 'words';
      }
      var overCountText =
        'The text you entered contains at least ' +
        allCapsWordCount +
        ' ALL CAPS ' +
        word +
        '. If the use of uppercase letters is acronym- or abbreviation-related, that is fine. In general, be sure to use <a href="https://en.wikipedia.org/wiki/Title_case" target="_blank">Title Case</a> here or the entire post may be hidden.';
      var overPercentText =
        'The text you entered is ' +
        totalCapsPercent +
        '% capital letters.  In general, be sure to use Title Case here or the post may be hidden.';
      var overCountPercentText =
        'The text you entered contains at least ' +
        allCapsWordCount +
        ' ALL CAPS ' +
        word +
        ' and is ' +
        totalCapsPercent +
        '% capital letters. If the use of uppercase letters is acronym- or abbreviation-related, that is fine.  In general, be sure to use <a href="https://en.wikipedia.org/wiki/Title_case" target="_blank">Title Case</a> here or the entire post may be hidden.';
      if (allCapsWordCount >= 1 && totalCapsPercent >= 30) {
        $(this).siblings('.caps-warning').html(overCountPercentText);
      } else if (allCapsWordCount >= 1) {
        $(this).siblings('.caps-warning').html(overCountText);
      } else if (totalCapsPercent >= 30) {
        $(this).siblings('.caps-warning').html(overPercentText);
      } else {
        $(this).siblings('.caps-warning').text('');
      }
    });
  };
  capitalizationWarning();

  // Warning to prevent mistaken choice of California as Event Region.
  californiaEventWarning = function () {
    $('#region_id').change(function () {
      var regionVal = $(this).val();
      switch (regionVal) {
        case '42':
          alert(
            'Selecting "California" as the Event Region means that this ' +
              'event will not be held in one of the other Northern California ' +
              "options here, or it's happening at multiple locations " +
              'statewide. If the event is in San Francisco or the East Bay, ' +
              'for example, select those instead.',
          );
          break;
      }
    });
  };
  if ($('body').hasClass('page-event-add')) {
    californiaEventWarning();
  }

  // Confirms before leaving publish pages.
  if (
    $('body').hasClass('page-publish') &&
    $('.pagecontent:not(:contains("Thanks for Publishing"))').length
  ) {
    var clicked = false;
    $('input[type="submit"]').on('click', function () {
      clicked = true;
    });
    window.onbeforeunload = function () {
      if (!clicked) {
        return 'Are you sure you want to leave this page? Are updates saved?';
      }
    };
  }
});
