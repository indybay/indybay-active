$(function () {
  $('a.livecast-trigger').click(function () {
    $('span.livecast-target').load('/js/livecast.html');
    $('a.livecast-trigger').toggle('slow');
    return false;
  });
});
