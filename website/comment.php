<?php

/**
 * @file
 * This file is used for displaying, adding and confirming.
 */

// The adding of commnets to posts (and other comments)
include_once '../classes/config/indybay.cfg';

$GLOBALS['page_title'] = 'Add Comment';
$GLOBALS['body_class'] = 'page-comment';
$GLOBALS['page_display'] = 'f';
$GLOBALS['jquery'][] = 'validate';
$GLOBALS['js'] = ['publish', 'fileupload'];

include INCLUDE_PATH . '/common/content-header.inc';

use Indybay\Page;

$page = new Page('comment', 'article');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}

include INCLUDE_PATH . '/common/footer.inc';
