<?php

/**
 * @file
 * Breaking news (obsolete).
 */

include_once '../../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('breaking_index', 'breaking');
$page->buildPage();

include INCLUDE_PATH . '/common/content-header.inc';

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  echo $page->getHtml();
}
include INCLUDE_PATH . '/common/index_bottom.inc';
// Include INCLUDE_PATH . '/common/center_right.inc';.
include INCLUDE_PATH . '/common/footer.inc';
