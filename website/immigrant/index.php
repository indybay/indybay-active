<?php

/**
 * @file
 * Immigration page.
 */

$GLOBALS['page_title'] = 'Immigrant Rights News';
$GLOBALS['page_ids'] = [56];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
