<?php

/**
 * @file
 * This page shows a list of calendars for the archives.
 */

include_once '../../classes/config/indybay.cfg';
use Indybay\Page;

$page = new Page('archived_blurb_list', 'feature_page');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  $GLOBALS['body_class'] = 'page-feature-archive';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
