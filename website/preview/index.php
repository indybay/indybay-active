<?php

/**
 * @file
 * Preview redirect page.
 */

header('Location: /?preview=1');
exit();
