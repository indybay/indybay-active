<?php

/**
 * @file
 * Referrer forbidden page.
 */
?>
<!DOCTYPE html>
<html><head><meta charset="utf-8"><title>403 Error: Referer URL Forbidden</title></head><body>

<div style="width: 468; margin: 0 auto;"><a href="/"><img style="font-family: MS Sans 
Serif, Verdana, Arial, sans-serif; text-align: center"
src="/images/imc_banner.gif" width="468" height="60"></a>

<p style="font-family: MS Sans Serif, Verdana, Arial, sans-serif;
<?php $referer = !empty($_SERVER['HTTP_REFERER']) ? htmlspecialchars($_SERVER['HTTP_REFERER']) : ''; ?>
text-align: center">The referer URL <small>&lt;<a href="<?php echo $referer; ?>"><?php
echo $referer; ?></a>&gt;</small> from which you are attempting to access this file
is forbidden.  Please link to the article URL, not the URL of an attached
file.</p>

<p style="font-family: MS Sans Serif, Verdana, Arial, sans-serif;
text-align: center"><strong>Why are we disallowing some referer URLs?</strong> The
bandwidth required to serve many gigabytes of realvideos and jpegs each
day costs us money!  Until you send us some, we simply can't afford to
provide free webhosting for the thousands of videos, images and audio
files uploaded to our open-publishing site.</p>

<h4 style="font-family: MS Sans Serif, Verdana, Arial, sans-serif;
text-align: center; color: #cc0000">Support independent media :: <a href="/process/donate.php">Donate!</a></h4>

<h5 style="font-family: MS Sans Serif, Verdana, Arial, sans-serif;
text-align: center"><a href="/">&lt;&lt; Return to the front page</a></h5>

<address style="font-family: MS Sans Serif, Verdana, Arial, sans-serif;
text-align: center">Questions? <a href="mailto:indybay[at]lists.riseup.net">indybay at lists.riseup.net</a></address>

</div>

</body></html>
