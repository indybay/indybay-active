<?php

/**
 * @file
 * California page.
 */

$GLOBALS['page_title'] = 'California News';
$GLOBALS['page_ids'] = [42];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
