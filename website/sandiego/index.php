<?php

/**
 * @file
 * San Diego page.
 */

$GLOBALS['page_title'] = 'San Diego Indymedia';
$GLOBALS['page_ids'] = [61];
$GLOBALS['page_id'] = 61;
include_once '../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/common/feature_page_includes.inc';
