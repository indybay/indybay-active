<?php

/**
 * @file
 * Education page.
 */

$GLOBALS['page_title'] = 'Education & Student Activism News';
$GLOBALS['page_ids'] = [30];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
