<?php

/**
 * @file
 * Spanish page.
 */

$GLOBALS['page_title'] = 'Noticias En Español';
$GLOBALS['page_ids'] = [33];

include_once '../../classes/config/indybay.cfg';
$GLOBALS['lang'] = 'es';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
