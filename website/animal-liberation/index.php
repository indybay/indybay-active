<?php

/**
 * @file
 * Animal liberation page.
 */

$GLOBALS['page_title'] = 'Animal Liberation News';
$GLOBALS['page_ids'] = [58];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
