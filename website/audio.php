<?php

/**
 * @file
 * This file is used for displaying a video element.
 */

include_once '../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('audio', 'article');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
