<?php

/**
 * @file
 * LGBTQ page.
 */

$GLOBALS['page_title'] = 'LGBT/Queer News';
$GLOBALS['page_ids'] = [29];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
