<?php

/**
 * @file
 * Santa Cruz page.
 */

$GLOBALS['page_title'] = 'Santa Cruz Indymedia';
$GLOBALS['page_ids'] = [60];
// $GLOBALS['page_id'] = 60;
include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
