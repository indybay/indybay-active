<?php

/**
 * @file
 * Index.php is the main page on the indymedia site.
 */

// It displays a cached center column and a cached summary list
// of the latest posts.
include_once '../classes/config/indybay.cfg';

$GLOBALS['is_front_page'] = 1;
$GLOBALS['body_class']    = 'page-front';
$GLOBALS['page_ids']      = [12];

include_once INCLUDE_PATH . '/common/content-header.inc';
include_once INCLUDE_PATH . '/common/index_center.inc';
include_once INCLUDE_PATH . '/common/index_bottom.inc';
include_once INCLUDE_PATH . '/feature_page/feature_list.inc';
include_once INCLUDE_PATH . '/common/center_right.inc';
include_once INCLUDE_PATH . '/common/footer.inc';
