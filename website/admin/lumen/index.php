<?php

/**
 * @file
 * This page displays the main list of admin options.
 */

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('lumen', 'admin/misc');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  include INCLUDE_PATH . '/admin/admin-header.inc';
  $page->buildPage();
  echo $page->getHtml();
  include INCLUDE_PATH . '/admin/admin-footer.inc';
}
