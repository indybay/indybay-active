<?php

/**
 * @file
 * This is the login page.
 */

$GLOBALS['display'] = TRUE;
$GLOBALS['body_class'] = 'page-login';

include_once '../../../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('authenticate_display_logon', 'login');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  include INCLUDE_PATH . '/common/content-header.inc';
  $page->buildPage();
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
