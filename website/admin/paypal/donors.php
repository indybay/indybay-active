<?php

/**
 * @file
 * This page displays the main list of admin options.
 */

use Indybay\DB\DB;

define('DB_DATABASE', 'radio');
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/admin/admin-header.inc';
?>
<div class="inner-wrap">

  <div>
    <h1>Recent Donors</h1>
  </div>

  <p>
    <a href="index.php">Paypal Donations</a><!-- | 
    <a href="labels.php">Print donor mailing labels</a> -->
  </p>

</div><!-- END .inner-wrap -->

<hr>

<div class="grid grid--3-cols donations donors">

  <div class="grid--row-full">
    
    <form>

      <div class="grid grid--2-cols-form donations-select">
        <div>
          <select name="seconds">
            <?php
            $_GET['seconds'] = empty($_GET['seconds']) ? 2592000 * 12 : intval($_GET['seconds']);
            foreach (range(2592000, 24 * 2592000, 2592000) as $seconds) {
              echo '<option value="', $seconds, '"';
              if ($_GET['seconds'] === $seconds) {
                echo ' selected="selected"';
              }
              echo '>Past ', ($seconds / 24 / 60 / 60), ' days (since ', date('Y-m-d H:i:s  T', time() - $seconds), ')</option>';
            }
            ?>
          </select>
        </div>
        <div>
          <input type="submit" value="Go!">
        </div>
      </div><!-- END grid--2-cols donations select -->

    </form>

  </div><!-- END grid--row-full -->
  
  <div class="bg-header">Gross</div>
  <div class="bg-header">Payer</div>
  <div class="bg-header">Payer ID</div>

  <?php
  $query = "SELECT payer_email, last_name, first_name, payer_id, SUM(mc_gross) AS lilsum
    FROM chaching_paypal_ipns
    WHERE timestamp >= UNIX_TIMESTAMP() - {$_GET['seconds']}
    AND payment_status = 'Completed'
    GROUP BY payer_email, last_name, first_name, payer_id
    ORDER BY SUM(mc_gross) DESC, payer_email, last_name, first_name, payer_id";

  $db_obj = new DB();
  $result = $db_obj->query($query);
  foreach ($result as $row) {
    echo '
  <div class="attribute bg-grey" data-attr="Gross:">', htmlspecialchars($row['lilsum']), '</div>
  <div class="attribute bg-grey" data-attr="Payer:"><span>', htmlspecialchars($row['first_name']), ' ', htmlspecialchars($row['last_name']), ' <span class="nowrap">&lt;<a href="mailto:', htmlspecialchars($row['payer_email']), '">', htmlspecialchars($row['payer_email']), '</a>&gt;</span></span></div>
  <div class="attribute bg-grey" data-attr="Payer ID:">', htmlspecialchars($row['payer_id']), '</div>';
  }
  ?>

</div><!-- END grid--3-cols donations donors -->

<?php include INCLUDE_PATH . '/admin/admin-footer.inc';
