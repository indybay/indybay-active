<?php

/**
 * @file
 * This page displays the main list of admin options.
 */

use Indybay\DB\DB;

define('DB_DATABASE', 'radio');
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/admin/admin-header.inc';
?>
<div class="inner-wrap">

<div><h1>Paypal Donations</h1>
  </div>

<p>
  <a href="donors.php">Recent Donors</a><!-- | 
  <a href="labels.php">Print donor mailing labels</a> -->
</p>

</div><!-- END .inner-wrap -->

<hr>

<div class="grid grid--2-cols-form donations paypal">

  <?php
  $query = "SELECT txn.*,
    subscr.subscr_date AS subscr_subscr_date,
    subscr.amount3 AS subscr_amount3,
    subscr.period3 AS subscr_period3,
    old_subscr.subscr_date AS old_subscr_subscr_date,
    old_subscr.amount3 AS old_subscr_amount3,
    old_subscr.period3 AS old_subscr_period3
    FROM chaching_paypal_ipns txn
    LEFT JOIN chaching_paypal_ipns subscr ON txn.subscr_id = subscr.subscr_id AND subscr.txn_type = 'subscr_signup'
    LEFT JOIN chaching_paypal_ipns old_subscr ON txn.old_subscr_id = old_subscr.subscr_id AND old_subscr.txn_type = 'subscr_signup'
    ORDER BY id DESC";
  $db_obj = new DB();
  $result = $db_obj->query($query);

  foreach ($result as $row) : ?>

  <div class="row-<?php echo $row['id']; ?> col1 bg-grey">

    <div>
      <?php if ($row['txn_type']) :
        echo htmlspecialchars($row['txn_type']);
      endif;
      if ($row['payment_status']) :
        ?>: <?php echo htmlspecialchars($row['payment_status']);
      endif; ?>
    </div>

    <div>
      <?php echo date_format(date_create($row['payment_date'] ?: "@{$row['timestamp']}"), 'Y-m-d H:i:s T'); ?>
    </div>

    <div>
      <span>
        <?php if ($row['first_name']) : ?>
          <?php echo htmlspecialchars($row['first_name']); ?>
        <?php endif; ?>
        <?php if ($row['last_name']) : ?>
          <?php echo htmlspecialchars($row['last_name']); ?>
        <?php endif; ?>
        <?php if ($row['payer_business_name']) : ?>
          / <?php echo htmlspecialchars($row['payer_business_name']); ?>
        <?php endif; ?>

        <?php if ($row['payer_email']) : ?>
          <span class="nowrap">&lt;<a href="mailto:<?php echo htmlspecialchars($row['payer_email']); ?>"><?php echo htmlspecialchars($row['payer_email']); ?></a>&gt;</span>
        <?php endif; ?>
      </span>
    </div>

    <div>
      <?php if ($row['mc_gross']) : ?>
        $<?php echo $row['mc_gross']; ?>
      <?php endif; ?>
      <?php if ($row['mc_currency']) : ?>
        <?php echo htmlspecialchars($row['mc_currency']); ?>
      <?php endif; ?>
      <?php if ($row['mc_fee']) : ?>
        (fee: $<?php echo $row['mc_fee']; ?>)
      <?php endif; ?>
    </div>

    <?php if ($row['item_name']) : ?>

      <div>
        <span>Item: <?php echo htmlspecialchars($row['item_name']); ?></span>
      </div>

    <?php endif; ?>

    <?php if ($row['memo']) : ?>

      <div>
        Memo: <?php echo htmlspecialchars($row['memo']); ?>
      </div>

    <?php endif; ?>

  </div><!-- END first grid item -->

  <div class="row-<?php echo $row['id']; ?> col2 bg-grey">

    <?php if ($row['time_created']) : ?>

      <div>Recurring info:<br>
        <?php echo date_format(date_create($row['time_created']), 'Y-m-d H:i:s T'); ?><br>
        $<?php echo $row['amount_per_cycle']; ?> / <?php echo htmlspecialchars($row['payment_cycle']); ?>
      </div>

    <?php endif; ?>

    <?php if ($row['subscr_date']) : ?>

      <div>Subscription info:<br>
        <?php echo date_format(date_create($row['subscr_date']), 'Y-m-d H:i:s T'); ?><br>
        $<?php echo $row['amount3']; ?> / <?php echo htmlspecialchars($row['period3']); ?>
      </div>

    <?php elseif ($row['subscr_subscr_date']) : ?>

      <div>Subscription info:<br>
        <?php echo date_format(date_create($row['subscr_subscr_date']), 'Y-m-d H:i:s T'); ?><br>
        $<?php echo $row['subscr_amount3']; ?> / <?php echo htmlspecialchars($row['subscr_period3']); ?>
      </div>

    <?php elseif ($row['old_subscr_subscr_date']) : ?>

      <div>Subscription info:<br>
        <?php echo date_format(date_create($row['old_subscr_subscr_date']), 'Y-m-d H:i:s T'); ?><br>
        $<?php echo $row['old_subscr_amount3']; ?> / <?php echo htmlspecialchars($row['old_subscr_period3']); ?>
      </div>

    <?php endif; ?>

    <?php if ($row['address_name']) : ?>

      <div>
        Mailing address: <br>
        <?php echo htmlspecialchars($row['address_name']); ?><br>
        <?php if ($row['address_street']) : ?>
          <?php echo htmlspecialchars($row['address_street']); ?> <br>
        <?php endif; ?>
        <?php if ($row['address_city']) : ?>
          <?php echo htmlspecialchars($row['address_city']); ?>,
        <?php endif; ?>
        <?php if ($row['address_state']) : ?>
          <?php echo htmlspecialchars($row['address_state']); ?>
        <?php endif; ?>
        <?php if ($row['address_zip']) : ?>
          <?php echo htmlspecialchars($row['address_zip']); ?>
        <?php endif; ?>
        <?php if ($row['address_country_code']) : ?>
          <br> <?php echo htmlspecialchars($row['address_country_code']); ?>
        <?php endif; ?>
      </div>

    <?php endif; ?>

  </div><!-- END second grid item -->

  <?php endforeach; ?>

</div><!-- END grid--2-cols-form donations paypal -->

<?php include INCLUDE_PATH . '/admin/admin-footer.inc';
