<?php

/**
 * @file
 * Category_display_list.php displays a list of the features in the DB.
 */

// With drilldowns to lists of stories for each feature.
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('feature_page_list', 'admin/feature_page');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
