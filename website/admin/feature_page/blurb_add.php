<?php

/**
 * @file
 * This is the page that handles content publishing from users.
 */

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';

include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('blurb_add', 'admin/feature_page');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
