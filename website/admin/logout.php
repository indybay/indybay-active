<?php

/**
 * @file
 * This page displays the main list of admin options.
 */

include_once '../../classes/config/indybay.cfg';
if (!isset($_SESSION)) {
  ini_set('session.save_path', SESSIONS_PATH);
  session_start();
}
session_destroy();
include_once INCLUDE_PATH . '/admin/admin-header.inc';
header('HTTP/1.0 301 Moved Permanently');
header('Location: ' . SERVER_URL . '/admin/');
