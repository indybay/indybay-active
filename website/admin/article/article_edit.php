<?php

/**
 * @file
 * This page is for editing articles.
 */

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';

include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('article_edit', 'admin/article');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
