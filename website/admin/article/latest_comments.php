<?php

/**
 * @file
 * Displays a list of features for a given category.
 */

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate(DATE_RFC7231));
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('latest_comments', 'admin/article');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml('article_display_list');
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
