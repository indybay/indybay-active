<?php

/**
 * @file
 * Environment page.
 */

$GLOBALS['page_title'] = 'Environment & Forest Defense News';
$GLOBALS['page_ids'] = [14];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
