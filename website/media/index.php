<?php

/**
 * @file
 * Media page.
 */

$GLOBALS['page_title'] = 'Media Activism & Independent Media News';
$GLOBALS['page_ids'] = [32];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
