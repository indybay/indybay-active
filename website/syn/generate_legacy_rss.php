<?php

/**
 * @file
 * This is the page that handles content publishing from users.
 */

include_once '../../classes/config/indybay.cfg';

use Indybay\Syndication\LegacyRssGenerator;

$legacy_rss_generator = new LegacyRssGenerator();

if (isset($_GET['old_url'])) {
  $old_file = urldecode($_GET['old_url']);
  $i = strrpos($old_file, '/');
  $old_file = substr($old_file, $i + 1);
  header('Content-Type: application/rss+xml; charset=UTF-8');
  echo $legacy_rss_generator->generateRss($old_file);
}
else {
  header('Location: ' . SERVER_URL . '/syn/generate_rss.php?rss_version=' . intval($_GET['rss_version']), TRUE, 301);
}
