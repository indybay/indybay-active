<?php

/**
 * @file
 * Renders an embeddable news feed via JavaScript.
 */

header('Content-type: application/javascript; charset: UTF-8');
include_once '../../classes/config/indybay.cfg';
use Indybay\Syndication\RSSAggregator;

$aggregator = new RSSAggregator();

$sorted_stories = $aggregator->renderForJavascript();

if (is_array($sorted_stories)) {
  foreach ($sorted_stories as $li) {
    ?>document.write('<li class="indybay_newsfeed"><a class="indybay_newsfeed" target="_blank" href="<?php echo $li['link']; ?>"><?php
// SimplePie leaves &amp; encoded.
echo htmlspecialchars(htmlspecialchars_decode($li['title']));
?></a></li>');
  <?php }
}
