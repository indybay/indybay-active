<?php

/**
 * @file
 * This is the page that handles content publishing from users.
 */

include_once '../../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('mpublish', 'article');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
