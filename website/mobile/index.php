<?php

/**
 * @file
 * Redirects to front page.
 */

header('Location: /', TRUE, 301);
