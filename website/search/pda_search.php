<?php

/**
 * @file
 * Redirect to search page.
 */

header('Location: /search/?' . http_build_query($_GET), TRUE, 301);
