<?php

/**
 * @file
 * Missing page handler.
 */

include_once '../classes/config/indybay.cfg';

use Indybay\Cache\ArticleCache;
use Indybay\Page;
use Indybay\Routing;

if ($route = Routing::getCurrentRoute()) {
  if (!file_exists($route['cache_file'])) {
    $article_cache_class = new ArticleCache();
    $article_cache_class->cacheEverythingForArticle($route['news_item_id']);
  }
  http_response_code(200);
  $_GET['printable'] = TRUE;
  require $route['cache_file'];
  return;
}

$page = new Page('missing', 'misc');

if ($page->getError()) {
  echo 'Fatal error: ', $page->getError();
}
else {
  $page->buildPage();
  // Page should have executed.
  $GLOBALS['page_display'] = 'f';
  $GLOBALS['body_class'] = 'page-404';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
