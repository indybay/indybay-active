(function ($) {
  $(document).ready(function () {

    // This file is for inserting special notices w/o touching other code.
    setNotice = function () {
      var queryString = $('#css-js-query-string').attr('data-css-js-query-string');
      $('#notices').load('/notice/notice.html' + queryString, function () {

        //Previously loaded notice to side of main site regarding responsivness.
        //$('#notices #notice-responsive').prependTo('#siteinner');

        var url = $(location).attr('href');

        setNoticeColors();
        $('#notice-donate').css('background-color', '#' + colBack);
        $('#notice-donate').hover(function () {
          $(this).css('background-color', '#' + colBackHov);
          }, function () {
          $(this).css('background-color', '#' + colBack);
        });
        $('#notice-donate').css('border-color', '#' + colBord);

        // Determine whether to display full donate banner or just progress bar.
        if ((url.indexOf('16659041') > -1) || (url.indexOf('18775298') > -1)) {
          // if donate blurb
          $('.pagecontent').css('padding-top', '1em');
          $('.label-page-wrapper,.feature-categories,.feature-blurb-date').hide();
          $('.blurbtext h3:first, .blurbtext p:first').css('display','block');
          $('.blurbtext h3:first, .blurbtext p:first').css('clear','both');
          $('.blurbtext h3:first, .blurbtext p:first').css('line-height','1.4');
          $('#donate-progress').prependTo('.blurbtext');
        } else {
          // else every other page
          $('#notice-donate-link').prependTo('.pagecontent');
        }

              // removes duplicate donate banners that appear sometimes
              // when jumping back and forth between pages/sites
              // $('[id]').each(function () {
              //     $('[id="' + this.id + '"]:gt(0)').remove();
              // });

        // Set a 5-minute epoch for donation updates.
        var epoch = Math.round(Date.now() / 300000);
        $.getJSON('/notice/donate.json', 'f' + epoch, function (data) {
          calcDonateProgress(data);
        });
      });
    };

    calcDonateProgress = function (data) {
      $('#donate-fundraiser-raised').text(data.total.formatted + ' of');
      $('#donate-fundraiser-sought').text(data.goal.formatted);
      var donationRaisedPercent = (data.total.amount / data.goal.amount) * 100;
      if (donationRaisedPercent < 2) {
        donationRaisedPercent = 2;
      }
      if (donationRaisedPercent > 100) {
        donationRaisedPercent = 100;
      }
      $('#donate-progress-bar-color').animate({
        width: donationRaisedPercent + '%'
      }, 1500);
    };

    setNoticeColors = function () {
      var arrColor = [
        ['fdc98b', 'fee4c4', '7a7a7a'],
        ['cbddcf', 'd8e6db', '4f65ea'],
        ['a5c0fe', 'bdd1fd', '7b628f'],
        ['e8c5c5', 'ecd2d2', '757575'],
        ['f3caf3', 'f1d6f1', 'a3a085'],
        ['f7f0a7', 'f9f5c8', 'b88140']
      ];
      var randInd = Math.floor(Math.random() * arrColor.length);
      colBack = arrColor[randInd][0];
      colBackHov = arrColor[randInd][1];
      colBord = arrColor[randInd][2];
    };

    // runs after page content loads
    setNotice(); // Comment or uncomment this line to display special notices.

  });
})(jQuery);
