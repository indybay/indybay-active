#!/bin/sh
set -e
cp -a node_modules/@fontsource/noticia-text/files/noticia-text-latin-[47]00-normal.woff* website/themes/files
cp -a node_modules/@fontsource/noticia-text/latin.css website/themes/noticia-text.css
cp -a node_modules/jquery/dist/jquery.min.js website/js
cp -a node_modules/jquery-validation/dist/jquery.validate.min.js website/js
mkdir -p website/js/jquery.ui
cp -a node_modules/jquery-ui-dist/jquery-ui.min.* node_modules/jquery-ui-dist/images website/js/jquery.ui
./node_modules/.bin/sass website/themes/bundle.scss website/themes/bundle.css
