<?php

/**
 * @file
 * Event list for feature pages.
 */
?>
<div class="module module-unified module-unified-color" name="calendar">
  <div class="module-header">
    <a title="iCal feed"
      href="/calendar/<?php
      if ($GLOBALS['page_ids'][0] != 12) {
        echo '?page_id=' . $GLOBALS['page_ids'][0];
      }?>#ical"
      title="iCal feed"><img
        align="right"
        src="/im/rss-rounded.svg"
        class="mediaicon"
        alt="iCal feed" 
        width="12" 
        height="12"></a>

    From the
    <a href="/calendar/<?php
    if ($GLOBALS['page_ids'][0] != 12) {
      echo '?page_id=' . $GLOBALS['page_ids'][0];
    }?>"
      name="calendar">Calendar</a>
  </div>
  <div class="module-inner">
    <?php
    use Indybay\Cache\EventListCache;

    $event_list_cache = new EventListCache();
    echo $event_list_cache->loadCachedEventsForPage($GLOBALS['page_id']); ?>
  </div>
</div>
<!-- end .module.module-unified -->
