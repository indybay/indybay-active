<?php

/**
 * @file
 * Article header.
 */

if (array_key_exists('page_categories', $GLOBALS)) {
  foreach ($GLOBALS['page_categories'] as $cats) {
    $GLOBALS['catnum'][] = $cats[0];
  }
}
$GLOBALS['rssautodiscover'] = 'http://www.indybay.org/syn/newswire.rss';
include INCLUDE_PATH . '/common/content-header.inc';
?>
<?php if (!empty($_SESSION['session_is_editor'])) : ?>
  <div class="adminlinks">
    <!-- <a href="/admin/">admin</a> -  -->
    <a href="/admin/article/article_edit.php?id=<?php echo $GLOBALS['news_item_id'];?>">Edit Page</a>
    <span class="edit-comment-link"> -
      <a href="/admin/article/index.php?parent_item_id=<?php echo $GLOBALS['news_item_id'];?>&amp;force_parent_id_search=1">Edit Comments</a>
    </span>
  </div>
<?php endif;
