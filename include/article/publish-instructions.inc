<!-- newswire publishing header -->

<div class="inner-wrap">

  <div>
    <h1>Publish to the Newswire</h1>
  </div>

  <p>Use the <a
  href="/calendar/event_add.php?page_id=TPL_LOCAL_PAGE_ID">Add an Event</a> form to publish announcements to the <a
  href="/calendar/?page_id=TPL_LOCAL_PAGE_ID">calendar</a>. This is for newswire posts.</p>

  <div class="clicktip">

    <p class="trigger" title="click to learn more">Never published to the newswire before? Read the publishing guidelines!</p>

    <div class="target">

      <p>Want to show off your photos of an event? Got audio or video that people just
      have to see? This is the place to put it. We want to hear your story. Use any format you want, from journalistic, to academic, to
      personal account.</p>

      <p><span class="strong">WHAT TO PUBLISH:</span> Please use this form to contribute new stories and ideas. We think comments
      belong with the story being discussed. So to have your say in response to a story on the site, please use the &quot;add your
      comments&quot; link at the bottom of each story. Instead of reposting corporate media articles to the site, try posting your own
      critique or summary, short quotes, and links.</p>

      <p><span class="strong">MULTIMEDIA:</span>
      If your image exceeds 800 pixels in width or 500 pixels in height, it
      will be resized to these maximums.  We suggest that you crop and resize your images yourself to obtain the
      results you're looking for; a variety of free (libre) open-source software is available for this purpose.
      For playability of media files on browsers and mobile devices,
      videos will be automatically converted to MP4 with H.264 encoding.
      For audio, upload MP3s.</p>

      <p><span class="strong">EDITORIAL POLICY:</span>
      After stories have been published, they may be edited or hidden by the
      collective running this site. We generally only fix obvious mistakes, such
      as typos, or try to improve the formatting. Please read our full
      <span class="strong"><a href="/editorial-policy">editorial
      policy</a></span>.</p>

      <p><span class="strong">LEGALESE:</span>
      Unless otherwise stated, all content contributed to this site is free for
      non-commercial reuse, reprint or rebroadcast. If you want to specify different conditions, please do so in the summary, including any
      &copy; copyright (or copyleft) statement. Please read our <a href="/privacy">privacy policy</a>, <a href="/copyright">copyright policy</a>, and <a
      href="/disclaimer">disclaimer</a> statements before continuing.</p>

    </div><!-- END .target -->

  </div><!-- END .clicktip -->

</div><!-- END .inner-wrap -->

<!-- end newswire publishing header -->
