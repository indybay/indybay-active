<!-- event publishing header -->

<div class="inner-wrap">

  <div>
    <h1>Publish to the Calendar</h1>
  </div>

  <div class="clicktip">

    <p class="trigger" title="click to learn more">Never published to the calendar before? Read the publishing guidelines!</p>

    <div class="target">

      <p><span class="strong">EVENT PUBLISHING POLICY:</span>
      Please use this form to contribute
      events to the Indybay calendar. Events should be political in nature,
      such as social and environmental justice events, and conform with
      Indybay's <a href="/newsitems/2003/12/08/16643971.php">principles of
      unity</a>. It is preferable if events are free, "no one turned away for
      lack of funds," or at most require a small fee or donation. If there is
      a charge for admission, the price should be stated in the text of the
      post. Do not UPPERCASE the event title. Calendar submissions that do not
      meet these criteria may be hidden.</p>

      <p><span class="strong">EVENT EDITORIAL PROCESS:</span>
      After events have been published,
      they can be edited, linked or hidden by the collective running this
      site. Once we notice an event, we can highlight it. As the event
      approaches, highlighted events will appear as a highlighted link at the top of
      the pages associated with the region and topic of the event.
      To request corrections or updates, <a
      href="/newsitems/2003/12/15/16658991.php">contact the editorial
      collective</a>.</p>

    </div><!-- END .target -->

  </div><!-- END .clicktip -->

</div><!-- END .inner-wrap -->

<!-- end event publishing header -->
