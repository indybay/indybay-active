<?php

/**
 * @file
 * Renders the admin page header.
 */

use Indybay\Date;
use Indybay\Translate;

$sftr = new Translate();

$date = new Date();
$date->setTimeZone();
$fdate = $date->getFormattedDate();
$ftime = $date->getFormattedTime();
$username = $_SESSION['session_username'];

?>

    <div id="headermenu-admin" class="headermenu">

      <div>
        <span>Admin</span>
      </div>

      <a class="nav" href="/admin/feature_page/feature_page_list.php"><?php echo $sftr->trans('adminpages'); ?></a>

      <a class="nav" href="/admin/article/?include_posts=1&amp;include_events=1&amp;include_comments=1&amp;news_item_status_restriction=0&amp;news_item_status_restriction=874"><?php echo $sftr->trans('adminunclassified'); ?></a>

      <a class="nav" href="/admin/article/?include_posts=1&amp;include_events=1&amp;news_item_status_restriction=0"><?php echo $sftr->trans('adminallarticles'); ?></a>

      <a class="nav" href="/admin/media/upload_list.php">Manage Uploads</a>

      <a class="nav" href="/admin/user/user_list.php"><?php echo $sftr->trans('adminusers'); ?></a>

      <a class="nav" href="/admin/email/admin_email.php"><?php echo $sftr->trans('adminemail'); ?></a>

      <!-- <a class="nav" href="/admin/stats/">Site Stats</a> -->

      <!-- <a class="nav" href="/admin/article/?news_item_status_restriction=0&amp;parent_item_id=0&amp;submitted_search=Go">Breaking News</a> -->

      <!-- <a class="nav" href="/admin/logs/?method=DB">Logs</a> -->

      <a class="nav" href="/admin/logs/spam_identification.php">Spam</a>

      <a class="nav" href="/admin/status/">Server Status</a>

      <a class="nav" href="/admin/paypal/">Paypal Donations</a>

      <a class="nav" href="/admin/lumen/">Submit Legal Demand to Lumen</a>

      <!-- <a class="nav" href="/admin/feed_pull/inbound_feed_list.php">Inbound Feeds</a> -->

      <!-- <a class="nav" href="/admin/cities/">Refresh Cities List</a> -->

      <div>
        <span class="headermenu-small">
          <?php
          echo $sftr->trans('adminloggedin');
          ?>: <?php
echo htmlspecialchars($username);
?>
          <br>
          <?php
          echo $fdate;
          ?>, <?php
echo $ftime;
?>
        </span>
      </div>

      <a class="nav" href="/admin/logout.php">Logout</a>

    </div><!-- END .headermenu-admin -->
