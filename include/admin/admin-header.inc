<?php

/**
 * @file
 * Admin page header.
 */

header('X-Frame-Options: SAMEORIGIN');
use Indybay\DB\UserDB;

if (in_array('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == '' && strpos(' ' . ADMIN_ROOT_URL, 'https') > 0) {
  header('Location: ' . ADMIN_ROOT_URL . $_SERVER['REQUEST_URI']);
  exit();
}

if (!empty($_POST)) {
  if (!empty($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] . '/' !== ADMIN_ROOT_URL) {
    http_response_code(404);
    throw new Exception('Admin POST has invalid Origin.');
  }
  if (empty($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], ADMIN_ROOT_URL) !== 0) {
    http_response_code(404);
    throw new Exception('Admin POST has invalid Referer.');
  }
}

if (!isset($_SESSION['session_last_activity_time'])
        || (time() - $_SESSION['session_last_activity_time']) > SESSION_TIMEOUT_SECONDS) {
  $_SESSION['session_is_editor'] = FALSE;
}

if ($_SESSION['session_is_editor'] == TRUE) {
  if (isset($GLOBALS['display'])) {
    include INCLUDE_PATH . '/admin/admin-header-html.inc';
    if (DB_DATABASE !== 'radio') {
      $user_db_class = new UserDB();
      $user_db_class->updateLastActivityDate();
    }
    $_SESSION['session_last_activity_time'] = time();

    ?>

    <?php

  }
}
else {
  $goto = urlencode($_SERVER['REQUEST_URI']);
  header('Location: ' . ADMIN_ROOT_URL . "admin/authentication/authenticate_display_logon.php?goto=$goto");
  exit();
}
