<?php

/**
 * @file
 * Admin page footer.
 */
?>

            </div><!-- END .admincontent -->
          </div><!-- END .page -->

        </div><!-- END #content-main -->

        <div class="footer">
        </div><!-- end .footer -->

      </div><!-- END #siteinner -->
    </div><!-- END #sitewrapper -->

  </body>
</html>

<?php
if (array_key_exists('log_id', $GLOBALS) && $GLOBALS['log_id'] + 0 != 0) {
  $log_class = new LogDB();
  $log_class->updateLogIdEndTime();
}
