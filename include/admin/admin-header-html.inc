<?php

/**
 * @file
 * Renders the admin page header.
 */

use Indybay\Date;
use Indybay\Translate;

$sftr = new Translate();

$date = new Date();
$date->setTimeZone(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="color-scheme" content="dark light">
    <meta name="robots" content="noindex, nofollow">
    <title>Indybay Admin</title>

    <script>
      // Script loads extra CSS for older browsers that do not recognize prefers-color-scheme.
      if (window.matchMedia("(prefers-color-scheme)").media == "not all") {
        document.write(
          '<link type="text/css" rel="stylesheet" href="/themes/color.css" media="all">'
        );
      }
    </script>
    <link
      type="text/css"
      rel="stylesheet"
      href="/themes/bundle.css<?php echo CSS_JS_QUERY_STRING; ?>"
      media="all"
   >

    <?php include INCLUDE_PATH . '/common/javascript.inc'; ?>
    <script defer src="/js/admin.js"></script>
  </head>
  <body class="page-admin">
    <div id="sitewrapper">
      <div id="shadow-content-main" class="closenav"></div>

      <div id="siteinner">
        <div class="mast">
          <div class="mastimage">
            <img loading="lazy" src="/im/banner-home.png" alt="">
          </div>

          <div class="mast-inner">
            <div class="mastleft">
              <div class="masttitle indybay">Indybay</div>
              <div class="masttitleshadow indybay">Indybay</div>
              <div class="masttitle indybay-logo"><img src="/im/banner_logo.svg" alt="Indybay" height="37"></div>
              <div class="masttitleshadow indybay-logo"><img src="/im/banner_logo.svg" alt="Indybay" height="37"></div>
              <div class="mastcheer">
                <a href="/" title="Indybay home">
                  <img src="/im/banner_cheer.svg" alt="protest cheer"  height="45">
                </a>
              </div>
            </div>
            <!-- END .mastleft -->

            <div class="mastright">
              <div class="headerlogo">
                <a href="/" title="home">
                  <img src="/im/banner_logo-right.svg" alt="Indybay" height="45">
                </a>
              </div>

              <div class="headerbuttons">
                <div id="headerbutton-admin" class="headerbutton" title="Admin">
                  <img src="/im/menu_gear.svg" alt="admin menu" height="27">
                </div>
              </div>
              <!-- END #headerbuttons -->
            </div>
            <!-- END .mastright -->
          </div>
          <!-- END .mast-inner -->

          <div class="headermenus">
            <?php
            include INCLUDE_PATH . '/admin/admin-nav.inc';
            ?>
          </div>
          <!-- END .headermenus -->
        </div>
        <!-- END .mast -->

        <div class="adminbreadcrumbnav">
          <span>
            <span class="strong">
              <?php
              echo $sftr->trans('adminsite'); ?> :
              <a class="bar" href="/admin/logout.php"
                ><?php echo $sftr->trans('adminlogout'); ?></a
              >
              :
              <a class="bar nowrap" title="open live site in new window" target="_blank" href="/"
                ><?php echo $sftr->trans('adminviewsite'); ?></a
              >
            </span>
          </span>
        </div>

        <div id="content-main">
          <div class="page">
            <div class="admincontent">
