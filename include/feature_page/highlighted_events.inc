<?php

/**
 * @file
 * Highlighted events.
 */

require_once CLASS_PATH . '/cache/event_list_cache_class.inc';

$event_list_cache_class = new EventListCache();


$page_id = $GLOBALS[page_id];

$html = $event_list_cache_class->loadCachedHighlightedEventsCacheForPage($page_id);

echo $html;
