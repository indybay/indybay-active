<!-- feature_list.inc -->
<div class="latestheadlines">
  <div class="archivelink2">
    <span class="headlines1">
      <a
        href="http://www.indybay.org/syn/generate_rss.php?include_blurbs=1&amp;include_posts=0"
        title="RSS feed"
        ><img
          src="/im/rss-rounded.svg"
          class="mediaicon"
          alt="RSS feed"
          width="12" 
          height="12"
     ></a>
      <a name="features">Latest features from all sections of the site:</a>
    </span>
  </div><!-- END .archivelink2 -->

  <?php
  /**
   * @file
   * This is for front page footer latest feature list.
   */

  // And HTML is formatted in classes/renderer/feature_page_renderer_class.inc.
  use Indybay\Cache\FeaturePageCache;

  $feature_page_cache_class = new FeaturePageCache();
  echo $feature_page_cache_class->loadCachedRecentFeatures(0); ?>

</div><!-- END .latestheadlines -->

<!-- end feature_list.inc -->
