<div class="latestheadlines">
  <div class="archivelink2">
    <span class="headlines1">
      <a
        href="/syn/generate_rss.php?include_blurbs=1&amp;include_posts=0"
        title="RSS feed"
        ><img
          src="/im/rss-rounded.svg"
          class="mediaicon"
          alt="RSS feed"
          width="12" 
          height="12"
     ></a>
      <a name="features">Latest features from all sections of the site:</a>
      <!-- single_item_feature_list.inc -->
    </span>
  </div>
  <!-- END .archivelink2 -->

  <?php
  /**
   * @file
   * This code displays latest headlines at footer of single feature/blurb page.
   */

  use Indybay\Cache\ArticleCache;
  use Indybay\DB\BlurbDB;
  // This code seems duplicative of function in FeaturePageRenderer.
  use Indybay\DB\FeaturePageDB;

  $feature_page_db_class = new FeaturePageDB();
  $blurb_db_class = new BlurbDB();
  $article_cache_class = new ArticleCache();
  $results = $feature_page_db_class->getRecentBlurbVersionIds();
  $i = 0;
  $previous_titles = []; foreach ($results as $version_id) {
    $blurb_info =
    $blurb_db_class->getBlurbInfoFromVersionId($version_id); $title2 =
    $blurb_info['title2'];
    $title1 = $blurb_info['title1']; if (trim($title1) ==
    '' || trim($title2) == '') {
      continue;
    } if (!isset($previous_titles[$title2])) {
      $pages =
      $feature_page_db_class->getPagesWithPushedAutoVersionsOfBlurb($blurb_info['news_item_id']);
      if (count($pages) != 0 && !(count($pages) == 1 && $pages[0]['page_id'] ==
        FRONT_PAGE_CATEGORY_ID)) {
        $create_time_formatted = date('Y-m-d',
        $blurb_info['creation_timestamp']);
        $page_link = '';
        $pi = 0;
        $page_link = '';
        foreach ($pages as $page_info) {
          $pi = $pi + 1; if ($pi > 1) {
            $page_link .= '
  | ';
          } $page_link .= '<a href="/' . $page_info['relative_path'] . '"
    >';
          $page_link .= htmlspecialchars($page_info['long_display_name']);
          $page_link .= '</a
  >';
        } $previous_titles[$title2] = 1; $searchlink =
          $article_cache_class->getWebCachePathForNewsItemGivenDate($blurb_info['news_item_id'],
        $blurb_info['creation_timestamp']); $row = '<span class="archivelink2-item"
    >'; $row .= '<span class="archivelink2-date"
      >';
        $row .= $create_time_formatted; $row .= '</span
    >'; $row .= '
    <a
      href="' . $searchlink;
        $row .= '"
      >';
        $row .= htmlspecialchars($blurb_info['title2']); $row .= '</a
    >
    <span class="archivelink2-categories"
      >';
        $row .= $page_link; $row .= '</span
    ></span
  >';
        echo $row;
        $i = $i + 1;
        if ($i > 10) {
          break;
        }
      }
    }
  } ?>
</div>
<!-- END .latestheadlines -->
