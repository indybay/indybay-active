<?php

/**
 * @file
 * Renders bottom of main content area.
 */
?>
    <div class="feature-bottom">

      <div class="module-wrappers">

        <div class="module module-unified module-unified-grey module-support">

          <div class="module-header">
            <a href="/newsitems/2003/12/15/16659041.php" class="">Support Independent Media</a>
          </div>

          <div class="module-inner" >

            <div class="module-inner-horiz">
              We are 100% volunteer and depend on your participation to sustain our efforts!
            </div>

            <div class="module-inner-thirds support-item support-donate">

              <h3>Donate</h3>

              <p>
                <a href="/newsitems/2003/12/15/16659041.php" title="Donate Now!">
                  <img src="/im/btn_donate-dark.png" class="donate-button" alt="donate button">
                </a>
              </p>

              <p>$<span id="donation-amount"><?php if (is_readable(CACHE_PATH . '/paypal.txt')) :
                readfile(CACHE_PATH . '/paypal.txt'); ?>
              <?php else : ?>
                0.00
              <?php endif; ?></span> donated<br>in the past month</p>

            </div><!-- END .module-inner-thirds.support-donate -->

            <div class="module-inner-thirds support-item support-volunteer">

              <h3>Get Involved</h3>

              <p>If you'd like to help with maintaining or developing the website, <a href="/contact" title="Contact Us">contact us</a>.</p>

            </div><!-- END .module-inner-thirds.support-volunteer -->

            <div class="module-inner-thirds support-item support-publish">

              <h3>Publish</h3>

              <p>Publish <a href="/publish.php" title="Publish to Newswire"><em>your</em> stories</a> and <a href="/calendar/event_add.php" title="Publish to Calendar">upcoming events</a> on Indybay.</p>

            </div><!-- END .module-inner-thirds.support-publish -->

          </div><!-- END .module-inner -->

        </div><!-- END .module.module-unified.module-unified-grey -->

        <div class="module module-top module-regions">

          <div class="module-header">
            <a class="" href="/news/2005/12/1789457.php" title="Regional Pages Pages">Regions</a>
          </div>

          <div class="module-inner">

            <div class="module-inner-horiz">

              <div  class="<?php
              if (in_array('41', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/north-coast/" title="North Coast">North Coast</a></div>

              <div  class="<?php
              if (in_array('35', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/central-valley/" title="Central Valley">Central Valley</a></div>

              <div  class="<?php
              if (in_array('40', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/north-bay/" title="North Bay">North Bay</a></div>

              <div  class="<?php
              if (in_array('38', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/east-bay/" title="East Bay">East Bay</a></div>

              <div  class="<?php
              if (in_array('37', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/south-bay/" title="South Bay">South Bay</a></div>

              <div  class="<?php
              if (in_array('36', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/sf/" title="San Francisco">San Francisco</a></div>

              <div  class="<?php
              if (in_array('39', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/peninsula/" title="Peninsula">Peninsula</a></div>

              <div  class="<?php
              if (in_array('60', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/santa-cruz/" title="Santa Cruz">Santa Cruz</a></div>

              <div  class="<?php
              if (in_array('42', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/california/" title="California">California</a></div>

              <div  class="<?php
              if (in_array('43', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/us/" title="U.S.">US</a></div>

              <div  class="<?php
              if (in_array('44', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/" title="International">International</a></div>

            </div><!-- END .module-inner-horiz -->

          </div><!-- END .module-inner -->

        </div><!-- END .module.module-top.module-regions -->

        <div class="module module-top module-topics">

            <div class="module-header">
              <span class="">Topics</span>
              <!-- <a class="" href="/#features" title="Topic Pages">Topics</a> -->
            </div>

            <div class="module-inner">

              <div class="module-inner-horiz">

              <div  class="<?php
              if (in_array('58', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/animal-liberation/" title="Animal Liberation News">Animal Lib</a></div>

              <div  class="<?php
              if (in_array('18', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/antiwar/" title="Anti-War and Militarism News">Anti-War</a></div>

              <div  class="<?php
              if (in_array('34', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/arts/" title="Arts + Action News">Arts + Action</a></div>

              <div  class="<?php
              if (in_array('27', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/drug-war/" title="Drug War News">Drug War</a></div>

              <div  class="<?php
              if (in_array('30', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/education/" title="Education &amp; Student Activism News">Education</a></div>

              <div  class="<?php
              if (in_array('33', $GLOBALS['page_ids'])) {
                $tag = TRUE; ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE;
                ?>featurelistitem<?php
              } ?>"><a class="bottomf" <?php
if ($tag) {
  ?>rel="tag"<?php
} ?> href="/espanol/" title="Noticias en Español">En Español</a></div>

              <div  class="<?php
              if (in_array('14', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/environment/" title="Environment &amp; Forest Defense News">Environment</a></div>

              <div  class="<?php
              if (in_array('22', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/global-justice/" title="Global Justice &amp; Anti-Capitalism News">Global Justice</a></div>

              <div  class="<?php
              if (in_array('45', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/government/" title="Government &amp; Elections News">Government</a></div>

              <div  class="<?php
              if (in_array('16', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/health-housing/" title="Health, Housing, &amp; Public Services News">Health/Housing</a></div>

              <div  class="<?php
              if (in_array('56', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/immigrant/" title="Immigrant Rights News">Immigrant</a></div>

              <div  class="<?php
              if (in_array('32', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/media/" title="Media Activism &amp; Independent Media News">Media</a></div>

              <div  class="<?php
              if (in_array('19', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/labor/" title="Labor &amp; Workers News">Labor</a></div>

              <div  class="<?php
              if (in_array('29', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/lgbtqi/" title="LGBTI / Queer News">LGBTI / Queer</a></div>

              <div  class="<?php
              if (in_array('13', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/police/" title="Police State &amp; Prisons News">Police State</a></div>

              <div  class="<?php
              if (in_array('15', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/race/" title="Racial Justice News">Racial Justice</a></div>

              <div  class="<?php
              if (in_array('31', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/womyn/" title="Womyn's News">Womyn</a></div>

            </div><!-- end .module-inner-horiz -->

          </div><!-- end .module-inner -->

        </div><!-- END .module.module-top.module-topics -->

        <div class="module module-top module-intl">

          <div class="module-header">
          <a class="" href="/international/" title="International Pages">International</a>
          </div>

          <div class="module-inner">

            <div class="module-inner-horiz">

              <div  class="<?php
              if (in_array('53', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/americas/" title="Americas">Americas</a></div>

              <div  class="<?php
              if (in_array('50', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/haiti/" title="Haiti">Haiti</a></div>

              <div  class="<?php
              if (in_array('48', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/iraq/" title="Iraq">Iraq</a></div>

              <div  class="<?php
              if (in_array('49', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/palestine/" title="Palestine">Palestine</a></div>

              <div  class="<?php
              if (in_array('51', $GLOBALS['page_ids'])) {
                $tag = TRUE;
                ?>thisfeaturelistitem<?php
              }
              else {
                $tag = FALSE; ?>featurelistitem<?php
              } ?>"><a
              class="bottomf" <?php
              if ($tag) {
                ?>rel="tag"<?php
              } ?> href="/international/afghanistan/" title="Afghanistan">Afghanistan</a></div>

            </div><!-- end .module-inner-horiz -->

          </div><!-- end .module-inner -->

        </div><!-- END .module.module-top.module-intl -->

        <div class="module module-top module-more">

          <div class="module-header">
            <span class="">More</span>
          </div>

          <div class="module-inner">

            <div class="module-inner-horiz">

              <div class="bottom"><a class="" href="/newsitems/2017/08/15/18801561.php" title="Frequently Asked Questions">FAQ</a></div>
              <div class="bottom"><a class="" href="/news/2003/12/1665902.php" title="Make Media &amp; Publish Your Own News">Make Media</a></div>
              <div  class="bottom"><a class="" href="/news/2003/12/1665901.php" title="Get Involved with Indybay">Get Involved</a></div>
              <div class="bottom"><a class="" href="/gallery/" title="Photo Gallery">Photo Gallery</a></div>
              <div class="bottom"><a class="" href="/archives/archived_blurb_list.php?page_id=12" title="Center-Column Feature Archives">Feature Archives</a></div>
              <div class="bottom"><a class="" href="/faultlines/" title="Fault Lines Newspaper Archives">Fault Lines</a></div>
              <div class="bottom bottomlast"><a class="" href="/news/2003/12/1665900.php" title="Links to Other Sites">Links</a></div>

              <?php if (0) : ?>
              <div id="stop-sopa" style="display: none; font-size: 13px">
              <img src="/uploads/admin_uploads/2011/12/18/stop-sopa-200.png" width="200" height="197" alt="Stop SOPA" style="margin: 0 auto;">
              <br>
              If <a
              href="https://www.eff.org/issues/coica-internet-censorship-and-copyright-bill">SOPA/PIPA</a> Internet blacklist legislation
              passes, the government would have the power to shut down Indybay.org.
              <br> <br>This act would allow Indybay's domain name to be hijacked,
              forcing Internet providers to stop serving Indybay's pages, and could
              allow for deep packet inspection to determine who posts to the site,
              taking away community member's anonymity.<br> <br> EFF suggests
              how you can <a href="/newsitems/2011/12/18/18703033.php">help stop
              SOPA/PIPA</a>.</div>
              <?php endif; ?>

            </div><!-- end .module-inner-horiz -->

            <div class="module-inner-thirds searchform">

              <span class="searchtitle"><span class="strong">Search Indybay's Archives</span></span>
              <form action="/search/search_results.php" method="get">
                <input name="search" size="25" type="text" placeholder="Enter keyword(s)">
                <input value="Search" type="submit">
              </form>
              <span class="small"><a href="/search/advanced_search.php" title="Advanced Search">Advanced Search</a></span>

            </div><!-- END .module-inner-thirds.searchform -->

          </div><!-- end .module-inner -->

        </div><!-- END .module.module-top.module-more -->

        <?php if (array_key_exists('60', $GLOBALS['page_ids'])) : ?>
        <div class="module-header"><a href="http://www.freakradio.org" class="">Freak Radio</a></div>
        <div class="restofbottom" ><a class="" href="http://www.freakradio.org/listen.html">FRSC live stream</a><br>
        <center ><a href="http://www.freakradio.org/listen.html"><img
        src="/images/meter.gif" alt="radio" width="65" height="30"></a></center></div>
        <?php endif; ?>

        <div class="module module-top module-cities">

          <div class="module-header">
            <span class="indy-link bottommf-trash" title="click to see full list of IMCs">
              <span class="closed">&#x25BA;</span>
              <span class="open">&#x25BC;</span>
              IMC Network
            </span>
          </div>

          <div class="module-inner">

            <div class="cities-list"></div>

          </div><!-- end .module-inner -->

        </div><!-- END .module.module-top.module-cities -->

      </div><!-- END .module-wrappers -->

    </div><!-- END .feature-bottom -->
