<?php

/**
 * @file
 * Renders the Indybay header links.
 */
?>
<div id="headernav" class="flex flex--align-center">
  <div class="navbar flex--grow-1">
    <a href="/" title="Indybay Home">Home</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/about" title="About the IMC">About</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/contact" title="Talk to Us">Contact</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/subscribe/" title="News Services">Subscribe</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/calendar/?page_id=<?php
    $current_url = $_SERVER['SCRIPT_NAME'];
    $page_id = '';
    if (strpos($current_url, 'news') < 1 && count($GLOBALS['page_ids']) == 1) {
      $page_id = $GLOBALS['page_ids'][0];
      echo $page_id;
    } ?>" title="Event Announcements">Calendar</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/publish.php<?php
    if (isset($GLOBALS['page_ids'][0]) && count($GLOBALS['page_ids']) == 1) {
      echo '?page_id=' . $GLOBALS['page_ids'][0];
    } ?>" title="Publish to Newswire">Publish</a>
  </div>
  <div class="navbar flex--grow-1">
    <a href="/donate" title="Support Indpendent Media">Donate</a>
  </div>
</div><!-- END #headernav -->
