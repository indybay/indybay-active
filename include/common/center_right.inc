<?php

/**
 * @file
 * Renders end of center column and start of right column.
 */

$page_id = $GLOBALS['page_ids'][0] ?? $GLOBALS['page_id'] ?? FRONT_PAGE_CATEGORY_ID;

?>
<div class="rightcol tabmenu">
  <div class="newswire tabmenu">
    <div class="newswirewrap newswirewrap-header">
      <div class="newswirewrap-header-title">Newswire media types:</div>

      <?php include INCLUDE_PATH . '/common/media_key.inc'; ?>
    </div>
    <!-- END .newswirewrap-header -->

    <?php
    include CACHE_PATH . '/newswires/newswire_page' . $page_id;
    ?>

    <div class="rss-link">
      <a
        href="/syn/generate_rss.php?news_item_status_restriction=1155&amp;page_id=<?php
        if ($page_id != FRONT_PAGE_CATEGORY_ID) {
          echo $page_id;
        }
        ?>"
        title="RSS feed"
        ><img src="/im/rss-rounded.svg" class="mediaicon" alt="RSS feed" width="12" height="12">
      </a>
    </div>
    <!-- END .rss-link -->
  </div>
  <!-- END .newswire -->
</div>
<!-- END .rightcol -->
