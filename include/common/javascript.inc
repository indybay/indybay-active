<?php

/**
 * @file
 * Renders script tags.
 */
?>
<script defer src="/js/jquery.min.js"></script>

<script defer src="/js/main.js<?php echo CSS_JS_QUERY_STRING; ?>"></script>

<!-- <script defer id="css-js-query-string" data-css-js-query-string="<?php
// Echo CSS_JS_QUERY_STRING;. ?>" src="/notice/notice.js<?php
// Echo CSS_JS_QUERY_STRING;. ?>"></script> -->

<?php if (isset($GLOBALS['jquery'])) : ?>
  <?php foreach ((array) $GLOBALS['jquery'] as $plugin) : ?>
    <script defer src="/js/jquery.<?php echo $plugin; ?>.min.js"></script>
  <?php endforeach; ?>
<?php endif; ?>

<?php if (isset($GLOBALS['ui'])) : ?>
  <script defer src="/js/jquery.ui/jquery-ui.min.js"></script>
  <style type="text/css" media="all">@import "/js/jquery.ui/jquery-ui.min.css";</style>
<?php endif; ?>
<?php if (isset($GLOBALS['js'])) : ?>
  <?php foreach ((array) $GLOBALS['js'] as $js) : ?>
    <script defer src="/js/<?php echo $js; ?>.js<?php echo CSS_JS_QUERY_STRING; ?>"></script>
  <?php endforeach; ?>
<?php endif;
