<?php

/**
 * @file
 * Renders media icons.
 */
?>
<div class="nooz-types grid grid--2-cols">
  <div>
    <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=0&amp;media_type_grouping_id=1"
      title="browse articles"
      class="newswire">
      <img src="/im/imc_article.svg" alt="article icon" width="12" height="12">
      article
    </a>
  </div>
  <div>
    <a href="/gallery/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=2"
      title="photo gallery"
      class="newswire">
      <img src="/im/imc_photo.svg" alt="photo icon" width="12" height="12">
      photo
    </a>
  </div>
  <div>
    <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=4"
      title="browse video"
      class="newswire">
      <img src="/im/imc_video.svg" alt="video icon" width="12" height="12">
      video
    </a>
  </div>
  <div>
    <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=3"
      title="browse audio"
      class="newswire">
      <img src="/im/imc_audio.svg" alt="audio icon" width="12" height="12">
      audio
    </a>
  </div>
</div>
