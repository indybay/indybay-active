<?php

/**
 * @file
 * Renders disclaimer and footer links.
 */
?>
&copy; 2000&#8211;<?php echo date('Y'); ?> <?php print $GLOBALS['site_name']; ?>.
Unless otherwise stated by the author, all content is free for
non-commercial reuse, reprint, and rebroadcast, on the net and
elsewhere. Opinions are those of the contributors and are not
necessarily endorsed by the SF Bay Area IMC.<br>  
<a href="/disclaimer">Disclaimer</a> |
<a href="/copyright">Copyright Policy</a> |
<a href="/privacy">Privacy</a> |
<a href="/contact">Contact</a> |
<a href="https://gitlab.com/indybay">Source Code</a>
