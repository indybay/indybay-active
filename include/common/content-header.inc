<?php

/**
 * @file
 * Renders start of page.
 */

header('X-Frame-Options: SAMEORIGIN');
include INCLUDE_PATH . '/common/headers_and_links.cfg';
use Indybay\Renderer\Renderer;
use Indybay\Translate;

$sftr = new Translate();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="color-scheme" content="dark light">
    <title><?php echo Renderer::pageTitle(); ?></title>
    <meta name="geo.position" content="37.765;-122.4183">
    <meta name="ICBM" content="37.765,-122.4183">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if (isset($GLOBALS['page_display']) && $GLOBALS['page_display'] == 'f') : ?>
      <?php if (str_contains($_SERVER['REQUEST_URI'], '/index.php')) : ?>
        <link rel="canonical" href="<?php echo Renderer::htmlPageUrl(); ?>">
      <?php endif; ?>
    <?php elseif (isset($GLOBALS['page_summary'])) : ?>
      <meta name="description" property="og:description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'])); ?>">
      <meta name="twitter:site" content="@indybay">
      <meta name="twitter:title" property="og:title" content="<?php echo Renderer::pageTitle(); ?>">
      <meta name="twitter:description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'])); ?>">
      <meta property="og:type" content="article">
      <?php if (str_contains($_SERVER['REQUEST_URI'], '/index.php')) : ?>
        <meta property="og:url" content="<?php echo Renderer::htmlPageUrl(); ?>">
      <?php endif; ?>
      <meta property="og:site_name" content="Indybay">
      <meta property="article:publisher" content="https://www.facebook.com/indybay.org">
      <?php if (isset($GLOBALS['image_url']) && preg_match('/.*mp4$/', $GLOBALS['image_url'])) : ?>
        <meta name="twitter:card" content="player">
        <meta name="twitter:player:stream" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta name="twitter:player:stream:content_type" content="video/mp4">
        <meta name="twitter:player" content="https://www.indybay.org/video.php/<?php echo $GLOBALS['news_item_id']; ?>">
        <meta name="twitter:player:width" content="<?php echo $GLOBALS['image_width']; ?>">
        <meta name="twitter:player:height" content="<?php echo $GLOBALS['image_height']; ?>">
        <?php if (!empty($GLOBALS['poster_url'])) : ?>
          <meta name="twitter:image" property="og:image" content="<?php echo $GLOBALS['poster_url']; ?>">
        <?php endif; ?>
        <meta property="og:video" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta property="og:video:secure_url" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta property="og:video:type" content="video/mp4">
        <meta property="og:video:width" content="<?php echo $GLOBALS['image_width']; ?>">
        <meta property="og:video:height" content="<?php echo $GLOBALS['image_height']; ?>">
      <?php elseif (isset($GLOBALS['image_url']) && preg_match('/.*(mp3|ogg)$/', $GLOBALS['image_url'])) : ?>
        <meta property="og:audio" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta property="og:audio:secure_url" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta property="og:audio:type" content="<?php echo $GLOBALS['mime_type']; ?>">
        <meta name="twitter:card" content="player">
        <meta name="twitter:player:stream" content="<?php echo $GLOBALS['image_url']; ?>">
        <meta name="twitter:player:stream:content_type" content="audio/mpeg">
        <meta name="twitter:player" content="https://www.indybay.org/audio.php/<?php echo $GLOBALS['news_item_id']; ?>">
        <meta name="twitter:player:width" content="300">
        <meta name="twitter:player:height" content="28">
        <meta name="twitter:image" property="og:image" content="https://www.indybay.org/im/meta_audio.png">
        <meta name="twitter:image:width" property="og:image:width" content="280">
        <meta name="twitter:image:height" property="og:image:height" content="150">
      <?php elseif (isset($GLOBALS['image_url']) && preg_match('/.*(jpg|jpeg|gif|png)$/', $GLOBALS['image_url'])) : ?>
        <?php if ((!empty($GLOBALS['image_width']) && $GLOBALS['image_width'] < 280) || (!empty($GLOBALS['image_height']) && $GLOBALS['image_height'] < 150)) : ?>
          <meta name="twitter:card" content="summary">
        <?php else : ?>
          <meta name="twitter:card" content="summary_large_image">
        <?php endif; ?>
        <meta name="twitter:image" property="og:image" content="<?php echo $GLOBALS['image_url']; ?>">
        <?php if (!empty($GLOBALS['image_width'])) : ?>
          <meta name="twitter:image:width" property="og:image:width" content="<?php echo $GLOBALS['image_width']; ?>">
        <?php endif; ?>
        <?php if (!empty($GLOBALS['image_height'])) : ?>
          <meta name="twitter:image:height" property="og:image:height" content="<?php echo $GLOBALS['image_height']; ?>">
        <?php endif; ?>
        <?php if (!empty($GLOBALS['image_alt'])) : ?>
          <meta name="twitter:image:alt" property="og:image:alt" content="<?php echo $GLOBALS['image_alt']; ?>">
        <?php endif; ?>
      <?php elseif ($GLOBALS['body_class'] == 'page-event') : ?>
        <meta name="twitter:card" content="summary">
        <meta name="twitter:image" property="og:image" content="https://www.indybay.org/im/meta_event.png">
        <meta name="twitter:image:width" property="og:image:width" content="280">
        <meta name="twitter:image:height" property="og:image:height" content="150">
      <?php else : ?>
        <meta name="twitter:card" content="summary">
        <meta name="twitter:image" property="og:image" content="https://www.indybay.org/im/meta_article.png">
        <meta name="twitter:image:width" property="og:image:width" content="280">
        <meta name="twitter:image:height" property="og:image:height" content="150">
      <?php endif; ?>
      <?php if (str_contains($_SERVER['REQUEST_URI'], '/index.php')) : ?>
        <link rel="canonical" href="<?php echo Renderer::htmlPageUrl(); ?>">
      <?php endif; ?>
    <?php else : ?>
      <meta name="description" property="og:description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network.">
      <meta name="twitter:card" content="summary">
      <meta name="twitter:site" content="@indybay">
      <meta name="twitter:title" property="og:title" content="<?php echo Renderer::pageTitle(); ?>">
      <meta name="twitter:description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network.">
      <meta property="og:type" content="website">
      <?php if (str_contains($_SERVER['REQUEST_URI'], '/index.php')) : ?>
        <meta property="og:url" content="<?php echo Renderer::htmlPageUrl(); ?>">
      <?php endif; ?>
      <meta property="og:site_name" content="Indybay">
      <meta property="article:author" content="Indybay">
      <meta property="article:publisher" content="https://www.facebook.com/indybay.org">
      <meta name="twitter:image" property="og:image" content="https://www.indybay.org/im/meta_logo-url.png">
      <meta name="twitter:image:width" property="og:image:width" content="280">
      <meta name="twitter:image:height" property="og:image:height" content="150">
      <link rel="me" href="https://kolektiva.social/@indybay">
      <?php if (str_contains($_SERVER['REQUEST_URI'], '/index.php')) : ?>
        <link rel="canonical" href="<?php echo Renderer::htmlPageUrl(); ?>">
      <?php endif; ?>
    <?php endif; ?>
    <?php if (!empty($GLOBALS['indexing_suspended']) || (isset($GLOBALS['page_display']) && $GLOBALS['page_display'] == 'f') || (isset($_GET['news_item_status_restriction']) && $_GET['news_item_status_restriction'] == 323)) : ?>
      <meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOARCHIVE">
    <?php endif; ?>
    <?php if (!empty($GLOBALS['syndication_suspended'])) : ?>
      <meta name="googlebot-news" content="noindex, nofollow, noarchive">
    <?php endif; ?>
    <script>
      // Script loads extra CSS for older browsers that do not recognize prefers-color-scheme.
      if (window.matchMedia("(prefers-color-scheme)").media == "not all") {
        document.write(
          '<link type="text/css" rel="stylesheet" href="/themes/color.css" media="all">'
        );
      }
    </script>
    <link type="text/css" rel="stylesheet" href="/themes/bundle.css<?php echo CSS_JS_QUERY_STRING; ?>" media="all">
    <?php if (array_key_exists('rssautodiscover', $GLOBALS) && $GLOBALS['rssautodiscover']) : ?>
      <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php echo $GLOBALS['rssautodiscover']; ?>">
    <?php endif; ?>
    <link rel="alternate" type="application/rss+xml" title="Indybay Newswire" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155">
    <link rel="alternate" type="application/rss+xml" title="Indybay Features" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?include_blurbs=1&amp;include_posts=0">
    <link rel="alternate" type="application/rss+xml" title="Indybay Podcast" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=3&amp;news_item_status_restriction=1155">
    <link rel="alternate" type="application/rss+xml" title="Indybay Videoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=4&amp;news_item_status_restriction=1155">
    <link rel="alternate" type="application/rss+xml" title="Indybay Photoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=2&amp;news_item_status_restriction=1155">
    <link rel="alternate" type="application/rss+xml" title="Indybay Newswire RSS 1.0" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155&amp;rss_version=1">
    <?php include INCLUDE_PATH . '/common/javascript.inc'; ?>
  </head>

  <body<?php
  if (!empty($GLOBALS['body_class'])) :
    ?>
  class="<?php echo $GLOBALS['body_class'];?>"<?php
  endif; ?>>

    <div id="sitewrapper">
      <div id="shadow-headertabs" class="closenav"></div>
      <div id="shadow-content-main" class="closenav"></div>

      <div id="siteinner">
        <!-- #back to top box -->
        <div id="backtotopbox" title="back to top"><span>&#x25B2;</span> top</div>

        <div class="mast">
          <div class="mastimage">
            <img loading="lazy" src="<?php echo $GLOBALS['header_bg_image']; ?>" alt="">
          </div>

          <div class="mast-inner">
            <div class="mastleft">
              <div class="masttitle">
                <?php echo $GLOBALS['header_short_title']; ?>
              </div>
              <div class="masttitleshadow">
                <?php echo $GLOBALS['header_short_title']; ?>
              </div>
              <div class="masttitle indybay">Indybay</div>
              <div class="masttitleshadow indybay">Indybay</div>
              <div class="masttitle indybay-logo"><img src="/im/banner_logo.svg" alt="Indybay" height="37"></div>
              <div class="masttitleshadow indybay-logo"><img src="/im/banner_logo.svg" alt="Indybay" height="37"></div>
              <div class="mastcheer">
                <a href="<?php echo $GLOBALS['header_link']; ?>" title="home">
                  <?php echo $GLOBALS['header_cheer']; ?>
                </a>
              </div>
            </div>
            <!-- END .mastleft -->

            <div class="mastright">
              <div class="headerlogo">
                <a href="/" title="home">
                  <img src="/im/banner_logo-right.svg" alt="Indybay" height="45">
                </a>
              </div>

              <div class="headerbuttons">
                <?php if (!empty($_SESSION['session_is_editor'])) : ?>

                <div id="headerbutton-admin" class="headerbutton" title="Admin">
                  <img src="/im/menu_gear.svg" alt="admin menu" height="27">
                </div>

                <?php endif; ?>

                <div id="headerbutton-about" class="headerbutton" title="Indybay">
                  <img src="/im/menu_bars.svg" alt="indybay menu" height="25">
                </div>

                <div
                  id="headerbutton-category"
                  class="headerbutton"
                  title="Topics & Regions"
                >
                  <img src="/im/menu_category.svg" alt="category menu" height="25">
                </div>

                <div
                  id="headerbutton-publish"
                  class="headerbutton"
                  title="Publish to Newswire"
                >
                  <img src="/im/menu_publish.svg" alt="publish menu" height="25">
                </div>
              </div>
              <!-- END #headerbuttons -->
            </div>
            <!-- END .mastright -->
          </div>
          <!-- END .mast-inner -->

          <div class="headermenus">
            <?php if (!empty($_SESSION['session_is_editor'])) : ?>

              <?php
              include INCLUDE_PATH . '/admin/admin-nav.inc';
              ?>

            <?php endif; ?>

            <div id="headermenu-about" class="headermenu">
              <div>
                <span>Indybay</span>
              </div>
            </div>

            <div id="headermenu-category" class="headermenu">
              <!-- <a href="/publish.php">categories</a> -->
              <div class="headermenu-categories headermenu-category-regions">
                <span>Regions</span>
                <div class="regionmap">
                  <img
                    src="/im/region_map_v3.gif"
                    usemap="#regionmap"
                    alt="Indybay Regions"
                    width="170"
                    height="130"
                 >
                  <map name="regionmap" id="regionmap">
                    <area
                      shape="poly"
                      coords="88,1,86,8,93,15,100,13,104,13,108,11,115,10,120,12,124,17,128,19,135,13,133,8,130,1"
                      href="/north-coast/"
                      alt="North Coast"
                      title="North Coast"
                   >
                    <area
                      shape="poly"
                      coords="134,1,136,9,140,15,142,19,145,23,147,25,154,26,160,26,161,32,160,36,164,38,162,44,161,48,164,50,169,74,169,78,169,1"
                      href="/central-valley/"
                      alt="Central Valley"
                      title="Central Valley"
                   >
                    <area
                      shape="poly"
                      coords="96,17,103,28,107,32,115,44,113,48,118,51,120,55,124,55,129,58,129,53,129,48,134,42,140,47,143,48,147,44,150,47,157,49,158,45,161,41,161,39,157,37,158,34,158,29,148,28,143,25,140,20,137,14,133,19,126,22,122,18,117,14,112,13"
                      href="/north-bay/"
                      alt="North Bay"
                      title="North Bay"
                   >
                    <area
                      shape="poly"
                      coords="136,55,139,59,140,63,142,65,144,71,145,75,150,76,154,75,166,74,164,66,162,58,162,52,154,52,149,49,144,53,138,51"
                      href="/east-bay/"
                      alt="East Bay"
                      title="East Bay"
                   >
                    <area
                      shape="poly"
                      coords="142,79,142,83,144,86,147,89,149,91,156,94,160,97,163,101,169,97,169,75,161,77,152,78"
                      href="/south-bay/"
                      alt="South Bay"
                      title="South Bay"
                   >
                    <area
                      shape="rect"
                      coords="129,61,135,66"
                      href="/sf/"
                      alt="San Francisco"
                      title="San Francisco"
                   >
                    <area
                      shape="poly"
                      coords="129,67,129,73,131,75,134,82,133,87,135,90,139,88,142,88,139,83,139,77,143,76,134,70,132,67"
                      href="/peninsula/"
                      alt="Peninsula"
                      title="Peninsula"
                   >
                    <area
                      shape="poly"
                      coords="138,92,144,90,148,94,154,96,158,99,162,103,169,101,169,129,152,129,150,119,156,115,156,108,155,103,149,100,145,100,140,96"
                      href="/santa-cruz/"
                      alt="Santa Cruz IMC - Independent Media Center for the Monterey Bay Area"
                      title="Santa Cruz IMC - Independent Media Center for the Monterey Bay Area"
                   >
                    <area
                      shape="rect"
                      coords="5,2,81,13"
                      href="/north-coast/"
                      target="_top"
                      alt="North Coast"
                      title="North Coast"
                   >
                    <area
                      shape="rect"
                      coords="5,14,93,24"
                      href="/central-valley/"
                      target="_top"
                      alt="Central Valley"
                      title="Central Valley"
                   >
                    <area
                      shape="rect"
                      coords="5,25,68,35"
                      href="/north-bay/"
                      target="_top"
                      alt="North Bay"
                      title="North Bay"
                   >
                    <area
                      shape="rect"
                      coords="5,36,60,46"
                      href="/east-bay/"
                      target="_top"
                      alt="East Bay"
                      title="East Bay"
                   >
                    <area
                      shape="rect"
                      coords="5,47,69,57"
                      href="/south-bay/"
                      target="_top"
                      alt="South Bay"
                      title="South Bay"
                   >
                    <area
                      shape="rect"
                      coords="5,58,23,69"
                      href="/sf/"
                      target="_top"
                      alt="San Francisco"
                      title="San Francisco"
                   >
                    <area
                      shape="rect"
                      coords="33,58,95,69"
                      href="/peninsula/"
                      target="_top"
                      alt="Peninsula"
                      title="Peninsula"
                   >
                    <area
                      shape="rect"
                      coords="5,70,100,81"
                      href="/santa-cruz/"
                      target="_top"
                      alt="Santa Cruz IMC - Independent Media Center for the Monterey Bay Area"
                      title="Santa Cruz IMC - Independent Media Center for the Monterey Bay Area"
                   >
                    <area
                      shape="rect"
                      coords="5,82,67,92"
                      href="/california/"
                      target="_top"
                      alt="California"
                      title="California"
                   >
                    <area
                      shape="rect"
                      coords="79,82,96,92"
                      href="/us/"
                      target="_top"
                      alt="United States"
                      title="United States"
                   >
                    <area
                      shape="rect"
                      coords="5,93,87,103"
                      href="/international/"
                      target="_top"
                      alt="International"
                      title="International"
                   >
                    <area
                      shape="rect"
                      coords="5,105,62,112"
                      href="/international/americas/"
                      target="_top"
                      alt="Americas"
                      title="Americas"
                   >
                    <area
                      shape="rect"
                      coords="79,105,110,112"
                      href="/international/haiti/"
                      target="_top"
                      alt="Haiti"
                      title="Haiti"
                   >
                    <area
                      shape="rect"
                      coords="5,113,32,120"
                      href="/international/iraq/"
                      target="_top"
                      alt="Iraq"
                      title="Iraq"
                   >
                    <area
                      shape="rect"
                      coords="48,113,111,120"
                      href="/international/palestine/"
                      target="_top"
                      alt="Palestine"
                      title="Palestine"
                   >
                    <area
                      shape="rect"
                      coords="5,121,87,128"
                      href="/international/afghanistan/"
                      target="_top"
                      alt="Afghanistan"
                      title="Afghanistan"
                   >
                  </map>
                </div>
              </div>
              <!-- END .headermenu-categories.headermenu-category-regions -->
              <div class="headermenu-categories headermenu-category-topics">
                <span>Topics</span>
              </div>
              <!-- END .headermenu-categories.headermenu-category-topics -->
            </div>
            <!-- END #headermenu-category.headermenu -->

            <div id="headermenu-publish" class="headermenu">
              <div>
                <span>Publish</span>
              </div>
              <a
                href="/publish.php<?php
                if (isset($GLOBALS['page_ids'][0]) && count($GLOBALS['page_ids']) == 1) {
                  echo '?page_id=' . $GLOBALS['page_ids'][0];
                } ?>"
                title="Publish to Newswire"
                ><img
                  src="/im/imc_article.svg"
                  alt="article"
                  class="mediaicon"
                  width="16"
                  height="16"
               >Publish to Newswire</a
              >
              <a
                href="/calendar/event_add.php<?php
                if (isset($GLOBALS['page_ids'][0]) && count($GLOBALS['page_ids']) == 1) {
                  echo '?page_id=' . $GLOBALS['page_ids'][0];
                } ?>"
                title="Publish to Calendar"
                ><img
                  src="/im/imc_event.svg"
                  alt="event"
                  class="mediaicon"
                  width="16"
                  height="16"
               ><?php
                echo $sftr->trans('add_an_event'); ?></a
              >
            </div>
          </div>
          <!-- END .headermenus -->
        </div>
        <!-- END .mast -->

        <div id="headertabs">
          <div id="headertab-newswire" class="headertab-bttn" title="Newswire">
            Newswire
          </div>
          <div id="headertab-cal" class="headertab-bttn" title="Calendar">
          <a href="/calendar/">Calendar</a>
          </div>
          <div
            id="headertab-feat"
            class="headertab-bttn active"
            title="Indybay Features"
          >
            Features
          </div>
        </div>
        <!-- END #headertabs -->

        <?php
        include 'about-indybay.inc';
        ?>

        <div id="content-main">
          <div id="calendarwrap" class="tabmenu"></div>
          <div class="page">
            <div class="pagecontent">
