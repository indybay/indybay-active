<?php

/**
 * @file
 * Renders center column.
 */

use Indybay\Cache\EventListCache;
use Indybay\Cache\FeaturePageCache;
use Indybay\DB\FeaturePageDB;
use Indybay\Renderer\FeaturePageRenderer;

if (array_key_exists('preview', $_GET) && strlen($_GET['preview']) >
0) {
  if (array_key_exists('page_id', $_GET) && $_GET['page_id'] != '') {
    $page_id = $_GET['page_id'] + 0;
  } $feature_page_db_class = new FeaturePageDB();
  $feature_page_renderer_class = new FeaturePageRenderer(); $page_info =
  $feature_page_db_class->getFeaturePageInfo($page_id); $blurb_list =
  $feature_page_db_class->getCurrentBlurbList($page_id); $html =
  $feature_page_renderer_class->renderPage($blurb_list, $page_info); if (strpos($html, 'INDYBAY_HIGHLIGHTED_EVENTS') > 0) {
    $event_list_cache_class =
    new EventListCache(); $cached_events =
    $event_list_cache_class->loadCachedHighlightedEventsCacheForPage($page_id);
    $html = str_replace('INDYBAY_HIGHLIGHTED_EVENTS', $cached_events, $html);
  } echo $html;
}
else {
  $page_id = $GLOBALS['page_id'];
  $feature_page_cache_class = new FeaturePageCache(); $html =
  $feature_page_cache_class->loadCacheForFeaturePage($page_id);
  echo $html;
} ?>
<div class="flex flex--align-center flex--justify-both flex--wrap archivelink">
  <span>
    <a href="/syn/generate_rss.php?page_id=<?php echo $GLOBALS['page_id']; ?>&amp;include_posts=0&amp;include_blurbs=1"
    title="RSS feed"><img alt="RSS feed" 
      src="/im/rss-rounded.svg"
      class="rss mediaicon" width="12" height="12"></a>
  </span>
  <span>
    <a href="/archives/archived_blurb_list.php?page_id=<?php
    echo $GLOBALS['page_id']; ?>" title="Previous Stories">Previous Stories 
    <img 
      src="/im/arrow-next.svg" alt="next" 
      class="mediaicon pageicon" width="12" height="12"></a>
  </span>
</div>
<!-- END .archivelink -->
