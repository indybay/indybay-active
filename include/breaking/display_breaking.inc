<?php

/**
 * @file
 * Displays breaking news.
 */

$page = new Page('breaking_index');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
