# Indybay Active

**Live site: [www.indybay.org](https://www.indybay.org)**

## Synopsis

An open-publishing system developed by and for the San Francisco Bay Area
Independent Media Center.  Portions of indybay-active are based on sf-active.
This is unsupported pre-alpha software.  Use at your own risk.

## GitLab repo: (current Sept 2015)

https://gitlab.com/indybay/indybay-active/

## Containerization

Containerization is a work-in-progress, and is currently only used for
running a few automated tests.  Eventually the app should be fully
functional for development or production in a containerized environment.

## CI / CD

The following continuous integration jobs run at
https://gitlab.com/indybay/indybay-active/pipelines and are defined in the
.gitlab-ci.yml file:

* __build__ (manually triggered): Builds and pushes a docker image suitable for
running tests.  This docker image does not yet have all required dependencies
(command-line tools, etc.) for actually running the app.
* __test__ (automatically run): Runs JS and PHP tests (we need to add some! :)

## System requirements:
* [Composer](https://getcomposer.org/): 2.5+
* [MySQL](https://www.mysql.com/): 8.0+
* [Node](https://nodejs.org/): 18.13+
* [npm](https://www.npmjs.com/): 9.2+
* [PHP](https://www.php.net/): 8.2+

## Installation

1. Clone repo and install Composer and NPM packages:

    ```
    git clone https://gitlab.com/indybay/indybay-active/
    cd indybay-active/
    composer install
    npm install
    ```
    PHP extensions: gd, imagick, mbstring, mysqli

2. Copy `/classes/config/indybay.cfg.sample` to `/classes/config/indybay.cfg`
and update with local paths, db, etc

3. Make directories writable:
    * `/cache/`
    * `/cache/feature_page/recent_blurbs/`
    * `/cache/HTMLPurifier/`
    * `/cache/newswires/`
    * `/session/`
    * `/uploads/`
    * `/website/newsitems/`

4. Create symlink for non-existent `/website/uploads` to `/uploads/`

5. Set up `missing.php` as the 404 page.  Apache configuration for this would
be:

    `ErrorDocument 404 /missing.php`

6. Various commandline programs are needed to process uploads: convert,
exiftran, ffmpeg, ffmpegthumbnailer.

## Watch SCSS files and rebuild CSS

The `npm run watch` command can be used to watch SCSS files for changes
and automatically rebuild the CSS file.

## PHP Code Beautifier and Fixer

Syntax problems can be fixed automatically by running:

`./vendor/bin/phpcbf`

## Clear caches

  * Newswire: Click 'Regenerate All Newswires' link on Features admin page
  (or delete files/subdirectories in `/cache/newswires/`)
  * Newsitems: Delete subdirectories in `/website/newsitems/` (`/2017/`, etc)

## Category Updates

A redirect file allows changing page URL, adding new aliases, etc:

1. Edit `./classes/config/indybay.redirects` to add aliases

2. Move the actual page i.e. `git mv website/foobar website/foobaz`

3. Manually reload webserver settings

## Reset password

In terminal run: `php scripts/reset-password.php`

## Authors

indybay-active is authored and maintained by the SF Bay Area IMC collective:
indybay@lists.riseup.net

## License

This project is licensed under the AGPLv3 license - see the
[LICENSE.md](LICENSE.md) file for details
