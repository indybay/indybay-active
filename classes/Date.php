<?php

namespace Indybay;

/**
 * This class represents a date object.
 *
 * And should have all common functions related to manipulation of dates.
 *
 * In the past, PHP had no native Date function that could do things
 * like moving forward a day.
 */
class Date {

  /**
   * Timestamp.
   *
   * @var int
   */
  public $timestamp = 0;

  /**
   * Returns the hour portion of the date object.
   */
  public function getHour() {
    $datearray = getdate($this->timestamp);
    return $datearray['hours'];
  }

  /**
   * Returns the minute portion of the date object.
   */
  public function getMinute() {
    $datearray = getdate($this->timestamp);
    return $datearray['minutes'];
  }

  /**
   * Returns the day portion of the date object.
   */
  public function getDay() {
    $datearray = getdate($this->timestamp);
    return $datearray['mday'];
  }

  /**
   * Returns the month portion of the date object.
   */
  public function getMonth() {
    $datearray = getdate($this->timestamp);
    return $datearray['mon'];
  }

  /**
   * Returns the year portion of the dat object.
   */
  public function getYear() {
    $datearray = getdate($this->timestamp);
    return $datearray['year'];
  }

  /**
   * Sets the time based on the system offset, then the local offset.
   */
  public function setTimeZone() {
    $this->setDateToNow();
    $offset = 0;

    if (array_key_exists('time_diff', $GLOBALS)) {
      $offset = $GLOBALS['time_diff'] + 0;
    }

    if (array_key_exists('server_time_offset', $GLOBALS)) {
      $offset = $offset - ($GLOBALS['server_time_offset'] + 0);
    }
    $this->timestamp += ($offset * 3600);
  }

  /**
   * Returns formatted date.
   */
  public function getFormattedDate() {
    $form_date = date('D, M d, Y', $this->timestamp);
    return $form_date;
  }

  /**
   * Returns formatted time.
   */
  public function getFormattedTime() {
    $form_time = date('h:i A', $this->timestamp);
    return $form_time;
  }

  /**
   * Returns the text name of the day of the week ("Mon", "Tue" etc..)
   */
  public function getDayOfWeek() {
    if (date('D', $this->timestamp) == 'Sun') {
      return 0;
    }
    elseif (date('D', $this->timestamp) == 'Mon') {
      return 1;
    }
    elseif (date('D', $this->timestamp) == 'Tue') {
      return 2;
    }
    elseif (date('D', $this->timestamp) == 'Wed') {
      return 3;
    }
    elseif (date('D', $this->timestamp) == 'Thu') {
      return 4;
    }
    elseif (date('D', $this->timestamp) == 'Fri') {
      return 5;
    }
    elseif (date('D', $this->timestamp) == 'Sat') {
      return 6;
    }
    else {
      return 'error';
    }
  }

  /**
   * Returns the date of the beginning of the week.
   */
  public function getWeekname() {
    $newdate = $this->clonedate();
    $newdate->findStartOfWeek();
    $return_string = $newdate->getYear();
    $return_string = $return_string . '_';
    $return_string = $return_string . $newdate->getMonth();
    $return_string = $return_string . '_';
    $return_string = $return_string . $newdate->getDay();
    return $return_string;
  }

  /**
   * Sets this date object to point to teh current date.
   */
  public function setDateToNow() {
    $this->timestamp = time();
  }

  /**
   * Moves date object to beginning of week (compared to its current date)
   */
  public function findStartOfWeek() {
    $this->moveToStartOfDay();
    while ($this->getDayOfWeek() > 0) {
      $this->moveForwardDays(-1);
    }
  }

  /**
   * Moves date object to beginning of day that it currently represents.
   */
  public function moveToStartOfDay() {
    $hour = 0;
    $minute = 0;
    $second = 0;
    $day = $this->getDay();
    $month = $this->getMonth();
    $year = $this->getYear();
    $newtimestamp = mktime($hour, $minute, $second, $month, $day, $year);
    $this->timestamp = $newtimestamp;
  }

  /**
   * Moves the date object forward N days (can be negative to go backwards)
   */
  public function moveForwardDays($n) {
    $hour = $this->getHour();
    $minute = $this->getMinute();
    $second = 0;
    $day = $this->getDay() + $n;
    $month = $this->getMonth();
    $year = $this->getYear();
    $newtimestamp = mktime($hour, $minute, $second, $month, $day, $year);
    $this->timestamp = $newtimestamp;
  }

  /**
   * Moves teh date object forward N weeks (can be negative to go backwards)
   */
  public function moveForwardWeeks($n) {
    $this->moveForwardDays(7 * $n);
  }

  /**
   * Returns a new Date object given an old one.
   */
  public function clonedate() {
    $newdate = new Date();
    $newdate->timestamp = $this->timestamp;
    return $newdate;
  }

  /**
   * Returns a date in the format needed for inserts by MySQL.
   */
  public function getSqlDate() {
    $year = $this->getYear();
    $month = $this->getMonth();
    if (strlen($month) < 2) {
      $month = '0' . $month;
    }

    $day = $this->getDay();

    if (strlen($day) < 2) {
      $day = '0' . $day;
    }
    return $year . '-' . $month . '-' . $day . ' 00:00:00';
  }

  /**
   * Not yet written.
   */
  public function getSqlTime() {
  }

  /**
   * Retuns the Unix timestamp of the given date.
   */
  public function getUnixTime() {
    return $this->timestamp;
  }

  /**
   * Sets the datetime of the date object based off an SQL datetime.
   */
  public function setTimeFromSqlTime($sqltime) {

    $year = substr($sqltime, 0, 4) + 0;

    $month = substr($sqltime, 5, 2) + 0;

    $day = substr($sqltime, 8, 2) + 0;

    $hour = substr($sqltime, 11, 2) + 0;

    $minute = substr($sqltime, 14, 2) + 0;

    $this->setTime($hour, $minute, $day, $month, $year);

  }

  /**
   * Sets the datetime of the date object based off a unix time stamp.
   */
  public function setTimeFromUnixTime($unixtime) {
    $this->timestamp = $unixtime;
  }

  /**
   * Not yet written.
   */
  public function setTimeFromString($string_time) {
  }

  /**
   * Sets the Date object given the hour, minute, day, month, and  year.
   */
  public function setTime($newhour, $newminute, $newday, $newmonth, $newyear) {
    $newtimestamp = mktime((int) $newhour, (int) $newminute, 0, (int) $newmonth, (int) $newday, (int) $newyear);
    $this->timestamp = $newtimestamp;
  }

}
