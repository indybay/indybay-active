<?php

namespace Indybay;

/**
 * A convenience class to make it easy to write RSS 2.0 classes.
 *
 *  A convenience class to make it easy to write RSS 2.0 classes, patched to not
 *  longer print directly but to gather everything in $GLOBALS['rss-print'].
 *  some other patches to allow things we want :-)
 *
 * @package sf-active
 * @subpackage syndication
 * @original-author Edd Dumbill <edd+rsswriter@usefulinc.com>
 * @edited-by lotus <lotus@indymedia.org>
 */
class RSS2Writer {

  /*
  A convenience class to make it easy to write RSS classes
  Edd Dumbill <mailto:edd+rsswriter@usefulinc.com>
  Revision 1.1  2001/05/17 18:17:46  edmundd
  Start of a convenience library to help RSS1.0 creation
   */

  /**
   * Channel info array.
   */
  protected array $chaninfo;

  /**
   * Channel URI.
   */
  protected string $channelURI;

  /**
   * Image array.
   */
  protected array $image;

  /**
   * Items array.
   */
  protected array $items;

  /**
   * Modules array.
   */
  protected array $modules;

  /**
   * Return value string.
   */
  protected string $returnValue;

  /**
   * Website URL.
   */
  protected string $website;

  /**
   * Class constructor.
   *
   * @param string $uri
   *   Website URI.
   * @param string $title
   *   The title of yur feed.
   * @param string $description
   *   Description of your feed.
   * @param string $about
   *   Channel URI.
   * @param array $meta
   *   All other meta info.
   */
  public function __construct($uri, $title, $description, $about, array $meta = []) {
    // Constructor.
    $this->chaninfo = [];
    $this->website = $uri;
    $this->chaninfo['link'] = $uri;
    $this->chaninfo['description'] = $description;
    $this->chaninfo['title'] = $title;
    $this->items = [];
    $this->modules = [
      'dc' => 'http://purl.org/dc/elements/1.1/',
      'itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd',
      'media' => 'http://search.yahoo.com/mrss/',
      'atom' => 'http://www.w3.org/2005/Atom',
    ];
    $this->channelURI = $about;
    foreach ($meta as $key => $value) {
      $this->chaninfo[$key] = $value;
    }
    $this->returnValue = '';
  }

  /**
   * Sets additional modules ("dc" is build in)
   *
   * @param string $prefix
   *   Name of the module.
   * @param string $uri
   *   The URL declaring the module.
   */
  public function useModule($prefix, $uri) {
    $this->modules[$prefix] = $uri;
  }

  /**
   * Sets the image of your feed.
   *
   * @param string $imgURI
   *   URL of your image.
   * @param string $imgAlt
   *   Alt. message of your image.
   * @param int $imgWidth
   *   Width of the image.
   * @param int $imgHeight
   *   Height of the image.
   */
  public function setImage($imgURI, $imgAlt, $imgWidth = 88, $imgHeight = 31) {
    $this->image = [
      'uri' => $imgURI,
      'title' => $imgAlt,
      'width' => $imgWidth,
      'height' => $imgHeight,
    ];
  }

  /**
   * Adds an item to your feed.
   *
   * @param string $uri
   *   URL of the item.
   * @param string $title
   *   Title of the item.
   * @param array $meta
   *   All other data of your item (like dc: stuff and so)
   */
  public function addItem($uri, $title, array $meta = []) {
    $item = [
      'uri' => $uri,
      'link' => $uri,
      'title' => $this->deTag($title),
    ];
    foreach ($meta as $key => $value) {
      if ($key == 'description' || $key == 'dc:description') {
        $value = $this->deTag($value);
      }
      $item[$key] = $value;
    }
    $this->items[] = $item;
  }

  /**
   * Renders all information into a string containing your feed.
   */
  public function serialize() {
    $this->preamble();
    $this->channelinfo();

    // Lotus - these aren't present in the sample feed i'm looking at
    // $this->image();
    $this->items();
    $this->postamble();
    return $this->returnValue;
  }

  /**
   * Strips tags.
   *
   * @param string $in
   *   Input.
   */
  public function deTag($in) {
    while (preg_match('/<[^>]+>/', $in)) {
      $in = preg_replace('/<[^>]+>/', '', $in);
    }
    return $in;
  }

  /**
   * Renders the head of your feed.
   */
  public function preamble() {

    $this->returnValue .= '<?xml version="1.0" ?>
<rss version="2.0" 
';
    foreach ($this->modules as $prefix => $uri) {
      $this->returnValue .= "         xmlns:{$prefix}=\"{$uri}\"\n";
    }
    $this->returnValue .= ">\n\n<channel>";
  }

  /**
   * Renders the <channel> part of your feed.
   *
   * Lotus - not used for 2.0 feed.
   */
  public function channelinfo() {

    $this->returnValue .= '';
    $i = $this->chaninfo;
    foreach (['title', 'link', 'dc:source', 'description', 'dc:language', 'dc:publisher', 'language',
      'dc:creator', 'dc:rights', 'ttl', 'itunes:author', 'itunes:subtitle', 'itunes:explicit',
    ] as $f) {
      if (isset($i[$f])) {
        $this->returnValue .= "    <{$f}>" . htmlspecialchars($i[$f]) . "</{$f}>\n";
      }
    }
    foreach (['itunes:category'] as $f) {
      if (isset($i[$f])) {
        $this->returnValue .= "    <{$f} text=\"" . htmlspecialchars($i[$f]) . "\" />\n";
      }
    }
    $this->returnValue .= '    <atom:link href="' . SERVER_URL . htmlspecialchars($_SERVER['REQUEST_URI']) . '" rel="self" type="application/rss+xml" />' . "\n";
  }

  /**
   * Renders the image part.
   *
   * Lotus - not used for 2.0 feed.
   */
  public function image() {

    if (isset($this->image)) {
      $this->returnValue .= '  <image rdf:about="' . htmlspecialchars($this->image['uri']) . "\">\n";
      $this->returnValue .= '     <title>' . htmlspecialchars($this->image['title']) . "</title>\n";
      $this->returnValue .= '     <url>' . htmlspecialchars($this->image['uri']) . "</url>\n";
      $this->returnValue .= '     <link>' . htmlspecialchars($this->website) . "</link>\n";
      if ($this->chaninfo['description']) {
        $this->returnValue .= '     <dc:description>' . htmlspecialchars($this->chaninfo['description']) .
        "</dc:description>\n";
        $this->returnValue .= "  </image>\n\n";
      }
    }
  }

  /**
   * Renders the bottom part.
   */
  public function postamble() {

    $this->returnValue .= '</channel></rss>';
  }

  /**
   * Renders all items.
   */
  public function items() {
    foreach ($this->items as $item) {
      $this->returnValue .= "  <item>\n";
      foreach ($item as $key => $value) {
        if ($key !== 'uri' && $key !== 'content:encoded' && $key !== 'dcterms:hasPart' && $key !== 'description' && $key !== 'enclosure' && $key !== 'guid' && $key !== 'media:thumbnail') {
          if (is_array($value)) {
            foreach ($value as $v1) {
              $this->returnValue .= "    <{$key}>" . htmlspecialchars($v1) . "</{$key}>\n";
            }
          }
          else {
            $this->returnValue .= "    <{$key}>" . htmlspecialchars($value) . "</{$key}>\n";
          }
        }
        if ($key == 'content:encoded' or $key == 'description') {
          $this->returnValue .= "    <{$key}><![CDATA[" . $value . "]]></{$key}>\n";
        }
        if ($key == 'dcterms:hasPart' && strlen($value) > 0) {
          $this->returnValue .= "    <{$key} rdf:resource=\"" . $value . "\" />\n";
        }
        if ($key == 'enclosure' && strlen($value) > 0) {

          [$url, $mime, $size] = mb_split(',', $value);
          if (strlen($url) > 0) {
            $this->returnValue .= "   <enclosure url=\"$url\" length=\"$size\" type=\"$mime\" />\n";
          }
        }
        if ($key == 'guid') {
          $this->returnValue .= '   <guid isPermaLink="true">' . $value . '</guid>' . "\n";
        }
        if ($key == 'media:thumbnail') {
          $this->returnValue .= '   <media:thumbnail url="' . $value . '" />' . "\n";
        }
      }
      $this->returnValue .= "  </item>\n\n";
    }

  }

}
