<?php

namespace Indybay\Syndication;

use Indybay\DB\FeaturePageDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemVersionDB;
use Indybay\DB\SearchDB;
use Indybay\RSS2Writer;
use Indybay\RSSWriter;
use Indybay\Renderer\NewsItemRenderer;
use Indybay\Renderer\NewsItemVersionRenderer;

/**
 * RSS utilities.
 *
 * Written January 2006
 *  * Modification Log:
 * 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class RSSUtil extends NewsItemVersionRenderer {

  /**
   * Test function to be phased out.
   */
  public function makeNewsItemRssString(
    $num_results,
    $news_item_status_restriction,
    $include_posts,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $include_full_text,
    $rss_version,
    $search = '',
  ) {

    // Dont syndicate nonpushed blurbs.
    if ($include_posts + 0 == 0 && $include_events + 0 == 0 && $include_blurbs + 0 == 1) {
      $news_item_status_restriction = NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED;
    }

    $page_number = 0;
    $parent_item_id = 0;
    $page_size = $num_results * 2;
    $search_db_class = new SearchDB();

    $include_attachments = 0;
    $include_comments = 0;
    if ($media_type_grouping_id + 0 == 3 || $media_type_grouping_id + 0 == 4) {
      $include_attachments = 1;
    }

    $results = $search_db_class->standardSearch($page_size, $page_number, $search, $news_item_status_restriction,
            $include_posts, $include_comments, $include_attachments, $include_events, $include_blurbs, $media_type_grouping_id, $topic_id, $region_id, $parent_item_id);

    if ($topic_id + 0 == FRONT_PAGE_CATEGORY_ID) {
      $topic_id = 0;
    }
    if ($region_id + 0 == FRONT_PAGE_CATEGORY_ID) {
      $region_id = 0;
    }
    $page_id = 0;
    if ($topic_id + 0 == 0 && $region_id + 0 == 0) {
      $shortname = SHORT_SITE_NAME;
      $longname = SITE_NAME;
      $rss_url = FULL_ROOT_URL;
    }
    else {
      if ($region_id + 0 != 0) {
        $page_id = $region_id;
      }
      if ($topic_id + 0 != 0) {
        $page_id = $topic_id;
      }
      if ($page_id != 0) {

        $feature_page_class = new FeaturePageDB();
        $page_info = $feature_page_class->getFeaturePageInfo($page_id);

        $shortname = strtolower(SHORT_SITE_NAME . ' ');
        $longname = SHORT_SITE_NAME . ' ';
        if ($page_id == 60) {
          $shortname = '';
          $longname = '';
        }
        $shortname .= $page_info['short_display_name'] ?? '';
        $longname .= $page_info['long_display_name'] ?? '';

        $rss_url = FULL_ROOT_URL . ($page_info['relative_path'] ?? '');

      }
    }
    if ($include_posts + 0 == 0 && $include_events + 0 == 0 && $include_blurbs + 0 == 1) {
      $shortname .= ' features';
      $longname .= ' Features';
    }
    elseif ($include_posts + 0 == 1) {
      $shortname .= ' newswire';
      $longname .= ' Newswire';
    }
    elseif ($include_events + 0 == 1 && $include_blurbs + 0 == 0) {
      $shortname .= ' events';
      $longname .= ' Events';
    }

    if ($media_type_grouping_id + 0 == 2) {
      $shortname .= ' (photos)';
      $longname .= ' (photos)';
    }
    elseif ($media_type_grouping_id + 0 == 3) {
      $shortname .= ' (audio)';
      $longname .= ' (audio)';
    }
    elseif ($media_type_grouping_id + 0 == 4) {
      $shortname .= ' (video)';
      $longname .= ' (video)';
    }

    $author = SITE_NAME;
    if ($page_id + 0 == 60) {
      $author = 'Santa Cruz Indymedia';
    }

    $str = $this->convertNewsItemListIntoRssString($shortname, $longname, $rss_url, $author, $results, $include_full_text, $rss_version, $num_results);

    return $str;
  }

  /**
   * Creates and caches an rss feed (without full content).
   */
  public function convertNewsItemListIntoRssString($rss_shortname, $rss_long_name, $rss_url, $author, $results, $include_full_text, $rss_version, $num_results) {
    $news_item_renderer = new NewsItemRenderer();

    if (!is_array($results) || count($results) == 0) {
      return '';
    }

    $language = 'en-US';
    $webroot_url = SERVER_URL;
    $GLOBALS['print_rss'] = '';

    $xml_logo = $GLOBALS['xml_logo'];

    if ($rss_version == 1) {
      $rss = new RSSWriter($rss_url, $rss_shortname, $rss_long_name, $rss_url . '/syn/', [
        'dc:publisher' => $author,
        'dc:creator' => $author,
        'dc:language' => $language,
      ]);
      $rss->useModule('dcterms', 'http://purl.org/dc/terms/');
      $rss->useModule('content', 'http://purl.org/rss/1.0/modules/content/');
    }
    else {
      $rss = new RSS2Writer($rss_url, $rss_shortname, $rss_long_name, $webroot_url . '/syn/', [
        'dc:publisher' => $author,
        'dc:creator' => $author,
        'dc:language' => $language,
      ]);
      $rss->useModule('dcterms', 'http://purl.org/dc/terms/');
      $rss->useModule('content', 'http://purl.org/rss/1.0/modules/content/');
    }

    $rss->setImage($xml_logo, $rss_shortname);
    $news_item_version_db_class = new NewsItemVersionDB();

    $already_added_title_array = [];

    $numrows = 0;
    foreach ($results as $row) {
      $row = str_replace(['src="/', 'href="/'], [
        "src=\"$webroot_url/",
        "href=\"$webroot_url/",
      ], $row);
      $link = FULL_ROOT_URL . substr($news_item_renderer->getRelativeWebPathFromItemInfo($row), 1);
      $title1 = trim(strip_tags($row['title1']));
      $title2 = trim(strip_tags($row['title2']));

      if (($title1 && isset($already_added_title_array[$title1])) || ($title2 && isset($already_added_title_array[$title2]))) {
        continue;
      }
      else {
        $already_added_title_array[$title1] = 1;
        $already_added_title_array[$title2] = 1;
      }
      $numrows++;
      if ($numrows > $num_results) {
        break;
      }

      if (strlen(trim($title2)) == 0) {
        $title2 = $title1;
      }
      if (strlen(trim($title1)) == 0) {
        $title2 = $title1;
      }
      if (NEWS_ITEM_TYPE_ID_EVENT == $row['news_item_type_id']) {
        if (!empty($row['displayed_timestamp'])) {
          $title1 = date('l n/j: ', $row['displayed_timestamp']) . $title1;
        }
        elseif (!empty($row['displayed_date'])) {
          $title1 = date_create($row['displayed_date'])->format('l n/j: ') . $title1;
        }
      }
      $displayed_author_name = trim($row['displayed_author_name']);

      // $a=str_replace(array('&','<','>',chr(19),chr(20),chr(24),chr(25),chr(28),chr(29),chr(128),chr(145),chr(146),chr(147),chr(148),chr(149),chr(150),chr(151),chr(133)),array('&amp;','&lt;','&gt;','&#8211;','&#8212;','"',"'",'"','"','&#8364;','&#8216;','&#8217;','&#8220;','&#8221;','&#8226;','&#8211;','&#8212;','&#8230;'),$row['summary']);.
      if ($row['news_item_type_id'] == 2 && $row['summary'] == '') {
        $row['summary'] = $row['text'];
      }
      $summary = str_replace('href="/', 'href="' . FULL_ROOT_URL, $row['summary']);
      $summary = str_replace('src="/', 'src="' . FULL_ROOT_URL, $summary);
      $summary = trim($summary);

      $text = $include_full_text ? $row['text'] : '';
      if (strlen($text) > 0) {
        if (!preg_match('/^./us', $text)) {
          $text = @iconv('Windows-1252', 'utf-8', $text);
        }
        if ($row['is_text_html']) {
          $text = $this->htmlPurify($text);
        }
        else {
          $text = $this->cleanupText($text);
        }
      }

      $date = gmdate('Y-m-d\TH:i:s\Z', $row['creation_timestamp']);
      $modified = gmdate('Y-m-d\TH:i:s\Z', $row['creation_timestamp']);
      // Fix me: we used to put the $file url here but..?
      $file = '';
      if ($rss_version == 1) {
        $item_array = [
          'dcterms:alternative' => $title2,
          'description' => $summary,
          'dc:date' => $date,
          'dcterms:modified' => $modified,
          'dc:creator' => $displayed_author_name,
          'dc:language' => $language,
          'content:encoded' => $text,
          'dcterms:hasPart' => $file,
        ];
      }
      else {
        $item_array = [
          'dcterms:alternative' => $title2,
          'description' => $summary,
          'dc:date' => $date,
          'dcterms:modified' => $modified,
          'dc:creator' => $displayed_author_name,
          'dc:language' => $language,
          'dcterms:hasPart' => $file,
          'guid' => $link,
          'content:encoded' => $text,
        ];
      }
      $category_list = $news_item_version_db_class->getNewsItemCategoryInfo($row['news_item_id'], 0);
      $subject_array = [];
      foreach ($category_list as $catrow) {
        array_push($subject_array, $catrow['name']);
      }
      $item_array['dc:subject'] = $subject_array;

      if ($row['media_attachment_id'] != 0) {
        $media_attachment_db_class = new MediaAttachmentDB();
        if ($media_attachment_info = $media_attachment_db_class->getMediaAttachmentInfo($row['media_attachment_id'])) {
          $mime_type = $media_attachment_info['mime_type'];
          $item_array['dc:format'] = $mime_type;

          if ($rss_version != 1) {
            $rendered_video_info = $media_attachment_db_class->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
            if (is_array($rendered_video_info)) {
              $item_array['media:thumbnail'] = SERVER_URL . UPLOAD_URL . $rendered_video_info['relative_path'] . $rendered_video_info['file_name'];
            }
            $file_name = $media_attachment_info['file_name'];
            $relative_path = $media_attachment_info['relative_path'];
            $file_dir = UPLOAD_PATH . '/' . $media_attachment_info['relative_path'];
            $full_file_path = $file_dir . $file_name;
            $file_url = FULL_ROOT_URL . 'uploads/' . $relative_path . $file_name;
            $file_size = 0;
            if (file_exists($full_file_path)) {
              $file_size = filesize($full_file_path);
            }
            if ($file_size > 0) {
              $item_array['enclosure'] = $file_url . ',' . $mime_type . ',' . $file_size;
            }
          }
        }
      }

      $rss->addItem($link, $title1, $item_array);
    }
    $print = $rss->serialize();

    return $print;
  }

}
