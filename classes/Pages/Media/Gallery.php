<?php

namespace Indybay\Pages\Media;

use Indybay\Pages\Admin\Article\ArticleList;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Renderer\MediaAttachmentRenderer;

/**
 * Renders a photo gallery.
 */
class Gallery extends ArticleList {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Quick fix for idiotic XSS vulns.
    foreach ($_GET as $key => $value) {
      $_GET[$key] = htmlspecialchars($value);
    }
    $_GET['media_type_grouping_id'] = 2;
    if (isset($_GET['location_id']) && !isset($_GET['region_id'])) {
      $_GET['region_id'] = $_GET['location_id'];
    }
    if (!isset($_GET['include_attachments']) || $_GET['include_attachments'] == '') {
      $_GET['include_attachments'] = 1;
    }
    if (!isset($_GET['news_item_status_restriction']) && empty($_GET['region_id']) && empty($_GET['topic_id'])) {
      $_GET['news_item_status_restriction'] = 1155;
    }
    parent::execute();
  }

  /**
   * Renders row.
   */
  public function renderRow($cell_counter, $row, $parent_row, $tr, $article_renderer, $article_cache_class, $display_options, $article_db_class) {

    $parent_item_id = $row['parent_item_id'];
    $news_item_id = $row['news_item_id'];
    $media_attachment_renderer = new MediaAttachmentRenderer();

    if ($parent_item_id != $news_item_id && $parent_item_id != 0) {
      $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($parent_row['news_item_id'], $parent_row['creation_timestamp']);
      $searchlink .= '#' . $news_item_id;
    }
    else {
      $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);
    }
    $gridhtml = '';
    $gridhtml .= '<div class="bg-grey"><a href="' . $searchlink . '">';

    if ($_GET['gallery_display_option_id'] + 0 > 1) {
      $gridhtml .= $media_attachment_renderer->renderSmallThumbnailFromNewsItemInfo($row, ' ');
    }
    else {
      $t = substr($row['title1'], 0, 35);
      if (strlen($row['title1']) > 35) {
        $t .= '...';
      }
      $gridhtml .= '<span class="origimagetitle">' . $t . '</span>';
      $gridhtml .= $media_attachment_renderer->renderMediumThumbnailFromNewsItemInfo($row, ' ');
    }
    $gridhtml .= '</a></div>';

    return $gridhtml;
  }

  /**
   * Returns search template.
   */
  public function getSearchTemplate() {
    return 'article/gallery_search.tpl';
  }

  /**
   * Returns news item status select list.
   */
  public function getNewsItemStatusSelectList() {
    $display_options = [];
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlighted-Local';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted-NonLocal';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted';
    // $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]="All";.
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_OTHER] = 'All';
    return $display_options;
  }

  /**
   * Disallow bulk classify.
   */
  public function bulkClassify($posted_fields) {
    echo 'Classifictions Requires Being Logged Into The Admin System!!';
  }

  /**
   * Sets additional values.
   */
  public function setAdditionalValues($search_param_array) {
    $article_renderer = new ArticleRenderer();
    if (!array_key_exists('gallery_display_option_id', $_GET)) {
      $_GET['gallery_display_option_id'] = 0;
    }
    // Originally displayed in options below, only relevant for desktop:
    // (4x8) (4x16) (6x8) (6x16)
    $gallery_display_options = [];
    $gallery_display_options[0] = 'large images (32)';
    $gallery_display_options[1] = 'large images (64)';
    $gallery_display_options[2] = 'small images (48)';
    $gallery_display_options[3] = 'small images (96)';
    $search_param_array['TPL_GALLERY_DISPLAY_OPTIONS'] = $article_renderer->makeSelectForm('gallery_display_option_id', $gallery_display_options, $_GET['gallery_display_option_id'] + 0);
    return $search_param_array;
  }

  /**
   * Gets row size.
   */
  public function getRowSize() {
    $ret = 3;
    if (!array_key_exists('gallery_display_option_id', $_GET)) {
      $_GET['gallery_display_option_id'] = 0;
    }
    if ($_GET['gallery_display_option_id'] + 0 == 0) {
      $ret = 4;
    }
    elseif ($_GET['gallery_display_option_id'] == 1) {
      $ret = 4;
    }
    elseif ($_GET['gallery_display_option_id'] == 2) {
      $ret = 6;
    }
    elseif ($_GET['gallery_display_option_id'] == 3) {
      $ret = 6;
    }
    return $ret;
  }

  /**
   * Gets page size.
   */
  public function getPageSize() {
    $ret = 32;
    if (!array_key_exists('gallery_display_option_id', $_GET) || $_GET['gallery_display_option_id'] + 0 == 0) {
      $ret = 32;
    }
    elseif ($_GET['gallery_display_option_id'] == 1) {
      $ret = 64;
    }
    elseif ($_GET['gallery_display_option_id'] == 2) {
      $ret = 48;
    }
    elseif ($_GET['gallery_display_option_id'] == 3) {
      $ret = 96;
    }
    return $ret;
  }

}
