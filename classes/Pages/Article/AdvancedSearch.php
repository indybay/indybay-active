<?php

namespace Indybay\Pages\Article;

use Indybay\DB\CategoryDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Translate;

/**
 * Advanced search.
 */
class AdvancedSearch extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $tr = new Translate();
    $tr->createTranslateTable('article');

    if (!isset($_GET['include_posts']) && !isset($_GET['submitted_search'])) {
      $include_posts = 1;
    }
    else {
      $include_posts = $_GET['include_posts'] + 0;
    }

    if (isset($_GET['media_type_grouping_id'])) {
      $media_type_grouping_id = (int) $_GET['media_type_grouping_id'];
    }
    else {
      $media_type_grouping_id = 0;
    }

    if (isset($_GET['include_comments'])) {
      $include_comments = (int) $_GET['include_comments'];
    }
    else {
      $include_comments = 1;
    }

    if (!isset($_GET['include_attachments']) && $media_type_grouping_id != 0 && !isset($_GET['submitted_search'])) {
      $include_attachments = 1;
    }
    elseif (isset($_GET['include_attachments'])) {
      $include_attachments = (int) $_GET['include_attachments'];
    }
    else {
      $include_attachments = 1;
    }

    if (!isset($_GET['include_events']) && !isset($_GET['submitted_search'])) {
      $include_events = 1;
    }
    elseif (isset($_GET['include_events'])) {
      $include_events = (int) $_GET['include_events'];
    }
    else {
      $include_events = 1;
    }

    if (isset($_GET['include_blurbs'])) {
      $include_blurbs = (int) $_GET['include_blurbs'];
    }
    else {
      $include_blurbs = 1;
    }

    if (isset($_GET['topic_id'])) {
      $topic_id = (int) $_GET['topic_id'];
    }
    else {
      $topic_id = 0;
    }

    if (isset($_GET['region_id'])) {
      $region_id = (int) $_GET['region_id'];
    }
    else {
      $region_id = 0;
    }

    if (isset($_GET['page_id'])) {
      $page_id = (int) $_GET['page_id'];
    }
    else {
      $page_id = 0;
    }

    if ($page_id > 0) {
      $feature_page_db = new FeaturePageDB();
      $topic_id = $feature_page_db->getFirstTopicForPage($page_id);
      $region_id = $feature_page_db->getFirstRegionForPage($page_id);
    }
    if (isset($_GET['news_item_status_restriction'])) {
      $news_item_status_restriction = (int) $_GET['news_item_status_restriction'];
    }
    else {
      $news_item_status_restriction = 690690;
    }

    $media_attachment_db_class = new MediaAttachmentDB();
    $article_renderer = new ArticleRenderer();
    $category_db_class = new CategoryDB();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_topic_options, $cat_international_options);
    $cat_topic_options[0] = 'All Topics';
    $this->tkeys['CAT_TOPIC_SELECT'] = $article_renderer->makeSelectForm('topic_id', $cat_topic_options, $topic_id);
    $cat_region_options[0] = 'All Regions';
    $cat_region_options = $this->arrayMergeWithoutRenumbering($cat_region_options, $category_db_class->getCategoryInfoListByType(1, 0));
    $this->tkeys['CAT_REGION_SELECT'] = $article_renderer->makeSelectForm('region_id', $cat_region_options, $region_id);

    $display_options = $this->getNewsItemStatusSelectList();
    // $display_options[0] = "All Statuses";.
    $medium_options = $media_attachment_db_class->getMediumOptions();
    $medium_options[0] = 'All Media';

    if (isset($_GET['search'])) {
      $this->tkeys['LOCAL_SEARCH'] = htmlspecialchars($_GET['search']);
    }
    else {
      $this->tkeys['LOCAL_SEARCH'] = '';
    }

    $this->tkeys['SEARCH_MEDIUM'] = $article_renderer->makeSelectForm('media_type_grouping_id', $medium_options, $media_type_grouping_id);

    $this->tkeys['LOCAL_CHECKBOX_COMMENTS'] = $article_renderer->makeBooleanCheckboxForm('include_comments', $include_comments);
    $this->tkeys['LOCAL_CHECKBOX_ATTACHMENTS'] = $article_renderer->makeBooleanCheckboxForm('include_attachments', $include_attachments);
    $this->tkeys['LOCAL_CHECKBOX_EVENTS'] = $article_renderer->makeBooleanCheckboxForm('include_events', $include_events);
    $this->tkeys['LOCAL_CHECKBOX_POSTS'] = $article_renderer->makeBooleanCheckboxForm('include_posts', $include_posts);
    $this->tkeys['LOCAL_CHECKBOX_BLURBS'] = $article_renderer->makeBooleanCheckboxForm('include_blurbs', $include_blurbs);

    $this->tkeys['SEARCH_DISPLAY'] = $article_renderer->makeSelectForm('news_item_status_restriction', $display_options, $news_item_status_restriction);

    $date_options = [];
    $date_options[0] = 'Don\'t Restrict Dates';
    $date_options['creation_date'] = 'Restrict By Date Posted';
    $date_options['displayed_date'] = 'Restrict By Event Date';
    if (isset($_GET['search_date_type'])) {
      $search_type = htmlspecialchars($_GET['search_date_type']);
    }
    else {
      $search_type = '';
    }
    $this->tkeys['SEARCH_DATE_TYPE_SELECT'] = $article_renderer->makeSelectForm('search_date_type', $date_options, $search_type);

    $date_range_start_day = 0;
    if (isset($_GET['date_range_start_day'])) {
      $date_range_start_day = (int) $_GET['date_range_start_day'];
    }
    if ($date_range_start_day == 0) {
      $date_range_start_day = date('j');
    }
    $date_range_start_month = 0;
    if (isset($_GET['date_range_start_month'])) {
      $date_range_start_month = (int) $_GET['date_range_start_month'];
    }
    if ($date_range_start_month == 0) {
      $date_range_start_month = date('n');
    }
    $date_range_start_year = 0;
    if (isset($_GET['date_range_start_year'])) {
      $date_range_start_year = (int) $_GET['date_range_start_year'];
    }
    if ($date_range_start_year == 0) {
      $date_range_start_year = date('Y');
    }

    $date_range_end_day = 0;
    if (isset($_GET['date_range_end_day'])) {
      $date_range_end_day = (int) $_GET['date_range_end_day'];
    }
    if ($date_range_end_day == 0) {
      $date_range_end_day = ('j');
    }
    $date_range_end_month = 0;
    if (isset($_GET['date_range_end_month'])) {
      $date_range_end_month = (int) $_GET['date_range_end_month'];
    }
    if ($date_range_end_month == 0) {
      $date_range_end_month = date('n');
    }
    $date_range_end_year = 0;
    if (isset($_GET['date_range_end_year'])) {
      $date_range_end_year = (int) $_GET['date_range_end_year'];
    }
    if ($date_range_end_year == 0) {
      $date_range_end_year = date('Y');
    }
    $years = date('Y') - 2000;
    $this->tkeys['date_between_start'] = $article_renderer->makeDateSelectForm('date_range_start', $years, 2, $date_range_start_day, $date_range_start_month, $date_range_start_year);
    $this->tkeys['date_between_end'] = $article_renderer->makeDateSelectForm('date_range_end', $years, 2, $date_range_end_day, $date_range_end_month, $date_range_end_year);
  }

  /**
   * Gets news item status select list.
   */
  public function getNewsItemStatusSelectList() {
    $display_options = [];
    $display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN] = 'All';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlighted-Local';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted-NonLocal';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted';
    $display_options[NEWS_ITEM_STATUS_ID_NEW *
    NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED *
    NEWS_ITEM_STATUS_ID_OTHER * NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = 'Other';
    // $display_options[NEWS_ITEM_STATUS_ID_HIDDEN*NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]="Hidden";.
    return $display_options;
  }

  /**
   * Reject bulk classification requests.
   */
  public function bulkClassify() {
    echo 'Classifictions Requires Being Logged Into The Admin System!!';
  }

}
