<?php

namespace Indybay\Pages\Article;

/**
 * Class for publish page.
 */
class Mpublish extends Publish {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    if (is_array($_POST)) {
      // print_r($_POST);
      if ($_POST['title'] <> '') {
        $_POST['title1'] = $_POST['title'];
      }
      if ($_POST['author'] <> '') {
        $_POST['displayed_author_name'] = $_POST['author'];
      }
      $posted_fields = $this->cleanPostedData($_POST);
      // print_r($_POST);
    }
    // Test connection.
    $db_class = new DB();
    $db_class->getConnection();

    $this->tkeys['site_status'] = 'UKNOWN STATUS';

    if ($posted_fields['publish'] != '') {
      if ($posted_fields['is_text_html']) {
        $posted_fields['is_summary_html'] = TRUE;
      }
      if ($this->validatePost($posted_fields)) {
        $article_cache_class = new ArticleCache();
        $logdb = new LogDB();
        $logdb->addLogEntryBeforePublish();

        $associated_attachment_ids_array = $this->processUploads($posted_fields);

        $posted_fields = $this->cleanPostedData($posted_fields);

        // Only process categories and update newswires if it is not a
        // duplicate.
        $news_item_id = $this->getDuplicate($posted_fields);
        if ($news_item_id + 0 == 0) {
          $news_item_id = $this->addPostToDb($posted_fields, $associated_attachment_ids_array);
          if ($news_item_id > 0) {
            $this->processCategories($news_item_id, $posted_fields);
            $this->updateSyndicationWires($news_item_id, $posted_fields);
          }
        }

        $article_db_class = new ArticleDB();

        if ($news_item_id > 0) {
          $parent_item_id = $news_item_id;
          $article_renderer_class = new ArticleRenderer();
          $article_cache_class->cacheEverythingForArticle($news_item_id);
          $dependent_list_cache_class = new DependentListCache();
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
          $dependent_list_cache_class->updateAllDependenciesOnAdd($article_info);

        }

        // Setup ids for duplicates.
        $article_info = '';
        $news_item_id2 = $news_item_id;
        if ($news_item_id2 < 0 && $news_item_id2 != -1) {
          $news_item_id2 = $news_item_id2 * -1;
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id2);
          $parent_item_id = $article_info['parent_item_id'];
          $article_renderer_class = new ArticleRenderer();
          $article_renderer_class->getRenderedArticleHtml($article_info);
        }

        if ($parent_item_id == $news_item_id2) {
          // $this->tr->trans('publish_successful');.
          $this->tkeys['local_publish_result'] = 'SUCCESS';
          if (!is_array($article_info)) {
            $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id2);
          }
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($news_item_id2, $article_info['creation_timestamp']);
          $this->forcedTemplateFile = 'article/mpublish_success.tpl';
        }
        elseif ($news_item_id2 > 0) {
          $this->tkeys['local_publish_result'] = 'SUCCESS';
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($parent_item_id);
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($parent_item_id, $article_info['creation_timestamp']) . '?show_comments=1#' . $news_item_id2;
          $this->forcedTemplateFile = 'article/mpublish_success.tpl';
        }
        else {
          $this->tkeys['local_publish_result'] = 'FAILURE';
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($_GET['top_id']);
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($_POST['parent_item_id'], $article_info['creation_timestamp']);
        }
        $this->tkeys['LOCAL_PUBLISH_LINK'] = SERVER_URL . $rel_link;

        $logdb->updateLogEntryAfterPublish();
      }
    }
    else {
      $this->tkeys['site_status'] = 'VERIFIED';
    }
    if (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1' && ($posted_fields['publish'] != '' || $posted_fields['preview'] != '')) {
      $this->clearValidationMessages();
      $this->addValidationMessage('The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.');
      $this->addValidationMessage("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validatePost($posted_fields) {
    $ret = 1;

    // This is placeholder security during testing
    // first real version will probably have external file with list of valid
    // values for this field.
    if (!isset($posted_fields['application_key']) ||$posted_fields['application_key'] != 'at#Hqe_t342qtzz') {
      $this->addValidationMessage('Invalid Application Key');
      $ret = 0;
    }

    // There shuld be more validation of this field but for first releases it
    // will either be used to block or to act almost as a password if we have
    // spam issues.
    if (!isset($posted_fields['user_key']) || mb_strlen(trim($posted_fields['user_key']), 'UTF-8') < 5) {
      $this->addValidationMessage('Invalid User Key');
      $ret = 0;
    }

    if (!isset($posted_fields['title1']) || mb_strlen(trim($posted_fields['title1']), 'UTF-8') < 5) {
      $this->addValidationMessage('Title Was Required And It Must At Least Be 5 Characters Long');
      $ret = 0;
    }
    if (!isset($posted_fields['displayed_author_name']) || mb_strlen(trim($posted_fields['displayed_author_name']), 'UTF-8') < 1) {
      $this->addValidationMessage('Author Name Is Required');
      $ret = 0;
    }
    if (!isset($posted_fields['summary']) || mb_strlen(trim($posted_fields['summary']), 'UTF-8') < 3) {
      $this->addValidationMessage('Summary Is Required');
      $ret = 0;
    }

    if ($posted_fields['file_count'] == 0) {
      if (!isset($posted_fields['text']) || mb_strlen(trim($posted_fields['text']), 'UTF-8') < 10) {
        $this->addValidationMessage("Text Either Wasn't Entered Or Was Too Short");
        $ret = 0;
      }
    }
    $spam_cache_class = new SpamCache();
    $spamvalidatelist = $spam_cache_class->loadValidationStringFile();
    $spamvalidatelist = str_replace("\r", '', $spamvalidatelist);
    $spamvalidate_array = explode("\n", $spamvalidatelist);
    foreach ($spamvalidate_array as $spamstr) {
      $spamstr = trim($spamstr);
      if ($spamstr == '') {
        continue;
      }
      if (strpos(' ' . $posted_fields['summary'], $spamstr) > 0) {
        $this->addValidationMessage("Indybay's Spam Filter Doesnt Allow The String \"" . $spamstr . '" To Be In The Summary or Text Of Posts');
        $ret = 0;
      }
      if (strpos(' ' . $posted_fields['text'], $spamstr) > 0) {
        $this->addValidationMessage("Indybay's Spam Filter Doesnt Allow The String \"" . $spamstr . '" To Be In The Summary or Text Of Posts');
        $ret = 0;
      }
    }

    return $ret;
  }

}
