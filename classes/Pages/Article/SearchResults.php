<?php

namespace Indybay\Pages\Article;

use Indybay\DB\FeaturePageDB;
use Indybay\DB\MediaAttachmentDB;

/**
 * Search results.
 */
class SearchResults extends Newswire {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    parent::execute();
    $types = [];

    $feature_page_db_class = new FeaturePageDB();
    $search_results = '<span class="strong">';

    if ($this->tkeys['LOCAL_REGION_ID'] + 0 != 0) {
      $page_info = $feature_page_db_class->getFeaturePageInfo($this->tkeys['LOCAL_REGION_ID']);
      $search_results .= $page_info['long_display_name'] . ' ';
    }
    if ($this->tkeys['LOCAL_TOPIC_ID'] + 0 != 0) {
      $page_info = $feature_page_db_class->getFeaturePageInfo($this->tkeys['LOCAL_TOPIC_ID']);
      if (is_array($page_info)) {
        $search_results .= $page_info['long_display_name'] . ' ';
      }
    }

    if ($this->tkeys['LOCAL_MEDIA_TYPE_GROUPING_ID'] + 0 != 0) {
      $media_attachment_db_class = new MediaAttachmentDB();
      $medium_options = $media_attachment_db_class->getMediumOptions();
      $str = $medium_options[$this->tkeys['LOCAL_MEDIA_TYPE_GROUPING_ID'] + 0];
      $search_results .= $str . ' ';
    }

    if ($this->tkeys['local_include_events'] == 1) {
      array_push($types, 'Events');
    }
    if ($this->tkeys['local_include_posts'] == 1) {
      array_push($types, 'Posts');
    }
    if ($this->tkeys['local_include_attachments'] == 1) {
      array_push($types, 'Attachments');
    }
    if ($this->tkeys['local_include_blurbs'] == 1) {
      array_push($types, 'Featured Center Column Stories');
    }
    $ii = count($types);
    $i = 1;
    foreach ($types as $next_type) {
      if ($ii == $i && $i > 1) {
        $search_results .= ' and ';
      }
      if ($i < $ii && $ii > 2 & $i > 1) {
        $search_results .= ', ';
      }
      $search_results .= $next_type;
      $i = $i + 1;
    }

    if ($this->tkeys['LOCAL_DATE_TYPE'] == 'displayed_date') {
      $search_results .= '<!-- <br> --> Occuring Between ';
    }
    elseif ($this->tkeys['LOCAL_DATE_TYPE'] == 'creation_date') {
      $search_results .= '<br>Posted Between ';
    }

    if ($this->tkeys['LOCAL_DATE_TYPE'] == 'displayed_date' || $this->tkeys['LOCAL_DATE_TYPE'] == 'creation_date') {
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_START_MONTH'] . '/';
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_START_DAY'] . '/';
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_START_YEAR'];
      $search_results .= ' and ';
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_END_MONTH'] . '/';
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_END_DAY'] . '/';
      $search_results .= $this->tkeys['LOCAL_DATE_RANGE_END_YEAR'];
    }
    if ($this->tkeys['LOCAL_SEARCH'] != '') {
      $search_results .= '<br>matching the search query <span class="strong"><em>' . $this->tkeys['LOCAL_SEARCH'] . '</em></span>';
    }
    if ($this->tkeys['LOCAL_DATE_TYPE'] == 'displayed_date') {
      $search_results .= '</span><br><span class="small">(sorted by event date in chronological order)</span>';
    }
    elseif ($this->tkeys['LOCAL_DATE_TYPE'] == 'creation_date') {
      $search_results .= '</span><br><span class="small">(sorted by date posted in chronological order)</span>';
    }
    else {
      $search_results .= '</span><br><span class="small">(sorted by date posted in reverse chronological order)</span>';
    }

    $this->tkeys['local_advanced_search_results_info'] = $search_results;
  }

  /**
   * Gets search template.
   */
  public function getSearchTemplate() {
    return 'article/hidden_search.tpl';
  }

  /**
   * Gets news item status select list.
   */
  public function getNewsItemStatusSelectList() {
    $display_options = [];
    $display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN] = 'All';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlighted-Local';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted-NonLocal';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED *
    NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted';
    $display_options[NEWS_ITEM_STATUS_ID_NEW *
    NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED *
    NEWS_ITEM_STATUS_ID_OTHER * NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = 'Other';
    $display_options[NEWS_ITEM_STATUS_ID_HIDDEN * NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN] = 'Hidden';
    return $display_options;
  }

  /**
   * Disable bulk classification.
   */
  public function bulkClassify($posted_fields) {
    echo 'Classifictions Requires Being Logged Into The Admin System!!';
  }

}
