<?php

namespace Indybay\Pages\Article;

use Indybay\DB\ArticleDB;
use Indybay\DB\MediaAttachmentDB;

/**
 * Renders Oembed data.
 */
class Oembed {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $article_db_class = new ArticleDB();
    $media_attachment = new MediaAttachmentDB();
    if (!isset($_GET['url'])) {
      http_response_code(404);
      return;
    }
    if (!preg_match('@/[0-9]{4}/[0-9]{2}/[0-9]{2}/([0-9]+)\.php(?:/([0-9]+))?(?:\?show_comments=1)?(?:#([0-9]+))?@', $_GET['url'], $matches)) {
      http_response_code(404);
      return;
    }
    $news_item_id = $matches[3] ?? (!empty($matches[2]) ? $matches[2] : $matches[1]);
    $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
    if (!$news_item_version_id) {
      http_response_code(404);
      return;
    }
    $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    if ($version_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN || $version_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN) {
      http_response_code(404);
      return;
    }
    $media_attachment_info = $media_attachment->getMediaAttachmentInfo($version_info['media_attachment_id']);
    $rendered_video_info = $media_attachment->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
    if (is_array($rendered_video_info)) {
      $rendered_video_file_name = $rendered_video_info['file_name'];
      $rendered_video_relative_path = $rendered_video_info['relative_path'];
      if (!$media_attachment_info['browser_compat']) {
        if ($preview = $media_attachment->getRenderedH264($media_attachment_info['media_attachment_id'])) {
          $media_attachment_info = $preview;
        }
      }
      $file_name = $media_attachment_info['file_name'];
      $relative_path = $media_attachment_info['relative_path'];
      $relative_url = '/uploads/' . $relative_path . $file_name;
      $thumbnail = '<br><span class="video-thumbnail"><img src="' . SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '"></span><br>';
      $poster = SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
      $dimension = $media_attachment_info['image_width'] <= $media_attachment_info['image_height'] ? 'height' : 'width';
    }
    $data['type'] = 'video';
    $data['version'] = '1.0';
    $data['title'] = $version_info['title1'];
    $data['author_name'] = $version_info['displayed_author_name'];
    $data['author_url'] = $version_info['related_url'];
    $data['provider_name'] = 'Indybay';
    $data['provider_url'] = 'https://www.indybay.org/';
    $data['cache_age'] = 1209600;
    $data['width'] = isset($_GET['maxwidth']) ? (int) $_GET['maxwidth'] : (int) $media_attachment_info['image_width'];
    $scale = $data['width'] / $media_attachment_info['image_width'];
    $data['height'] = round($scale * $media_attachment_info['image_height']);
    if (isset($_GET['maxheight']) && $data['height'] > $_GET['maxheight']) {
      $data['height'] = (int) $_GET['maxheight'];
      $scale = $data['height'] / $media_attachment_info['image_height'];
      $data['width'] = round($scale * $media_attachment_info['image_width']);
    }
    $data['html'] = '<video ' . $dimension . '="100%" preload="none" poster="' . $poster . '" controls autoplay><source src="' . SERVER_URL . $relative_url . '" type="video/mp4">Download video: <a class="video" href="' . SERVER_URL . $relative_url . '">' . $thumbnail . $file_name . '</a></video>';
    $data['thumbnail_url'] = SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
    $data['thumbnail_width'] = $data['width'];
    $data['thumbnail_height'] = $data['height'];
    header('Last-Modified: ' . gmdate(DATE_RFC7231, $version_info['modified_timestamp']));
    if (isset($_GET['format']) && $_GET['format'] === 'xml') {
      $document = new \SimpleXMLElement('<oembed></oembed>');
      foreach ($data as $element => $value) {
        $document->{$element} = $value;
      }
      header('Content-Type: text/xml; charset=UTF-8');
      echo $document->asXML();
      return;
    }
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode($data);
  }

}
