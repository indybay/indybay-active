<?php

namespace Indybay\Pages\Article;

use Indybay\DB\ArticleDB;
use Indybay\DB\MediaAttachmentDB;

/**
 * Renders video element.
 */
class Video {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $article_db_class = new ArticleDB();
    $media_attachment = new MediaAttachmentDB();
    $news_item_id = (int) basename($_SERVER['PHP_SELF']);
    $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
    $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    $media_attachment_info = $media_attachment->getMediaAttachmentInfo($version_info['media_attachment_id']);
    $thumbnail = '';
    $poster = '';
    $rendered_video_info = $media_attachment->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
    if (is_array($rendered_video_info)) {
      $rendered_video_file_name = $rendered_video_info['file_name'];
      $rendered_video_relative_path = $rendered_video_info['relative_path'];
      $thumbnail = '<br><span class="video-thumbnail"><img src="' . SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '"></span><br>';
      $poster = 'poster="' . SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '" ';
    }
    if (!$media_attachment_info['browser_compat']) {
      if ($preview = $media_attachment->getRenderedH264($media_attachment_info['media_attachment_id'])) {
        $media_attachment_info = $preview;
      }
    }
    $file_name = $media_attachment_info['file_name'];
    $relative_path = $media_attachment_info['relative_path'];
    $relative_url = '/uploads/' . $relative_path . $file_name;
    $dimension = $media_attachment_info['image_width'] <= $media_attachment_info['image_height'] ? 'height' : 'width';
    header('Last-Modified: ' . gmdate(DATE_RFC7231, $version_info['modified_timestamp']));
    echo '<video ', $dimension, '="100%" preload="none" ', $poster, 'controls autoplay><source src="', SERVER_URL, $relative_url, '" type="video/mp4">Download video: <a class="video" href="', SERVER_URL, $relative_url, '">', $thumbnail, $file_name, '</a></video>';
  }

}
