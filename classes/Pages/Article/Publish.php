<?php

namespace Indybay\Pages\Article;

use Indybay\Cache\ArticleCache;
use Indybay\Cache\DependentListCache;
use Indybay\Cache\SpamCache;
use Indybay\DB\ArticleDB;
use Indybay\DB\CategoryDB;
use Indybay\DB\DB;
use Indybay\DB\LogDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemDB;
use Indybay\MediaAndFileUtil\FileUtil;
use Indybay\MediaAndFileUtil\UploadUtil;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Renderer\Captcha;
use Indybay\Renderer\Renderer;
use Indybay\Translate;

/**
 * Class for publish page.
 */
class Publish extends Page {

  const FILE_ACCEPT = 'accept=".amr,.avif,.flac,.gif,.heic,.jpeg,.jpg,.m4a,.m4v,.mov,.mp3,.mp4,.mpg,.oga,.ogg,.pdf,.png,.vtt,.wav,.webm,.webp"';

  /**
   * The number of bytes in a kilobyte.
   *
   * @see http://wikipedia.org/wiki/Kilobyte
   */
  const KILOBYTE = 1024;

  /**
   * The captcha service.
   */
  protected Captcha $captcha;

  /**
   * The filebox markup.
   */
  protected string $filebox;

  /**
   * Parses a given byte size.
   *
   * @param mixed $size
   *   An integer or string size expressed as a number of bytes with optional SI
   *   or IEC binary unit prefix (e.g. 2, 3K, 5MB, 10G, 6GiB, 8 bytes, 9mbytes).
   *
   * @return int
   *   An integer representation of the size in bytes.
   */
  public static function toInt($size) {
    // Remove the non-unit characters from the size.
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
    // Remove the non-numeric characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size);
    if ($unit) {
      // Find the position of the unit in the ordered string which is the power
      // of magnitude to multiply a kilobyte by.
      return round($size * pow(self::KILOBYTE, stripos('bkmgtpezy', $unit[0])));
    }
    else {
      return round($size);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $is_published = FALSE;
    $preview = FALSE;
    if (is_array($_POST)) {
      $posted_fields = $this->cleanPostedData($_POST);
    }

    // Test connection.
    $db_class = new DB();
    $db_class->getConnection();

    $category_db_class = new CategoryDB();
    $article_db_class = new ArticleDB();
    $article_renderer_class = new ArticleRenderer();
    $tr = new Translate();
    $tr->createTranslateTable('publish');

    // Set up the template variables for this page.
    $this->tkeys['upload_max_filesize'] = round(min(self::toInt(ini_get('upload_max_filesize')), self::toInt(ini_get('post_max_size'))) / self::KILOBYTE / self::KILOBYTE) . 'MB';
    $this->tkeys['post_max_size'] = round(self::toInt(ini_get('post_max_size')) / self::KILOBYTE / self::KILOBYTE) . 'MB';
    $this->tkeys['max_execution_time'] = ini_get('max_execution_time') / 60 / 60;
    $this->tkeys['file_accept'] = self::FILE_ACCEPT;

    $this->captcha = new Captcha();
    $this->tkeys['captcha_form'] = $this->captcha->makeForm();

    $parent_item_id = 0;
    $parent_formatted_info = '';
    if (array_key_exists('top_id', $_GET) && (int) $_GET['top_id'] > 0) {
      $parent_item_id = intval($_GET['top_id']);
    }
    if (array_key_exists('parent_item_id', $posted_fields) && (int) $posted_fields['parent_item_id'] > 0) {
      $parent_item_id = (int) $posted_fields['parent_item_id'];
    }
    if ($parent_item_id != 0) {
      $article_db_class = new ArticleDB();
      $article_info = $article_db_class->getArticleInfoFromNewsItemId($parent_item_id);
      $parent_formatted_info = '<span class="strong">Add A Comment On "' . $article_info['title1'] . '" by ' . $article_info['displayed_author_name'] . '<br>';
      $parent_formatted_info .= '<span class="small">' . mb_substr($article_info['summary'], 0, 150) . '</span></span><br>';
    }
    $this->tkeys['local_parent_item_id'] = $parent_item_id;

    $this->tkeys['local_parent_info'] = $parent_formatted_info;

    $topic_id = 0;
    if (array_key_exists('topic_id', $posted_fields)) {
      $topic_id = $posted_fields['topic_id'];
    }
    elseif (array_key_exists('page_id', $_GET) && $_GET['page_id'] != '') {
      $topic_id = (int) $_GET['page_id'];
    }

    $region_id = 0;
    if (array_key_exists('region_id', $posted_fields)) {
      $region_id = $posted_fields['region_id'];
    }
    elseif (array_key_exists('page_id', $_GET) && $_GET['page_id'] != '') {
      $region_id = (int) $_GET['page_id'];
    }

    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0, 1);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44, 1);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_international_options, $cat_topic_options);
    $this->tkeys['cat_topic_select'] = $article_renderer_class->makeSelectForm('topic_id', $cat_topic_options, $topic_id, 'Please Select');
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0, 1);
    $this->tkeys['cat_region_select'] = $article_renderer_class->makeSelectForm('region_id', $cat_region_options, $region_id, 'Please Select');

    $this->tkeys['local_preview'] = '';
    if ((int) $posted_fields['file_count'] == 0) {
      if (empty($this->captcha->valid)) {
        $this->tkeys['local_preview'] = '<p>You must preview your post before publishing.</p>';
      }
      $this->tkeys['local_preview'] .= '<input type="submit" name="preview" value="Preview">';
    }

    $form_vars = ['title1', 'displayed_author_name', 'summary', 'related_url', 'text', 'email', 'address', 'phone',
      'event_duration',
    ];

    // Initialize template variables.
    foreach ($form_vars as $fv) {
      if (array_key_exists($fv, $posted_fields) && mb_strlen($posted_fields[$fv]) > 0) {
        $this->tkeys['local_' . $fv] = $posted_fields[$fv];
      }
      else {
        $this->tkeys['local_' . $fv] = '';
      }
    }

    $this->tkeys['local_checkbox_is_text_html'] = $article_renderer_class->makeBooleanCheckboxForm('is_text_html', (int) $posted_fields['is_text_html']);
    $this->tkeys['local_checkbox_display_contact_info'] =
      $article_renderer_class->makeBooleanCheckboxForm('display_contact_info',
      $posted_fields['display_contact_info']);

    $this->generateFileboxHtml($posted_fields);
    $this->tkeys['file_boxes'] = $this->filebox;

    // Set up the file number select list.
    $max_num_uploads = [];

    if ((int) $parent_item_id != 0) {
      $max_num_uploads = [0 => 0, 1 => 1];
    }
    elseif ($this->getParentNewsItemTypeId() == NEWS_ITEM_TYPE_ID_EVENT) {
      $max_num_uploads = [0 => 0, 1 => 1, 2 => 2, 3 => 3];
    }
    else {
      for ($x = 0; $x <= 20; $x++) {
        $max_num_uploads[$x] = $x;
      }
    }

    // Check if we are actually dealing with a comment.
    if ($parent_item_id > 0) {
      $ctr = new Translate();
      $this->forcedTemplateFile = 'article/comment.tpl';
      $this->tkeys['pub_stepone'] = $ctr->trans('comment_pub_one');
      $this->tkeys['pub_steptwo'] = $ctr->trans('comment_pub_two');
      $this->tkeys['pub_stepthree'] = $ctr->trans('comment_pub_three');
      $this->tkeys['pub_optional'] = $ctr->trans('optional');
      $this->tkeys['local_site_nick'] = $GLOBALS['site_nick'];
      $this->tkeys['pub_author'] = $ctr->trans('author');
      $this->tkeys['pub_url'] = $ctr->trans('link');
    }

    if ($posted_fields['publish']) {
      if ($posted_fields['is_text_html']) {
        $posted_fields['is_summary_html'] = TRUE;
      }
      if ($this->validatePost($posted_fields)) {
        $article_cache_class = new ArticleCache();
        $logdb = new LogDB();
        $logdb->addLogEntryBeforePublish();

        $associated_attachment_ids_array = $this->processUploads($posted_fields);

        // First check for duplicate and only save if not duplicate.
        $is_duplicate = FALSE;
        $news_item_id = $this->getDuplicate($posted_fields);
        if ((int) $news_item_id == 0) {
          $news_item_id = $this->addPostToDb($posted_fields, $associated_attachment_ids_array);
        }
        else {
          $is_duplicate = TRUE;
        }

        // Set parent id for duplicate and nonduplicate cases.
        $parent_item_id = (int) $parent_item_id;
        if ($parent_item_id == 0) {
          $parent_item_id = $news_item_id;
        }

        // Proces categories, regen newswires and feeds and regen cache only if
        // it is not a duplicate.
        if ($news_item_id > 0 && !$is_duplicate) {

          if ($parent_item_id == $news_item_id) {
            $this->processCategories($news_item_id, $posted_fields);
            $this->updateSyndicationWires($news_item_id, $posted_fields);
          }

          $article_renderer_class = new ArticleRenderer();
          $article_db_class = new ArticleDB();

          if ($parent_item_id == $news_item_id) {
            $rendered_html = $article_cache_class->cacheEverythingForArticle($news_item_id);
            $dependent_list_cache_class = new DependentListCache();
            $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
            $dependent_list_cache_class->updateAllDependenciesOnAdd($article_info);

          }
          else {
            $rendered_html = $article_cache_class->cacheEverythingForComment($news_item_id, $parent_item_id);
          }
        }

        $is_published = TRUE;

        // Setup ids for duplicates.
        $article_info = '';
        if ($news_item_id > 0 && $is_duplicate) {
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
          $parent_item_id = $article_info['parent_item_id'];
          $article_renderer_class = new ArticleRenderer();
          $rendered_html = $article_renderer_class->getRenderedArticleHtml($article_info);
        }

        // Post or calendar item.
        if ($parent_item_id == $news_item_id) {
          $this->tkeys['local_publish_result'] = $tr->trans('publish_successful');
          if (!is_array($article_info)) {
            $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
          }
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($news_item_id, $article_info['creation_timestamp']);

          // Comment.
        }
        elseif ($news_item_id > 0) {
          $this->tkeys['local_publish_result'] = $tr->trans('comment_successful');
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($parent_item_id);
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($parent_item_id, $article_info['creation_timestamp']) . '?show_comments=1#' . $news_item_id;

          // Duplicate comment on a different post from original.
        }
        elseif ($news_item_id == -1) {
          $this->tkeys['local_publish_result'] = "<font color=\"#cc0000\">Indybay's spam filter prevents posting of the exact same comment to more than one post.<br> If you really need the exact same comment on this post, click back and make some minor change.</font>";
          $article_info = $article_db_class->getArticleInfoFromNewsItemId($_GET['top_id']);
          $rel_link = $article_cache_class->getWebCachePathForNewsItemGivenDate($_POST['parent_item_id'], $article_info['creation_timestamp']);
        }

        $this->tkeys['LOCAL_DISPLAY_PREVIEW'] = $rendered_html ?? '';
        $this->tkeys['LOCAL_PUBLISH_LINK'] = SERVER_URL . $rel_link;

        $logdb->updateLogEntryAfterPublish();

        $this->forcedTemplateFile = 'article/publish_success.tpl';

      }

      $instructions = 0;

    }
    else {

      if ($posted_fields['preview']) {
        $preview = 1;
      }
      else {
        if ($parent_item_id == 0) {
          $instructions = 1;
        }
      }
    }

    if (!$is_published) {
      foreach ($form_vars as $fv) {
        $this->tkeys["local_$fv"] = htmlspecialchars($this->tkeys['local_' . $fv]);
      }

      if (isset($instructions)) {
        $this->printPublishInstructions();

      }

      if ($preview) {

        $this->validatePost($posted_fields);
        $article_renderer_class = new ArticleRenderer();
        $posted_fields['creation_date'] = date('D M j H:i:s Y');
        $posted_fields['created'] = date('D M j H:i:s Y');
        if ((int) $parent_item_id != 0) {
          $posted_fields['news_item_type_id'] = $this->getChildNewsItemTypeId();
        }
        else {
          $posted_fields['news_item_type_id'] = $this->getParentNewsItemTypeId();
        }
        $posted_fields['is_preview'] = 1;
        $posted_fields['is_summary_html'] = !empty($posted_fields['is_text_html']);
        $this->tkeys['LOCAL_DISPLAY_PREVIEW'] = '<h1 class="headline-text">Publish Preview:</h1>' . $article_renderer_class->getRenderedArticleHtml($posted_fields, TRUE);
      }
      else {
        if (!array_key_exists('LOCAL_DISPLAY_PREVIEW', $this->tkeys)) {
          $this->tkeys['LOCAL_DISPLAY_PREVIEW'] = '';
        }
      }

    }
    else {
      // Echo $article->cachestatus;
      // echo $article_obj->cachestatus;.
    }

    $this->tkeys['select_filecount'] = $article_renderer_class->makeSelectForm('file_count', $max_num_uploads, $posted_fields['file_count']);

    $this->tkeys['hide1'] = '';
    $this->tkeys['hide2'] = '';
    if (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1' && ($posted_fields['publish'] || $posted_fields['preview'])) {
      $this->clearValidationMessages();
      $this->addValidationMessage('The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.');
      $this->addValidationMessage("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
    }
    elseif (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1') {
      $this->tkeys['hide1'] = '<!-- ';
      $this->addStatusMessage('The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.');
      $this->addStatusMessage("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
      $this->tkeys['hide2'] = ' -->';
    }

  }

  /**
   * Generates filebox HTML.
   */
  public function generateFileboxHtml($posted_fields) {

    $tr = new Translate();
    $tr->createTranslateTable('publish');

    if ($posted_fields['publish'] == '' && $posted_fields['file_count'] > 0) {

      if (!$this->validatePost($posted_fields)) {
        $this->addValidationMessage('Validation Must Be Successful To Choose Uploads');
        $posted_fields['file_count'] = 0;
      }
    }

    $fbox = '';

    if ($posted_fields['file_count'] > 0) {
      $file_count = $posted_fields['file_count'];
    }
    else {
      $file_count = 0;
    }

    for ($counter = 0; $file_count >= $counter; $counter++) {
      if ($counter > 0) {
        if ($counter > 1) {
          $fbox .= "<hr>\n";
        }
        $fbox .= '<table class="publish-table" width="100%" cellspacing="2" cellpadding="4"><tr>';
        $fbox .= '<td width="25%" valign="top"><strong>';
        $fbox .= $tr->trans('select_file') . ' #' . $counter . "</strong></td>\n";
        $maxsize = min(self::toInt(ini_get('upload_max_filesize')), self::toInt(ini_get('post_max_size')));
        $fbox .= '<td width="75%">
                	  <input type="hidden" name="MAX_FILE_SIZE" value="' . $maxsize . '">
                	  <input class="fileinput" type="file" name="linked_file_' . $counter . '" >
                    <br>';
        $fbox .= $tr->trans('upload_limit') . "</td></tr>\n";

        if ($counter > 1) {
          if (!array_key_exists('linked_file_title_' . $counter, $posted_fields)) {
            $posted_fields['linked_file_title_' . $counter] = '';
          }
          if (!array_key_exists('linked_file_comment_' . $counter, $posted_fields)) {
            $posted_fields['linked_file_comment_' . $counter] = '';
          }
          $fbox .= '<tr><td valign="top" width="25%"><strong>Title #' . $counter . '</strong> <small>(';
          $fbox .= $tr->trans('required') . ')</small></td><td>';
          $fbox .= '<input size="25" maxlength="90" type="text" name="linked_file_title_' . $counter;
          $fbox .= '" value="' . $posted_fields['linked_file_title_' . $counter] . '">';
          $fbox .= '</td></tr><tr><td valign="top" width="25%">';
          $fbox .= 'Comment #' . $counter . "</td>\n<td>";
          $fbox .= '<textarea name="linked_file_comment_' . $counter . '" rows="3" cols="50" wrap="virtual">';
          $fbox .= $posted_fields['linked_file_comment_' . $counter] . "</textarea></td></tr>\n";
        }

        $fbox .= "</table><!-- END .publish-table -->\n";
      }
    }

    $this->filebox = $fbox;

  }

  /**
   * Processes uploads.
   */
  public function processUploads($posted_fields) {
    $associated_ids_array = [];
    if (is_array($_FILES)) {
      for ($counter = 1; $posted_fields['file_count'] >= $counter; $counter++) {
        if ($counter > 25) {
          echo 'Error!! file counter>25';
          exit;
        }
        $associated_attachment_ids = [];
        $has_file = isset($_FILES['linked_file_' . $counter]);
        if ($has_file && is_array($_FILES['linked_file_' . $counter])) {
          if ($_FILES['linked_file_' . $counter]['name'] != '') {

            $upload_util_class = new UploadUtil();
            $media_attachement_db_class = new MediaAttachmentDB();
            $tmp_file_path = $_FILES['linked_file_' . $counter]['tmp_name'];
            $mime_type_from_post = $_FILES['linked_file_' . $counter]['type'];
            $original_file_name = $_FILES['linked_file_' . $counter]['name'];
            $file_info = $upload_util_class->processAnonymousUpload($tmp_file_path, $original_file_name, $mime_type_from_post);
            $image_name = $original_file_name;
            $file_info['alt_tag'] = '';
            if (array_key_exists('linked_file_alt_tag_' . $counter, $posted_fields)) {
              $file_info['alt_tag'] = $posted_fields['linked_file_alt_tag_' . $counter];
              if (mb_strlen($file_info['alt_tag']) > 140) {
                $file_info['alt_tag'] = mb_substr($file_info['alt_tag'], 0, 137) . '...';
              }
            }
            if (!$file_info['alt_tag'] && $counter == 1 && array_key_exists('summary', $posted_fields)) {
              $file_info['alt_tag'] = $posted_fields['summary'];
              if (mb_strlen($file_info['alt_tag']) > 140) {
                $file_info['alt_tag'] = mb_substr($file_info['alt_tag'], 0, 137) . '...';
              }
            }
            $media_attachment_id = $media_attachement_db_class->addMediaAttachment($image_name,
                $file_info['file_name'], $file_info['relative_path'], $file_info['alt_tag'], $file_info['original_file_name'], $file_info['media_type_id'], 0, UPLOAD_TYPE_POST, UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
            $associated_attachment_ids = $upload_util_class->makeSecondaryFilesAndAssociate($media_attachment_id, $file_info);
          }
        }

        $associated_ids_array[$counter] = $associated_attachment_ids;
      }
    }
    return $associated_ids_array;
  }

  /**
   * Validates post.
   */
  public function validatePost($posted_fields) {
    $ret = 1;
    $parent_item_id = 0;

    if (empty($this->captcha->valid)) {
      $this->addValidationMessage('Please answer the CAPTCHA (anti-spam question) correctly.');
      $ret = 0;
    }

    if (array_key_exists('parent_item_id', $posted_fields)) {
      $parent_item_id = (int) $posted_fields['parent_item_id'];
    }

    if (!isset($posted_fields['title1']) || mb_strlen(trim($posted_fields['title1']), 'UTF-8') < 5) {
      $this->addValidationMessage('Title is required and it must at least be 5 characters long.');
      $ret = 0;
    }
    if (!isset($posted_fields['displayed_author_name']) || mb_strlen(trim($posted_fields['displayed_author_name']), 'UTF-8') < 1) {
      $this->addValidationMessage('Author name is required.');
      $ret = 0;
    }
    if ($parent_item_id == 0) {
      if (!isset($posted_fields['summary']) || mb_strlen(trim($posted_fields['summary']), 'UTF-8') < 3) {
        $this->addValidationMessage('Summary is required.');
        $ret = 0;
      }
    }

    if ($parent_item_id != 0 && $posted_fields['file_count'] == 0) {
      if (!isset($posted_fields['text']) || mb_strlen(trim($posted_fields['text']), 'UTF-8') < 10) {
        $this->addValidationMessage("Text either wasn't entered or was too short.");
        $ret = 0;
      }
    }
    $spam_cache_class = new SpamCache();
    $spamvalidatelist = $spam_cache_class->loadValidationStringFile();
    $spamvalidatelist = str_replace("\r", '', $spamvalidatelist);
    $spamvalidate_array = explode("\n", $spamvalidatelist);
    foreach ($spamvalidate_array as $spamstr) {
      $spamstr = trim($spamstr);
      if ($spamstr == '') {
        continue;
      }
      if (strpos(' ' . $posted_fields['summary'], $spamstr) > 0) {
        $this->addValidationMessage("Indybay's Spam Filter Doesnt Allow The String \"" . $spamstr . '" To Be In The Summary or Text Of Posts');
        $ret = 0;
      }
      if (strpos(' ' . $posted_fields['text'], $spamstr) > 0) {
        $this->addValidationMessage("Indybay's Spam Filter Doesnt Allow The String \"" . $spamstr . '" To Be In The Summary or Text Of Posts');
        $ret = 0;
      }
    }

    return $ret;
  }

  /**
   * Cleans posted data.
   */
  public function cleanPostedData($posted_fields) {
    $renderer = new Renderer();
    if (array_key_exists('title1', $posted_fields) && is_string($posted_fields['title1'])) {
      $posted_fields['title1'] = trim($posted_fields['title1']);
    }
    else {
      $posted_fields['title1'] = '';
    }

    $repl = ["\n", "\r", '     ', '   ', '  ', '  '];
    $replw = [' ', ' ', ' ', ' ', ' ', ' '];

    $posted_fields['title1'] = str_replace($repl, $replw, $posted_fields['title1']);

    if (!array_key_exists('publish', $posted_fields)) {
      $posted_fields['publish'] = 0;
    }

    if (!array_key_exists('preview', $posted_fields)) {
      $posted_fields['preview'] = 0;
    }

    if (!array_key_exists('parent_item_id', $posted_fields)) {
      $posted_fields['parent_item_id'] = 0;
    }

    if (array_key_exists('displayed_author_name', $posted_fields)) {
      $posted_fields['displayed_author_name'] = strip_tags($posted_fields['displayed_author_name']);
    }
    else {
      $posted_fields['displayed_author_name'] = '';
    }
    if (array_key_exists('related_url', $posted_fields) && is_string($posted_fields['related_url'])) {
      $posted_fields['related_url'] = strip_tags($posted_fields['related_url']);
    }
    else {
      $posted_fields['related_url'] = '';
    }
    if (array_key_exists('email', $posted_fields)) {
      $posted_fields['email'] = strip_tags($posted_fields['email']);
    }
    else {
      $posted_fields['email'] = '';
    }
    if (array_key_exists('phone', $posted_fields)) {
      $posted_fields['phone'] = strip_tags($posted_fields['phone']);
    }
    else {
      $posted_fields['phone'] = '';
    }
    if (array_key_exists('address', $posted_fields)) {
      $posted_fields['address'] = strip_tags($posted_fields['address']);
    }
    else {
      $posted_fields['address'] = '';
    }
    if (array_key_exists('summary', $posted_fields)) {
      $posted_fields['summary'] = $renderer->htmlPurify($posted_fields['summary']);
    }
    else {
      $posted_fields['summary'] = '';
    }
    if (array_key_exists('text', $posted_fields)) {
      $posted_fields['text'] = $renderer->htmlPurify($posted_fields['text']);
    }
    else {
      $posted_fields['text'] = '';
    }

    if (array_key_exists('is_text_html', $posted_fields)) {
      $posted_fields['is_text_html'] = (int) $posted_fields['is_text_html'];
    }
    else {
      $posted_fields['is_text_html'] = 0;
    }

    if (array_key_exists('display_contact_info', $posted_fields)) {
      $posted_fields['display_contact_info'] = (int) $posted_fields['display_contact_info'];
    }
    else {
      $posted_fields['display_contact_info'] = 0;
    }

    if (!array_key_exists('file_count', $posted_fields)) {
      $posted_fields['file_count'] = 0;

    }
    else {
      for ($counter = 2; (int) $posted_fields['file_count'] >= $counter; $counter++) {
        if ($counter > 25) {
          echo 'Error!! file counter>25';
          exit;
        }
        if (array_key_exists('linked_file_title_' . $counter, $posted_fields)) {
          $posted_fields['linked_file_title_' . $counter] = strip_tags($posted_fields['linked_file_title_' . $counter]);
          $posted_fields['linked_file_comment_' . $counter] = isset($posted_fields['linked_file_comment_' . $counter]) ? $renderer->htmlPurify($posted_fields['linked_file_comment_' . $counter]) : '';
        }
      }
    }

    return $posted_fields;

  }

  /**
   * Adds post to database.
   */
  public function addPostToDb($posted_fields, $associated_ids_array) {

    $article_db_class = new ArticleDB();
    if (isset($associated_ids_array[1]) && isset($associated_ids_array[1]['media_attachment_id'])) {
      $posted_fields['media_attachment_id'] = $associated_ids_array[1]['media_attachment_id'];
      $posted_fields['media_type_grouping_id'] = $associated_ids_array[1]['media_type_grouping_id'];
      if (isset($associated_ids_array[1]['thumbnail_media_attachment_id'])) {
        $posted_fields['thumbnail_media_attachment_id'] = $associated_ids_array[1]['thumbnail_media_attachment_id'];
      }
      else {
        $posted_fields['thumbnail_media_attachment_id'] = 0;
      }
    }

    if (!array_key_exists('parent_item_id', $posted_fields) || $posted_fields['parent_item_id'] == 0) {
      $news_item_id = $article_db_class->createNewArticle($this->getParentNewsItemTypeId(), $posted_fields);
    }
    else {
      $news_item_id = $article_db_class->createNewArticle($this->getChildNewsItemTypeId(), $posted_fields);
    }

    for ($counter = 2; $posted_fields['file_count'] >= $counter; $counter++) {
      if ($counter > 25) {
        echo 'Error!! file counter>25';
        exit;
      }
      $attachment_fields = [];
      if (array_key_exists('linked_file_title_' . $counter, $posted_fields)) {
        $attachment_fields['title1'] = $posted_fields['linked_file_title_' . $counter];
      }
      else {
        $attachment_fields['title1'] = $posted_fields['title1'];
      }

      if (array_key_exists("linked_file_comment_$counter", $posted_fields)) {
        $attachment_fields['text'] = $posted_fields["linked_file_comment_$counter"];
        $attachment_fields['is_text_html'] = !empty($posted_fields['is_text_html']);
      }
      else {
        $attachment_fields['text'] = '';
      }

      if (array_key_exists('displayed_author_name', $posted_fields)) {
        $attachment_fields['displayed_author_name'] = $posted_fields['displayed_author_name'];
      }
      else {
        $attachment_fields['displayed_author_name'] = '';
      }

      if (array_key_exists('related_url', $posted_fields)) {
        $attachment_fields['related_url'] = $posted_fields['related_url'];
      }
      else {
        $attachment_fields['related_url'] = '';
      }

      $attachment_fields['parent_item_id'] = $news_item_id;
      if (count($associated_ids_array) >= $counter && is_array($associated_ids_array[$counter])) {
        if (array_key_exists('media_attachment_id', $associated_ids_array[$counter])) {
          $attachment_fields['media_attachment_id'] = $associated_ids_array[$counter]['media_attachment_id'];
        }
        else {
          $attachment_fields['media_attachment_id'] = 0;
        }
        if (array_key_exists('media_type_grouping_id', $associated_ids_array[$counter])) {
          $attachment_fields['media_type_grouping_id'] = $associated_ids_array[$counter]['media_type_grouping_id'];
        }
        else {
          $attachment_fields['media_type_grouping_id'] = 0;
        }
        if (array_key_exists('thumbnail_media_attachment_id', $associated_ids_array[$counter])) {
          $attachment_fields['thumbnail_media_attachment_id'] = $associated_ids_array[$counter]['thumbnail_media_attachment_id'];
        }
        else {
          $attachment_fields['thumbnail_media_attachment_id'] = 0;
        }
      }
      if (trim($attachment_fields['title1']) != '' || trim($attachment_fields['text']) != '' || (int) $attachment_fields['media_attachment_id'] > 0) {
        $article_db_class->createNewArticle(NEWS_ITEM_TYPE_ID_ATTACHMENT, $attachment_fields);
      }
    }
    $this->addAdditionalInfoForPublishSubtypes($news_item_id, $posted_fields);

    return $news_item_id;
  }

  /**
   * Processes categories.
   */
  public function processCategories($news_item_id, $posted_fields) {
    $news_item_db_class = new NewsItemDB();
    if (array_key_exists('topic_id', $posted_fields)) {
      $posted_topic_category = (int) $posted_fields['topic_id'];
      if ($posted_topic_category > 0) {
        $news_item_db_class->addNewsItemCategory($news_item_id, $posted_topic_category);
      }
    }
    if (array_key_exists('region_id', $posted_fields)) {
      $posted_region_category = (int) $posted_fields['region_id'];
      if ($posted_region_category > 0) {
        $news_item_db_class->addNewsItemCategory($news_item_id, $posted_region_category);
      }
    }
  }

  /**
   * Gets child news item type ID.
   */
  public function getChildNewsItemTypeId() {
    return NEWS_ITEM_TYPE_ID_COMMENT;
  }

  /**
   * Gets parent news item type ID.
   */
  public function getParentNewsItemTypeId() {
    return NEWS_ITEM_TYPE_ID_POST;
  }

  /**
   * Updates syndication wires.
   */
  public function updateSyndicationWires($news_item_id, $posted_fields) {
  }

  /**
   * Prints publish instructions.
   */
  public function printPublishInstructions() {
    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      $util = new FileUtil();
      $str = $util->loadFileAsString(INCLUDE_PATH . '/article/publish-instructions.inc');
      $str = str_replace('TPL_LOCAL_PAGE_ID', isset($_GET['page_id']) ? (int) $_GET['page_id'] : '', $str);
      echo $str;
    }
  }

  /**
   * Adds additional info for publish subtypes.
   */
  public function addAdditionalInfoForPublishSubtypes($news_item_id, $posted_fields) {
  }

  /**
   * Determines whether or not two strings are near duplicates.
   */
  public static function isNearDuplicate(string $string1, string $string2): bool {
    similar_text($string1, $string2, $percent);
    return $percent >= 99;
  }

  /**
   * Gets duplicate.
   */
  public function getDuplicate($posted_fields) {
    if ($posted_fields['file_count'] > 0) {
      return 0;
    }

    $article_db_class = new ArticleDB();
    $possible_dups = $article_db_class->findAllRecentDuplicateNonhiddenVersionsByTitle($posted_fields['title1']);

    if (is_array($possible_dups) && count($possible_dups) > 0) {
      foreach ($possible_dups as $possible_dup) {
        if ($possible_dup['summary'] == $posted_fields['summary']
          && $possible_dup['displayed_author_name'] == $posted_fields['displayed_author_name']
          && $possible_dup['related_url'] == $posted_fields['related_url']
          && $possible_dup['email'] == $posted_fields['email']
          && (int) $possible_dup['is_text_html'] == (int) $posted_fields['is_text_html']
          && ($possible_dup['text'] == $posted_fields['text'] || trim($possible_dup['text']) == trim($posted_fields['text']) || static::isNearDuplicate($possible_dup['text'], $posted_fields['text']))
          ) {
          if ((int) $posted_fields['parent_item_id'] != 0 && (int) $possible_dup['parent_item_id'] != $posted_fields['parent_item_id']) {
            return -1;
          }
          return $possible_dup['news_item_id'];
        }
      }
      return 0;
    }
    else {
      return 0;
    }

  }

}
