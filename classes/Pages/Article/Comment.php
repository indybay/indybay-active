<?php

namespace Indybay\Pages\Article;

use Indybay\DB\ArticleDB;
use Indybay\Renderer\Renderer;

/**
 * Renders comment form.
 */
class Comment extends Publish {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $article_db_class = new ArticleDB();
    $news_item_id = 0;
    if (array_key_exists('top_id', $_GET)) {
      $news_item_id = (int) $_GET['top_id'];
    }
    if ($news_item_id == 0 && array_key_exists('parent_item_id', $_POST)) {
      $news_item_id = (int) $_POST['parent_item_id'];
    }
    if ($news_item_id + 0 != 0) {
      $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
      if (is_array($article_info)) {
        $this->tkeys['local_parent_title1'] = htmlspecialchars($article_info['title1']);
        $this->tkeys['local_parent_displayed_author_name'] = htmlspecialchars($article_info['displayed_author_name']);
        $this->tkeys['local_parent_summary'] = Renderer::htmlPurify($article_info['summary']);
      }
    }

    if ($news_item_id + 0 == 0 || !is_array($article_info) || count($article_info) == 0) {
      echo "<h2>You have chosen to add a comment to a nonexistent article</h2>
				if you got here by clicking on 'add comment' click back and try again, otherwise you were probably given a bad link";
      exit;
    }
    parent::execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getChildNewsItemTypeId() {
    return NEWS_ITEM_TYPE_ID_COMMENT;
  }

}
