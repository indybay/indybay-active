<?php

namespace Indybay\Pages\Article;

use Indybay\DB\ArticleDB;
use Indybay\DB\MediaAttachmentDB;

/**
 * Renders audio element.
 */
class Audio {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $article_db_class = new ArticleDB();
    $media_attachment = new MediaAttachmentDB();
    $news_item_id = (int) basename($_SERVER['PHP_SELF']);
    $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
    $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    $media_attachment_info = $media_attachment->getMediaAttachmentInfo($version_info['media_attachment_id']);
    $file_name = $media_attachment_info['file_name'];
    $relative_path = $media_attachment_info['relative_path'];
    $relative_url = '/uploads/' . $relative_path . $file_name;
    header('Last-Modified: ' . gmdate(DATE_RFC7231, $version_info['modified_timestamp']));
    echo '<audio width="300" height="28" preload="none" controls><source src="', SERVER_URL, $relative_url, '" type="video/mp4"><a class="audio" href="', SERVER_URL, $relative_url, '">Download Audio ', $file_name, '</a></audio>';
  }

}
