<?php

namespace Indybay\Pages\Article;

use Indybay\DB\MediaAttachmentDB;
use Indybay\Pages\Admin\Article\ArticleList;
use Indybay\Renderer\MediaAttachmentRenderer;

/**
 * Renders a list of posts.
 */
class Newswire extends ArticleList {

  /**
   * Renders row.
   */
  public function renderRow($cell_counter, $row, $parent_row, $tr, $article_renderer, $article_cache_class, $display_options, $article_db_class) {

    $parent_item_id = $row['parent_item_id'];
    $news_item_id = $row['news_item_id'];
    $news_item_type_id = $row['news_item_type_id'];
    $media_attachment_renderer = new MediaAttachmentRenderer();

    if ($parent_item_id != $news_item_id && $parent_item_id != 0) {
      $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($parent_row['news_item_id'], $parent_row['creation_timestamp']);
      $searchlink .= "?show_comments=1#$news_item_id";
    }
    else {

      $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);
    }

    $search = ["'\''", "'<'", "'>'"];
    $replace = ['&#039;', '&lt;', '&gt;'];
    $title = preg_replace($search, $replace, $row['title2']);
    if ($title == '') {
      $title = preg_replace($search, $replace, $row['title1']);
    }

    if ($parent_item_id != $news_item_id && $parent_item_id != 0) {
      $parent_title = preg_replace($search, $replace, $parent_row['title1']);
      if ($news_item_type_id == 3) {
        $title .= ' <span class="list-attachment">(Comment On: ' . $parent_title . ')</span>';
      }
      if ($news_item_type_id == NEWS_ITEM_TYPE_ID_ATTACHMENT) {
        $title .= ' <span class="list-attachment">(Attachment On: ' . $parent_title . ')</span>';
      }
    }
    $author = preg_replace($search, $replace, $row['displayed_author_name']);
    $length = ($parent_item_id != $news_item_id) ? 160 : 497;
    $key = ($parent_item_id != $news_item_id) ? 'text' : 'summary';
    $summary = htmlspecialchars(mb_substr(trim(strip_tags($row[$key])), 0, $length)) . '...';

    $media_type_grouping_id = $row['media_type_grouping_id'];

    if ($media_type_grouping_id == 0) {
      $mime = 'text" src="' . MEDIA_TYPE_GROUPING_TEXT_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_TEXT) {
      $mime = 'text" src="' . MEDIA_TYPE_GROUPING_TEXT_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_IMAGE) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_IMAGE_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_AUDIO) {
      $mime = 'audio" src="' . MEDIA_TYPE_GROUPING_AUDIO_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_VIDEO) {
      $mime = 'video" src="' . MEDIA_TYPE_GROUPING_VIDEO_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_DOCUMENT) {
      $mime = 'document" src="' . MEDIA_TYPE_GROUPING_DOCUMENT_ICON . ' ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_OTHER) {
      $mime = 'other" src="' . MEDIA_TYPE_GROUPING_OTHER_ICON . ' ';
    }

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $mime = 'calendar" src="' . MEDIA_TYPE_GROUPING_CALENDAR_ICON . ' ';
    }

    if (($news_item_type_id == NEWS_ITEM_TYPE_ID_ATTACHMENT || $news_item_type_id == NEWS_ITEM_TYPE_ID_COMMENT)
                        && $media_type_grouping_id < MEDIA_TYPE_GROUPING_IMAGE) {
      $mime = 'image" src="' . COMMENT_ICON . '" ';
    }

    $divhtml = '<div class="searchwrap';
    if (!is_int($cell_counter / 2)) {
      $divhtml .= ' bg-grey';
    }
    $divhtml .= '">';

    if ($row['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
      $divhtml .= '<strike>';
    }

    $divhtml .= '<span class="list-titlewrap"><a href="' . $searchlink . '"><img alt="' . $mime . '"></a><a href="' . $searchlink . '">' . $title . '</a></span>';

    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_IMAGE || $media_type_grouping_id == MEDIA_TYPE_GROUPING_DOCUMENT) {

      $divhtml .= ' ';

      $divhtml .= '<div class="list-imgwrap"><a href="' . $searchlink . '" class="list-imglink">';
      $divhtml .= $media_attachment_renderer->renderSmallThumbnailFromNewsItemInfo($row, ' ');
      $divhtml .= '</a></div>';

    }
    elseif ($media_type_grouping_id != 0 && $media_type_grouping_id != 1) {

      $media_attachment_db = new MediaAttachmentDB();
      $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($row['media_attachment_id']);
      $file_name = $media_attachment_info['file_name'];
      $file_dir = UPLOAD_PATH . '/' . $media_attachment_info['relative_path'];

      $full_file_path = $file_dir . $file_name;
      $mime_type = $media_attachment_info['mime_type'];
      $file_size = 0;
      if (file_exists($full_file_path)) {
        $file_size = $media_attachment_renderer->renderFileSize(filesize($full_file_path));
      }

      $divhtml .= ' <span class="list-mediatype">(' . $mime_type . ' ' . $file_size . ')</span></span>';

    }
    else {

      $divhtml .= ' </span>';

    }

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
      $author = SHORT_SITE_NAME;
    }
    $divhtml .= ' <span class="list-author">by ' . $author . '</span>';

    if ($row['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
      $divhtml .= '</strike>';
    }

    $divhtml .= '<div class="list-summary">' . $summary;

    $divhtml .= '</div>';
    // If ($media_type_grouping_id==MEDIA_TYPE_GROUPING_IMAGE)
    // $divhtml.="<br clear=\"all\">";.
    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $divhtml .= '<div class="list-eventdate" >Event Date: ' . date(static::DATE_LONG, $row['displayed_timestamp']) . '</div>';
    }
    $divhtml .= '<div class="list-posted" >Posted: ' . date(static::DATE_LONG, $row['creation_timestamp']) . '</div>';

    $divhtml .= '</div>';

    return $divhtml;
  }

  /**
   * Returns search template.
   */
  public function getSearchTemplate() {
    return 'article/newswire_search.tpl';
  }

  /**
   * Gets news item status select list.
   */
  public function getNewsItemStatusSelectList() {

    $display_options = [];
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlighted-Local';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted-NonLocal';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED *
    NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted (Local & NonLocal)';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NEW *
    NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED *
    NEWS_ITEM_STATUS_ID_OTHER * NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = 'All';
    return $display_options;
  }

  /**
   * Disables bulk classification.
   */
  public function bulkClassify($posted_fields) {
    echo 'Classifictions Requires Being Logged Into The Admin System!!';
  }

}
