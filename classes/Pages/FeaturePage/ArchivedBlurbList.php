<?php

namespace Indybay\Pages\FeaturePage;

use Indybay\DB\FeaturePageDB;
use Indybay\DB\LogDB;
use Indybay\Page;
use Indybay\Renderer\BlurbRenderer;

/**
 * Class for display_by_date.
 */
class ArchivedBlurbList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    // Old google news link.
    if (!empty($_GET['current'])) {
      header('HTTP/1.0 301 Moved Permanently');
      header('Location: ' . SERVER_URL . '/news/search_results.php?news_item_status_restriction=690690&include_blurbs=1&include_posts=0&include_events=0');
      exit;
    }

    $logdb = new LogDB();
    $logdb->addLogEntryBeforeSearch();

    $page_id = (int) $_GET['page_id'];
    $page_number = isset($_GET['page_number']) ? (int) $_GET['page_number'] : 0;

    if ($page_id == 0) {
      $page_id = FRONT_PAGE_CATEGORY_ID;
    }

    $feature_page_db_class = new FeaturePageDB();

    $page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    if (empty($GLOBALS['db_down'])) {
      if (!is_array($page_info)) {
        $this->redirect('/');
      }
      $feature_page_name = $page_info['long_display_name'];
      $feature_page_url = '/' . $page_info['relative_path'];
      $this->tkeys['local_feature_page_name'] = $feature_page_name;
      $this->tkeys['local_feature_page_url'] = $feature_page_url;

      $start_limit = 0;

      $page_size = 7;
      $start_limit = ($page_number) * $page_size;
      if ($start_limit < 0) {
        http_response_code(404);
        $blurb_list = [];
      }
      else {
        $blurb_list = $feature_page_db_class->getArchivedBlurbListLimitedInfo($page_id, $start_limit, $page_size + 1);
      }
      if (is_array($blurb_list)) {
        $this->tkeys['nav'] = '';
        if ($page_number > 0) {
          $this->tkeys['nav'] .= '<a href="/archives/archived_blurb_list.php?page_id=' .
                        $page_id . '&page_number=' . ($page_number - 1) .
                        '"><img src="/im/arrow-back.svg" alt="back" title="back page" class="mediaicon pageicon" width="12" height="12"></a>&#160;&#160;';
        }
        $this->tkeys['nav'] .= $page_number + 1;
        if (count($blurb_list) > $page_size) {
          $this->tkeys['nav'] .= '&#160;&#160;
			        	<a href="/archives/archived_blurb_list.php?page_id=' .
                            $page_id . '&page_number=' . ($page_number + 1) .
                            '"><img src="/im/arrow-next.svg" alt="next" title="next page" class="mediaicon pageicon" width="12" height="12"></a>';
        }
        $this->tkeys['nav'] .= '';

        $blurb_renderer_class = new BlurbRenderer();
        $i = 0;
        $tblhtml = '';
        foreach ($blurb_list as $blurb) {
          $news_item_id = $blurb['news_item_id'];
          $next_html = $blurb_renderer_class->getRenderedBlurbHtmlForListFromNewsItemId($news_item_id);
          $tblhtml .= $next_html;
          $i = $i + 1;
          if ($i == $page_size) {
            break;
          }
        }
      }
      $this->tkeys['local_html'] = $tblhtml;
      $this->tkeys['local_page_id'] = $page_id;
    }
    else {
      $this->tkeys['local_html'] = '';
      $this->tkeys['local_page_id'] = 0;
      $this->tkeys['nav'] = '';
      $this->tkeys['local_feature_page_name'] = 'Feature Page';
      $this->tkeys['local_feature_page_url'] = '/';
      $this->addStatusMessage('The database running this site is busy due to a large number of people looking at the site. Try searching again in a few minutes');
      $this->addStatusMessage("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
    }

    $logdb->updateLogIdAfterSearch();

  }

}
