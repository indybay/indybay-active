<?php

namespace Indybay\Pages\Misc;

use Indybay\Page;
use Indybay\Translate;

/**
 * Class for network page.
 */
class Network extends Page {

  /**
   * Class constructor, does nothing.
   */
  public function __construct() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // We are only assigning variables here.
    $tr = new Translate();
    $this->tkeys['local_cities_list'] = '';

    $cities = file(INDYBAY_BASE_PATH . '/cities.inc');
    foreach ($cities as $line) {
      $this->tkeys['local_cities_list'] .= $line . "<br>\n";
    }

    $this->tkeys['local_site_crumb'] = $GLOBALS['site_crumb'];
    $this->tkeys['local_site_nick']  = $GLOBALS['site_nick'];
    $this->tkeys['local_refurl']     = ROOT_URL . $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'];
    $this->tkeys['local_updated']    = $tr->trans('last_updated') . ' ' . date('r', filemtime(INDYBAY_BASE_PATH . '/cities.inc'));
  }

}
