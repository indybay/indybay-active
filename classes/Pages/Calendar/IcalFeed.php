<?php

namespace Indybay\Pages\Calendar;

use Indybay\Cache\NewsItemCache;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\SearchDB;
use Indybay\Date;
use Indybay\Page;

/**
 * Generate an icalendar event feed.
 */
class IcalFeed extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
  }

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $feature_page_db_class = new FeaturePageDB();
    $search_db_class = new SearchDB();
    $news_item_cache_class = new NewsItemCache();
    $topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : '0';
    $region_id = isset($_GET['region_id']) ? intval($_GET['region_id']) : '0';
    $news_item_status_restriction = isset($_GET['news_item_status_restriction']) ? intval($_GET['news_item_status_restriction']) : 1365;
    $calendar_name = SHORT_SITE_NAME;
    $pages = [];
    if ($topic_id) {
      $pages[] = $feature_page_db_class->getFeaturePageInfo($topic_id);
    }
    if ($region_id) {
      $pages[] = $feature_page_db_class->getFeaturePageInfo($region_id);
    }
    foreach ($pages as $page) {
      $calendar_name .= ' - ' . $page['long_display_name'];
    }
    $this->tkeys['local_calendar_name'] = $calendar_name;
    $date_range_start = new Date();
    $date_range_start->setDateToNow();
    $date_range_end = new Date();
    $date_range_end->setDateToNow();
    $date_range_end->moveForwardDays(365);
    $search_result = $search_db_class->standardSearch(180, 0, NULL, $news_item_status_restriction, 0, 0, 0, 1, 0, NULL, $topic_id, $region_id, 0, 'displayed_date', $date_range_start, $date_range_end);
    $this->tkeys['local_events'] = '';
    foreach ($search_result as $article_info) {
      $event = [];
      $event['url'] = SERVER_URL . $news_item_cache_class->getWebCachePathForNewsItemGivenDate($article_info['news_item_id'], $article_info['creation_timestamp']);
      $article_info['text'] .= "\n" . $event['url'];
      $event['summary'] = wordwrap(str_replace(["\r\n", "\n"], "\\n", html_entity_decode(strip_tags($article_info['summary']), ENT_QUOTES, 'UTF-8')), 75, " \r\n ");
      $event['description'] = wordwrap(str_replace(["\r\n", "\n"], "\\n", html_entity_decode(strip_tags($article_info['text']), ENT_QUOTES, 'UTF-8')), 75, " \r\n ");
      $event['title'] = wordwrap($article_info['title1'], 65, " \r\n ");
      $event['news_item_id'] = $article_info['news_item_id'];
      $event['news_item_version_id'] = $article_info['news_item_version_id'];
      $created_timestamp = $article_info['creation_timestamp'];
      $start_timestamp = $article_info['displayed_timestamp'];
      $duration = $article_info['event_duration'];
      $end_timestamp = $start_timestamp + ($duration * 60 * 60);
      $event['email'] = '';
      $event['ical_created'] = gmdate('Ymd', $created_timestamp) . 'T' . gmdate('Hi00', $created_timestamp) . 'Z';
      $event['ical_start'] = gmdate('Ymd', $start_timestamp) . 'T' . gmdate('Hi00', $start_timestamp) . 'Z';
      $event['ical_end'] = gmdate('Ymd', $end_timestamp) . 'T' . gmdate('Hi00', $end_timestamp) . 'Z';
      $template_name = 'pages/calendar/ical_feed_event.tpl';
      $temp_array = [];
      foreach ($event as $key => $value) {
        $temp_array['TPL_LOCAL_' . strtoupper($key)] = $value;
      }
      $this->tkeys['local_events'] .= $this->render($template_name, $temp_array);
    }
  }

}
