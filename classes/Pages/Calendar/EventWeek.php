<?php

namespace Indybay\Pages\Calendar;

use Indybay\Cache\EventCache;
use Indybay\DB\CategoryDB;
use Indybay\DB\FeaturePageDB;
use Indybay\Date;
use Indybay\Page;
use Indybay\Renderer\EventRenderer;

/**
 * Class for the Calender Main Page...
 */
class EventWeek extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Quick fix for idiotic XSS vulns.
    foreach ($_GET as $key => $value) {
      $_GET[$key] = htmlspecialchars($value);
    }
    if (array_key_exists('page_id', $_GET)) {
      $page_id = (int) $_GET['page_id'];
    }
    else {
      $page_id = 0;
    }
    $topic_id = 0;
    $region_id = 0;
    if ($page_id > 0) {
      $feature_page_db = new FeaturePageDB();
      $topic_id = $feature_page_db->getFirstTopicForPage($page_id);
      $region_id = $feature_page_db->getFirstRegionForPage($page_id);
    }
    else {
      if (isset($_GET['topic_id'])) {
        $topic_id = (int) $_GET['topic_id'];
      }
      if (isset($_GET['region_id'])) {
        $region_id = (int) $_GET['region_id'];
      }
    }

    $GLOBALS['page_title'] = $GLOBALS['dict']['calendar'];
    $event_renderer = new EventRenderer();

    $date = new Date();
    $date->setDateToNow();
    if (!array_key_exists('day', $_GET)) {
      $_GET['day'] = '';
    }
    if (!array_key_exists('month', $_GET)) {
      $_GET['month'] = '';
    }
    if (!array_key_exists('year', $_GET)) {
      $_GET['year'] = '';
    }
    if (strlen($_GET['day']) > 0) {
      $date->setTime(0, 0, $_GET['day'], $_GET['month'], $_GET['year']);
    }
    $datenow = $date->clonedate();
    $datenow->findStartOfWeek();
    $date->findStartOfWeek();

    if (!array_key_exists('site_start_year', $GLOBALS)) {
      $GLOBALS['site_start_year'] = 2000;
    }
    if ($_GET['year'] > date('Y') + 1 || $date->getYear() < $GLOBALS['site_start_year']) {
      $this->forcedTemplateFile = 'misc/missing.tpl';
      http_response_code(404);
      return;
    }
    $this->tkeys['local_day'] = $date->getDay();
    $this->tkeys['local_month'] = $date->getMonth();
    $this->tkeys['local_year'] = $date->getYear();
    $nextweek = $date->clonedate();
    if ($nextweek->getYear() > $GLOBALS['site_start_year'] - 1 && $nextweek->getYear() < date('Y') + 2) {
      $nextweek->moveForwardWeeks(1);
    }
    $nextweek_day = $nextweek->getDay();
    $nextweek_month = $nextweek->getMonth();
    $nextweek_year = $nextweek->getYear();

    $this->tkeys['local_next_week_day'] = $nextweek_day;
    $this->tkeys['local_next_week_month'] = $nextweek_month;
    $this->tkeys['local_next_week_year'] = $nextweek_year;

    $lastweek = $date->clonedate();
    if ($lastweek->getYear() > $GLOBALS['site_start_year'] - 1 && $lastweek->getYear() < date('Y') + 2) {
      $lastweek->moveForwardWeeks(-1);
    }
    $lastweek_day = $lastweek->getDay();
    $lastweek_month = $lastweek->getMonth();
    $lastweek_year = $lastweek->getYear();

    $this->tkeys['local_last_week_day'] = $lastweek_day;
    $this->tkeys['local_last_week_month'] = $lastweek_month;
    $this->tkeys['local_last_week_year'] = $lastweek_year;

    $this->tkeys['local_topic_id'] = $topic_id;

    $this->tkeys['local_region_id'] = $region_id;

    if (isset($_GET['news_item_status_restriction'])) {
      $news_item_status_restriction = intval($_GET['news_item_status_restriction']);
    }
    else {
      $news_item_status_restriction = 0;
    }
    // Short Month View.
    $this->tkeys['local_news_item_status_restriction'] = $news_item_status_restriction;

    $this->tkeys['CAL_EVENT_MONTH_VIEW_PREV'] = $event_renderer->renderShortMonthViewPrev($date, $datenow, $topic_id, $region_id, $news_item_status_restriction);

    $this->tkeys['CAL_EVENT_MONTH_VIEW_NEXT'] = $event_renderer->renderShortMonthViewNext($date, $datenow, $topic_id, $region_id, $news_item_status_restriction);

    // Topic Drop Down.
    $category_db_class = new CategoryDB();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_international_options, $cat_topic_options);

    $this->tkeys['CAL_EVENT_TOPIC_DROPDOWN'] = $event_renderer->makeSelectForm('topic_id', $cat_topic_options, $topic_id, 'All');
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['CAL_EVENT_LOCATION_DROPDOWN'] = $event_renderer->makeSelectForm('region_id', $cat_region_options, $region_id, 'All');

    $category_db_class = new CategoryDB();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_topic_options, $cat_international_options);
    $this->tkeys['CAL_CUR_DATE'] = date('m/d/Y', $date->getUnixTime());

    $this->tkeys['CAL_DAY'] = $_GET['day'];
    $this->tkeys['CAL_MONTH'] = $_GET['month'];
    $this->tkeys['CAL_YEAR'] = $_GET['year'];

    // Daytitles.
    $tmpday = 0;
    $daytitles = '<div class="grid--row-full cal-main-nav-mobile">';
    $daytitles .= '<span>Jump to:</span> ';
    while ($tmpday < 7) {
      $daytitles .= '<span class="cal-main-nav-day nowrap">';
      $daytitles .= '<a href="#week-day-' . $tmpday . '">';
      $daytitles .= date('D n/j', $date->getUnixTime());
      $daytitles .= '</a> ';
      if ($tmpday !== 6) {
        $daytitles .= '</span> | ';
      }
      else {
        $daytitles .= '</span>';
      }
      $date->moveForwardDays(1);
      $tmpday++;
    }
    $daytitles .= '</div>';

    $this->tkeys['CAL_EVENT_MONTH_DAYTITLE'] = $daytitles;

    // Event Week Render.
    $date->moveForwardDays(-7);

    $newdate = $date->clonedate();
    $newdate->findStartOfWeek();

    $event_cache_class = new EventCache();
    $html_for_week = $event_cache_class->getCachedWeek(
    $date, $topic_id, $region_id, $news_item_status_restriction);

    $this->tkeys['CAL_EVENT_MONTH_VIEW_FULL'] = $html_for_week;

    return 1;
  }

}
