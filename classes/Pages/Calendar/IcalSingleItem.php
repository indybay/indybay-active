<?php

namespace Indybay\Pages\Calendar;

use Indybay\Cache\NewsItemCache;
use Indybay\DB\ArticleDB;
use Indybay\Page;

/**
 * Renders single item ical feed.
 */
class IcalSingleItem extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
  }

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $news_item_id = intval($_GET['news_item_id']);
    $article_db_class = new ArticleDB();
    $news_item_cache_class = new NewsItemCache();
    if ($article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id)) {
      $article_info['text'] .= "\n " . SERVER_URL . $news_item_cache_class->getWebCachePathForNewsItemGivenDate($article_info['news_item_id'], $article_info['creation_timestamp']);
      $this->tkeys['local_title'] = $article_info['title1'];
      $this->tkeys['local_summary'] = wordwrap(str_replace(["\r\n", "\n"], "\\n", html_entity_decode(strip_tags($article_info['summary']), ENT_QUOTES, 'UTF-8')), 75, " \r\n ");
      $this->tkeys['local_description'] = wordwrap(str_replace(["\r\n", "\n"], "\\n", html_entity_decode(strip_tags($article_info['text']), ENT_QUOTES, 'UTF-8')), 75, " \r\n ");
      $this->tkeys['local_news_item_id'] = $news_item_id;
      $this->tkeys['local_news_item_version_id'] = $article_info['news_item_version_id'];
      $created_timestamp = $article_info['creation_timestamp'];
      $start_timestamp = $article_info['displayed_timestamp'];
      $duration = $article_info['event_duration'];
      // Echo "test st was \"".$start_time_stamp."\"";.
      $end_timestamp = $start_timestamp + ($duration * 60 * 60);
      $this->tkeys['local_email'] = '';
      $this->tkeys['local_ical_created'] = gmdate('Ymd', $created_timestamp) . 'T' . gmdate('Hi00', $created_timestamp) . 'Z';
      $this->tkeys['local_ical_start'] = gmdate('Ymd', $start_timestamp) . 'T' . gmdate('Hi00', $start_timestamp) . 'Z';
      $this->tkeys['local_ical_end'] = gmdate('Ymd', $end_timestamp) . 'T' . gmdate('Hi00', $end_timestamp) . 'Z';
      $this->tkeys['local_url'] = SERVER_URL . $news_item_cache_class->getWebCachePathForNewsItemGivenDate($article_info['news_item_id'], $article_info['creation_timestamp']);
    }
    else {
      header('HTTP/1.1 404 Not Found');
    }
  }

}
