<?php

namespace Indybay\Pages\Rss;

use Indybay\Cache\Cache;
use Indybay\DB\LogDB;
use Indybay\Syndication\RSSUtil;

/**
 * RSS generator.
 */
class RssGenerator {

  /**
   * Max age in seconds.
   */
  const MAX_AGE = 1800;

  /**
   * Generates RSS.
   */
  public function generate($search_fields) {

    $num_results = 30;
    $news_item_status_restriction = 0;
    $include_posts = 1;
    $include_events = 0;
    $include_blurbs = 0;
    $media_type_grouping_id = 0;
    $topic_id = 0;
    $region_id = 0;

    if (!isset($search_fields['include_posts']) && !isset($search_fields['submitted_search'])
    && !isset($search_fields['include_blurbs'])) {
      $include_posts = 1;
    }
    elseif (isset($search_fields['include_posts'])) {
      $include_posts = (int) $search_fields['include_posts'];
    }
    else {
      $include_posts = 0;
    }

    if (isset($search_fields['media_type_grouping_id'])) {
      $media_type_grouping_id = (int) $search_fields['media_type_grouping_id'];
    }
    else {
      $media_type_grouping_id = 0;
    }

    if (!isset($search_fields['include_events']) && !isset($search_fields['submitted_search']) && !isset($search_fields['include_blurbs'])) {
      $include_events = 1;
    }
    elseif (isset($search_fields['include_events'])) {
      $include_events = (int) $search_fields['include_events'];
    }
    else {
      $include_events = 0;
    }

    if (isset($search_fields['include_blurbs'])) {
      $include_blurbs = (int) $search_fields['include_blurbs'];
    }
    else {
      $include_blurbs = 0;
    }

    if (isset($search_fields['topic_id'])) {
      $topic_id = (int) $search_fields['topic_id'];
    }
    else {
      $topic_id = 0;
    }
    if (isset($search_fields['region_id'])) {
      $region_id = (int) $search_fields['region_id'];
    }
    else {
      $region_id = 0;
    }

    if (isset($search_fields['page_id'])) {
      $page_id = (int) $search_fields['page_id'];
    }
    else {
      $page_id = 0;
    }

    // Assumes topic and region ids line up with page ids.
    if (!$region_id && $page_id > 0) {
      $region_id = $page_id;
    }
    if (!$topic_id && $page_id > 0) {
      $topic_id = $page_id;
    }

    if (!isset($news_item_status_restriction) || $news_item_status_restriction == 0) {
      if (!empty($search_fields['news_item_status_restriction'])) {
        $news_item_status_restriction = (int) $search_fields['news_item_status_restriction'];
      }
      else {
        $news_item_status_restriction = 690690;
      }
    }

    if (isset($search_fields['include_full_text'])) {
      $include_full_text = (int) $search_fields['include_full_text'];
    }
    else {
      $include_full_text = 0;
    }

    if (isset($search_fields['rss_version'])) {
      $rss_version = (int) $search_fields['rss_version'];
    }
    else {
      $rss_version = 0;
    }

    if (isset($search_fields['search'])) {
      $search = strip_tags($search_fields['search']);
    }
    else {
      $search = '';
    }

    $result = $this->cacheHelper($num_results, $news_item_status_restriction,
       $include_posts, $include_events, $include_blurbs,
       $media_type_grouping_id, $topic_id, $region_id, $include_full_text, $rss_version, $search);
    $result = $this->removeExclusions($result, $_GET);

    if ('' === $result) {
      http_response_code(404);
    }
    return $result;

  }

  /**
   * Removes exclusions.
   */
  public function removeExclusions($rss_string, $fields) {

    if (isset($_GET['exclude_duplicates_from_cat_1']) && $_GET['exclude_duplicates_from_cat_1'] != '' && $_GET['rss_version'] == 1) {

      $objXml = new \DOMDocument();
      $objXml->loadXML($rss_string);
      $root_node = $objXml->getElementsByTagName('RDF')->item(0);
      $nodes = $root_node->getElementsByTagName('item');
      foreach ($nodes as $node) {
        $subjects = $node->getElementsByTagName('subject');
        foreach ($subjects as $subject) {
          if ($subject->nodeValue == 'Santa Cruz') {
            $root_node->removeChild($node);
          }
        }
      }
      return $objXml->saveXML();
    }
    elseif (isset($_GET['exclude_duplicates_from_cat_1']) && $_GET['exclude_duplicates_from_cat_1'] != '') {

      $objXml = new \DOMDocument();
      $objXml->loadXML($rss_string);
      $root_node = $objXml->getElementsByTagName('channel')->item(0);
      $nodes = $root_node->getElementsByTagName('item');
      foreach ($nodes as $node) {
        $subjects = $node->getElementsByTagName('subject');
        foreach ($subjects as $subject) {
          if ($subject->nodeValue == 'Santa Cruz') {
            $root_node->removeChild($node);
          }
        }
      }
      return $objXml->saveXML();
    }
    else {
      return $rss_string;
    }

  }

  /**
   * Cache helper.
   */
  public function cacheHelper(
    $num_results,
    $news_item_status_restriction,
    $include_posts,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $include_full_text,
    $rss_version,
    $search,
  ) {

    $cache_util = new Cache();

    $file_path = $this->makeRssName($num_results, $news_item_status_restriction,
       $include_posts, $include_events, $include_blurbs,
       $media_type_grouping_id, $topic_id, $region_id, $include_full_text, $rss_version, $search);
    $var_array = [];
    $var_array[0] = (int) $num_results;
    $var_array[1] = (int) $news_item_status_restriction;
    $var_array[2] = (int) $include_posts;
    $var_array[3] = (int) $include_events;
    $var_array[4] = (int) $include_blurbs;
    $var_array[5] = (int) $media_type_grouping_id;
    $var_array[6] = (int) $topic_id;
    $var_array[7] = (int) $region_id;
    $var_array[8] = (int) $include_full_text;
    $var_array[9] = (int) $rss_version;
    $vars = implode('_', $var_array);
    if ($search != '') {
      $args = [];
      $args[0] = $vars;
      $args[1] = $search;
      $result = $cache_util->cacheOrCallBasedOffAge($this, 'rssCacheHelper', $args, $file_path, static::MAX_AGE, $file_m_time);
    }
    else {
      $result = $cache_util->cacheOrCallBasedOffAge($this, 'rssCacheHelper', $vars, $file_path, static::MAX_AGE, $file_m_time);
    }
    header('Cache-Control: max-age=' . static::MAX_AGE);
    header('Last-Modified: ' . gmdate(DATE_RFC7231, $file_m_time));
    return $result;
  }

  /**
   * RSS cache helper.
   */
  public function rssCacheHelper($args) {

    if (is_array($args)) {
      $vars = $args[0];
      $search = $args[1];
    }
    else {
      $vars = $args;
      $search = '';
    }

    $logdb = new LogDB();
    $logdb->addLogEntryBeforeSearch();

    $rss_util_class = new RSSUtil();
    $var_array = explode('_', $vars);
    $num_results = $var_array[0];
    $news_item_status_restriction = $var_array[1];
    $include_posts = $var_array[2];
    $include_events = $var_array[3];
    $include_blurbs = $var_array[4];
    $media_type_grouping_id = $var_array[5];
    $topic_id = $var_array[6];
    $region_id = $var_array[7];
    $include_full_text = $var_array[8];
    $rss_version = $var_array[9];

    $result = $rss_util_class->makeNewsItemRssString($num_results, $news_item_status_restriction,
                    $include_posts, $include_events, $include_blurbs,
                    $media_type_grouping_id, $topic_id, $region_id, $include_full_text, $rss_version, $search);

    $logdb->updateLogIdAfterSearch();

    return $result;
  }

  /**
   * Makes RSS name.
   */
  public function makeRssName(
    $num_results,
    $news_item_status_restriction,
    $include_posts,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $include_full_text,
    $rss_version,
    $search = '',
  ) {
    $filename = CACHE_PATH . '/rss/cache_file_' . $num_results . '_' . $news_item_status_restriction;
    $filename .= '_' . $include_posts . '_' . $include_events . '_' . $include_blurbs;
    $filename .= '_' . $media_type_grouping_id . '_' . $topic_id . '_' . $region_id . '_' . $include_full_text . '_';

    if ($search != '') {
      $filename .= md5($search) . '_';
    }
    $filename .= $rss_version . '.rss';

    return $filename;
  }

}
