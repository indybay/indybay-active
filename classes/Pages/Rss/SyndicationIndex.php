<?php

namespace Indybay\Pages\Rss;

use Indybay\DB\FeaturePageDB;
use Indybay\Page;

/**
 * Class used for /syn/index.php.
 */
class SyndicationIndex extends Page {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $feature_page_db_class = new FeaturePageDB();

    $page_list = $feature_page_db_class->getPageList(1, 0);
    $this->tkeys['LOCAL_REGION_SYN_LIST'] = $this->renderSyndicationRows($page_list);

    $page_list = $feature_page_db_class->getPageList(2, 0);
    $this->tkeys['LOCAL_TOPIC_SYN_LIST'] = $this->renderSyndicationRows($page_list);

    $page_list = $feature_page_db_class->getPageList(2, 44);
    $this->tkeys['LOCAL_INT_SYN_LIST'] = $this->renderSyndicationRows($page_list);
  }

  /**
   * Renders syndication rows.
   */
  public function renderSyndicationRows($page_list) {
    $syndication_index = '';
    foreach ($page_list as $row) {
      // $syndication_index .= "\r\n<tr><td><strong>" .
      // $row['short_display_name'] . '</strong></td></tr>';.
      $syndication_index .= '<tr><td class="bg-greyold"><strong>' .
        $row['long_display_name'] . '</strong><br>';

      $syndication_index .= '<a href="' . ROOT_URL . 'syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_blurbs=1&amp;rss_version=1&amp;page_id=' .
        $row['page_id'] . '">Features</a> (simple) / ';
      // TPL_FEATURES not working as link text above.
      $syndication_index .= "\r\n<a href=\"" . ROOT_URL . 'syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_blurbs=1&amp;rss_version=1&amp;include_full_text=1&amp;page_id=' .
        $row['page_id'] . '">Features</a> (with content) <br>';

      $syndication_index .= '<a href="' . ROOT_URL . 'syn/generate_rss.php?include_posts=1&amp;include_events=0&amp;news_item_status_restriction=3&amp;rss_version=1&amp;page_id=' . $row['page_id'] . '">Newswire (rdf)</a> / ';

      $syndication_index .= '<a href="' . ROOT_URL . 'syn/generate_rss.php?include_posts=1&amp;include_events=0&amp;news_item_status_restriction=3&amp;page_id=' . $row['page_id'] . '">Newswire (xml)</a> / ';

      $syndication_index .= '<a href="' . ROOT_URL . 'syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_events=1&amp;page_id=' . $row['page_id'] . '">';

      $syndication_index .= 'Calendar (rss)</a></td></tr><tr><td>&#160;</td></tr>';
      // TPL_CALENDAR not working as link text above.
    }
    return $syndication_index;

  }

}
