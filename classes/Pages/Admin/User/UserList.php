<?php

namespace Indybay\Pages\Admin\User;

use Indybay\DB\UserDB;
use Indybay\Page;

/**
 * Class for user_display_list page.
 */
class UserList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $user_db_obj = new UserDB();
    $user_list = $user_db_obj->getRecentList(15);

    $i = 1;
    $gridhtml = '';
    foreach ($user_list as $nextuser) {
      $gridhtml .= '<!-- start getRecentList -->';
      if ($i === 1) {
        $addClass = ' first-bg-header-mobile';
      }
      else {
        $addClass = '';
      }
      if ($nextuser['user_id'] == $_SESSION['session_user_id']) {
        $gridhtml .= '<div class="attribute bg-grey' . $addClass . '" data-attr="Username:"><a title="Edit User" href="user_edit.php?user_id=';
      }
      else {
        $gridhtml .= '<div class="attribute bg-grey" data-attr="Username:"><a title="View User" href="user_detail.php?user_id=';
      }
      $gridhtml .= $nextuser['user_id'] . '">';
      $gridhtml .= $nextuser['username'] . '</a></div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="First Name:">' . $nextuser['first_name'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Name:">' . $nextuser['last_name'] . '</div>';
      if ($nextuser['email']) {
        $nextemail = '<a href="mailto:' . $nextuser['email'] . '" title="' . $nextuser['email'] . '">@ email</a>';
      }
      else {
        $nextemail = '';
      }
      if ($nextuser['phone']) {
        $nextphone = '<a href="tel:' . $nextuser['phone'] . '" title="' . $nextuser['phone'] . '">&#9742; phone</a>';
      }
      else {
        $nextphone = '';
      }
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Contact:">' . $nextemail . '<br>' . $nextphone . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="ID:">' . $nextuser['user_id'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Login:">' . $nextuser['last_login'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Activity:">' . $nextuser['last_activity'] . '</div>';
      $gridhtml .= '<!-- END getRecentList -->';
      $i++;
    }
    $this->tkeys['enabled_users_recent'] = $gridhtml;

    $user_list = $user_db_obj->getNonrecentList(15);
    $i = 1;
    $gridhtml = '';
    foreach ($user_list as $nextuser) {
      $gridhtml .= '<!-- start getNonrecentList -->';
      if ($i === 1) {
        $addClass = ' first-bg-header-mobile';
      }
      else {
        $addClass = '';
      }
      if ($nextuser['user_id'] == $_SESSION['session_user_id']) {
        $gridhtml .= '<div class="attribute bg-grey' . $addClass . '" data-attr="Username:"><a title="Edit User" href="user_edit.php?user_id=';
      }
      else {
        $gridhtml .= '<div class="attribute bg-grey' . $addClass . '" data-attr="Username:"><a title="View User" href="user_detail.php?user_id=';
      }
      $gridhtml .= $nextuser['user_id'] . '">';
      $gridhtml .= $nextuser['username'] . '</a></div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="First Name:">' . $nextuser['first_name'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Name:">' . $nextuser['last_name'] . '</div>';
      if ($nextuser['email']) {
        $nextemail = '<a href="mailto:' . $nextuser['email'] . '" title="' . $nextuser['email'] . '">@ email</a>';
      }
      else {
        $nextemail = '';
      }
      if ($nextuser['phone']) {
        $nextphone = '<a href="tel:' . $nextuser['phone'] . '" title="' . $nextuser['phone'] . '">&#9742; phone</a>';
      }
      else {
        $nextphone = '';
      }
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Contact:">' . $nextemail . '<br>' . $nextphone . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="ID:">' . $nextuser['user_id'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Login:">' . $nextuser['last_login'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Activity:">' . $nextuser['last_activity'] . '</div>';
      $gridhtml .= '<!-- END getNonrecentList -->';
      $i++;
    }
    $this->tkeys['enabled_users_nonrecent'] = $gridhtml;

    $user_list = $user_db_obj->getList(0);
    $i = 1;
    $gridhtml = '';
    foreach ($user_list as $nextuser) {
      $gridhtml .= '<!-- start getList -->';
      if ($i === 1) {
        $addClass = ' first-bg-header-mobile';
      }
      else {
        $addClass = '';
      }
      $gridhtml .= '<div class="attribute bg-grey' . $addClass . '" data-attr="Username:"><a title="Edit User" href="user_edit.php?user_id=';
      $gridhtml .= $nextuser['user_id'] . '">';
      $gridhtml .= $nextuser['username'] . '</a></div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="First Name:">' . $nextuser['first_name'] . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Last Name:">' . $nextuser['last_name'] . '</div>';
      if ($nextuser['email']) {
        $nextemail = '<a href="mailto:' . $nextuser['email'] . '" title="' . $nextuser['email'] . '">@ email</a>';
      }
      else {
        $nextemail = '';
      }
      $gridhtml .= '<div class="attribute bg-grey" data-attr="Contact:">' . $nextemail . '</div>';
      $gridhtml .= '<div class="attribute bg-grey" data-attr="ID:">' . $nextuser['user_id'] . '</div>';
      $gridhtml .= '<div class="grid--item-span-2 attribute bg-grey" data-attr="Last Activity:">' . $nextuser['last_login'] . '</div>';
      $gridhtml .= '<!-- END getList -->';
      $i++;
    }
    $this->tkeys['disabled_users'] = $gridhtml;

    $this->tkeys['local_session_user_id'] = $_SESSION['session_user_id'];
    return 1;
  }

}
