<?php

namespace Indybay\Pages\Admin\User;

use Indybay\DB\UserDB;
use Indybay\Page;
use Indybay\Renderer\UserRenderer;

/**
 * Class for user_add page.
 */
class UserAdd extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $user_renderer_class = new UserRenderer();

    if (isset($_POST['new_password1'])) {
      $password1 = $_POST['new_password1'];
    }
    else {
      $password1 = $this->createRandomPassword();
    }

    if (isset($_POST['new_password2'])) {
      $password2 = $_POST['new_password2'];
    }
    else {
      $password2 = $this->createRandomPassword();
    }

    if (isset($_POST['username'])) {
      $username = $_POST['username'];
    }
    else {
      $username = '';
    }
    if (isset($_POST['first_name'])) {
      $first_name = $_POST['first_name'];
    }
    else {
      $first_name = '';
    }
    if (isset($_POST['last_name'])) {
      $last_name = $_POST['last_name'];
    }
    else {
      $last_name = '';
    }
    if (isset($_POST['email'])) {
      $email = $_POST['email'];
    }
    else {
      $email = '';
    }
    if (isset($_POST['phone'])) {
      $phone = $_POST['phone'];
    }
    else {
      $phone = '';
    }
    if (isset($_POST['has_login_rights'])) {
      $has_login_rights = $_POST['has_login_rights'];
    }
    else {
      $has_login_rights = '';
    }

    if (isset($_POST['is_posted'])) {
      $user_obj = new UserDB();
      $failed = 0;
      if (!isset($username) || !count($username > 3)) {
        $this->addValidationMessage('Username Is Required And Must Be At Least 3 Characters Long');
        $failed = 1;
      }
      if (!isset($password1) || strlen($password1) < 6) {
        $this->addValidationMessage('Password Is Required And Must Be At Least 6 Characters Long');
        $failed = 1;
      }
      elseif ($password1 != $password2) {
        $this->addValidationMessage('Passwords Did Not Match');
        $failed = 1;
      }
      if (!isset($email) || !strlen($email) > 3) {
        $this->addValidationMessage('Email Is Required And Must Be In Valid Format');
        $failed = 1;
      }
      if (!$failed) {
        $_POST['password'] = $_POST['new_password1'];
        $user_id = $user_obj->add($_POST);
        if ($user_id + 0 > 0) {
          $this->redirect('user_list.php');
        }
        else {
          echo 'error adding user<br>';
        }
      }
    }

    $this->tkeys['local_username'] = $username;

    $this->tkeys['local_email'] = $email;
    $this->tkeys['local_phone'] = $phone;
    $this->tkeys['local_first_name'] = $first_name;
    $this->tkeys['local_last_name'] = $last_name;
    $this->tkeys['local_form_action'] = 'user_update.php';

    $this->tkeys['local_checkbox_has_login_rights'] = $user_renderer_class->makeBooleanCheckboxForm('has_login_rights', $has_login_rights);

    return 1;
  }

  /**
   * Returns a random password.
   */
  public function createRandomPassword() {

    $chars = 'abcdefghijkmnopqrstuvwxyz023456789';
    $i = 0;
    $pass = '';

    while ($i < 20) {
      $num = random_int(0, 33);
      $pass .= substr($chars, $num, 1);
      $i++;
    }

    return $pass;
  }

}
