<?php

namespace Indybay\Pages\Admin\User;

use Indybay\DB\UserDB;
use Indybay\Page;

/**
 * Class for user_update page.
 */
class UserUpdate extends Page {

  /**
   * We can do everything in the constructor.
   */
  public function __construct() {
    $user_obj = new UserDB();

    // We check if the password is safe.
    if ($user_obj->check_pass_security($_POST['password']) === 'ok') {
      $user_fields = [
        user_id => $_POST['user_id1'],
        username => $_POST['username1'],
        password => $_POST['password'],
        email => $_POST['email'],
        phone => $_POST['phone'],
        first_name => $_POST['first_name'],
        last_name => $_POST['last_name'],
      ];

      $user_obj->update($user_fields);
      if ($_SESSION['secure'] !== 'yes') {
        $_SESSION['secure'] = 'yes';
        $new_loc = '/admin/';
        header("Location: $new_loc");
        exit;
      }
      else {
        header('Location: user_list.php');
        $new_loc = 'user_edit.php?user_id1=' . $_POST['user_id1'];
        header("Location: $new_loc");
        exit;
      }
    }
    else {
      $new_loc = 'user_display_edit.php?user_id1=' . $_POST['user_id1'] . '&insecure=yes';
      header("Location: $new_loc");
      exit;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Execution method, does nothing.
    return 1;
  }

}
