<?php

namespace Indybay\Pages\Admin\User;

use Indybay\DB\UserDB;
use Indybay\Page;
use Indybay\Renderer\UserRenderer;

/**
 * Class for user_display_edit page.
 */
class UserEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $user_renderer_class = new UserRenderer();
    $this->tkeys['local_form_action'] = 'user_add.php';

    if (isset($_POST['user_id'])) {
      $user_id = $_POST['user_id'] + 0;
    }
    if (isset($_GET['user_id'])) {
      $user_id = $_GET['user_id'] + 0;
    }

    $user_obj = new UserDB();
    if (isset($_POST['user_id'])) {
      $user_obj->update($user_id, $_POST);
      $this->redirect('user_list.php');
    }
    $user_detail = $user_obj->getUserInfo($user_id);
    $this->tkeys['local_username'] = $user_detail['username'];

    $this->tkeys['local_email'] = $user_detail['email'];
    $this->tkeys['local_phone'] = $user_detail['phone'];
    $this->tkeys['local_first_name'] = $user_detail['first_name'];
    $this->tkeys['local_last_name'] = $user_detail['last_name'];
    $this->tkeys['local_last_login'] = $user_detail['last_login'];
    $this->tkeys['local_form_action'] = 'user_update.php';
    $this->tkeys['local_user_id'] = $user_id;

    $this->tkeys['local_checkbox_has_login_rights'] = $user_renderer_class->makeBooleanCheckboxForm('has_login_rights', $user_detail['has_login_rights']);
    if (isset($user_detail['created'])) {
      $creation_date = $user_detail['created'];
    }
    else {
      $creation_date = '';
    }
    if (isset($user_detail['created_by_id'])) {
      $created_by_id = $user_detail['created_by_id'];
    }
    else {
      $created_by_id = 0;
    }
    $user_detail2 = '';
    if ($created_by_id != 0) {
      $user_detail2 = $user_obj->getUserInfo($created_by_id);
    }
    if (is_array($user_detail2) == 0) {
      $this->tkeys['local_creation_info'] = 'created by database migration on ' . $creation_date;
    }
    else {
      $this->tkeys['local_creation_info'] = 'created by ' . $user_detail2['username'] . ' on ' . $creation_date;
    }

    return 1;
  }

}
