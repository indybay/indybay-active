<?php

namespace Indybay\Pages\Admin\User;

use Indybay\DB\UserDB;
use Indybay\Page;

/**
 * Class for user_add page.
 */
class UserChangePassword extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $this->tkeys['local_form_action'] = 'user_add.php';

    if (isset($_POST['user_id'])) {
      $user_id = $_POST['user_id'];
    }
    if (isset($_GET['user_id'])) {
      $user_id = $_GET['user_id'];
    }

    $user_obj = new UserDB();

    if (isset($_POST['change_password'])) {
      if ($_POST['new_password1'] != $_POST['new_password2']) {
        $this->addValidationMessage('new passwords must match');
      }
      elseif (strlen(trim($_POST['new_password1'])) < 5) {
        $this->addValidationMessage('password must be at least 5 characters long');
      }
      elseif ($user_obj->validateOldPassword($user_id, $_POST['old_password']) != 1) {
        $this->addValidationMessage('old password entered was not correct');
      }
      else {
        $user_obj->changeUserPassword($user_id, trim($_POST['new_password1']));
        $this->redirect('user_list.php');
      }
    }

    $user_detail = $user_obj->getUserInfo($user_id);

    if (isset($_POST['email_password']) && strlen($_POST['email_password']) > 0) {
      $email_address = $user_detail['email'];
      if (strlen(trim($email_address)) < 5 or strpos($email_address, '@') < 1
      or strpos($email_address, ' ') > 0
      or strpos($email_address, '(') > 0
      or strrpos($email_address, '.') < 3
      ) {
        $this->addValidationMessage($email_address . ' is not a valid email address');
      }
      else {
        $subject = 'indybay admin message';

        $body = 'This email may eventually contain a temporrary password that will expire within a few hours of when its sent'
        . 'but there are enough security risks in emailing passwords I need to review the functionality with the group'
        . 'before implementing it. If you locked yourself out of the dev machine email sfbay-web and I can change the password via the DB';
        if (mail($email_address, $subject, $body)) {
          $this->addStatusMessage('A password has been emailed to ' . $email_address);
        }
        else {
          echo 'Unable to send email at this time';
        }
      }
    }

    $this->tkeys['local_username'] = $user_detail['username'];

    $this->tkeys['local_email'] = $user_detail['email'];
    $this->tkeys['local_user_id'] = $user_id;
    $this->tkeys['local_user_id'] = $user_id;
    return 1;
  }

}
