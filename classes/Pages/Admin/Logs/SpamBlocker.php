<?php

namespace Indybay\Pages\Admin\Logs;

use Indybay\Cache\LegacySpamCache;
use Indybay\Page;
use Indybay\Renderer\Renderer;

/**
 * Class for admin_index page.
 */
class SpamBlocker extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    if (isset($_POST['ip'])) {
      $ip = $_POST['ip'];
    }
    else {
      $ip = '';
    }
    if (isset($_POST['ip_part1'])) {
      $ip_part1 = $_POST['ip_part1'];
    }
    else {
      $ip_part1 = '';
    }
    if (isset($_POST['ip_part1'])) {
      $ip_part2 = $_POST['ip_part2'];
    }
    else {
      $ip_part2 = '';
    }
    if (isset($_POST['ip_part1'])) {
      $ip_part3 = $_POST['ip_part3'];
    }
    else {
      $ip_part3 = '';
    }
    if (isset($_POST['http_method'])) {
      $http_method = $_POST['http_method'];
    }
    else {
      $http_method = '';
    }
    if (isset($_POST['url'])) {
      $url = $_POST['url'];
    }
    else {
      $url = '';
    }
    if (isset($_POST['referring_url'])) {
      $referring_url = $_POST['referring_url'];
    }
    else {
      $referring_url = '';
    }
    if (isset($_POST['keyword'])) {
      $keyword = $_POST['keyword'];
    }
    else {
      $keyword = '';
    }
    if (isset($_POST['note'])) {
      $note = $_POST['note'];
    }
    else {
      $note = '';
    }
    if (isset($_POST['parent_news_item_id'])) {
      $parent_news_item_id = $_POST['parent_news_item_id'];
    }
    else {
      $parent_news_item_id = '';
    }
    if (isset($_POST['redirect_url'])) {
      $destination = $_POST['redirect_url'];
    }
    else {
      $destination = '';
    }

    $legacy_spam_cache = new LegacySpamCache();

    if (isset($_POST['add_block'])) {
      $legacy_spam_cache->addEntryToSpamCache($ip, $ip_part1, $ip_part2, $ip_part3,
               $url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note);
    }
    if (isset($_POST['remove_block_id'])) {
      $legacy_spam_cache->removeEntryFromSpamCache($_POST['remove_block_id']);
    }

    $this->tkeys['local_ip'] = $ip;
    $this->tkeys['local_ip1'] = $ip_part1;
    $this->tkeys['local_ip2'] = $ip_part2;
    $this->tkeys['local_ip3'] = $ip_part3;
    $this->tkeys['local_url'] = $url;

    $this->tkeys['local_referring_url'] = $referring_url;
    // $this->tkeys['local_news_item_id'] = $news_item_id;.
    $this->tkeys['local_parent_news_item_id'] = $parent_news_item_id;
    $this->tkeys['local_keyword'] = $keyword;
    $this->tkeys['local_redirect_url'] = $destination;
    $this->tkeys['local_note'] = $note;

    $options = [];
    $options['GET'] = 'GET';
    $options['POST'] = 'POST (nonDB save)';
    $options['DB'] = 'POST (DB save)';
    $renderer = new Renderer();
    $this->tkeys['local_method_select'] = $renderer->makeSelectForm('http_method', $options, $http_method, 'ALL');

    $block_list = $this->renderBlocks();

    $this->tkeys['local_block_list'] = $block_list;

    return 1;

  }

  /**
   * Renders blocks.
   */
  public function renderBlocks() {
    $legacy_spam_cache = new LegacySpamCache();
    $block_list = $legacy_spam_cache->listBlocks();

    $i = 0;
    $gridhtml = '';
    foreach ($block_list as $nextblockentry) {
      $i = $i + 1;
      $gridhtml .= '<tr ';
      if (!is_int($i / 2)) {
        $gridhtml .= 'class="bg-grey"';
      }
      $gridhtml .= ' ><div class="bg-grey">';
      $gridhtml .= '<form method="POST"><input type="Submit" value="remove block"><input type="hidden" name="remove_block_id" value="' . $nextblockentry['id'];

      $gridhtml .= '"></form></div><div class="bg-grey">';
      $gridhtml .= $nextblockentry['ip'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['ip_part1'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['ip_part2'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['ip_part3'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      if ($nextblockentry['http_method'] == '' || $nextblockentry['http_method'] == '0') {
        $gridhtml .= 'ALL';
      }
      else {
        $gridhtml .= $nextblockentry['http_method'];
      }
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['url'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['referring_url'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['parent_news_item_id'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['keyword'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['destination'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['note'];
      $gridhtml .= '</div>';
      $gridhtml .= '<div class="bg-grey">';
      $gridhtml .= $nextblockentry['added_info'];
      $gridhtml .= '</div>';
      $gridhtml .= '</Tr>';
    }

    return $gridhtml;
  }

}
