<?php

namespace Indybay\Pages\Admin\Logs;

use Indybay\Cache\SpamCache;
use Indybay\Page;

/**
 * Class for admin_index page.
 */
class SpamIdentification extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $spam_cache = new SpamCache();

    if (isset($_POST['save_ip_block_list'])) {
      $spam_cache->saveIpBlockFile($_POST['ip_block_list']);
    }
    $ip_block_list = '';
    $ip_block_list = $spam_cache->loadIpBlockFile();
    $this->tkeys['local_block_ips'] = $ip_block_list;

    if (isset($_POST['save_ip_spam_list'])) {
      $spam_cache->saveIpSpamFile($_POST['ip_spam_list']);
    }
    $ip_spam_list = '';
    $ip_spam_list = $spam_cache->loadIpSpamFile();
    $this->tkeys['local_spam_ips'] = $ip_spam_list;

    if (isset($_POST['save_title_spam_list'])) {
      $spam_cache->saveTitleSpamFile($_POST['title_spam_list']);
    }
    $spam_title_list = '';
    $spam_title_list = $spam_cache->loadTitleSpamFile();
    $this->tkeys['local_spam_title'] = $spam_title_list;

    if (isset($_POST['save_text_spam_list'])) {
      $spam_cache->saveTextSpamFile($_POST['text_spam_list']);
    }
    $spam_text_list = '';
    $spam_text_list = $spam_cache->loadTextSpamFile();
    $this->tkeys['local_spam_text'] = $spam_text_list;

    if (isset($_POST['save_validation_string_list'])) {
      $spam_cache->saveValidationStringFile($_POST['validation_string_list']);
    }
    $val_string_list = '';
    $val_string_list = $spam_cache->loadValidationStringFile();
    $this->tkeys['local_validation_strings'] = $val_string_list;

    return 1;

  }

}
