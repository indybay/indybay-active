<?php

namespace Indybay\Pages\Admin\FeedPull;

use Indybay\DB\CategoryDB;
use Indybay\DB\RSSPullDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;

/**
 * Class for inbound_feed_edit page.
 */
class InboundFeedEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $rss_pull_db = new RSSPullDB();
    if (isset($_POST['delete'])) {
      $feed_id = $_POST['feed_id'];
      if (count($rss_pull_db->getItemListForFeed($feed_id)) > 0) {
        echo 'This Feed Has Pulled Items, You Must Delete Them To Delete The Feed';
      }
      else {
        $rss_pull_db->deleteFeed($feed_id);
        $this->redirect('inbound_feed_list.php');
      }
    }
    if (isset($_POST['delete_items'])) {
      $feed_id = $_POST['feed_id'];
      $rss_pull_db->deleteAllItemsForFeed($feed_id);
      echo 'Items Deleted';
    }
    if (isset($_POST['update'])) {
      $feed_id = $_POST['feed_id'];
      $feed_info = $_POST;
      $feed_info['rss_feed_id'] = $feed_info['feed_id'];
      $feed_info['rss_version'] = 2;
      $rss_pull_db->updateFeed($feed_info);
      foreach ($_POST as $key => $value) {
        $feed_info[$key] = htmlentities($value);
      }
    }
    else {
      $feed_id = $_GET['feed_id'];
      $feed_info = $rss_pull_db->getFeedInfo($feed_id);
      foreach ($feed_info as $key => $value) {
        $feed_info[$key] = htmlentities($value);
      }
    }
    if (isset($_POST['clone'])) {
      $_POST['feed_id'] = '';
      $url = 'inbound_feed_add.php?cloned=1';
      foreach ($_POST as $key => $value) {
        $url .= '&' . $key . '=' . urlencode($value);
      }
      $this->redirect($url);
    }

    $category_db_class = new CategoryDB();
    $article_renderer_class = new ArticleRenderer();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_international_options, $cat_topic_options);
    $this->tkeys['cat_topic_select'] = $article_renderer_class->makeSelectForm('default_topic_id', $cat_topic_options, $feed_info['default_topic_id'], 'Select A Default');
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['cat_region_select'] = $article_renderer_class->makeSelectForm('default_region_id', $cat_region_options, $feed_info['default_region_id'], 'Select A Default');
    $status_options = [];
    $status_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlight Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlight NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Other';

    $this->tkeys['status_select'] = '<SELECT name="default_status_id">' . $article_renderer_class->createDropdown($status_options, $feed_info['default_status_id']) . '</SELECT>';

    $feed_types = [];
    $feed_types[0] = 'RSS';
    $feed_types[1] = 'HTML';
    $feed_types[2] = 'iCal';
    $this->tkeys['local_feed_type_select'] = '<SELECT name="feed_format_id">' . $article_renderer_class->createDropdown($feed_types, $feed_info['feed_format_id']) . '</SELECT>';

    $this->tkeys['local_name'] = $feed_info['name'];
    $this->tkeys['local_url'] = $feed_info['url'];
    $this->tkeys['local_author_template'] = $feed_info['author_template'];
    $this->tkeys['local_summary_template'] = $feed_info['summary_template'];
    $this->tkeys['local_text_template'] = $feed_info['text_template'];
    $this->tkeys['local_rss_feed_id'] = $feed_info['rss_feed_id'];
    $this->tkeys['local_scrape_summary_start'] = $feed_info['summary_scrape_start'];
    $this->tkeys['local_scrape_summary_end'] = $feed_info['summary_scrape_end'];
    $this->tkeys['local_scrape_text_start'] = $feed_info['text_scrape_start'];
    $this->tkeys['local_scrape_text_end'] = $feed_info['text_scrape_end'];
    $this->tkeys['local_scrape_date_start'] = $feed_info['date_scrape_start'];
    $this->tkeys['local_scrape_date_end'] = $feed_info['date_scrape_end'];
    $this->tkeys['local_scrape_title_start'] = $feed_info['title_scrape_start'];
    $this->tkeys['local_scrape_title_end'] = $feed_info['title_scrape_end'];
    $this->tkeys['local_scrape_author_start'] = $feed_info['author_scrape_start'];
    $this->tkeys['local_scrape_author_end'] = $feed_info['author_scrape_end'];
    $this->tkeys['local_replace_url_from'] = $feed_info['replace_url'];
    $this->tkeys['local_replace_url_to'] = $feed_info['replace_url_with'];
    $this->tkeys['local_restrict_urls'] = $feed_info['restrict_urls'];
  }

}
