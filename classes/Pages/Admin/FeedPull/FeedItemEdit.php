<?php

namespace Indybay\Pages\Admin\FeedPull;

use Indybay\DB\CategoryDB;
use Indybay\DB\RSSPullDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Syndication\FeedPuller;

/**
 * Class for feed_item_edit page.
 */
class FeedItemEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $rss_pull_db = new RSSPullDB();

    $rss_item_id = 0;
    if (isset($_POST['rss_item_id'])) {
      $rss_item_id = $_POST['rss_item_id'] + 0;
    }
    elseif (isset($_GET['rss_item_id'])) {
      $rss_item_id = $_GET['rss_item_id'] + 0;
    }
    if ($rss_item_id == 0) {
      echo 'you must hand in an rss item id to see this page';
      exit;
    }
    if (isset($_POST['update'])) {
      $item_info = $_POST;
      $catarray = $_POST['catartarray'];
      $categories = '';
      if (is_array($catarray)) {
        $jj = 0;
        foreach ($catarray as $catid) {
          $categories .= $catid;
          if ($jj != count($catarray) - 1) {
            $categories .= ',';
          }
          $jj = $jj + 1;
        }
        $item_info['categories'] = $categories;
      }
      $rss_pull_db->updateItem($item_info);
    }

    if (isset($_POST['publish'])) {
      $categories = '';
      $item_info = $_POST;
      $catarray = $_POST['catartarray'];
      if (is_array($catarray)) {
        $jj = 0;
        foreach ($catarray as $catid) {
          $categories .= $catid;
          if ($jj != count($catarray) - 1) {
            $categories .= ',';
          }
          $jj = $jj + 1;
        }
        $item_info['categories'] = $categories;
      }
      $rss_pull_db->updateItem($item_info);
      $this->redirect('publish_preview.php?item_id=' . $item_info['rss_item_id']);
    }

    if (isset($_GET['rescrape'])) {
      $old_item_info = $rss_pull_db->getItemInfo($rss_item_id);
      $feed_info = $rss_pull_db->getFeedInfo($old_item_info['rss_feed_id']);

      $feed_puller = new FeedPuller();
      $item_info = $old_item_info;
      $item_info['text'] = '';
      $item_info['link'] = $item_info['related_url'];
      $item_info['pubdate'] = date('Y-m-d H:i', $item_info['creation_date_timestamp']);
      $item_info = $feed_puller->convertRssItemToFeedItem($item_info, $feed_info, FALSE, 0);
      if ($item_info['title'] == '') {
        $item_info['title'] = $old_item_info['title'];
      }
      else {
        $item_info['title'] = $item_info['title'];
      }
      if ($item_info['text'] == '') {
        $item_info['text'] = $old_item_info['text'];
      }
      if ($item_info['summary'] == '') {
        $item_info['text'] = $old_item_info['summary'];
      }
      $item_info['author'] = $old_item_info['author'];
      $item_info['categories'] = $old_item_info['categories'];
      $item_info['news_item_status_id'] = $old_item_info['news_item_status_id'];
      $item_info['rss_item_id'] = $rss_item_id;
      $rss_pull_db->updateItem($item_info, TRUE);
    }

    $item_info = $rss_pull_db->getItemInfo($rss_item_id);

    if (isset($_POST['delete'])) {
      $rss_pull_db->deleteItem($rss_item_id);
      $this->redirect('feed_item_list.php?feed_id=' . $item_info['rss_feed_id']);
    }
    $feed_id = $item_info['rss_feed_id'];
    $rss_pull_db = new RSSPullDB();
    $feed_info = $rss_pull_db->getFeedInfo($feed_id);
    $this->tkeys['local_feed_name'] = $feed_info['name'];
    $this->tkeys['local_feed_id'] = $feed_id;
    $this->tkeys['local_rss_item_id'] = $rss_item_id;
    $this->tkeys['local_title'] = $this->mbHtmlentities($item_info['title']);
    $this->tkeys['local_author'] = $this->mbHtmlentities($item_info['author']);
    $this->tkeys['local_related_url'] = $this->mbHtmlentities($item_info['related_url']);
    $this->tkeys['local_summary'] = $item_info['summary'];
    $this->tkeys['local_text'] = $item_info['text'];
    $this->tkeys['local_publish_info'] = '<br>(Not Yet Published)';
    if ($item_info['is_published'] == '1') {
      $this->tkeys['local_publish_info'] = '<br><h3><a href="/newsitems/' . $item_info['published_news_item_id'] . '.php">Already Published</a></h3>';
    }
    if ($item_info['creation_date_timestamp'] > strtotime('1/1/2000')) {
      $this->tkeys['local_creation_date'] = date('h:ia l, F j, Y ', $item_info['creation_date_timestamp']);
    }
    else {
      $this->tkeys['local_creation_date'] = 'No Date Found';
    }

    $this->tkeys['local_pull_date'] = $item_info['pull_date'];

    $category_db_class = new CategoryDB();
    $article_renderer_class = new ArticleRenderer();
    $saved_categories_for_article = explode(',', $item_info['categories']);
    $available_categories = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['local_checkbox_region'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);
    $available_categories = $category_db_class->getCategoryInfoListByType(2, 0);
    $this->tkeys['local_checkbox_topic'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);
    $available_categories = $category_db_class->getCategoryInfoListByType(2, 44);
    $this->tkeys['local_checkbox_int'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);

    $status_options = [];
    $status_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlight Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlight NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Other';
    $status_options[NEWS_ITEM_STATUS_ID_HIDDEN] = 'Exclude From Posting';

    $this->tkeys['status_select'] = '<SELECT name="news_item_status_id">' . $article_renderer_class->createDropdown($status_options, $item_info['news_item_status_id']) . '</SELECT>';

  }

  /**
   * Should not be needed.
   */
  public function mbHtmlentities($str, $encoding = 'utf-8') {
    mb_regex_encoding($encoding);
    $pattern = ['<', '>', '"', '\''];
    $replacement = ['&lt;', '&gt;', '&quot;', '&#39;'];
    for ($i = 0; $i < count($pattern); $i++) {
      $str = mb_ereg_replace($pattern[$i], $replacement[$i], $str);
    }
    return $str;
  }

}
