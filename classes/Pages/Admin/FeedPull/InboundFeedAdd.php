<?php

namespace Indybay\Pages\Admin\FeedPull;

use Indybay\DB\CategoryDB;
use Indybay\DB\RSSPullDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;

/**
 * Class for inbound_feed_add page.
 */
class InboundFeedAdd extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    if (isset($_POST['name']) && isset($_POST['url'])) {
      $rss_pull_db = new RSSPullDB();
      $feed_info = $_POST;

      $feed_id = $rss_pull_db->addFeed($feed_info);
      if ($feed_id > 0) {
        $this->redirect('inbound_feed_list.php');
      }
      else {
        echo 'AN ERROR OCCURED; Couldnt Add Feed To DB!';
      }
      $feed_info = $_POST;
    }
    elseif (isset($_GET['cloned'])) {
      $feed_info = $_GET;
      $feed_info['name'] = 'Clone of ' . $feed_info['name'];
    }
    else {
      $feed_info = [];
      $feed_info['author_template'] = 'AUTHOR, FEED_NAME (reposted)';
      $feed_info['summary_template'] = '<strong>CREATION_DATE:</strong> SHORTENED_SUMMARY';
      $feed_info['text_template'] = 'SHORTENED_TEXT<p><strong>Read More</strong>';
    }

    if (!isset($feed_info['url'])) {
      $feed_info['url'] = '';
    }
    if (!isset($feed_info['scrape_url'])) {
      $feed_info['scrape_url'] = '';
    }
    if (!isset($feed_info['default_topic_id'])) {
      $feed_info['default_topic_id'] = '';
    }
    if (!isset($feed_info['default_region_id'])) {
      $feed_info['default_region_id'] = '';
    }
    if (!isset($feed_info['feed_format_id'])) {
      $feed_info['feed_format_id'] = '';
    }
    if (!isset($feed_info['name'])) {
      $feed_info['name'] = '';
    }
    if (!isset($feed_info['author_template'])) {
      $feed_info['author_template'] = '';
    }
    if (!isset($feed_info['summary_scrape_start'])) {
      $feed_info['summary_scrape_start'] = '';
    }
    if (!isset($feed_info['summary_scrape_end'])) {
      $feed_info['summary_scrape_end'] = '';
    }
    if (!isset($feed_info['text_scrape_start'])) {
      $feed_info['text_scrape_start'] = '';
    }
    if (!isset($feed_info['text_scrape_end'])) {
      $feed_info['text_scrape_end'] = '';
    }

    if (!isset($feed_info['author_scrape_start'])) {
      $feed_info['author_scrape_start'] = '';
    }
    if (!isset($feed_info['author_scrape_end'])) {
      $feed_info['author_scrape_end'] = '';
    }
    if (!isset($feed_info['date_scrape_start'])) {
      $feed_info['date_scrape_start'] = '';
    }
    if (!isset($feed_info['date_scrape_end'])) {
      $feed_info['date_scrape_end'] = '';
    }
    if (!isset($feed_info['title_scrape_start'])) {
      $feed_info['title_scrape_start'] = '';
    }
    if (!isset($feed_info['title_scrape_end'])) {
      $feed_info['title_scrape_end'] = '';
    }
    if (!isset($feed_info['replace_url'])) {
      $feed_info['replace_url'] = '';
    }
    if (!isset($feed_info['replace_url_with'])) {
      $feed_info['replace_url_with'] = '';
    }
    if (!isset($feed_info['restrict_urls'])) {
      $feed_info['restrict_urls'] = '';
    }

    $category_db_class = new CategoryDB();
    $article_renderer_class = new ArticleRenderer();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_international_options, $cat_topic_options);
    $this->tkeys['cat_topic_select'] = $article_renderer_class->makeSelectForm('default_topic_id', $cat_topic_options, $feed_info['default_topic_id'], 'Please Select');
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['cat_region_select'] = $article_renderer_class->makeSelectForm('default_region_id', $cat_region_options, $feed_info['default_region_id'], 'Please Select');
    $status_options = [];
    $status_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlight Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlight NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost Local';
    $status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED] = 'Corporate Repost NonLocal';
    $status_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Other';

    $feed_types = [];
    $feed_types[0] = 'RSS';
    $feed_types[1] = 'HTML';
    $feed_types[2] = 'iCal';
    $this->tkeys['local_feed_type_select'] = '<SELECT name="feed_format_id">' . $article_renderer_class->createDropdown($feed_types, $feed_info['feed_format_id']) . '</SELECT>';

    $this->tkeys['status_select'] = '<SELECT name="default_status_id">' . $article_renderer_class->createDropdown($status_options, NEWS_ITEM_STATUS_ID_NEW) . '</SELECT>';

    $this->tkeys['local_name'] = $feed_info['name'];
    $this->tkeys['local_url'] = $feed_info['url'];
    $this->tkeys['local_author_template'] = $feed_info['author_template'];
    $this->tkeys['local_summary_template'] = $feed_info['summary_template'];
    $this->tkeys['local_text_template'] = $feed_info['text_template'];
    $this->tkeys['local_scrape_url'] = $feed_info['scrape_url'];
    $this->tkeys['local_scrape_summary_start'] = $feed_info['summary_scrape_start'];
    $this->tkeys['local_scrape_summary_end'] = $feed_info['summary_scrape_end'];
    $this->tkeys['local_scrape_text_start'] = $feed_info['text_scrape_start'];
    $this->tkeys['local_scrape_text_end'] = $feed_info['text_scrape_end'];
    $this->tkeys['local_scrape_author_start'] = $feed_info['author_scrape_start'];
    $this->tkeys['local_scrape_author_end'] = $feed_info['author_scrape_end'];
    $this->tkeys['local_scrape_date_start'] = $feed_info['date_scrape_start'];
    $this->tkeys['local_scrape_date_end'] = $feed_info['date_scrape_end'];
    $this->tkeys['local_scrape_title_start'] = $feed_info['title_scrape_start'];
    $this->tkeys['local_scrape_title_end'] = $feed_info['title_scrape_end'];
    $this->tkeys['local_replace_url_from'] = $feed_info['replace_url'];
    $this->tkeys['local_replace_url_to'] = $feed_info['replace_url_with'];
    $this->tkeys['local_restrict_urls'] = $feed_info['restrict_urls'];

  }

}
