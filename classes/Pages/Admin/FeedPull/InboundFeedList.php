<?php

namespace Indybay\Pages\Admin\FeedPull;

use Indybay\DB\RSSPullDB;
use Indybay\Page;
use Indybay\Syndication\FeedPuller;

/**
 * Class for inbound_feed_list page.
 */
class InboundFeedList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (isset($_POST['delete_all'])) {
      $this->deleteAll();
    }
    if (isset($_POST['pull_all']) || isset($_POST['pull_local']) || isset($_POST['pull_nonlocal'])
    || isset($_POST['pull_corplocal']) || isset($_POST['pull_corpnonlocal'])
     || isset($_POST['pull_other'])
    ) {
      $this->pullFeeds();
    }
    if (isset($_GET['feeds_to_pull'])) {
      $feeds_to_pull = $_GET['feeds_to_pull'];
    }
    else {
      $feeds_to_pull = '';
    }
    if ($feeds_to_pull != '') {
      $rss_pull_db = new RSSPullDB();
      $feed_pull_array = explode(',', $feeds_to_pull);
      if (isset($_GET['initial_pull_size'])) {
        $initial_pull_size = $_GET['initial_pull_size'] + 0;
      }
      else {
        $initial_pull_size = count($feed_pull_array);
      }
      $next_feed_id = array_pop($feed_pull_array);
      if ($next_feed_id + 0 != 0) {
        $feed_puller = new FeedPuller();
        $feed_puller->pullFeedIntoStagingTable($next_feed_id);
        $feed_info = $rss_pull_db->getFeedInfo($next_feed_id + 0);
        echo '<h3>Pulled Feed "' . $feed_info['name'] . '" In ' . ($feed_info['last_pull_duration_usecs'] / 1000000.00) . ' Seconds</h3>';
        if (count($feed_pull_array) > 0) {
          $feed_info = $rss_pull_db->getFeedInfo($feed_pull_array[count($feed_pull_array) - 1] + 0);
          echo '<h3>Pulling Feed "' . $feed_info['name'] . '"</h3>';
          echo '<h4>' . ($initial_pull_size - count($feed_pull_array)) . ' Feeds Pulled, ' . count($feed_pull_array) . ' Left To Pull</h4>';
          $new_feed_url = '/admin/feed_pull/inbound_feed_list.php?initial_pull_size=' . $initial_pull_size . '&feeds_to_pull=' . implode(',', $feed_pull_array);
        }
        else {
          if ($_GET['return_to_item_list'] == '') {
            $new_feed_url = '/admin/feed_pull/inbound_feed_list.php';
          }
          else {
            if ($_GET['return_to_item_list'] == 0) {
              $new_feed_url = '/admin/feed_pull/feed_item_list.php?feed_id=0';
            }
            else {
              $new_feed_url = '/admin/feed_pull/feed_item_list.php?feed_id=0&status_id=' . $_GET['return_to_item_list'];
            }
          }
        }
        if ($_GET['return_to_item_list'] != '') {
          $new_feed_url .= '&return_to_item_list=' . $_GET['return_to_item_list'];
        }
        $this->redirect($new_feed_url, 3);
      }
    }
    $tblhtml = $this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
    $this->tkeys['local_highlightedlocal_feed_list'] = $tblhtml;
    $tblhtml = $this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED);
    $this->tkeys['local_highlightednonlocal_feed_list'] = $tblhtml;
    $tblhtml = $this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED);
    $this->tkeys['local_corplocal_feed_list'] = $tblhtml;
    $tblhtml = $this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED);
    $this->tkeys['local_corpnonlocal_feed_list'] = $tblhtml;
    $tblhtml = $this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NEW);
    $this->tkeys['local_other_feed_list'] = $tblhtml;

  }

  /**
   * Deletes all.
   */
  public function deleteAll() {
    $rss_pull_db = new RSSPullDB();
    $rss_feed_list = $rss_pull_db->getFeedList();
    foreach ($rss_feed_list as $rss_feed) {
      $rss_pull_db->deleteAllItemsForFeed($rss_feed['rss_feed_id']);
    }
  }

  /**
   * Pulls feeds.
   */
  public function pullFeeds() {
    $rss_pull_db = new RSSPullDB();
    $status_id = -1;

    if ($_POST['pull_local']) {
      $status_id = NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED;
    }
    if ($_POST['pull_nonlocal']) {
      $status_id = NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
    }
    if ($_POST['pull_corplocal']) {
      $status_id = NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED;
    }
    if ($_POST['pull_corpnonlocal']) {
      $status_id = NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED;
    }
    if ($_POST['pull_other']) {
      $status_id = 0;
    }

    if ($status_id == -1) {
      $rss_feed_list = $rss_pull_db->getFeedList();
    }
    else {
      $rss_feed_list = $rss_pull_db->getFeedListForDefaultStatus($status_id);
    }
    $new_feed_url = '/admin/feed_pull/inbound_feed_list.php?feeds_to_pull=';
    $feed_ids = [];
    if (is_array($rss_feed_list)) {
      foreach ($rss_feed_list as $rss_feed) {
        array_push($feed_ids, $rss_feed['rss_feed_id']);
      }
      $new_feed_url = $new_feed_url . implode(',', $feed_ids);
      $this->redirect($new_feed_url);
    }
  }

  /**
   * Renders feed list for status.
   */
  public function renderFeedListForStatus($status_id) {
    $rss_pull_db = new RSSPullDB();
    $feed_puller = new FeedPuller();
    $rss_feed_list = $rss_pull_db->getFeedListForDefaultStatus($status_id);

    $i = 0;
    $latestpulldate = 0;

    if (!is_array($rss_feed_list) || count($rss_feed_list) == 0) {
      return '<tr class="bg-grey" ><td colspan="5">No Feeds Yet Exist Of This Type</td></tr>';
    }
    $tblhtml = '';
    foreach ($rss_feed_list as $rss_feed) {
      $i = $i + 1;
      $itemcount = 0;
      $problemcount = 0;
      $rss_item_list = $rss_pull_db->getItemListForFeed($rss_feed['rss_feed_id']);
      if (is_array($rss_item_list)) {
        foreach ($rss_item_list as $rss_item) {
          $itemcount = $itemcount + 1;
          if ($feed_puller->itemHasProblems($rss_item)) {
            $problemcount = $problemcount + 1;
          }
        }
      }
      if ($rss_feed['last_pull_date'] + 0 != 0) {
        if ($rss_feed['last_pull_date'] > $latestpulldate) {
          $latestpulldate = $rss_feed['last_pull_date'];
        }
      }
      $tblhtml .= '<tr ';
      if (!is_int(($i) / 2)) {
        $tblhtml .= 'class="bg-grey"';
      }
      $tblhtml .= ' ><td>';

      $tblhtml .= $rss_feed['rss_feed_id'];
      $tblhtml .= '</td><td>';
      $tblhtml .= $rss_feed['name'];
      $tblhtml .= '</td><td>';

      $tblhtml .= '<a href="/admin/feed_pull/feed_item_list.php?feed_id=';
      $tblhtml .= $rss_feed['rss_feed_id'] . '">View Pulled Items</a>';
      $tblhtml .= '</td><td>';
      $tblhtml .= '<a ';
      $tblhtml .= ' href="/admin/feed_pull/inbound_feed_edit.php?feed_id=';
      $tblhtml .= $rss_feed['rss_feed_id'] . '">Edit/Delete Feed</a>';
      $tblhtml .= '</td><td>';
      $tblhtml .= $itemcount;
      $tblhtml .= '</td><td>';
      if ($problemcount == $itemcount && $itemcount != 0) {
        $tblhtml .= '<span class="error">';
      }
      elseif ($problemcount != 0) {
        $tblhtml .= '<span class="orange">';
      }
      $tblhtml .= $problemcount;
      if ($problemcount != 0) {
        $tblhtml .= '</span>';
      }
      $tblhtml .= '</td><td>url: <a href="';
      $tblhtml .= $rss_feed['url'];
      $urlshortened = $rss_feed['url'];
      if (strlen($urlshortened) > 40) {
        $urlshortened = substr($urlshortened, 0, 40) . '...';
      }
      $tblhtml .= '">' . $urlshortened . '</a><br>Last Pull Date:';
      $tblhtml .= $rss_feed['last_pull_date'];
      $tblhtml .= '<br>Last Pull Duration:';
      $tblhtml .= (($rss_feed['last_pull_duration_usecs'] + 0) / 1000000.0000);
      $tblhtml .= ' seconds</td>';
      $tblhtml .= '</Tr>';
    }

    return $tblhtml;
  }

}
