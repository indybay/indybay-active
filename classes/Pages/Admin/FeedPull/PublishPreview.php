<?php

namespace Indybay\Pages\Admin\FeedPull;

use Indybay\DB\ArticleDB;
use Indybay\DB\CategoryDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\RSSPullDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;

/**
 * Class for feed_item_edit page.
 */
class PublishPreview extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (isset($_POST['publish'])) {
      $item_ids = $this->getIdsFromPost($_POST);
      if (is_array($item_ids)) {
        $ret = $this->publishItems($item_ids);
        if ($ret) {
          $this->redirect('/admin/article/?include_posts=1&include_events=1&news_item_status_restriction=0');
        }
        else {
          echo 'error publishing<br>';
        }
      }
      else {
        echo 'error getting item ids to publish<br>';
      }
    }

    if ($_GET['item_id'] != '') {
      $item_ids = [];
      array_push($item_ids, $_GET['item_id']);
    }
    else {
      $item_ids = $this->getIdsFromHttpRequest();
    }

    $this->tkeys['local_previews'] = $this->renderPreviews($item_ids);

  }

  /**
   * Gets IDs from post.
   */
  public function getIdsFromPost($post_fields) {
    $item_ids = [];
    $i = 1;
    while ($post_fields['id_' . $i] != '') {
      array_push($item_ids, $post_fields['id_' . $i]);
      $i = $i + 1;
    }
    return $item_ids;

  }

  /**
   * Gets IDs from HTTP request.
   */
  public function getIdsFromHttpRequest() {
    $item_ids = [];
    if ($_GET['feed_ids'] != '') {
      $ids = explode(',', $_GET['feed_ids']);
      foreach ($ids as $id) {
        array_push($item_ids, $id);
      }
    }
    return $item_ids;

  }

  /**
   * Publishes items.
   */
  public function publishItems($item_ids) {
    $article_db = new ArticleDB();
    $rss_pull_db = new RSSPullDB();
    $rss_item_list = $this->getItemListFromIdList($item_ids);
    $j = 0;
    $all_category_list = [];
    foreach ($rss_item_list as $rss_item) {
      $article_info = [];
      $article_info['title1'] = $rss_item['title'];
      $article_info['title2'] = $rss_item['title'];
      $article_info['summary'] = $rss_item['summary'];
      $article_info['text'] = $rss_item['text'];
      $article_info['related_url'] = $rss_item['related_url'];
      $article_info['displayed_author_name'] = $rss_item['author'];
      $article_info['is_text_html'] = '1';
      $article_info['is_summary_html'] = '1';
      $article_info['news_item_status_id'] = $rss_item['news_item_status_id'];
      $next_id = $article_db->createNewArticle(NEWS_ITEM_TYPE_ID_POST, $article_info);
      if ($rss_item['categories'] != '' && $next_id + 0 != 0) {
        $category_array = explode(',', $rss_item['categories']);
        $all_category_list = array_merge($category_array, $all_category_list);
        $this->processCategories($next_id, $category_array);
      }
      $article_db->updateNewsItemStatusAndType($next_id, $rss_item['news_item_status_id'], NEWS_ITEM_TYPE_ID_POST);
      $rss_pull_db->setItemToPublished($rss_item['rss_item_id'], $next_id);
      if ($next_id + 0 == 0) {
        echo 'Unable to add item with rss_item_id ' . $rss_item['rss_item_id'] . '<br>';
        return FALSE;
      }
      else {
        $j = $j + 1;
      }
    }

    $page_id_array = [];
    $page_db_class = new FeaturePageDB();
    if (count($all_category_list) > 0) {
      $page_id_array = $page_db_class->getAssociatedPageIds($all_category_list);
    }
    $more_pages = $page_db_class->getPagesWithAllCategoryNewswires();
    $merged_pages = array_merge($more_pages, $page_id_array);

    $newswire_cache_class = new NewswireCache();
    foreach ($merged_pages as $page_id) {
      $newswire_cache_class->regenerateNewswireForPageHelper($page_id, 0, 0);
    }

    if ($j + 0 > 0) {
      return TRUE;
    }
    else {
      echo 'No items found to publish<br>';
      return FALSE;
    }
  }

  /**
   * Gets item list from ID list.
   */
  public function getItemListFromIdList($item_ids) {
    $rss_item_list = [];
    if (is_array($item_ids)) {
      $rss_pull_db = new RSSPullDB();
      foreach ($item_ids as $item_id) {
        $next_item_info = $rss_pull_db->getItemInfo($item_id);
        array_push($rss_item_list, $next_item_info);
      }
    }
    return $rss_item_list;
  }

  /**
   * Renders previews.
   */
  public function renderPreviews($item_ids) {

    $rss_item_list = $this->getItemListFromIdList($item_ids);
    $tblhtml = '';
    $i = 0;
    if (is_array($rss_item_list)) {

      $i = 1;
      foreach ($rss_item_list as $rss_item) {
        $tblhtml .= '<tr class="bg-header" ';
        $cdate = date('m/d/Y h:ia', $rss_item['creation_date_timestamp']);
        $pdate = isset($rss_item['pull_date_timestamp']) ? date('m/d/Y h:ia', $rss_item['pull_date_timestamp']) : '';
        $tblhtml .= ' ><td valign="top"><strong>ID ' . $rss_item['rss_item_id'] . '</strong></td><td valign="top">creation date: ' . $cdate;
        $tblhtml .= '<br>pull date:' . $pdate . '<input type="hidden" name="id_' . $i . '" value="' . $rss_item['rss_item_id'] . '">';
        $tblhtml .= '</td></Tr><tr><td colspan="2">' . $this->previewItem($rss_item) . '</td></tr>';
        $i = $i + 1;
      }
    }
    return $tblhtml;
  }

  /**
   * Previews item.
   */
  public function previewItem($item_info) {
    $art_rend = new ArticleRenderer();
    $post_main_template_file = INDYBAY_BASE_PATH . '/templates/article/post_main_section.tpl';
    $template = file_get_contents($post_main_template_file);
    $template = str_replace('TPL_LOCAL_TITLE1', $item_info['title'], $template);
    $template = str_replace('TPL_LOCAL_DISPLAYED_AUTHOR_NAME', $item_info['author'], $template);
    $template = str_replace('TPL_LOCAL_EMAIL', '', $template);
    $template = str_replace('TPL_LOCAL_CREATED', date('l, F j, Y '), $template);
    $template = str_replace('TPL_LOCAL_SUMMARY', $item_info['summary'], $template);
    $template = str_replace('TPL_LOCAL_NOMEDIA1', '<!--', $template);
    $template = str_replace('TPL_LOCAL_NOMEDIA2', '-->', $template);
    $template = str_replace('TPL_LOCAL_TEXT', $item_info['text'], $template);
    $template = str_replace('TPL_LOCAL_RELATED_URL', $item_info['related_url'], $template);
    $template = str_replace('TPL_LOCAL_SHORTENED_RELATED_LINK', $art_rend->shortenLinkForDisplay($item_info['related_url']), $template);

    $category_names = '';
    if ($item_info['categories'] != '') {
      $category_ids = explode(',', $item_info['categories']);
      if (is_array($category_ids)) {
        $cat_db = new CategoryDB();
        $i = 0;
        foreach ($category_ids as $catid) {
          $name = $cat_db->getCategoryNameFromId($catid);
          if ($i > 0) {
            $category_names .= ', ';
          }
          $category_names .= $name;
          $i = $i + 1;
        }
      }
      if ($category_names != '') {
        $category_names = '<strong>Categories: ' . $category_names . '</strong><br>';
      }
    }
    $statusName = 'ERROR';
    switch ($item_info['news_item_status_id']) {
      case NEWS_ITEM_STATUS_ID_NEW:
        $statusName = 'New';
        break;

      case NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED:
        $statusName = 'HIghlighted Local';
        break;

      case NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED:
        $statusName = 'Highlighted Non Local';
        break;

      case NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED:
        $statusName = 'Corporate Report (Local)';
        break;

      case NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED:
        $statusName = 'Corporate Report (NonLocal)';
        break;

      case NEWS_ITEM_STATUS_ID_OTHER:
        $statusName = 'Other';
    }

    $statusstr = 'This item will be posted as <strong>' . $statusName . '</strong><br>';

    return $category_names . $statusstr . '<strong>Preview</strong>:<br>' . $template . '<p>&#160;<p>';
  }

  /**
   * Processes categories.
   */
  public function processCategories($news_item_id, $category_array) {
    $news_item_db_class = new NewsItemDB();
    foreach ($category_array as $next_cat_id) {
      $news_item_db_class->addNewsItemCategory($news_item_id, $next_cat_id);
    }

  }

}
