<?php

namespace Indybay\Pages\Admin\Article;

use Indybay\DB\ArticleDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;

/**
 * Class for article_edit page.
 */
class NewsitemHistory extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $article_db_class = new ArticleDB();
    $article_renderer_class = new ArticleRenderer();

    $this->tkeys['publish_result'] = '';

    if ($_GET['version_id'] > 0) {
      $news_item_version_id = $_GET['version_id'];
      $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
      $news_item_id = $version_info[news_item_id];
    }
    if ($_GET['id'] > 0) {
      $news_item_id = $_GET['id'];
      $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
      $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);

    }

    $this->tkeys['local_title1'] = htmlspecialchars($version_info['title1']);
    $this->tkeys['local_title2'] = htmlspecialchars($version_info['title2']);
    $this->tkeys['local_news_item_id'] = $news_item_id;

    $this->tkeys['local_previous_version_list'] =
            $article_renderer_class->renderVersionList(
      $article_db_class->getVersionListInfo($news_item_id, 100), '/admin/article/article_edit.php');

  }

}
