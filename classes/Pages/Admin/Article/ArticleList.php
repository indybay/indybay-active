<?php

namespace Indybay\Pages\Admin\Article;

use Indybay\Cache\ArticleCache;
use Indybay\Cache\EventListCache;
use Indybay\Cache\NewswireCache;
use Indybay\DB\ArticleDB;
use Indybay\DB\CategoryDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\LogDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemDB;
use Indybay\DB\SearchDB;
use Indybay\Date;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Translate;

/**
 * Article list base class.
 *
 * Admin article list class is used for both the admin list page as well as
 * being the parent for all nonadmin lists. This means you must be careful when
 * making changes that you check that all dependent pages work and you are not
 * opening up security holes by letting variables through that allow
 * classifying.
 */
class ArticleList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $logdb = new LogDB();
    $logdb->addLogEntryBeforeSearch();

    if (isset($_POST['classify'])) {
      if ($_SESSION['session_is_editor'] == TRUE) {
        $this->bulkClassify($_POST);
      }
    }

    $search_fields = $this->setupFields();

    $search = $search_fields['search'];

    $page_number = $search_fields['page_number'];

    if ($page_size = $search_fields['page_size'] + 0) {
      $page_size = $search_fields['page_size'] + 0;
    }

    $tr = new Translate();
    $tr->createTranslateTable('article');
    $article_renderer = new ArticleRenderer();
    $article_db_class = new ArticleDB();
    $article_cache_class = new ArticleCache();
    $divhtml = '<!-- articlelist -->';

    $search_db_class = new SearchDB();
    $media_type_grouping_id = $search_fields['media_type_grouping_id'] + 0;

    // Setup local vars (already cleaned and set to defaults if needed via
    // setupFields).
    $include_attachments = $search_fields['include_attachments'];
    $include_posts = $search_fields['include_posts'];
    $include_events = $search_fields['include_events'];
    $include_blurbs = $search_fields['include_blurbs'];
    $include_comments = $search_fields['include_comments'];
    $topic_id = $search_fields['topic_id'];
    $region_id = $search_fields['region_id'];
    $news_item_status_restriction = $search_fields['news_item_status_restriction'];
    $search_date_type = $search_fields['search_date_type'];
    $date_range_start_day = $search_fields['date_range_start_day'];
    $date_range_start_month = $search_fields['date_range_start_month'];
    $date_range_start_year = $search_fields['date_range_start_year'];
    $date_range_start = $search_fields['date_range_start'];
    $date_range_end_day = $search_fields['date_range_end_day'];
    $date_range_end_month = $search_fields['date_range_end_month'];
    $date_range_end_year = $search_fields['date_range_end_year'];
    $date_range_end = $search_fields['date_range_end'];

    $parent_item_id = $search_fields['parent_item_id'];
    $display_options = $article_db_class->getNewsItemStatusSelectList();

    $search_date_type = $search_fields['search_date_type'];
    $date_range_start_day = $search_fields['date_range_start_day'];

    $total_rows_returned = $search_db_class->standardSearch($page_size, $page_number, $search, $news_item_status_restriction,
     $include_posts, $include_comments, $include_attachments, $include_events, $include_blurbs,
     $media_type_grouping_id, $topic_id, $region_id, $parent_item_id, $search_date_type, $date_range_start, $date_range_end, 1, 0, 0);

    if (isset($search_fields['last_page']) || isset($search_fields['last_page.x'])) {
      $page_number = ceil(($total_rows_returned + 0) / $page_size) - 1;
    }
    if (isset($search_fields['first_page']) || isset($search_fields['first_page.x'])) {
      $page_number = 0;
    }
    $search_result = $search_db_class->standardSearch($page_size, $page_number, $search, $news_item_status_restriction,
       $include_posts, $include_comments, $include_attachments, $include_events, $include_blurbs,
      $media_type_grouping_id, $topic_id, $region_id, $parent_item_id, $search_date_type, $date_range_start, $date_range_end, 0);

    // Sanitize search for output!
    $search = htmlspecialchars($search);

    // Some template variables are in the dictionary.
    $this->tkeys = $GLOBALS['dict'];

    $this->setupSearchForm($page_size, $page_number, $search, $news_item_status_restriction,
       $include_posts, $include_comments, $include_attachments, $include_events,
       $include_blurbs, $media_type_grouping_id, $topic_id, $region_id, $parent_item_id, $search_date_type,
       $date_range_start_day, $date_range_start_month, $date_range_start_year,
       $date_range_end_day, $date_range_end_month, $date_range_end_year);

    $this->tkeys['total_rows_returned'] = $total_rows_returned + 0;

    $this->tkeys['last_page'] = ceil($this->tkeys['total_rows_returned'] / $page_size);
    $numrows = 0;
    if (isset($search_result)) {
      $numrows = !empty($page_size);
    }

    if ($numrows == 0) {
      $divhtml .= '<h2>';
      $divhtml .= '</h2>';
    }
    $min_row_in_results = 0;
    $max_row_in_results = 0;
    if (is_array($search_result)) {
      if ($numrows > 0) {
        $cell_counter = 0;

        if (($page_size == 32) || ($page_size == 64)) {
          $divhtml .= '<div class="grid grid--4-cols photo-gallery col-wrapper">';
        }
        elseif (($page_size == 48) || ($page_size == 96)) {
          $divhtml .= '<div class="grid grid--6-cols photo-gallery col-wrapper">';
        }
        else {
          $divhtml .= '<div class="col-wrapper">';
        }

        foreach ($search_result as $row) {
          $cell_counter++;
          $parent_row = '';
          if ($min_row_in_results == 0) {
            $min_row_in_results = $row['news_item_id'];
          }

          // Sql returns an extra line so we can do paging correctly.
          if (!($cell_counter > $page_size)) {
            $max_row_in_results = $row['news_item_id'];
            if ($row['parent_item_id'] != $row['news_item_id'] && $row['parent_item_id'] != 0) {
              $parent_row = $article_db_class->getArticleInfoFromNewsItemId($row['parent_item_id']);
            }
            $divhtml .= $this->renderRow($cell_counter, $row, $parent_row, $tr, $article_renderer, $article_cache_class, $display_options, $article_db_class);
          }
        }
        $divhtml .= '</div>';
      }
    }

    $this->tkeys['table_middle'] = $divhtml;

    $this->formatPaging($page_number, $search_fields, $numrows, $min_row_in_results, $max_row_in_results);

    reset($GLOBALS['dict']);
    if (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1') {
      $this->addStatusMessage('The database running this site is busy due to a large number of people looking at the site. Try searching again in a few minutes');
      $this->addStatusMessage("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
    }

    $logdb->updateLogIdAfterSearch();

    return 1;

  }

  /**
   * Setsup search form.
   */
  public function setupSearchForm(
    $page_size,
    $page_number,
    $search,
    $news_item_status_restriction,
    $include_posts,
    $include_comments,
    $include_attachments,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $parent_item_id,
    $search_date_type,
    $date_range_start_day,
    $date_range_start_month,
    $date_range_start_year,
    $date_range_end_day,
    $date_range_end_month,
    $date_range_end_year,
  ) {

    $tr = new Translate();
    $tr->createTranslateTable('article');

    $media_attachment_db_class = new MediaAttachmentDB();
    $article_renderer = new ArticleRenderer();

    $category_db_class = new CategoryDB();
    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_international_options, $cat_topic_options);
    $this->tkeys['CAT_TOPIC_SELECT'] = $article_renderer->makeSelectForm('topic_id', $cat_topic_options, $topic_id, 'All Topics');
    $this->tkeys['LOCAL_TOPIC_ID'] = $topic_id;
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['CAT_REGION_SELECT'] = $article_renderer->makeSelectForm('region_id', $cat_region_options, $region_id, 'All Regions');
    $this->tkeys['LOCAL_REGION_ID'] = $region_id;

    $display_options = $this->getNewsItemStatusSelectList();
    $medium_options = $media_attachment_db_class->getMediumOptions();
    $this->tkeys['local_news_item_status_restriction'] = $news_item_status_restriction;
    $this->tkeys['SEARCH_MEDIUM'] = $article_renderer->makeSelectForm('media_type_grouping_id', $medium_options, $media_type_grouping_id, 'All Media');
    $this->tkeys['LOCAL_MEDIA_TYPE_GROUPING_ID'] = $media_type_grouping_id;
    $this->tkeys['LOCAL_CHECKBOX_COMMENTS'] = $article_renderer->makeBooleanCheckboxForm('include_comments', $include_comments);
    $this->tkeys['local_include_comments'] = $include_comments;
    $this->tkeys['LOCAL_CHECKBOX_ATTACHMENTS'] = $article_renderer->makeBooleanCheckboxForm('include_attachments', $include_attachments);
    $this->tkeys['local_include_attachments'] = $include_attachments;
    $this->tkeys['LOCAL_CHECKBOX_EVENTS'] = $article_renderer->makeBooleanCheckboxForm('include_events', $include_events);
    $this->tkeys['local_include_events'] = $include_events;

    $this->tkeys['LOCAL_CHECKBOX_POSTS'] = $article_renderer->makeBooleanCheckboxForm('include_posts', $include_posts);
    $this->tkeys['local_include_posts'] = $include_posts;
    $this->tkeys['LOCAL_CHECKBOX_BLURBS'] = $article_renderer->makeBooleanCheckboxForm('include_blurbs', $include_blurbs);
    $this->tkeys['local_include_blurbs'] = $include_blurbs;

    $this->tkeys['SEARCH_DISPLAY'] = $article_renderer->makeSelectForm('news_item_status_restriction', $display_options, $news_item_status_restriction);
    $this->tkeys['LOCAL_SEARCH'] = $search;

    $this->tkeys['LOCAL_DATE_TYPE'] = $search_date_type;

    $this->tkeys['LOCAL_DATE_RANGE_START_DAY'] = $date_range_start_day;
    $this->tkeys['LOCAL_DATE_RANGE_START_MONTH'] = $date_range_start_month;
    $this->tkeys['LOCAL_DATE_RANGE_START_YEAR'] = $date_range_start_year;

    $this->tkeys['LOCAL_DATE_RANGE_END_DAY'] = $date_range_end_day;
    $this->tkeys['LOCAL_DATE_RANGE_END_MONTH'] = $date_range_end_month;
    $this->tkeys['LOCAL_DATE_RANGE_END_YEAR'] = $date_range_end_year;

    $this->tkeys['GO'] = $tr->trans('go');
    $this->tkeys['FORM_NAME'] = 'top_form';

    foreach ($this->tkeys as $key => $value) {
      $search_param_array['TPL_' . strtoupper($key)] = $value;
    }
    $search_param_array = $this->setAdditionalValues($search_param_array);

    $top_search_form = $this->render('pages/' . $this->getSearchTemplate(), $search_param_array);

    $this->tkeys['top_search_form'] = $top_search_form;
    $this->tkeys['FORM_NAME'] = 'bottom_form';

    $this->tkeys['bottom_search_form'] = $top_search_form;
    $this->tkeys['local_parent_item_id'] = $parent_item_id;
    $this->tkeys['local_page_number'] = $page_number;

  }

  /**
   * Sets additional values.
   */
  public function setAdditionalValues($search_param_array) {
    return $search_param_array;
  }

  /**
   * Renders row.
   */
  public function renderRow($cell_counter, $row, $parent_row, $tr, $article_renderer, $article_cache_class, $display_options, $article_db_class) {

    $parent_item_id = $row['parent_item_id'];
    $news_item_id = $row['news_item_id'];
    $news_item_type_id = $row['news_item_type_id'];

    $edit_block = '<span>';

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $edit_block = '<a href="/admin/calendar/event_edit.php?id=';
    }
    elseif ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
      $edit_block = '<a href="/admin/feature_page/blurb_edit.php?id=';
    }
    else {
      $edit_block = '<a href="/admin/article/article_edit.php?id=';
    }
    $edit_block .= $row['news_item_id'];
    $edit_block .= '" target="_blank">';
    $edit_block .= $tr->trans('edit');
    $edit_block .= '</a></span> ';

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $edit_block .= $article_renderer->makeSelectForm('value_' . $cell_counter, $this->getNewsItemEventDisplayOptions(), $row['news_item_status_id']);
    }
    elseif ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_POST || $row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_ATTACHMENT) {
      $edit_block .= $article_renderer->makeSelectForm('value_' . $cell_counter, $display_options, $row['news_item_status_id']);
    }
    else {
      $edit_block .= $article_renderer->makeSelectForm('value_' . $cell_counter, $this->getNewsItemCommentDisplayOptions(), $row['news_item_status_id']);
    }
    $edit_block .= '<input type="hidden" name="id_' . $cell_counter . '" value="' . $row['news_item_id'] . '">';
    $edit_block .= '<input type="hidden" name="old_display_' . $cell_counter . '" value="' . $row['news_item_status_id'] . '">';
    $edit_block .= '<input type="hidden" name="type_' . $cell_counter . '" value="' . $row['news_item_type_id'] . '">';

    $edit_block .= ' <span class="linklike admin-select-control" title="hide this post/event/comment" data-select-id="value_' .
      $cell_counter . '" data-select-set="' . NEWS_ITEM_STATUS_ID_HIDDEN . '">Hide</span>';

    if ($row['parent_item_id'] != $row['news_item_id'] && $row['parent_item_id'] != 0) {
      if ($parent_row['news_item_status_id'] != NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN
      && $parent_row['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN
      && $row['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN
      && $row['news_item_status_id'] != NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN
      ) {
        $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($parent_row['news_item_id'], $parent_row['creation_timestamp']);
        $searchlink .= '?show_comments=1#' . $row['news_item_id'];
      }
      else {
        $searchlink = '/admin/article/article_edit.php?id=' . $row['news_item_id'] . '#preview';
      }
    }
    else {

      $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);
    }

    $parent_item_id = $row['parent_item_id'];
    $title = htmlspecialchars($row['title1']);
    $author = htmlspecialchars($row['displayed_author_name']);
    $summary = $article_renderer->htmlPurify(mb_substr($row['summary'], 0, 800));
    $title2 = '';
    if ($parent_item_id && $parent_item_id != $row['news_item_id']) {
      $parent_title = htmlspecialchars($parent_row['title1']);
      if ($news_item_type_id == 3) {
        $title2 = ' <div class="list-attachment">(Comment On: ' . $parent_title . ')</div>';
      }
      if ($news_item_type_id == NEWS_ITEM_TYPE_ID_ATTACHMENT) {
        $title2 = ' <div class="list-attachment">(Attachment On: ' . $parent_title . ')</div>';
      }
      $summary = $article_renderer->htmlPurify(mb_substr($row['text'], 0, 800));
    }
    $news_item_type_id = $row['news_item_type_id'];
    $media_type_grouping_id = $row['media_type_grouping_id'];

    if ($media_type_grouping_id == 0) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_TEXT_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_TEXT) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_TEXT_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_IMAGE) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_IMAGE_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_AUDIO) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_AUDIO_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_VIDEO) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_VIDEO_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_DOCUMENT) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_DOCUMENT_ICON . '" ';
    }
    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_OTHER) {
      $mime = 'image" src="' . MEDIA_TYPE_GROUPING_OTHER_ICON . '" ';
    }
    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $mime = 'calendar" src="' . MEDIA_TYPE_GROUPING_CALENDAR_ICON . '" ';
    }
    if (($news_item_type_id == NEWS_ITEM_TYPE_ID_ATTACHMENT || $news_item_type_id == NEWS_ITEM_TYPE_ID_COMMENT)
                        && $media_type_grouping_id < MEDIA_TYPE_GROUPING_IMAGE) {
      $mime = 'image" src="' . COMMENT_ICON . '" ';
    }

    $divhtml = '<div class="searchwrap';
    if (!is_int($cell_counter / 2)) {
      $divhtml .= ' bg-grey';
    }
    $divhtml .= '">';
    if ($row['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
      $divhtml .= '<strike>';
    }
    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
      $author = SHORT_SITE_NAME;
    }

    $divhtml .= '<a target="_blank" class="admin-title" name="' . $cell_counter . '" href="' . $searchlink . '"><img alt="' . $mime . '">' . $title . '</a> ' . $title2;
    $divhtml .= '<div class="list-authordate">by ' . $author;
    $divhtml .= '</div>';

    $divhtml .= '<div class="list-summary">' . strip_tags($summary) . '</div>';
    if (isset($row['date_entered'])) {
      $divhtml .= ' <br><i>' . $row['date_entered'] . '</i>';
    }
    if ($row['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
      $divhtml .= '</strike>';
    }

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $divhtml .= '<div class="list-event-date">Event Date: ' . date(static::DATE_LONG, $row['displayed_timestamp']) . '</div>';
    }

    $category_list = $article_db_class->getNewsItemCategoryInfo($row['news_item_id'], 0);
    if (count($category_list) > 0) {
      $catlisting = ' <div class="list-categories"><a title="jump to edit categories for this item" target="_blank" class="admin-cats" href="/admin/calendar/event_edit.php?id=' . $row['news_item_id'] . '#page-list-categories">Categories</a>: ';
    }
    foreach ($category_list as $key => $catrow) {
      $catlisting .= $catrow['name'];
      if ($key === array_key_last($category_list)) {
        $catlisting .= '';
      }
      else {
        $catlisting .= ' | ';
      }
    }
    if (count($category_list) > 0) {
      $catlisting .= '</div>';
    }
    if (isset($catlisting)) {
      $divhtml .= $catlisting;
    }

    $divhtml .= '<div class="item-id">ID: ' . $row['news_item_id'] . ' ';
    if ($row['parent_item_id'] != $news_item_id) {
      $divhtml .= ' <span class="item-id-parent">(Parent ID: ' . $row['parent_item_id'] . ')</span>';
    }
    $divhtml .= '</div>';

    $divhtml .= '<div class="list-published small">Published: ' . date(static::DATE_LONG, $row['creation_timestamp']) . '</div>';

    $divhtml .= '<div class="edit-block">' . $edit_block . '</div>';

    $divhtml .= '</div><!-- END searchwrap -->';

    return $divhtml;
  }

  /**
   * Returns search template.
   */
  public function getSearchTemplate() {
    return 'admin/article/newswire_search.tpl';
  }

  /**
   * Returns news item status select list.
   */
  public function getNewsItemStatusSelectList() {
    $display_options = [];
    $display_options[NEWS_ITEM_STATUS_ID_NEW * NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN * NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = 'Needs Attention';
    $display_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    $display_options[0] = 'All Statuses';
    $display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN] = 'Not Hidden';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Highlighted-Local';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Highlighted-NonLocal';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED] = 'Reposts';
    $display_options[NEWS_ITEM_STATUS_ID_HIDDEN * NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN] = 'Hidden';
    $display_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Other';
    return $display_options;
  }

  /**
   * Returns event display options.
   */
  public function getNewsItemEventDisplayOptions() {

    $display_options = [];

    $display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = "Questionable/Don't Hide";
    $display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN] = 'Questionable/Hide';
    $display_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Calendar Only';
    $display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED] = 'Not Newswire | Feature Eventlinks';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED] = 'Not Newswire | Front Page Eventlinks';
    $display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED] = 'Newswire | Front Page Eventlinks';
    $display_options[NEWS_ITEM_STATUS_ID_HIDDEN] = 'Hidden';
    $display_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    return $display_options;
  }

  /**
   * Returns comment display options.
   */
  public function getNewsItemCommentDisplayOptions() {

    $display_options = [];

    $display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN] = "Questionable/Don't Hide";
    $display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN] = 'Questionable/Hide';
    $display_options[NEWS_ITEM_STATUS_ID_OTHER] = 'Other';
    $display_options[NEWS_ITEM_STATUS_ID_HIDDEN] = 'Hidden';
    $display_options[NEWS_ITEM_STATUS_ID_NEW] = 'New';
    return $display_options;
  }

  /**
   * Returns page size.
   */
  public function getPageSize() {
    return 20;
  }

  /**
   * Processes bulk classification.
   *
   * This should probably get broken up a bit but the logic to limit what has to
   * get updated is complicated enough its likely to remain like this.
   */
  public function bulkClassify($posted_fields) {
    $num_rows = $this->getPageSize() + 1;
    if ($num_rows + 1 > 0) {
      $GLOBALS['part_of_bulk_operation'] = 1;
      $i = 1;
      $pages = [];
      $news_item_db_class = new NewsItemDB();
      $page_db_class = new FeaturePageDB();
      $article_cache_class = new ArticleCache();
      while ($i < $num_rows) {

        $next_id_key = 'id_' . $i;
        $next_old_display_key = 'old_display_' . $i;
        $next_new_display_key = 'value_' . $i;
        $next_type_key = 'type_' . $i;
        if (array_key_exists($next_type_key, $posted_fields)) {
          $next_type_id = $posted_fields[$next_type_key];
          if ($posted_fields[$next_id_key] != '' && $posted_fields[$next_id_key] != 0 && $posted_fields[$next_id_key] + 0 == $posted_fields[$next_id_key]) {

            $next_id = $posted_fields[$next_id_key] + 0;
            $next_old_display = $posted_fields[$next_old_display_key];
            $next_new_display = $posted_fields[$next_new_display_key];

            if ($next_old_display != $next_new_display) {
              $news_item_db_class->updateNewsItemStatusId($next_id, $next_new_display);
              $article_cache_class->recacheJustArticle($next_id);
              if ($next_type_id == NEWS_ITEM_TYPE_ID_EVENT) {
                $article_db_class = new ArticleDB();
                $old_event_info = $article_db_class->getArticleInfoFromNewsItemId($next_id);
                $changed_event_info = $old_event_info;
                $changed_event_info['news_item_status_id'] = $next_new_display;
                $event_list_cache_class = new EventListCache();
                $event_list_cache_class->updateCachesOnChange(
                       $old_event_info, $changed_event_info);
              }
              $next_categories = $news_item_db_class->getNewsItemCategoryIds($next_id);
              $page_id_array = $page_db_class->getAssociatedPageIds($next_categories);
              $more_pages = $page_db_class->getPagesWithAllCategoryNewswires();
              $merged_pages = array_merge($more_pages, $page_id_array);

              foreach ($merged_pages as $page_id) {
                if (array_key_exists($page_id, $pages)) {
                  $next_status_change = $pages[$page_id];
                }
                else {
                  $next_status_change = [];
                }

                // Limit updates for things that correspond to the same
                // newswire.
                if ($next_old_display == NEWS_ITEM_STATUS_ID_NEW ||
                $next_old_display == NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN) {
                  $next_old_display = NEWS_ITEM_STATUS_ID_OTHER;
                }
                if ($next_old_display == NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED) {
                  $next_old_display = NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED;
                }
                $next_status_change[$next_old_display] = 1;
                $next_status_change[$next_new_display] = 1;
                $pages[$page_id] = $next_status_change;
              }
            }
          }
        }

        $i = $i + 1;
      }
      $GLOBALS['part_of_bulk_operation'] = 0;
      if (count($pages) > 0) {
        $newswire_cache_class = new NewswireCache();
        foreach ($pages as $page_id => $page_changes) {
          foreach ($page_changes as $old_display_status => $value) {
            $newswire_cache_class->regenerateNewswireForPageHelper($page_id, $old_display_status, $old_display_status);
          }
        }
      }

    }
    $tr = new Translate();
    $this->addStatusMessage($tr->trans('article_classify_success') . ' ');

  }

  /**
   * Formats paging.
   */
  public function formatPaging($page_number, $search_fields, $num_rows_on_this_page, $min_row_in_results, $max_row_in_results) {

    $firstarrow = '<img src="/im/arrow-first.svg" alt="first" title="first page" class="mediaicon pageicon" width="12" height="12">';
    $prevarrow = '<img src="/im/arrow-back.svg" alt="back" title="back page" class="mediaicon pageicon" width="12" height="12">';
    $nextarrow = '<img src="/im/arrow-next.svg" alt="next" title="next page" class="mediaicon pageicon" width="12" height="12">';
    $lastarrow = '<img src="/im/arrow-last.svg" alt="last" title="last page" class="mediaicon pageicon" width="12" height="12">';

    $this->tkeys['nav'] = '<span class="pagination articlelist-pagination">';

    if ($page_number > 0) {
      $this->tkeys['nav'] .= '<a href="' . $this->generatePagingUrl(0, $search_fields, $min_row_in_results, 0) . '" rel="nofollow">' . $firstarrow . '</a> ';
    }
    if ($page_number > 0) {
      $this->tkeys['nav'] .= ' <a href="' . $this->generatePagingUrl($page_number - 1, $search_fields, $min_row_in_results, 0) . '" rel="nofollow">' . $prevarrow . '</a> ';
    }
    $displaypage = $page_number + 1;
    $this->tkeys['nav'] .= "$displaypage of " . $this->tkeys['last_page'] . ' ';
    if ($this->tkeys['last_page'] > $page_number + 1) {
      $this->tkeys['nav'] .= ' <a href="' . $this->generatePagingUrl($page_number + 1, $search_fields, 0, $max_row_in_results) . '" rel="nofollow">' . $nextarrow . '</a>';
      $this->tkeys['nav'] .= ' <a href="' . $this->generatePagingUrl($this->tkeys['last_page'] - 1, $search_fields, 0, $max_row_in_results) . '" rel="nofollow">' . $lastarrow . '</a>';
    }
    $this->tkeys['nav'] .= '</span>';
    $this->tkeys['num_rows'] = $num_rows_on_this_page + 1;
    if ($num_rows_on_this_page + 1 > $search_fields['page_size']) {
      $this->tkeys['num_rows'] = $search_fields['page_size'];
    }

    if (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1') {
      $this->tkeys['nav'] = '';
    }
  }

  /**
   * Generate a HTML-encoded paging URL.
   */
  public function generatePagingUrl($page_number, array $search_fields) {
    // These parameters should already be integers but let's make sure.
    $url = $_SERVER['SCRIPT_NAME'] . '?page_number=' . intval($page_number);
    foreach ($search_fields as $key => $value) {
      // Filter out bogus query parameter keys starting with 'amp;'.
      if (!is_object($value) && $key != 'page_number' && !str_starts_with($key, 'amp;')) {
        $url = $url . '&amp;' . rawurlencode($key) . '=' . rawurlencode($value);
      }
    }
    return $url;
  }

  /**
   * Sets up fields.
   */
  public function setupFields() {
    $search_fields = $_GET;
    if (!array_key_exists('submitted_search', $search_fields)) {
      if (!array_key_exists('include_events', $search_fields) &&
                 !array_key_exists('include_posts', $search_fields)
                 && !array_key_exists('include_comments', $search_fields)
                    && !array_key_exists('include_attachments', $search_fields)
                    && !array_key_exists('include_blurbs', $search_fields)
                    ) {
        $search_fields['include_events'] = 1;

        $search_fields['include_posts'] = 1;

      }

      if (!array_key_exists('include_attachments', $search_fields)
             && array_key_exists('media_type_grouping_id', $search_fields)
             && $search_fields['media_type_grouping_id'] + 0 != 0) {
        $search_fields['include_attachments'] = 1;
      }
    }

    if (array_key_exists('page_number', $search_fields)) {
      $search_fields['page_number'] = (int) $search_fields['page_number'];
    }
    else {
      $search_fields['page_number'] = 0;
    }
    $search_fields['search'] = (array_key_exists('search', $search_fields)) ? trim($search_fields['search']) : '';
    if (array_key_exists('include_attachments', $search_fields)) {
      $search_fields['include_attachments'] = (int) $search_fields['include_attachments'];
    }
    else {
      $search_fields['include_attachments'] = 0;
    }
    if (array_key_exists('include_posts', $search_fields)) {
      $search_fields['include_posts'] = (int) $search_fields['include_posts'];
    }
    else {
      $search_fields['include_posts'] = 0;
    }
    if (array_key_exists('include_events', $search_fields)) {
      $search_fields['include_events'] = (int) $search_fields['include_events'];
    }
    else {
      $search_fields['include_events'] = 0;
    }
    if (array_key_exists('include_blurbs', $search_fields)) {
      $search_fields['include_blurbs'] = (int) $search_fields['include_blurbs'];
    }
    else {
      $search_fields['include_blurbs'] = 0;
    }
    if (array_key_exists('include_comments', $search_fields)) {
      $search_fields['include_comments'] = (int) $search_fields['include_comments'];
    }
    else {
      $search_fields['include_comments'] = 0;
    }
    if (array_key_exists('media_type_grouping_id', $search_fields)) {
      $search_fields['media_type_grouping_id'] = (int) $search_fields['media_type_grouping_id'];
    }
    else {
      $search_fields['media_type_grouping_id'] = 0;
    }
    if (array_key_exists('topic_id', $search_fields)) {
      $search_fields['topic_id'] = (int) $search_fields['topic_id'];
    }
    else {
      $search_fields['topic_id'] = 0;
    }
    if (array_key_exists('region_id', $search_fields)) {
      $search_fields['region_id'] = (int) $search_fields['region_id'];
    }
    else {
      $search_fields['region_id'] = 0;
    }
    if (array_key_exists('page_id', $search_fields)) {
      $search_fields['page_id'] = (int) $search_fields['page_id'];
    }
    else {
      $search_fields['page_id'] = 0;
    }
    if (array_key_exists('search_date_type', $search_fields)) {
      $search_fields['search_date_type'] = htmlspecialchars($search_fields['search_date_type']);
    }
    else {
      $search_fields['search_date_type'] = '';
    }

    if (array_key_exists('date_range_start_day', $search_fields)) {
      $search_fields['date_range_start_day'] = (int) $search_fields['date_range_start_day'];
    }
    else {
      $search_fields['date_range_start_day'] = '';
    }
    if (array_key_exists('date_range_start_month', $search_fields)) {
      $search_fields['date_range_start_month'] = (int) $search_fields['date_range_start_month'];
    }
    else {
      $search_fields['date_range_start_month'] = '';
    }
    if (array_key_exists('date_range_start_year', $search_fields)) {
      $search_fields['date_range_start_year'] = (int) $search_fields['date_range_start_year'];
    }
    else {
      $search_fields['date_range_start_year'] = '';
    }

    if (array_key_exists('date_range_end_day', $search_fields)) {
      $search_fields['date_range_end_day'] = (int) $search_fields['date_range_end_day'];
    }
    else {
      $search_fields['date_range_end_day'] = '';
    }
    if (array_key_exists('date_range_end_month', $search_fields)) {
      $search_fields['date_range_end_month'] = (int) $search_fields['date_range_end_month'];
    }
    else {
      $search_fields['date_range_end_month'] = '';
    }
    if (array_key_exists('date_range_end_year', $search_fields)) {
      $search_fields['date_range_end_year'] = (int) $search_fields['date_range_end_year'];
    }
    else {
      $search_fields['date_range_end_year'] = '';
    }

    $search_fields['date_range_start'] = new Date();
    if ($search_fields['search_date_type'] != '' && !$search_fields['date_range_start_day']) {
      $search_fields['date_range_start']->setDateToNow();
      $search_fields['date_range_start_day'] = $search_fields['date_range_start']->getDay();
      $search_fields['date_range_start_month'] = $search_fields['date_range_start']->getMonth();
      $search_fields['date_range_start_year'] = $search_fields['date_range_start']->getYear();
    }
    else {
      $search_fields['date_range_start']->setTime(0, 0, $search_fields['date_range_start_day'], $search_fields['date_range_start_month'],
      $search_fields['date_range_start_year']);
    }

    $search_fields['date_range_end'] = new Date();
    if ($search_fields['search_date_type'] != '' && !$search_fields['date_range_end_day']) {
      $search_fields['date_range_end']->setDateToNow();
      $search_fields['date_range_end']->moveForwardDays(365);
      $search_fields['date_range_end_day'] = $search_fields['date_range_end']->getDay();
      $search_fields['date_range_end_month'] = $search_fields['date_range_end']->getMonth();
      $search_fields['date_range_end_year'] = $search_fields['date_range_end']->getYear();
    }
    else {
      $search_fields['date_range_end']->setTime(0, 0, $search_fields['date_range_end_day'], $search_fields['date_range_end_month'],
      $search_fields['date_range_end_year']);
      $search_fields['date_range_end']->moveForwardDays(1);
    }

    if (!array_key_exists('parent_item_id', $search_fields)) {
      $search_fields['parent_item_id'] = 0;
    }
    else {
      $search_fields['parent_item_id'] = (int) $search_fields['parent_item_id'];
      if (array_key_exists('force_parent_id_search', $search_fields)) {
        $search_fields['include_posts'] = 1;
        $search_fields['include_comments'] = 1;
        $search_fields['include_attachments'] = 1;
        $search_fields['include_events'] = 1;
        $search_fields['include_blurbs'] = 1;
        $search_fields['topic_id'] = 0;
        $search_fields['region_id'] = 0;
        $search_fields['page_id'] = 0;
        $search_fields['news_item_status_restriction'] = 0;
      }
    }

    if (array_key_exists('page_id', $search_fields) && $search_fields['page_id'] + 0 > 0) {
      $feature_page_db = new FeaturePageDB();
      $search_fields['topic_id'] = $feature_page_db->getFirstTopicForPage($search_fields['page_id']);
      $search_fields['region_id'] = $feature_page_db->getFirstRegionForPage($search_fields['page_id']);
    }

    if (!isset($search_fields['news_item_status_restriction'])) {
      $search_fields['news_item_status_restriction'] = NEWS_ITEM_STATUS_ALL_NONHIDDEN;
    }

    // Legacy support PLUS THIS IS WHAT GOOGLE NEWS USES (so we can render it
    // more to their liking)
    if (array_key_exists('display', $search_fields) && $search_fields['display'] == 'lg') {
      $search_fields['news_item_status_restriction'] = NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
      $search_fields['include_events'] = 0;
      $search_fields['include_posts'] = 1;
      $search_fields['include_blurbs'] = 1;
      $search_fields['include_comments'] = 0;
      $search_fields['page_size'] = 45;
    }
    else {
      $search_fields['page_size'] = $this->getPageSize();
    }
    return $search_fields;
  }

}
