<?php

namespace Indybay\Pages\Admin\Article;

use Indybay\Cache\ArticleCache;
use Indybay\Cache\DependentListCache;
use Indybay\DB\ArticleDB;
use Indybay\DB\NewswireDB;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;

/**
 * Class for article_edit page.
 */
class ArticleMakeAttachment extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $article_db_class = new ArticleDB();
    $article_renderer_class = new ArticleRenderer();
    $article_cache_class = new ArticleCache();

    if ($_GET['item_id'] > 0) {
      $news_item_id = $_GET['item_id'];
    }
    elseif ($_POST['item_id'] > 0) {
      $news_item_id = $_POST['item_id'];
    }

    $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
    $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);

    if ($_POST['make_attachment'] != '') {
      $parent_item_id = $_POST['parent_item_id'] + 0;
      if ($parent_item_id == 0) {
        $parent_item_id = $_POST['selected_parent_id'] + 0;
      }
      if ($parent_item_id + 0 == 0) {
        $this->addValidationMessage('You Must Choose a Parent Item');
      }
      elseif ($parent_item_id == $news_item_id) {
        $this->addValidationMessage("You Can't Make An Item Its Own Parent");
      }
      else {
        $parent_item_version_id = $article_db_class->getCurrentVersionId($parent_item_id);
        if ($parent_item_version_id + 0 != 0) {
          $parent_version_info = $article_db_class->getArticleInfoFromVersionId($parent_item_version_id);
        }
        if (!is_array($parent_version_info)) {
          $this->addValidationMessage('No Item Exists With Id ' . $parent_item_id);
        }
        else {
          if ($parent_version_info['news_item_type_id'] != NEWS_ITEM_TYPE_ID_POST && $parent_version_info['news_item_type_id'] != NEWS_ITEM_TYPE_ID_EVENT) {
            $this->addValidationMessage('Parent Must Be A Post While Item ' . $parent_item_id . ' Is Not');
          }
          else {
            $parent_item_status_id = $parent_version_info['news_item_status_id'];
            if (trim($version_info['summary']) != '') {
              if ($version_info['is_text_html'] == '1') {
                $version_info['text'] = $version_info['summary'] . '<p>' . $version_info['text'];
                $version_info['summary'] = '';
              }
              else {
                $version_info['text'] = $version_info['summary'] . "\n\n" . $version_info['text'];
                $version_info['summary'] = '';
              }
            }

            $version_info2 = $version_info;
            $version_info2['summary'] = $version_info2['summary'];
            $version_info2['text'] = $version_info2['text'];
            $version_info2['title1'] = $version_info2['title1'];
            $version_info2['title2'] = $version_info2['title2'];
            $version_info2['related_url'] = $version_info2['related_url'];
            $version_info2['displayed_author_name'] = $version_info2['displayed_author_name'];
            $version_info2['email'] = $version_info2['email'];
            $version_info2['phone'] = $version_info2['email'];
            $version_info2['address'] = $version_info2['address'];
            $news_item_version_id = $article_db_class->createNewVersion($news_item_id, $version_info2);
            $article_db_class->updateNewsItemStatusAndType($news_item_id, $parent_item_status_id, NEWS_ITEM_TYPE_ID_ATTACHMENT);
            $article_db_class->updateParentId($news_item_id, $parent_item_id);
            $article_db_class->changeAllParentIdsForChildren($news_item_id, $parent_item_id);
            $article_cache_class->cacheRedirectForArticle($news_item_id, $parent_item_id);
            $article_cache_class->cacheEverythingForArticle($parent_item_id);
            $dependent_list_cache_class = new DependentListCache();
            $cat_list = $article_db_class->getNewsItemCategoryIds($news_item_id);
            $dependent_list_cache_class->updateAllDependenciesOnUpdate(
            $version_info, $version_info, $cat_list);
            $this->redirect('/admin/article/article_edit.php?id=' . $news_item_id);
          }
        }
      }
    }

    $rendered_html = $article_renderer_class->getRenderedArticleHtml($version_info);

    $newswire_db = new NewswireDB();
    $list = $newswire_db->getAllListForCategories([], 100);
    $recent_post_list = [];
    $recent_post_list['0'] = 'Choose A Parent News Article';
    $i = 0;
    foreach ($list as $row) {
      if ($row['news_item_id'] != $news_item_id && $row['news_item_type_id'] = NEWS_ITEM_TYPE_ID_POST) {
        $recent_post_list[$row['news_item_id']] = $row['title1'];
        $i = $i + 1;
      }
      if ($i > 40) {
        break;
      }
    }

    $this->tkeys['local_item_preview'] = $rendered_html;

    $this->tkeys['local_possible_parent_list'] = $article_renderer_class->makeSelectForm(
    'selected_parent_id', $recent_post_list,
    $_POST['selected_parent_id']);
    $this->tkeys['local_news_item_id'] = $news_item_id;

  }

}
