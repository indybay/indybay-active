<?php

namespace Indybay\Pages\Admin\Article;

use Indybay\Cache\ArticleCache;
use Indybay\Cache\DependentListCache;
use Indybay\DB\ArticleDB;
use Indybay\DB\CalendarDB;
use Indybay\DB\CategoryDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemDB;
use Indybay\DB\UserDB;
use Indybay\MediaAndFileUtil\ImageUtil;
use Indybay\MediaAndFileUtil\UploadUtil;
use Indybay\Page;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Renderer\UserRenderer;
use Indybay\Translate;

/**
 * Class for article_edit page.
 */
class ArticleEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $article_db_class = new ArticleDB();
    $article_renderer_class = new ArticleRenderer();
    $category_db_class = new CategoryDB();
    $tr = new Translate();

    $tr->createTranslateTable('article_edit');

    $this->tkeys['publish_result'] = '';

    if (array_key_exists('news_item_id', $_POST) && $_POST['news_item_id'] + 0 > 0) {
      $news_item_id = $_POST['news_item_id'];
      $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
      $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    }
    if (array_key_exists('version_id', $_GET)&& $_GET['version_id'] + 0 > 0) {
      $news_item_version_id = $_GET['version_id'] + 0;
      $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
      $news_item_id = $version_info['news_item_id'];
    }
    if (array_key_exists('id', $_GET)&& $_GET['id'] > 0) {
      $news_item_id = $_GET['id'];
      $news_item_version_id = $article_db_class->getCurrentVersionId($news_item_id);
      $version_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);

    }

    $this->tkeys['LOCAL_IMAGE_OPTIONS'] = '';
    if ($version_info['media_attachment_id'] + 0 != 0) {
      $media_attachment_db_class = new MediaAttachmentDB();
      $image_info = $media_attachment_db_class->getMediaAttachmentInfo($version_info['media_attachment_id']);
      if ($image_info['media_type_id'] == 14 || $image_info['media_type_id'] == 15 || $image_info['media_type_id'] == 25 || $image_info['media_type_id'] == 16) {
        $image_options = ' <div>Rotate Existing Attachment:</div>';
        $image_options .= ' <div>by ';
        $image_options .= ' <input type="submit" id="rotate" name="rotate" value="90">';
        $image_options .= ' degrees clockwise</div>';

        $this->tkeys['LOCAL_IMAGE_OPTIONS'] = $image_options;

      }
    }

    // Need to add generic redirect for this type of post.
    if (array_key_exists('blurbify', $_POST) && $_POST['blurbify'] != '') {
      $this->blurbifyPostFields($news_item_id);

      $page = new Page('blurb_add', 'admin/feature_page');
      $page->addStatusMessage('<h3>Add Blurb From Post</h3>' .
      '<span class="small">Since you don\'t have a preview on this page, you will have to add the blurb (but don\'t need to push live) to review the images that were preselected for you. The short version is the summary from the post and the long version should be the first paragraph from the text.<br><br>' .
      'It\'s likely you will have to make a lot of changes to make the blurb look reasonable. The "create blurb from post" functionality is only intended to speed the process of writing blurbs and not create them for you.</span>');
      $page->buildPage();
      echo $page->getHtml();
      include INCLUDE_PATH . '/admin/admin-footer.inc';
      exit;
    }

    if (array_key_exists('editswitch', $_POST)) {

      if ($this->validate($_POST) == 1) {
        $version_info = $this->addNewVersion($_POST);
        $news_item_id = $version_info['news_item_id'];
        $this->mediaAttachmentDependency($version_info);
      }
      else {

        $version_info['title1'] = $_POST['title1'];
        $version_info['title2'] = $_POST['title2'];
        $version_info['displayed_author_name'] = $_POST['displayed_author_name'];
        $version_info['summary'] = $_POST['summary'];
        $version_info['text'] = $_POST['text'];
        $version_info['email'] = $_POST['email'];
        $version_info['news_item_status_id'] = $_POST['news_item_status_id'];
        $version_info['display_contact_info'] = $_POST['display_contact_info'];
        $version_info['news_item_type_id'] = $_POST['news_item_type_id'];
        $version_info['is_summary_html'] = $_POST['is_summary_html'];
        $version_info['is_text_html'] = $_POST['is_text_html'];
        $version_info['related_url'] = $_POST['related_url'];

      }

    }
    else {
      if ($version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
        $preview = '';
        if (isset($_GET['p'])) {
          $preview = '#preview';
        }
        if (isset($_GET['version_id'])) {
          $this->redirect('/admin/feature_page/blurb_edit.php?version_id=' . $_GET['version_id'] . $preview);
        }
        else {
          $this->redirect('/admin/feature_page/blurb_edit.php?id=' . $news_item_id . $preview);
        }
        return;
      }
    }
    if ($version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $this->forcedTemplateFile = 'admin/calendar/event_edit.tpl';
    }
    else {
      $this->forcedTemplateFile = 'admin/article/article_edit.tpl';
    }

    if ($version_info['news_item_type_id'] + 0 == NEWS_ITEM_TYPE_ID_POST || $version_info['news_item_type_id'] + 0 == NEWS_ITEM_TYPE_ID_EVENT) {
      $type_options = [
        NEWS_ITEM_TYPE_ID_POST => 'Post',
        NEWS_ITEM_TYPE_ID_EVENT => 'Event',
      ];
    }
    elseif ($version_info['news_item_type_id'] + 0 == NEWS_ITEM_TYPE_ID_ATTACHMENT ||
      $version_info['news_item_type_id'] + 0 == NEWS_ITEM_TYPE_ID_COMMENT
       ) {
      $type_options = [
        NEWS_ITEM_TYPE_ID_ATTACHMENT => 'Attachment',
        NEWS_ITEM_TYPE_ID_COMMENT => 'Comment',
        'convert_to_post' => 'Post',
      ];
    }
    else {
      $type_options = $article_db_class->getNewsItemTypesSelectList();
    }

    if ($version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $article_list = new ArticleList();
      $disp_options = $article_list->getNewsItemEventDisplayOptions();
    }
    elseif ($version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_POST || $version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_ATTACHMENT) {
      $disp_options = $article_db_class->getNewsItemStatusSelectList();
    }
    else {
      $article_list = new ArticleList();
      $disp_options = $article_list->getNewsItemCommentDisplayOptions();
    }

    $user_db_class = new UserDB();
    $user_renderer_class = new UserRenderer();
    $last_updated_by_user_id = $version_info['version_created_by_id'];
    $created_by_user_id = $version_info['created_by_id'];
    if (!isset($last_updated_by_user_id)) {
      $last_updated_by_user_id = 0;
    }
    if (!isset($created_by_user_id)) {
      $created_by_user_id = 0;
    }

    $user_info = $user_db_class->getUserInfo($last_updated_by_user_id);
    $rendered_modified_user_info = $user_renderer_class->renderUserDetailInfo($user_info);
    $user_info = $user_db_class->getUserInfo($created_by_user_id);
    $rendered_created_user_info = $user_renderer_class->renderUserDetailInfo($user_info);

    $this->tkeys['local_last_updated_by_user_info'] = $rendered_modified_user_info;
    $this->tkeys['local_created_by_user_info'] = $rendered_created_user_info;
    $this->tkeys['local_news_item_id'] = htmlspecialchars($version_info['news_item_id']);
    $this->tkeys['local_news_item_version_id'] = htmlspecialchars($version_info['news_item_version_id']);
    $this->tkeys['local_summary'] = htmlspecialchars($version_info['summary']);
    $this->tkeys['local_email'] = htmlspecialchars($version_info['email']);
    $this->tkeys['local_phone'] = htmlspecialchars($version_info['phone']);
    $this->tkeys['local_address'] = htmlspecialchars($version_info['address']);
    $this->tkeys['local_text'] = htmlspecialchars($version_info['text']);

    $media_attachment_id = $version_info['media_attachment_id'];
    $this->tkeys['local_media_attachment_id'] = $media_attachment_id;
    if ($media_attachment_id != 0) {
      $this->tkeys['local_attachment_edit_link'] =
            '<span class="small">(<a href="/admin/media/upload_edit.php?media_attachment_id='
            . $media_attachment_id
            . '">edit attachment id # ' . $media_attachment_id . '</a>)</span>';
    }
    else {
      $this->tkeys['local_attachment_edit_link'] = '';
    }
    $thumbnail_attachment_id = $version_info['thumbnail_media_attachment_id'];
    $this->tkeys['local_thumbnail_media_attachment_id'] = $thumbnail_attachment_id;
    if ($thumbnail_attachment_id != 0) {
      $this->tkeys['local_thumbnail_edit_link'] =
            '<span class="small">(<a href="/admin/media/upload_edit.php?media_attachment_id='
            . $thumbnail_attachment_id
            . '">edit attachment id # ' . $thumbnail_attachment_id . '</a>)</span>';
    }
    else {
      $this->tkeys['local_thumbnail_edit_link'] = '';
    }

    $this->tkeys['local_displayed_author_name'] = htmlspecialchars($version_info['displayed_author_name']);
    $this->tkeys['local_parent_id'] = $version_info['parent_item_id'];
    $this->tkeys['local_related_url'] = htmlspecialchars($version_info['related_url']);
    if (array_key_exists('contact_info_id', $version_info)) {
      $this->tkeys['local_contact_info_id'] = htmlspecialchars($version_info['contact_info_id']);
    }
    else {
      $this->tkeys['local_contact_info_id'] = 0;
    }
    $this->tkeys['local_title1'] = htmlspecialchars($version_info['title1']);
    $this->tkeys['local_title2'] = htmlspecialchars($version_info['title2']);
    $this->tkeys['local_creation_date'] = $version_info['created'];
    $this->tkeys['local_version_creation_date'] = $version_info['modified'];
    $this->tkeys['local_id'] = $version_info['news_item_id'];
    $this->tkeys['local_event_duration'] = $version_info['event_duration'];
    $this->tkeys['local_old_display'] = $version_info['news_item_status_id'];
    $this->tkeys['select_display'] = $article_renderer_class->makeSelectForm('news_item_status_id', $disp_options, $version_info['news_item_status_id']);

    $this->tkeys['select_news_item_type_id'] = $article_renderer_class->makeSelectForm('news_item_type_id', $type_options, $version_info['news_item_type_id']);
    if ($version_info['news_item_type_id'] + 0 == NEWS_ITEM_TYPE_ID_POST) {
      $this->tkeys['select_news_item_type_id'] .= '<a href="/admin/article/article_make_attachment.php?item_id=' . $version_info['news_item_id'] . '">Convert This Item into an Attachment</a>';
    }

    $this->tkeys['local_checkbox_is_summary_html'] = $article_renderer_class->makeBooleanCheckboxForm('is_summary_html', $version_info['is_summary_html']);
    $this->tkeys['local_checkbox_is_text_html'] = $article_renderer_class->makeBooleanCheckboxForm('is_text_html', $version_info['is_text_html']);
    $this->tkeys['local_checkbox_display_contact_info'] = $article_renderer_class->makeBooleanCheckboxForm('display_contact_info', $version_info['display_contact_info']);

    $displayed_year = $version_info['displayed_date_year'];
    $displayed_month = $version_info['displayed_date_month'];
    $displayed_day = $version_info['displayed_date_day'];
    $displayed_hour = $version_info['displayed_date_hour'];
    $displayed_minute = $version_info['displayed_date_minute'];
    $displayed_ampm = 'AM';
    if ($displayed_hour >= 12) {
      $displayed_ampm = 'PM';
    }
    if ($displayed_hour > 12) {
      $displayed_hour = $displayed_hour - 12;
    }
    if ($displayed_hour == 0) {
      $displayed_hour = 12;
    }

    if ($version_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $calendar_db_class = new CalendarDB();
      $event_type_info = $calendar_db_class->getEventTypeInfoForNewsitem($version_info['news_item_id']);
      $event_type_id = $event_type_info['keyword_id'] ?? NULL;
      $keyword_list = $calendar_db_class->getEventTypeForSelectList();
      $this->tkeys['cat_type_select'] = $article_renderer_class->makeSelectForm(
      'event_type_id', $keyword_list,
      $event_type_id);
    }

    $this->tkeys['local_select_date_displayed_date'] = $article_renderer_class->makeDatetimeSelectForm(
        'displayed_date', 0, 1, $displayed_day, $displayed_month, $displayed_year,
        $displayed_hour, $displayed_minute, $displayed_ampm);

    $this->tkeys['local_select_date_displayed_date'] = $article_renderer_class->makeDatetimeSelectForm(
        'displayed_date', 10, 1, $version_info['displayed_date_day'], $version_info['displayed_date_month'], $version_info['displayed_date_year'],
        $displayed_hour, $version_info['displayed_date_minute'], $displayed_ampm);

    $saved_categories_for_article = $article_db_class->getNewsItemCategoryIds($news_item_id, 0);

    $available_categories = $category_db_class->getCategoryInfoListByType(1, 0);
    $this->tkeys['local_checkbox_region'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);
    $available_categories = $category_db_class->getCategoryInfoListByType(2, 0);
    $this->tkeys['local_checkbox_topic'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);
    $available_categories = $category_db_class->getCategoryInfoListByType(2, 44);
    $this->tkeys['local_checkbox_int'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);
    $available_categories = $category_db_class->getCategoryInfoListByType(3, 0);
    $this->tkeys['local_checkbox_other'] = $article_renderer_class->makeCheckboxForm('catartarray[]', $available_categories, $saved_categories_for_article, 3);

    $this->tkeys['local_previous_version_list'] =
            $article_renderer_class->renderVersionList(
      $article_db_class->getVersionListInfo($news_item_id, 5), '/admin/article/article_edit.php');

    if (array_key_exists('version_id', $_GET) && $_GET['version_id'] + 0 > 0) {
      $article_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    }
    else {
      $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    }
    $article_info['is_preview'] = 1;
    $rendered_html = $article_renderer_class->getRenderedArticleHtml($article_info);
    $rel_link = $article_renderer_class->getRelativeWebPathFromItemInfo($article_info);

    $this->tkeys['LOCAL_DISPLAY_PREVIEW'] = $rendered_html;
    $this->tkeys['LOCAL_PUBLISH_LINK'] = FULL_ROOT_URL . substr($rel_link, 1);
    if ($article_info['news_item_type_id'] + 0 != NEWS_ITEM_TYPE_ID_ATTACHMENT &&
       $article_info['news_item_type_id'] + 0 != NEWS_ITEM_TYPE_ID_COMMENT) {
      $this->tkeys['LOCAL_PARENT_SECTION'] = '<input type="hidden" name="parent_item_id" ';
      $this->tkeys['LOCAL_PARENT_SECTION'] .= 'size="10" value="' . $article_info['parent_item_id'] . '">';
      $this->tkeys['LOCAL_PARENT_SECTION'] .= $article_info['parent_item_id'];
    }
    else {
      $this->tkeys['LOCAL_PARENT_SECTION'] = '<input type="text" name="parent_item_id" ';
      $this->tkeys['LOCAL_PARENT_SECTION'] .= 'size="10" value="' . $article_info['parent_item_id'] . '">';
    }
    return 1;
  }

  /**
   * Validates article.
   */
  public function validate() {
    $ret = 1;
    if (trim($_POST['title1']) == '') {
      $this->addValidationMessage('Title Is Required.');
      $ret = 0;
    }
    if (trim($_POST['displayed_author_name']) == '') {
      $this->addValidationMessage('Author Name Is Required.');
      $ret = 0;
    }
    return $ret;
  }

  /**
   * Adds new version.
   */
  public function addNewVersion($post_array) {
    $article_db_class = new ArticleDB();
    $news_item_db_class = new NewsItemDB();

    $post_array = $this->replaceUploadIfChanged($post_array);
    if (isset($_POST['rotate'])) {
      $post_array = $this->rotateImage($post_array);
    }

    $news_item_id = $post_array['news_item_id'];
    $old_article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    $old_parent_id = $old_article_info['parent_item_id'];
    $old_display_status = $old_article_info['news_item_status_id'];
    $old_type_id = $old_article_info['news_item_type_id'];

    if (!isset($post_array['catartarray'])) {
      $post_array['catartarray'] = [];
    }
    $old_cat_list = $news_item_db_class->updateNewsItemCategoriesReturnOldCategories($news_item_id, $post_array['catartarray']);

    $news_item_version_id = $article_db_class->createNewVersion($news_item_id, $post_array);

    $news_item_db_class = new NewsItemDB();
    $news_item_status_id = $post_array['news_item_status_id'];
    $news_item_type_id = $post_array['news_item_type_id'];
    $parent_item_id = $post_array['parent_item_id'];

    if ($post_array['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $event_type_id = $post_array['event_type_id'];
      $calendar_db_class = new CalendarDB();
      $calendar_db_class->setOrChangeEventType($news_item_id, $event_type_id);
    }

    $article_cache_class = new ArticleCache();

    if ($news_item_type_id == 'convert_to_post') {
      $news_item_type_id = NEWS_ITEM_TYPE_ID_POST;
      $parent_item_id = $news_item_id;
    }

    if ($old_type_id != $news_item_type_id || $old_display_status != $news_item_status_id) {
      $news_item_db_class->updateNewsItemStatusAndType($news_item_id, $news_item_status_id, $news_item_type_id);
    }
    if ($old_parent_id != $parent_item_id) {
      $news_item_db_class->updateParentId($news_item_id, $parent_item_id);
    }

    if ($parent_item_id == $news_item_id || $parent_item_id == 0) {
      $article_cache_class->cacheEverythingForArticle($news_item_id);
    }
    if ($old_parent_id != $news_item_id && $old_parent_id != 0) {
      $article_cache_class->cacheEverythingForArticle($old_parent_id);
    }

    $article_info = $article_db_class->getArticleInfoFromVersionId($news_item_version_id);
    if ($parent_item_id == $news_item_id || $parent_item_id == 0) {
      $this->processAdditionalDbDependencies($old_article_info, $article_info);

      $dependent_list_cache_class = new DependentListCache();
      $dependent_list_cache_class->updateAllDependenciesOnUpdate(
      $old_article_info, $article_info, $old_cat_list);
    }
    $tr = new Translate();
    $this->addStatusMessage($tr->trans('article_update_success'));

    return $article_info;

  }

  /**
   * Processes additional db dependencies.
   */
  public function processAdditionalDbDependencies(
    $old_article_info,
    $new_article_info,
  ) {
  }

  /**
   * Processes media attachment dependency.
   */
  public function mediaAttachmentDependency($version_info) {
    $upload_util = new UploadUtil();
    $upload_util->generateSecondaryFilesOnChange($version_info);
  }

  /**
   * Setup post fields for blurb.
   */
  public function blurbifyPostFields($news_item_id) {
    $article_renderer_class = new ArticleRenderer();
    $category_db_class = new CategoryDB();
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    $rel_link = $article_renderer_class->getRelativeWebPathFromItemInfo($article_info);
    $_POST['news_item_id'] = '';
    $_POST['news_item_version_id'] = '';
    $_POST['text'] = substr($_POST['text'], 0, 2000);
    $i = strrpos($_POST['text'], "\n");
    if ($i > 200) {
      $_POST['text'] = substr($_POST['text'], 0, $i);
    }
    $_POST['text'] = trim($_POST['text']);
    if (empty($_POST['is_text_html'])) {
      $_POST['text'] = nl2br(htmlspecialchars($_POST['text']));
      // Plain text posts are permitted to contain link tags.
      $_POST['text'] = preg_replace('#&lt;a href=&quot;(.+?)&quot;&gt;(.+?)&lt;/a&gt;#', '<a href="$1">$2</a>', $_POST['text']);
    }
    $_POST['title2'] = $_POST['title1'];
    $read_more_set = 0;
    if ($_POST['media_attachment_id'] + 0 != 0) {
      $media_db_class = new MediaAttachmentDB();
      $media_info = $media_db_class->getMediaAttachmentInfo($_POST['media_attachment_id']);
      $media_grouping_id = $media_info['media_type_grouping_id'];
      if ($media_grouping_id + 0 != MEDIA_TYPE_GROUPING_IMAGE) {
        $_POST['media_attachment_id'] = 0;
        $_POST['thumbnail_media_attachment)id'] = 0;
        $media_type_id = $media_info['media_type_id'];

        if ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_AUDIO) {
          $read_more_set = 1;
          $_POST['text'] = $_POST['text'] . "\n\n<br><br>\n" .
          "<img src=\"/im/imc_audio.svg\" alt=\"audio\" width=\"12\" height=\"12\"> \n<a href=\"" . $rel_link . "\">Audio</a>\n";
        }
        elseif ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_VIDEO) {
          $read_more_set = 1;
          $_POST['text'] = $_POST['text'] . "\n\n<br><br>\n" .
          "<img src=\"/im/imc_video.svg\" alt=\"video\" width=\"12\" height=\"12\"> \n<a href=\"" . $rel_link . "\">Video</a>\n";
        }
        elseif ($media_type_id == 2) {
          $read_more_set = 1;
          $_POST['text'] = $_POST['text'] . "\n\n<br><br>\n" .
          "<img src=\"/im/imc_pdf.svg\" alt=\"pdf\" width=\"12\" height=\"12\"> \n<a href=\"" . $rel_link . "\">PDF</a>\n";
        }
      }
      else {
        $read_more_set = 1;
        $_POST['text'] = $_POST['text'] . "\n\n<br><br>\n" .
        "<img src=\"/im/imc_photo.svg\" alt=\"photo\" width=\"12\" height=\"12\"> \n<a href=\"" . $rel_link . "\">Photos</a>\n";
        $media_attachment_db_class = new MediaAttachmentDB();
        $parent_id = $_POST['media_attachment_id'];
        $med_thumb_id = $media_attachment_db_class->getThumbnailGivenParentIdAndType($parent_id, UPLOAD_TYPE_THUMBNAIL_MEDIUM);
        if ($med_thumb_id + 0 != 0) {
          $_POST['media_attachment_id'] = $med_thumb_id;
        }
        $small_thumb_id = $media_attachment_db_class->getThumbnailGivenParentIdAndType($parent_id, UPLOAD_TYPE_THUMBNAIL_SMALL);
        if ($small_thumb_id + 0 != 0) {
          $_POST['thumbnail_media_attachment_id'] = $small_thumb_id;
        }
      }
    }
    if ($read_more_set == 0) {
      $_POST['text'] .= "\n\n<br><br>\n<span class=\"strong\">\n<a href=\"$rel_link\">Read More</a>\n</span>";
    }

    $_POST['related_url'] = $rel_link;

    $saved_categories_for_article = $article_db_class->getNewsItemCategoryIds($news_item_id, 0);
    $_POST['category'] = [];
    if (is_array($saved_categories_for_article)) {
      $available_categories = $category_db_class->getCategoryInfoListByType(1, 0);
      foreach ($saved_categories_for_article as $cat) {
        if (!empty($available_categories[$cat])) {
          $_POST['category'][1] = $cat;
        }
      }
      $available_categories = $category_db_class->getCategoryInfoListByType(2, 44);
      foreach ($saved_categories_for_article as $cat) {
        if (!empty($available_categories[$cat])) {
          $_POST['category'][0] = $cat;
        }
      }
      $available_categories = $category_db_class->getCategoryInfoListByType(2, 0);
      foreach ($saved_categories_for_article as $cat) {
        if (!empty($available_categories[$cat])) {
          $_POST['category'][0] = $cat;
        }
      }
    }

  }

  /**
   * Replaces upload if changed.
   */
  public function replaceUploadIfChanged($post_array) {

    if (isset($_FILES['replace_upload']) && $_FILES['replace_upload']['name'] != '') {

      $upload_util_class = new UploadUtil();
      $media_attachement_db_class = new MediaAttachmentDB();
      $tmp_file_path = $_FILES['replace_upload']['tmp_name'];
      $mime_type_from_post = $_FILES['replace_upload']['type'];
      $original_file_name = $_FILES['replace_upload']['name'];

      $file_info = $upload_util_class->processAnonymousUpload($tmp_file_path, $original_file_name, $mime_type_from_post);

      $image_name = $original_file_name;

      $file_info['alt_tag'] = $post_array['summary'];
      if (mb_strlen($file_info['alt_tag']) > 140) {
        $file_info['alt_tag'] = mb_substr($file_info['alt_tag'], 0, 137) . '...';
      }

      $media_attachment_id = $media_attachement_db_class->addMediaAttachment($image_name,
                         $file_info['file_name'], $file_info['relative_path'], $file_info['alt_tag'], $file_info['original_file_name'], $file_info['media_type_id'], 0, UPLOAD_TYPE_POST, UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
      $associated_attachment_ids = $upload_util_class->makeSecondaryFilesAndAssociate($media_attachment_id, $file_info);

      $post_array['media_attachment_id'] = $associated_attachment_ids['media_attachment_id'];
      if (isset($associated_attachment_ids['thumbnail_media_attachment_id'])) {
        $post_array['thumbnail_media_attachment_id'] = $associated_attachment_ids['thumbnail_media_attachment_id'];
      }
      else {
        $post_array['thumbnail_media_attachment_id'] = 0;
      }
    }
    return $post_array;
  }

  /**
   * Rotates image.
   */
  public function rotateImage($post_array) {
    $upload_util_class = new UploadUtil();
    $media_attachement_db_class = new MediaAttachmentDB();
    $original_info = $media_attachement_db_class->getOriginalAttachmentInfoFromAttachmentId($post_array['media_attachment_id']);
    if (!is_array($original_info)) {
      $original_info = $media_attachement_db_class->getMediaAttachmentInfo($post_array['media_attachment_id']);
    }
    $upload_path = UPLOAD_PATH . '/';
    $date_rel_path = $original_info['relative_path'];
    $upload_dir = $upload_path . $date_rel_path;
    $existing_full_file_path = $upload_dir . $original_info['file_name'];
    $image_util_class = new ImageUtil();
    $rotated_file_path = $existing_full_file_path . '_rotated.jpg';

    @unlink($rotated_file_path);
    [$width_orig, $height_orig] = getimagesize($existing_full_file_path);
    $new_file_info = $image_util_class->scaleImageByWidthIfTooLarge($original_info['file_name'], $existing_full_file_path, $rotated_file_path, $height_orig, $width_orig, 0, $post_array['rotate']);

    if (is_array($new_file_info)) {

      $file_info = $upload_util_class->processAnonymousUpload($rotated_file_path, $original_info['file_name'], '', 1);

      // @fixme Set image name.
      $image_name = '';

      // @fixme Set alt tag.
      $alt_tag = '';

      $media_attachment_id = $media_attachement_db_class->addMediaAttachment($image_name,
                $file_info['file_name'], $file_info['relative_path'], $alt_tag, $file_info['original_file_name'], $file_info['media_type_id'], 0, UPLOAD_TYPE_POST, UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
      $associated_attachment_ids = $upload_util_class->makeSecondaryFilesAndAssociate($media_attachment_id, $file_info);

      $post_array['media_attachment_id'] = $associated_attachment_ids['media_attachment_id'];
      $post_array['thumbnail_media_attachment_id'] = $associated_attachment_ids['thumbnail_media_attachment_id'];
    }
    return $post_array;
  }

}
