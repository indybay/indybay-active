<?php

namespace Indybay\Pages\Admin\Breaking;

use Indybay\DB\NewsItemVersionDB;
use Indybay\Page;
use Indybay\Syndication\Microblog;
use Twilio\Rest\Client;

require_once 'syndication/microblog.inc';
/**
 * Allows admins to create breaking news.
 */
class BreakingCreate extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $tr = new Translate();
    $tr->createTranslateTable('breaking');
    $this->tkeys['create_result'] = '';
    $this->tkeys['parent_item_id'] = isset($_GET['parent_item_id']) ? intval($_GET['parent_item_id']) : 0;
    if (!empty($_POST['title1'])) {
      $posted_fields = [
        'created_by_id' => $_SESSION['session_user_id'],
        'displayed_author_name' => 'Indybay',
        'display_contact_info' => 0,
        'event_duration' => 0,
        'is_summary_html' => 1,
        'is_text_html' => 0,
        'media_attachment_id' => 0,
        'media_type_grouping_id' => 0,
        'news_item_status_id' => $_POST['display'] == 't' ? NEWS_ITEM_STATUS_ID_OTHER : NEWS_ITEM_STATUS_ID_HIDDEN,
        'related_url' => '',
        'summary' => $_POST['htm'],
        'text' => '',
        'thumbnail_media_attachment_id' => 0,
        'title1' => $_POST['title1'],
        'title2' => $_POST['title1'],
        'version_created_by_id' => $_SESSION['session_user_id'],
      ];
      $news_item_version_db_class = new NewsItemVersionDB();
      $news_item_id = $news_item_version_db_class->addNewsItem($_POST['display'] == 't' ? NEWS_ITEM_STATUS_ID_OTHER : NEWS_ITEM_STATUS_ID_HIDDEN, NEWS_ITEM_TYPE_ID_BREAKING, intval($_POST['parent_item_id']));
      $news_item_version_db_class->addNewsItemVersion($news_item_id, $posted_fields);
      $this->tkeys['create_result'] = 'Breaking news item #' . $news_item_id . ' created.';
      if ($_POST['send'] == 't') {
        $services = Microblog::update($_POST['title1']);
        foreach ($services as $key => $service) {
          if (empty($service['exception']) && !empty($service['status'])) {
            $this->tkeys['create_result'] .= '<br>Text message sent to <a href="' . $service['base'] . intval($service['status']->id) . '">' . $key . '</a>.';
          }
          else {
            $this->tkeys['create_result'] .= '<br>' . $key . ' status update failed. ';
            if (isset($service['exception']->message)) {
              $this->tkeys['create_result'] .= htmlspecialchars($service['exception']->type) . ': ' . htmlspecialchars($service['exception']->message);
            }
            else {
              $this->tkeys['create_result'] .= 'Service down or duplicate?';
            }
          }
        }
      }
      if ($_POST['display'] == 't') {
        $this->tkeys['create_result'] .= '<br>You now need to <a href="../feature_page/feature_page_preview.php?page_id=12"><strong>preview the front page and then push it live</strong></a>!';
      }
      if ($_POST['dispatch']) {
        $db_obj = new DB();
        $numbers = $db_obj->singleColumnQuery("SELECT phone FROM contact_info INNER JOIN user ON contact_info.contact_info_id = user.user_id WHERE user.has_login_rights = 1 AND contact_info.phone LIKE '+%'");
        try {
          require_once 'GoogleVoice.php';
          $gv = new GoogleVoice(GOOGLE_LOGIN, GOOGLE_PASS);
          foreach ($numbers as $number) {
            $gv->sendSms($number, $_POST['title1']);
          }
        }
        catch (Exception $e) {
          $this->tkeys['create_result'] .= print_r($e, TRUE);
        }

        try {
          $sid = TWILIO_SID;
          $token = TWILIO_TOKEN;
          $client = new Client($sid, $token);
          foreach ($numbers as $number) {
            $call = $client->account->calls->create(
              TWILIO_NUMBER,
              $number,
              'https://www.indybay.org/admin/breaking/dispatch.php',
              ['IfMachine' => 'Hangup', 'Timeout' => 15]
            );
            $query = "INSERT INTO dispatch (sid, news_item_id) VALUES ('" . $db_obj->prepareString($call->sid) . "', " . $news_item_id . ')';
            $db_obj->executeStatement($query);
          }
        }
        catch (Exception $e) {
          $this->tkeys['create_result'] .= '<p><code>Twilio error, check log.</code></p>';
        }
      }
    }
  }

}
