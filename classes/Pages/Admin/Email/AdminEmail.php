<?php

namespace Indybay\Pages\Admin\Email;

use Indybay\Cache\ArticleCache;
use Indybay\Cache\NewsItemCache;
use Indybay\DB\BlurbDB;
use Indybay\DB\EventListDB;
use Indybay\DB\FeaturePageDB;
use Indybay\Page;
use Indybay\Renderer\Renderer;
use Indybay\Translate;

/**
 * Class for admin_email page.
 */
class AdminEmail extends Page {

  /**
   * Class constructor, does nothing.
   */
  public function __construct() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Mostly we are just assigning template variables here, publishing
    // is done in publish class.
    $tr = new Translate();
    $this->tkeys['local_success'] = '';
    $this->tkeys['local_sendto_address'] = $GLOBALS['mailing_list_news'];
    $this->tkeys['local_nicedate'] = date('l, F j, Y');
    $this->tkeys['local_textarea'] = '';
    $this->tkeys['local_msubject'] = '';
    if (isset($_POST['generate'])) {
      $this->tkeys['local_msubject'] = 'Indybay News Update for ' . date('l, F jS, Y');
      $this->tkeys['local_textarea'] = $this->tkeys['local_msubject'];
      $this->tkeys['local_textarea'] .= "\nhttps://www.indybay.org\n\n\n";
      $this->tkeys['local_textarea'] .= "Publish an article:\n";
      $this->tkeys['local_textarea'] .= "https://www.indybay.org/publish.php\n\n";
      $this->tkeys['local_textarea'] .= "Announce an upcoming event:\n";
      $this->tkeys['local_textarea'] .= "https://www.indybay.org/calendar/event_add.php\n\n\n\n";
      $this->tkeys['local_textarea'] .= "IN THIS NEWS UPDATE\n\n";
      // Set to front page blurbs, was "0" all.
      $this->tkeys['local_textarea'] .= $this->generateRecentBlurbText(12);
      // $this->tkeys['local_textarea'] .= "\n";.
      $_POST['to'] = 'indybay-news@lists.riseup.net';
      $_POST['from'] = 'indybay@lists.riseup.net';
    }
    elseif (isset($_POST['generate_sc'])) {
      $this->tkeys['local_msubject'] = 'Santa Cruz Indymedia News Update for ' . date('l, F jS, Y');
      $this->tkeys['local_textarea'] = 'Santa Cruz Indymedia News Update for ' . date('l, F jS, Y');
      $this->tkeys['local_textarea'] .= "\nhttps://www.indybay.org/santa-cruz\n\n\n";
      $this->tkeys['local_textarea'] .= "IN THIS NEWS UPDATE\n\n";
      $this->tkeys['local_textarea'] .= $this->generateRecentBlurbText(60);
      // $this->tkeys['local_textarea'] .= "\n";.
      $_POST['to'] = 'scimc-news@lists.riseup.net';
      // $_POST['from'] = 'scimc@indymedia.org';.
      $_POST['from'] = 'scimc@indymedia.org';
    }
    $to_options = [];
    $to_options['indybay-news@lists.riseup.net'] = 'indybay-news@lists.riseup.net';
    $to_options['indybay@lists.riseup.net'] = 'indybay@lists.riseup.net';
    $to_options['scimc-news@lists.riseup.net'] = 'scimc-news@lists.riseup.net';
    $to_options['santa-cruz-progressive-email-list@googlegroups.com'] = 'santa-cruz-progressive-email-list@googlegroups.com';
    $to_options['bradley@riseup.net'] = 'Santa Cruz Developer (bradley@riseup.net)';
    $to_options['daveid@riseup.net'] = 'Indybay Developer (daveid@riseup.net)';
    $renderer = new Renderer();
    if (!isset($_POST['to'])) {
      $_POST['to'] = '';
    }
    $this->tkeys['local_to_dropdown'] = $renderer->makeSelectForm('to', $to_options, $_POST['to']);
    $from_options = [];
    $from_options['indybay@lists.riseup.net'] = 'indybay@lists.riseup.net';
    // $from_options['scimc@indymedia.org'] = 'scimc@indymedia.org';.
    $from_options['scimc@indymedia.org'] = 'scimc@indymedia.org';
    if (!isset($_POST['from'])) {
      $_POST['from'] = '';
    }
    $this->tkeys['local_from_dropdown'] = $renderer->makeSelectForm('from', $from_options, $_POST['from']);
    if (isset($_POST['Submit'])) {
      if (isset($_POST['failsafe']) && $_POST['failsafe'] != '') {
        $msubject = $_POST['msubject'];
        $mbody = $_POST['mbody'];
        $this->tkeys['local_msubject'] = $_POST['msubject'];
        $this->tkeys['local_textarea'] = $_POST['mbody'];
        $this->tkeys['local_success'] = $tr->trans('your_mail_not_sent');
        mail($_POST[to], $msubject, $mbody, 'From: ' . $_POST[from]);
        $this->tkeys['local_success'] = $tr->trans('your_mail_sent') . ' ' . $_POST[to];
      }
      else {
        $this->tkeys['local_msubject'] = $_POST['msubject'];
        $this->tkeys['local_textarea'] = $_POST['mbody'];
        $this->tkeys['local_success'] = $tr->trans('your_mail_not_sent');
      }
    }
  }

  /**
   * Generates recent blurb text.
   */
  public function generateRecentBlurbText($page_id) {
    $feature_page_db_class = new FeaturePageDB();
    $summary = "* Upcoming Events\n";
    $upcomevent = "UPCOMING EVENTS\n\n";
    $blurb_db_class = new BlurbDB();
    $article_cache_class = new ArticleCache();
    $results = $feature_page_db_class->getRecentBlurbVersionIds($page_id);
    $previous_titles = [];
    $ret = '';
    array_splice($results, 10);
    $count = count($results);
    $i = 0;
    foreach ($results as $version_id) {
      $i++;
      $blurb_info = $blurb_db_class->getBlurbInfoFromVersionId($version_id);
      $title1 = $blurb_info['title1'];
      $title2 = $blurb_info['title2'];
      if (trim($title1) == '' || trim($title2) == '') {
        continue;
      }
      if (!isset($previous_titles[$title2])) {
        $pages = $feature_page_db_class->getPagesWithPushedAutoVersionsOfBlurb($blurb_info['news_item_id']);
        if (isset($pages) && is_array($pages) && count($pages) > 0 && !(count($pages) == 1 && isset($pages[1]['page_id']) && $pages[1]['page_id'] == FRONT_PAGE_CATEGORY_ID)) {
          $previous_titles[$title2] = 1;
          $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($blurb_info['news_item_id'], $blurb_info['creation_timestamp']);
          $row = '';
          $row .= $blurb_info['title1'];
          $row .= "\n";
          $row .= "\n";
          if (strlen($row) < 700) {
            $row .= str_replace("\r", ' ', str_replace("\n", ' ', strip_tags($blurb_info['summary'])));
          }
          else {
            $blurb_info['summary'] = substr(strip_tags($blurb_info['summary']), 0, 700);
            $j = strrpos(' ', $blurb_info['summary']);
            if ($j > 0) {
              $blurb_info['summary'] = substr($blurb_info['summary'], 0, $j);
            }
            $row .= str_replace("\r", ' ', str_replace("\n", ' ', $blurb_info['summary'])) . '...';
          }
          $row .= "\n\nRead More\n";
          $row .= 'https://www.indybay.org' . $searchlink;
          $blurb_divider = "\n\n--\n\n";
          if ($i == $count) {
            $row .= "\n";
          }
          else {
            $row .= $blurb_divider;
          }
          $summary .= '* ' . $blurb_info['title1'] . "\n";
          $ret .= $row;
        }
      }
    }
    $event_list_db = new EventListDB();
    if ($page_id == 12) {
      $events = $event_list_db->getHighlightedListHelper(200, time() + (60 * 60 * 24) * 8);
    }
    else {
      $catlist = [];
      $catlist[0] = $page_id;
      $events = $event_list_db->getHighlightedListForCategoriesHelper($catlist, 200, time() + (60 * 60 * 24) * 30);
    }
    $news_item_cache_class = new NewsItemCache();
    $upcomevent .= '';
    foreach ($events as $event) {
      $upcomevent .= date('F jS, g:i A', $event['displayed_timestamp']) . ': ' . $event['title1'];
      $upcomevent .= "\n";
      $upcomevent .= 'https://www.indybay.org' . $news_item_cache_class->getWebCachePathForNewsItemGivenDate($event['news_item_id'], $event['creation_timestamp']);
      $upcomevent .= "\n\n";
    }
    if ($page_id == 60) {
      $ret .= "\n-------------------

Publish an article: https://www.indybay.org/publish.php?page_id=60

Announce an upcoming event: https://www.indybay.org/calendar/event_add.php?page_id=60

-------------------

Santa Cruz Independent Media Center is a non-commercial, democratic collective of Monterey Bay Area independent media makers and media outlets, and serves as the local organizing unit of the global Indymedia network.";
    }
    else {
      $ret .= "\n-------------------

The San Francisco Bay Area Independent Media Center is a non-commercial, democratic collective of Bay Area independent media makers and media outlets, and serves as the local organizing unit of the global Indymedia network.";
    }
    $ret2 = $summary . "\n\n\n" . $upcomevent;
    if ($page_id == 12) {
      $ret2 .= "More Events On Indybay's Calendar:\nhttps://www.indybay.org/calendar/\n\n\n\n";
    }
    elseif ($page_id == 60) {
      $ret2 .= "More Events On Santa Cruz Indymedia's Calendar:\nhttps://www.indybay.org/calendar/?page_id=60\n\n\n\n";
    }
    else {
      $ret2 .= "More Events: https://www.indybay.org/calendar/?page_id=$page_id\n\n\n\n";
    }
    $ret2 .= "LATEST FEATURES\n\n" . $ret;
    return $ret2;
  }

}
