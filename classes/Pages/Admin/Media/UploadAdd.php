<?php

namespace Indybay\Pages\Admin\Media;

use Indybay\MediaAndFileUtil\UploadUtil;
use Indybay\Page;
use Indybay\Renderer\MediaAttachmentRenderer;

/**
 * Class for upload_display_add page.
 */
class UploadAdd extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $media_attachment_renderer_class = new MediaAttachmentRenderer();
    $upload_util_class = new UploadUtil();
    $this->tkeys['local_media_attachment_info'] = '';
    $this->tkeys['local_update'] = '';
    $this->tkeys['local_uplimg'] = '';

    if (isset($_FILES['upload_file']['name'])) {
      $temp_file_name = $_FILES['upload_file']['tmp_name'];
      $original_file_name = $_FILES['upload_file']['name'];
      $mime_type = $_FILES['upload_file']['type'];
      $restrict_to_width = $_POST['max_width'] + 0;
      if (!empty($_POST['restrict_dimensions'])) {
        $restrict_to_height = (90 * $_POST['max_width'] / 135);
      }
      else {
        $restrict_to_height = 0;
      }
      $media_attachment_id = $upload_util_class->processAdminMediaUpload(
       $temp_file_name,
       $original_file_name,
       $mime_type,
       $restrict_to_width, $restrict_to_height, '', '');
      if ($media_attachment_id > 0) {
        $this->redirect('upload_edit.php?media_attachment_id=' . $media_attachment_id);
      }
      else {
        echo 'An error occured';
      }

    }

    $this->tkeys['local_select_max_size'] =
    $media_attachment_renderer_class->getRestrictImageSizeSelect('max_width');

    return 1;
  }

}
