<?php

namespace Indybay\Pages\Admin\Media;

use Indybay\DB\MediaAttachmentDB;
use Indybay\MediaAndFileUtil\UploadUtil;
use Indybay\Page;
use Indybay\Renderer\MediaAttachmentRenderer;

/**
 * Class for upload_display_add page.
 */
class UploadEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $related_links = '';
    $previous_links = '';
    $media_attachment_db_class = new MediaAttachmentDB();
    $media_attachment_renderer_class = new MediaAttachmentRenderer();

    $media_attachment_id = (int) $_GET['media_attachment_id'] ?? 0;

    if (isset($_POST['alt_tag'])) {
      $media_attachment_db_class->updateAltTag($media_attachment_id, $_POST['alt_tag']);
    }

    $image_info = $media_attachment_db_class
      ->getMediaAttachmentInfo($media_attachment_id);

    if (!$image_info) {
      echo 'Couldnt Find Upload With Id ' . $media_attachment_id;
      exit;
    }

    if (!empty($_POST['delete'])) {
      $upload_util = new UploadUtil();
      $upload_util->deleteUpload($media_attachment_id);
      $this->redirect('upload_list.php');
      return;
    }

    $this->tkeys['local_original_file_name'] = $image_info['original_file_name'];
    $this->tkeys['local_file_name'] = $image_info['file_name'];
    $this->tkeys['local_alt_tag'] = $image_info['alt_tag'];
    $this->tkeys['local_upload_type_name'] = $image_info['upload_type_name'];
    $this->tkeys['local_upload_name'] = $image_info['upload_name'];

    $rendered_attachment = $media_attachment_renderer_class->renderAttachment($image_info, 1);
    $this->tkeys['LOCAL_THUMBNAIL'] = $rendered_attachment;
    $this->tkeys['LOCAL_IMAGE_LOCATION'] = '<a href="/uploads/' . $image_info['relative_path'] . $image_info['file_name'] . '" title="view image by itself">' . UPLOAD_PATH . '/' . $image_info['relative_path'] . $image_info['file_name'] . '</a>';
    $this->tkeys['LOCAL_MEDIA_ATTACHMENT_ID'] = $media_attachment_id;
    if (isset($this->tkeys['local_upload_size'])) {
      $this->tkeys['local_upload_size'] = number_format($image_info['file_size']) . ' bytes ';
    }
    else {
      $this->tkeys['local_upload_size'] = number_format(filesize(UPLOAD_PATH . '/' . $image_info['relative_path'] . $image_info['file_name'])) . ' bytes ';
    }

    if ($image_info['image_width'] + 0 != 0) {
      $this->tkeys['local_upload_size'] .= '(' . $image_info['image_width'] . 'x' . $image_info['image_height'] . ')';
    }
    $all_related_news_item_ids = $media_attachment_db_class->getAllRelatedNewsItemIds($media_attachment_id);
    $current_related_news_item_ids = $media_attachment_db_class->getCurrentRelatedNewsItemIds($media_attachment_id);
    $current_non_hidden_related_news_item_ids = $media_attachment_db_class->getCurrentNonhiddenRelatedNewsItemIds($media_attachment_id);
    $current_hidden_related_news_item_ids = $media_attachment_db_class->getCurrentHiddenRelatedNewsItemIds($media_attachment_id);
    $previous_related_news_item_ids = array_diff_assoc($all_related_news_item_ids, $current_related_news_item_ids);
    $i = 0;
    $has_current_related_item_ids = 0;
    foreach ($current_non_hidden_related_news_item_ids as $next_id) {
      if ($i == 0) {
        $related_links .= '<span>This Attachment Is Currently Associated With The Following Non-Hidden NewsItems:</span><br>';
      }
      $has_current_related_item_ids = 1;
      $related_links .= '<a href="/admin/article/article_edit.php?id=';
      $related_links .= $next_id;
      $related_links .= '&amp;p=1#preview">' . $next_id;
      $related_links .= '</a><br>';
      $i = $i + 1;
    }
    $i = 0;
    foreach ($current_hidden_related_news_item_ids as $next_id) {
      if ($i == 0) {
        if ($has_current_related_item_ids == 1) {
          $related_links .= '<br>';
        }
        $related_links .= '<span>This Attachment Is Currently Associated With The Following Hidden NewsItems:</span><br>';
      }
      $has_current_related_item_ids = 1;
      $related_links .= '<a href="/admin/article/article_edit.php?id=';
      $related_links .= $next_id;
      $related_links .= '&amp;p=1#preview">' . $next_id;
      $related_links .= '</a><br>';
      $i = $i + 1;
    }

    $i = 0;
    foreach ($previous_related_news_item_ids as $next_id) {
      if ($i == 0) {
        $related_links .= '<br><span>This Attachment Is No Longer Associated With The Following NewsItems:</span><br>';
      }

      $previous_links .= '<a href="/admin/article/article_edit.php?id=';
      $previous_links .= $next_id;
      $previous_links .= '&amp;p=1#preview">' . $next_id;
      $previous_links .= '</a><br>';
      $i = $i + 1;
    }

    $this->tkeys['CURRENT_RELATED_NEWS_ITEM_LINKS'] = $related_links;
    $this->tkeys['PREVIOUS_RELATED_NEWS_ITEM_LINKS'] = $previous_links;

    $parent_image_info = $media_attachment_db_class->getParentAttachmentInfo($media_attachment_id);
    $child_image_info_list = $media_attachment_db_class->getChildAttachmentInfoList($media_attachment_id);

    if (is_array($parent_image_info)) {

      $parent_link = '<br><span>This image is a child image of <a href="';
      $parent_link .= '/admin/media/upload_edit.php?media_attachment_id=';
      $parent_link .= $parent_image_info['media_attachment_id'];
      $parent_link .= '">Image ID ' . $parent_image_info['media_attachment_id'] . '</a></span><br>';
      $this->tkeys['PARENT_IMAGE_LINK'] = $parent_link;
    }
    else {
      $this->tkeys['PARENT_IMAGE_LINK'] = '';
    }

    $child_image_links = '';
    if (is_array($child_image_info_list)) {
      $i = 0;
      foreach ($child_image_info_list as $child_image_info) {
        if ($i == 0) {
          $child_image_links .= '<br><span>The following images are associated with this image:</span><br>';
        }
        $child_image_links .= '<a href="/admin/media/upload_edit.php?media_attachment_id=';
        $child_image_links .= $child_image_info['media_attachment_id'];
        $child_image_links .= '">Image ID ' . $child_image_info['media_attachment_id'];
        $child_image_links .= ' (' . $child_image_info['type_name'] . ')';
        $child_image_links .= '</a><br>';
        $i = $i + 1;
      }
    }
    $this->tkeys['CHILD_IMAGE_LINKS'] = $child_image_links;
    if (!is_array($current_non_hidden_related_news_item_ids) || count($current_non_hidden_related_news_item_ids) == 0) {
      $this->tkeys['DELETE_LINK'] = ' | <form action="/admin/media/upload_edit.php?media_attachment_id=' . $media_attachment_id . '" method="post"><input type="submit" name="delete" value="Delete This Upload"></form>';
    }
    else {
      $this->tkeys['DELETE_LINK'] = '';
    }

    return 1;
  }

}
