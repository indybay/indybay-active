<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\DB\BlurbDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\NewsItemDB;
use Indybay\Renderer\BlurbRenderer;
use Indybay\Translate;

/**
 * Class for article_edit page.
 */
class BlurbRecategorize extends BlurbEdit {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $news_item_db_class = new NewsItemDB();
    $blurb_renderer_class = new BlurbRenderer();
    $tr = new Translate();

    $tr->createTranslateTable('article_edit');

    $this->tkeys['publish_result'] = '';

    $blurb_db_class = new BlurbDB();
    $blurb_renderer_class = new BlurbRenderer();
    if (isset($_GET['version_id'])) {
      $news_item_version_id = $_GET['version_id'];
      $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
      $news_item_id = $version_info[news_item_id];
    }
    if (isset($_GET['id'])) {
      $news_item_id = $_GET['id'];

      $news_item_version_id = $news_item_db_class->getCurrentVersionId($news_item_id);
      $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
    }

    if (isset($_GET['add_current'])) {
      $blurb_db_class->addBlurbToCategoriesAndPage($news_item_id, $_GET['add_page_id']);
    }

    if (isset($_GET['add_archived'])) {
      $news_item_db_class->addNewsItemCategory($news_item_id, $_GET['add_page_id']);
    }
    if (isset($_GET['remove_association'])) {
      $blurb_db_class->removeBlurbFromCategory($news_item_id, $_GET['remove_page_id']);
    }

    $this->tkeys['local_news_item_id'] = $version_info['news_item_id'];

    $this->setupAdminPageListsRetCurrentNum($news_item_id);

    // The common function above gets page info for display but
    // we need to get it again for the dropdowns
    // this could be in the common function but its something
    // likely only needed by this page.
    $page_info_list = $blurb_db_class->getCurrentPageInfoForBlurb($news_item_id);
    $already_on_pages = [];
    foreach ($page_info_list as $page_info) {
      $already_on_pages[$page_info['page_id']] = $page_info['long_display_name'];
    }
    $page_info_list = $blurb_db_class->getArchivedPageInfoForBlurb($news_item_id);
    foreach ($page_info_list as $page_info) {
      $already_on_pages[$page_info['page_id']] = $page_info['long_display_name'];
    }
    $page_info_list = $blurb_db_class->getHiddenPageInfoForBlurb($news_item_id);
    foreach ($page_info_list as $page_info) {
      $already_on_pages[$page_info['page_id']] = $page_info['long_display_name'];
    }
    $feature_page_db_class = new FeaturePageDB();
    $page_options = $feature_page_db_class->getAllPageSelectList();
    $page_options_not_already_selected = [];
    foreach ($page_options as $page_id => $page_name) {
      if (!isset($already_on_pages[$page_id])) {
        $page_options_not_already_selected[$page_id] = $page_name;
      }

    }
    $this->tkeys['local_select_nonassociated_pages'] = $blurb_renderer_class->makeSelectForm('add_page_id', $page_options_not_already_selected, 0);

    $this->tkeys['local_select_associated_pages'] = $blurb_renderer_class->makeSelectForm('remove_page_id', $already_on_pages, 0);

    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    return 1;
  }

}
