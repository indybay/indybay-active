<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\DB\BlurbDB;
use Indybay\DB\CategoryDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemDB;
use Indybay\MediaAndFileUtil\UploadUtil;
use Indybay\Page;
use Indybay\Renderer\BlurbRenderer;

/**
 * Class for publish page.
 */
class BlurbAdd extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $blurb_renderer_class = new BlurbRenderer();
    $category_db_class = new CategoryDB();
    $media_attachment_db_class = new MediaAttachmentDB();

    $category0 = isset($_POST['category'][0]) ? (int) $_POST['category'][0] : 0;
    $category1 = isset($_POST['category'][1]) ? (int) $_POST['category'][1] : 0;
    $category2 = isset($_POST['category'][2]) ? (int) $_POST['category'][2] : 0;

    if ($category0 + 0 == 0 && isset($_GET['preselect_page_id'])) {
      $category0 = (int) $_GET['preselect_page_id'] + 0;
    }

    if ($category1 + 0 == 0 && isset($_GET['preselect_page_id'])) {
      $category1 = (int) $_GET['preselect_page_id'] + 0;
    }

    if ($category2 + 0 == 0 && isset($_GET['preselect_page_id'])) {
      $category2 = (int) $_GET['preselect_page_id'] + 0;
    }

    $cat_topic_options = $category_db_class->getCategoryInfoListByType(2, 0);
    $cat_international_options = $category_db_class->getCategoryInfoListByType(2, 44);
    $cat_topic_options = $this->arrayMergeWithoutRenumbering($cat_topic_options, $cat_international_options);
    $cat_topic_options[0] = 'Please Select';
    if (empty($cat_topic_options[$category0])) {
      $category0 = 0;
    }
    $this->tkeys['cat_topic_select'] = $blurb_renderer_class->makeSelectForm('topic_select', $cat_topic_options, $category0);
    $cat_region_options = $category_db_class->getCategoryInfoListByType(1, 0);
    $cat_region_options[0] = 'Please Select';
    if (empty($cat_region_options[$category1])) {
      $category1 = 0;
    }
    $this->tkeys['cat_region_select'] = $blurb_renderer_class->makeSelectForm('region_select', $cat_region_options, $category1);
    $cat_other_options = $category_db_class->getCategoryInfoListByType(3, 0);
    $cat_other_options[0] = 'None Selected';
    if (empty($cat_other_options[$category2])) {
      $category2 = 0;
    }
    $this->tkeys['cat_other_select'] = $blurb_renderer_class->makeSelectForm('other_select', $cat_other_options, $category2);

    $form_vars = ['title1', 'title2', 'summary', 'related_url', 'text'];

    // Initialize template variables.
    foreach ($form_vars as $fv) {
      if (isset($_POST[$fv])) {
        $this->tkeys['local_' . $fv] = $_POST[$fv];
      }
      else {
        $this->tkeys['local_' . $fv] = '';
      }
    }

    if (isset($_POST['publish'])) {
      $this->tkeys['local_checkbox_is_text_html'] = $blurb_renderer_class->makeBooleanCheckboxForm('is_text_html', $_POST['is_text_html']);
      $this->tkeys['local_checkbox_is_summary_html'] = $blurb_renderer_class->makeBooleanCheckboxForm('is_summary_html', $_POST['is_summary_html']);
    }
    else {
      $this->tkeys['local_checkbox_is_text_html'] = $blurb_renderer_class->makeBooleanCheckboxForm('is_text_html', TRUE);
      $this->tkeys['local_checkbox_is_summary_html'] = $blurb_renderer_class->makeBooleanCheckboxForm('is_summary_html', TRUE);
      if ($this->tkeys['local_summary'] == '') {
        $this->tkeys['local_summary'] = "On <strong>[put date here]</strong>, ....\n";
        ;
      }
      if ($this->tkeys['local_text'] == '') {
        $this->tkeys['local_text'] = "<!-- This is just a template for a new long version of a blurb. You likely won't want or need all the stuff in this template, so delete what you don't need -->\n\n" .
                'On <strong>[put date here]</strong>, ....' .
                "\n\n<br><br>\nSecond paragraph..." .
                "\n\n<br><br>\n<img src=\"/im/imc_photo.svg\" alt=\"photo\"> \n<a href=\"[photo link]\">Photo post</a> | \n" .
                "<img src=\"/im/imc_video.svg\" alt=\"video\"> \n<a href=\"[video link]\">Video post</a> | \n" .
                "<img src=\"/im/imc_audio.svg\" alt=\"audio\"> \n<a href=\"[audio link]\">Audio post</a> | \n" .
                "<img src=\"/im/imc_pdf.svg\" alt=\"pdf\"> \n<a href=\"[pdf link]\">PDF post</a> | \n" .
                "<img src=\"/im/imc_article.svg\" alt=\"article\"> \n<a href=\"[article link]\">Article w/o Images</a> | \n" .
                "<img src=\"/im/imc_event.svg\" alt=\"event\"> \n<a href=\"[event link]\">Event Listing</a> | \n" .
                "<img src=\"/im/imc_external.svg\" alt=\"external\"> \n<a href=\"[external link]\">External Website</a>" .
                "\n\n<!-- optional link groupings below... -->" .
                "\n\n<br><br>\n<strong>See Also:</strong> \n" .
                "<img src=\"/im/imc_photo.svg\" alt=\"photo\"> \n<a href=\"[related link]\">related story title</a> | \n" .
                "<img src=\"/im/imc_video.svg\" alt=\"video\"> \n<a href=\"[related link 2]\">related story 2 title</a>" .
                "\n\n<br><br>\n<strong>Related Features:</strong> \n" .
                "<a href=\"[related feature]\">related feature title</a> | \n" .
                "<a href=\"[related feature 2]\">related feature 2 title</a>\n";
      }
    }
    if (isset($_POST['media_attachment_id'])) {
      $media_attachment_id = (int) $_POST['media_attachment_id'] + 0;
    }
    else {
      $media_attachment_id = '';
    }

    if (isset($_POST['media_attachment_select_id']) && (int) $_POST['media_attachment_select_id'] + 0 != 0) {
      // @fixme Figure out what media attachment select id is for.
      // $media_attachment_id = $media_attachment_select_id;.
    }

    if (isset($_POST['thumbnail_media_attachment_id'])) {
      $thumbnail_media_attachment_id = (int) $_POST['thumbnail_media_attachment_id'] + 0;
      $thumbnail_media_attachment_select_id = isset($_POST['thumbnail_media_attachment_select_id']) ? (int) $_POST['thumbnail_media_attachment_select_id'] : NULL;
      if ($thumbnail_media_attachment_select_id + 0 != 0) {
        $thumbnail_media_attachment_id = $thumbnail_media_attachment_select_id;
      }
    }
    else {
      $thumbnail_media_attachment_id = '';
    }

    if (isset($_POST['publish'])) {
      $posted_fields = $_POST;
      $changed_thumbnail_attachment_id = $this->processThumbnailUpload();
      if ($changed_thumbnail_attachment_id != 0) {
        $posted_fields['thumbnail_media_attachment_id'] = $changed_thumbnail_attachment_id;
      }
      else {
        $posted_fields['thumbnail_media_attachment_id'] = $thumbnail_media_attachment_id;
      }
      $changed_attachment_id = $this->processMainUpload();
      if ($changed_attachment_id != 0) {
        $posted_fields['media_attachment_id'] = $changed_attachment_id;
      }
      else {
        $posted_fields['media_attachment_id'] = $media_attachment_id;
      }
      if ($this->validatePost($posted_fields)) {
        $news_item_id = $this->addBlurbToDb($posted_fields);
        $this->processCategories($news_item_id, $posted_fields);
        $this->redirect('blurb_edit.php?id=' . $news_item_id);
      }
      else {
        // $this->tkeys['display_preview'] = $article->display_error_status();
      }

    }

    $recent_upload_options = $media_attachment_db_class->getRecentAdminUploadSelectList();
    $this->tkeys['local_recent_admin_uploads_select'] = $blurb_renderer_class->makeSelectForm('media_attachment_select_id', $recent_upload_options, $media_attachment_id, 'Select An Image');
    $this->tkeys['local_thumbnail_recent_admin_uploads_select'] = $blurb_renderer_class->makeSelectForm('thumbnail_media_attachment_select_id', $recent_upload_options, $thumbnail_media_attachment_id, 'Select An Image');

    $this->tkeys['local_media_attachment_id'] = $media_attachment_id;
    $this->tkeys['local_thumbnail_media_attachment_id'] = $thumbnail_media_attachment_id;
  }

  /**
   * Adds blurb to database.
   */
  public function addBlurbToDb($posted_fields) {
    $blurb_db_class = new BlurbDB();
    $news_item_id = $blurb_db_class->createNewBlurb($posted_fields);
    return $news_item_id;
  }

  /**
   * Validates post.
   */
  public function validatePost($posted_fields) {
    $ret = 1;
    if (!isset($posted_fields['title1']) || strlen(trim($posted_fields['title2'])) < 3) {
      $this->addValidationMessage('Title Is Required and It Must at Least Be 3 Characters Long');
      $ret = 0;
    }
    if (!isset($posted_fields['title2']) || strlen(trim($posted_fields['title2'])) < 3) {
      $this->addValidationMessage('SubTitle Is Required and It Must at Least Be 3 Characters Long');
      $ret = 0;
    }
    if (!isset($posted_fields['summary']) || strlen(trim($posted_fields['summary'])) < 3) {
      $this->addValidationMessage('Short Version Is Required');
      $ret = 0;
    }
    if (trim($posted_fields['media_attachment_id']) + 0 != 0) {
      $media_attachment_db_class = new MediaAttachmentDB();
      $media_atachment_info = $media_attachment_db_class->getMediaAttachmentInfo($posted_fields['media_attachment_id'] + 0);
      if (!is_array($media_atachment_info) || count($media_atachment_info) == 0) {
        $this->addValidationMessage('"' . $posted_fields['media_attachment_id'] . '" Is Not A Valid Image ID');
        $ret = 0;
      }
    }
    if (trim($posted_fields['thumbnail_media_attachment_id']) + 0 != 0) {
      $media_attachment_db_class = new MediaAttachmentDB();
      $media_atachment_info = $media_attachment_db_class->getMediaAttachmentInfo($posted_fields['thumbnail_media_attachment_id'] + 0);
      if (!is_array($media_atachment_info) || count($media_atachment_info) == 0) {
        $this->addValidationMessage('"' . $posted_fields['thumbnail_media_attachment_id'] . '" Is Not A Valid Image ID');
        $ret = 0;
      }
    }

    return $ret;
  }

  /**
   * Processes categories.
   */
  public function processCategories($news_item_id, $posted_fields) {
    $news_item_db_class = new NewsItemDB();
    $category_array = $posted_fields['category'];
    $posted_topic_category = $category_array[0];
    $posted_region_category = $category_array[1];
    $posted_other_category = $category_array[2];
    $blurb_db_class = new BlurbDB();

    if ($posted_topic_category > 0) {

      $news_item_db_class->addNewsItemCategory($news_item_id, $posted_topic_category);
      $blurb_db_class->addBlurbToPagesForGivenCategory($news_item_id, $posted_topic_category);

    }
    if ($posted_region_category > 0) {

      $news_item_db_class->addNewsItemCategory($news_item_id, $posted_region_category);
      $blurb_db_class->addBlurbToPagesForGivenCategory($news_item_id, $posted_region_category);

    }
    if ($posted_other_category > 0) {

      $news_item_db_class->addNewsItemCategory($news_item_id, $posted_other_category);
      $blurb_db_class->addBlurbToPagesForGivenCategory($news_item_id, $posted_other_category);
    }

  }

  /**
   * Processes thumbnail uploads.
   */
  public function processThumbnailUpload() {
    if (isset($_FILES['thumbnail_upload']) && $_FILES['thumbnail_upload']['name'] != '') {

      $upload_util_class = new UploadUtil();
      $temp_file_name = $_FILES['thumbnail_upload']['tmp_name'];
      $original_file_name = $_FILES['thumbnail_upload']['name'];
      if (isset($_FILES['thumbnail_upload']['mime_type'])) {
        $mime_type = $_FILES['thumbnail_upload']['mime_type'];
      }
      else {
        $mime_type = '';
      }
      // Disable to prevent auto-resizing to 480x320.
      $restrict_to_width = THUMBNAIL_WIDTH_MEDIUM;
      $restrict_to_height = THUMBNAIL_HEIGHT_MEDIUM;
      if (isset($_POST['title1'])) {
        $title = $_POST['title1'];
      }
      else {
        $title = '';
      }
      $new_thumbnail_media_attachment_id = $upload_util_class->processAdminMediaUpload(
       $temp_file_name,
       $original_file_name,
       $mime_type,
       $restrict_to_width, $restrict_to_height, '', $title
        );
    }
    else {
      $new_thumbnail_media_attachment_id = 0;
    }
    return $new_thumbnail_media_attachment_id;
  }

  /**
   * Processes main upload.
   */
  public function processMainUpload() {
    if (isset($_FILES['main_upload']) && $_FILES['main_upload']['name'] != '') {
      $upload_util_class = new UploadUtil();
      $temp_file_name = $_FILES['main_upload']['tmp_name'];
      $original_file_name = $_FILES['main_upload']['name'];
      $mime_type = '';
      if (isset($_FILES['main_upload']['mime_type'])) {
        $mime_type = $_FILES['main_upload']['mime_type'];
      }
      $restrict_to_width = THUMBNAIL_WIDTH_MEDIUM;
      $restrict_to_height = 0;
      if (isset($_POST['title1'])) {
        $title = $_POST['title1'];
      }
      else {
        $title = '';
      }
      $new_media_attachment_id = $upload_util_class->processAdminMediaUpload(
       $temp_file_name,
       $original_file_name,
       $mime_type,
       $restrict_to_width, $restrict_to_height, '', $title
        );
    }
    else {
      $new_media_attachment_id = 0;
    }
    return $new_media_attachment_id;
  }

}
