<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\Cache\ArticleCache;
use Indybay\DB\BlurbDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemDB;
use Indybay\DB\UserDB;
use Indybay\Renderer\BlurbRenderer;
use Indybay\Renderer\FeaturePageRenderer;
use Indybay\Renderer\UserRenderer;
use Indybay\Translate;

/**
 * Blurb edit class.
 */
class BlurbEdit extends BlurbAdd {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $tr = new Translate();
    $feature_page_renderer_class = new FeaturePageRenderer();
    $media_attachment_db_class = new MediaAttachmentDB();

    $this->tkeys['publish_result'] = '';

    $blurb_db_class = new BlurbDB();
    $blurb_cache_class = new ArticleCache();
    $blurb_renderer_class = new BlurbRenderer();

    if (isset($_POST['news_item_id']) && $_POST['news_item_id'] + 0 > 0) {
      $news_item_id = $_POST['news_item_id'];
      $news_item_version_id = $blurb_db_class->getCurrentVersionId($news_item_id);
      $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
    }
    if (isset($_GET['version_id']) && $_GET['version_id'] > 0) {
      $news_item_version_id = $_GET['version_id'];
      $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
      $news_item_id = $version_info['news_item_id'];
    }
    if (isset($_GET['id']) && $_GET['id'] > 0) {
      $news_item_id = $_GET['id'];

      $news_item_version_id = $blurb_db_class->getCurrentVersionId($news_item_id);
      $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
    }

    if (isset($_POST['media_attachment_id'])) {
      $media_attachment_id = $_POST['media_attachment_id'] + 0;
    }
    else {
      $media_attachment_id = 0;
    }
    if (isset($_POST['media_attachment_select_id'])) {
      $media_attachment_select_id = $_POST['media_attachment_select_id'] + 0;
    }
    else {
      $media_attachment_select_id = 0;
    }
    if ($media_attachment_select_id != 0) {
      $media_attachment_id = $media_attachment_select_id;
    }

    if (isset($_POST['thumbnail_media_attachment_id'])) {
      $thumbnail_media_attachment_id = $_POST['thumbnail_media_attachment_id'] + 0;
    }
    else {
      $thumbnail_media_attachment_id = 0;
    }

    if (isset($_POST['thumbnail_media_attachment_select_id'])) {
      $thumbnail_media_attachment_select_id = $_POST['thumbnail_media_attachment_select_id'];
    }
    else {
      $thumbnail_media_attachment_select_id = 0;
    }

    if ($thumbnail_media_attachment_select_id + 0 != 0) {
      $thumbnail_media_attachment_id = $thumbnail_media_attachment_select_id;
    }

    $num_cur_pages = $this->setupAdminPageListsRetCurrentNum($news_item_id);

    if (isset($_POST['editswitch']) > 0) {
      if ($this->validate($_POST) == 1) {
        $posted_fields = $_POST;
        $changed_thumbnail_attachment_id = $this->processThumbnailUpload();
        if ($changed_thumbnail_attachment_id != 0) {
          $posted_fields['thumbnail_media_attachment_id'] = $changed_thumbnail_attachment_id;
          $thumbnail_media_attachment_id = $changed_thumbnail_attachment_id;
        }
        else {
          $posted_fields['thumbnail_media_attachment_id'] = $thumbnail_media_attachment_id;
        }
        $changed_attachment_id = $this->processMainUpload();
        if ($changed_attachment_id != 0) {
          $posted_fields['media_attachment_id'] = $changed_attachment_id;
          $media_attachment_id = $changed_attachment_id;
        }
        else {
          $posted_fields['media_attachment_id'] = $media_attachment_id;
        }

        $version_info = $this->addNewVersion($posted_fields, $num_cur_pages);
        $news_item_id = $version_info['news_item_id'];

        $tr = new Translate();
        $this->addStatusMessage($tr->trans('blurb_edit_success') . '<br><br>');

      }
      else {
        $version_info['title1'] = $_POST['title1'];
        $version_info['title2'] = $_POST['title2'];
        $version_info['summary'] = $_POST['summary'];
        $version_info['text'] = $_POST['text'];
      }

    }

    $recent_upload_options = $media_attachment_db_class->getRecentAdminUploadSelectList();
    $this->tkeys['local_recent_admin_uploads_select'] = $blurb_renderer_class->makeSelectForm('media_attachment_select_id', $recent_upload_options, $media_attachment_id, 'Select An Image');
    $this->tkeys['local_thumbnail_recent_admin_uploads_select'] = $blurb_renderer_class->makeSelectForm('thumbnail_media_attachment_select_id', $recent_upload_options, $thumbnail_media_attachment_id, 'Select An Image');

    $this->tkeys['local_media_attachment_id'] = $media_attachment_id;
    $this->tkeys['local_thumbnail_media_attachment_id'] = $thumbnail_media_attachment_id;

    if (isset($_GET['add_current']) > 0) {
      $blurb_db_class->addBlurbToCategoriesAndPage($news_item_id, $_GET['add_page_id']);
    }

    if (isset($_GET['add_archived']) > 0) {
      $news_item_db_class = new NewsItemDB();
      $news_item_db_class->addNewsItemCategory($news_item_id, $_GET['add_page_id']);
    }

    $user_db_class = new UserDB();
    $user_renderer_class = new UserRenderer();
    $last_updated_by_user_id = $version_info['version_created_by_id'];
    $created_by_user_id = $version_info['created_by_id'];
    if (!isset($last_updated_by_user_id)) {
      $last_updated_by_user_id = 0;
    }
    if (!isset($created_by_user_id)) {
      $created_by_user_id = 0;
    }
    $user_info = $user_db_class->getUserInfo($last_updated_by_user_id);
    $rendered_modified_user_info = $user_renderer_class->renderUserDetailInfo($user_info);
    $user_info = $user_db_class->getUserInfo($created_by_user_id);
    $rendered_created_user_info = $user_renderer_class->renderUserDetailInfo($user_info);

    $this->tkeys['local_last_updated_by_user_info'] = $rendered_modified_user_info;
    $this->tkeys['local_created_by_user_info'] = $rendered_created_user_info;
    $this->tkeys['local_news_item_id'] = htmlspecialchars($version_info['news_item_id']);
    $this->tkeys['local_news_item_version_id'] = htmlspecialchars($version_info['news_item_version_id']);
    $this->tkeys['local_summary'] = htmlspecialchars($version_info['summary']);
    $this->tkeys['local_text'] = htmlspecialchars($version_info['text']);
    $this->tkeys['local_media_attachment_id'] = $version_info['media_attachment_id'];
    if ($version_info['media_attachment_id'] + 0 != 0) {
      $media_attachment_id = $version_info['media_attachment_id'];
      $thumbnail_edit_link = '(Last Saved ID Was ' . $media_attachment_id . ' -- <a href="/admin/media/upload_edit.php?media_attachment_id=' .
                $media_attachment_id . '">Edit Image</a>)';
      $this->tkeys['local_attachment_edit_link'] = $thumbnail_edit_link;
    }
    else {
      $this->tkeys['local_attachment_edit_link'] = '';
    }
    $this->tkeys['local_thumbnail_media_attachment_id'] = $version_info['thumbnail_media_attachment_id'];
    if ($version_info['thumbnail_media_attachment_id'] + 0 != 0) {
      $thumbnail_media_attachment_id = $version_info['thumbnail_media_attachment_id'];
      $thumbnail_edit_link = '(Last Saved ID Was ' . $thumbnail_media_attachment_id . ' -- <a href="/admin/media/upload_edit.php?media_attachment_id=' .
                $thumbnail_media_attachment_id . '">Edit Image</a>)';
      $this->tkeys['local_thumbnail_edit_link'] = $thumbnail_edit_link;
    }
    else {
      $this->tkeys['local_thumbnail_edit_link'] = '';
    }
    $this->tkeys['local_related_url'] = $version_info['related_url'];
    $this->tkeys['local_title1'] = htmlspecialchars($version_info['title1']);
    $this->tkeys['local_title2'] = htmlspecialchars($version_info['title2']);
    $this->tkeys['local_creation_date'] = $version_info['created'];
    $this->tkeys['local_version_creation_date'] = $version_info['modified'];
    $this->tkeys['local_id'] = $version_info['news_item_id'];

    $this->tkeys['local_checkbox_is_summary_html'] = $feature_page_renderer_class->makeBooleanCheckboxForm('is_summary_html', $version_info['is_summary_html']);
    $this->tkeys['local_checkbox_is_text_html'] = $feature_page_renderer_class->makeBooleanCheckboxForm('is_text_html', $version_info['is_text_html']);

    $this->tkeys['local_select_date_displayed_date'] = $feature_page_renderer_class->makeDateSelectForm(
        'displayed_date', 10, 1, $version_info['displayed_date_day'], $version_info['displayed_date_month'], $version_info['displayed_date_year']);

    $this->tkeys['local_previous_version_list'] =
            $blurb_renderer_class->renderBlurbVersionList(
      $blurb_db_class->getVersionListInfo($news_item_id, 5));
    if ($news_item_version_id + 0 > 0 && !isset($_POST['editswitch'])) {
      $blurb_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);
    }
    else {
      $blurb_info = $blurb_db_class->getBlurbInfoFromNewsItemId($news_item_id);
    }

    $template = $feature_page_renderer_class->determineSingleItemTemplate($blurb_info);
    $long_version_rendered_html = $blurb_renderer_class->getRenderedBlurbHtml($blurb_info, $template);
    $rel_link = $blurb_cache_class->getWebCachePathForNewsItemGivenDate($news_item_id, $version_info['creation_timestamp']);
    $this->tkeys['local_display_long_preview'] = $long_version_rendered_html;
    $this->tkeys['local_cached_link'] = ROOT_URL . $rel_link;

    $template = $feature_page_renderer_class->determineSampleShortTemplate($blurb_info);
    $short_version_rendered_html = $blurb_renderer_class->getRenderedBlurbHtml($blurb_info, $template);
    $this->tkeys['local_display_short_preview'] = $short_version_rendered_html;

    return 1;
  }

  /**
   * Validates blurb.
   */
  public function validate() {
    $ret = 1;
    if (trim($_POST['summary']) == '') {
      $this->addValidationMessage('You must have a summary');
      $ret = 0;
    }
    if (trim($_POST['title2']) == '') {
      $this->addValidationMessage('SubTitle Is Required.');
      $ret = 0;
    }
    return $ret;
  }

  /**
   * Adds new version.
   */
  public function addNewVersion($post_array, $num_cur_pages) {
    $blurb_db_class = new BlurbDB();
    $news_item_id = $post_array['news_item_id'];

    $news_item_version_id = $blurb_db_class->createNewVersion($news_item_id, $post_array);
    $this->cacheBlurb($news_item_id, $num_cur_pages);
    $version_info = $blurb_db_class->getBlurbInfoFromVersionId($news_item_version_id);

    return $version_info;

  }

  /**
   * Caches blurb.
   */
  public function cacheBlurb($news_item_id, $num_cur_pages) {
    $blurb_cache_class = new ArticleCache();
    // Recache only if not live on any page (since we then want to recache only
    // if the page is pushed live so we dont push live blurbs being worked on.
    if ($num_cur_pages == 0) {
      $rel_link = $blurb_cache_class->cacheEverythingForArticle($news_item_id);
      $this->tkeys['local_cached_link'] = ROOT_URL . $rel_link;
    }
    else {
      $this->tkeys['local_cached_link'] = '';
    }
  }

  /**
   * To be documented.
   */
  public function setupAdminPageListsRetCurrentNum($news_item_id) {
    $curnum = 0;
    $blurb_db_class = new BlurbDB();
    $tr = new Translate();

    $page_info_list = $blurb_db_class->getCurrentPageInfoForBlurb($news_item_id);
    $rendered_page_list = '';
    $ii = 1;
    $page_association_text = '';
    foreach ($page_info_list as $page_info) {
      if ($ii > 1) {
        $rendered_page_list .= ' | ';
      }
      $ii = $ii + 1;
      $rendered_page_list .= '<a href="/admin/feature_page/feature_page_blurb_list.php?page_id=' . $page_info['page_id'];
      $rendered_page_list .= '">' . $page_info['long_display_name'] . '</a>';
    }
    $curnum = $ii - 1;
    if (count($page_info_list) > 0) {
      $page_association_text .= $tr->trans('is_current_on_pages') . ':<br>';
      $page_association_text .= $rendered_page_list . '<br>';
    }
    $page_info_list = $blurb_db_class->getArchivedPageInfoForBlurb($news_item_id);
    $rendered_page_list = '';
    $ii = 1;
    foreach ($page_info_list as $page_info) {
      if ($ii > 1) {
        $rendered_page_list .= ' | ';
      }
      $ii = $ii + 1;
      $rendered_page_list .= '<a href="/admin/feature_page/feature_page_archived_blurb_list.php?page_id=' . $page_info['page_id'];
      $rendered_page_list .= '">' . $page_info['long_display_name'] . '</a>';
    }
    if (count($page_info_list) > 0) {
      $page_association_text .= $tr->trans('is_archived_on_pages') . ':<br>';
      $page_association_text .= $rendered_page_list . '<br>';
    }
    $page_info_list = $blurb_db_class->getHiddenPageInfoForBlurb($news_item_id);
    $rendered_page_list = '';
    $ii = 1;
    foreach ($page_info_list as $page_info) {
      if ($ii > 1) {
        $rendered_page_list .= ' | ';
      }
      $ii = $ii + 1;
      $rendered_page_list .= '<a href="/admin/feature_page/feature_page_hidden_blurb_list.php?page_id=' . $page_info['page_id'];
      $rendered_page_list .= '">' . $page_info['long_display_name'] . '</a>';
    }
    if (count($page_info_list) > 0) {
      $page_association_text .= $tr->trans('is_hidden_on_pages') . ':<br>';
      $page_association_text .= $rendered_page_list . '<br>';
    }
    $this->tkeys['local_page_associations'] = $page_association_text;
    return $curnum;
  }

}
