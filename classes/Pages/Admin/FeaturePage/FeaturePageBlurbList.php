<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\DB\BlurbDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\NewsItemDB;
use Indybay\Date;
use Indybay\Page;
use Indybay\Renderer\FeaturePageRenderer;
use Indybay\Translate;

/**
 * Feature page blurb list.
 */
class FeaturePageBlurbList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $feature_page_renderer_class = new FeaturePageRenderer();
    $blurb_db_class = new BlurbDB();
    if (isset($_GET['page_id'])) {
      $page_id = $_GET['page_id'];
    }
    else {
      $page_id = 0;
    }
    if ($page_id == 0 && isset($_POST['page_id'])) {
      $page_id = $_POST['page_id'];
    }
    $gridhtml = '';
    $tr = new Translate();

    if (isset($_GET['make_blurb_unhidden'])) {
      $news_item_id = $_GET['make_blurb_unhidden'] + 0;
      if ($news_item_id > 0) {
        $news_item_db_class = new NewsItemDB();
        $news_item_db_class->updateNewsItemStatusId($news_item_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      }
    }

    if (isset($_GET['make_blurb_current'])) {
      $news_item_id = $_GET['make_blurb_current'];
      if ($news_item_id > 0) {
        $blurb_db_class->addBlurbToPage($news_item_id, $page_id);

      }
    }

    if (isset($_POST['reorder']) > 0) {
      $i = 1;
      while (isset($_POST['news_item_id' . $i])) {
        $next_news_item_id = $_POST['news_item_id' . $i] + 0;
        $next_order_num = $_POST['blurb_order' . $next_news_item_id];
        $next_display_option_id = $_POST['next_display_option_id' . $next_news_item_id];
        $blurb_db_class->updateOrderEntry($page_id, $next_news_item_id, $next_order_num, $next_display_option_id);
        if ($_POST['display_' . $next_news_item_id] != 'display') {
        }{
        if ($_POST['display_' . $next_news_item_id] == 'archive') {
          $blurb_db_class->removeBlurbFromPage($next_news_item_id, $page_id);
        }
        elseif ($_POST['display_' . $next_news_item_id] == 'hide') {
          $blurb_db_class->removeBlurbFromPage($next_news_item_id, $page_id);
          $blurb_db_class->updateNewsItemStatusId($next_news_item_id, NEWS_ITEM_STATUS_ID_HIDDEN);
        }
        }
        $i = $i + 1;
      }
    }

    $this->tkeys['local_subtitle'] = $tr->trans('features_edit');

    $feature_page_db_class = new FeaturePageDB();
    $feature_page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $feature_page_long_name = $feature_page_info['long_display_name'];
    $this->tkeys['local_feature_page_name'] = $feature_page_long_name;
    $last_update_date_obj = new Date();
    $last_update_date_obj->setTimeFromSqlTime($feature_page_info['last_pushed_date']);
    $formatted_date = $last_update_date_obj->getFormattedDate() . ', ' . $last_update_date_obj->getFormattedTime();
    $this->tkeys['local_last_pushed_date'] = $formatted_date;

    $blurb_list = $feature_page_db_class->getCurrentBlurbListLimitedInfo($page_id);

    $this->tkeys['local_page_id'] = $page_id;
    $this->tkeys['local_current_link'] = $tr->trans('current');
    $this->tkeys['local_archived_link'] = "<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_archived_link'] .= $tr->trans('archived');
    $this->tkeys['local_archived_link'] .= '</a>';
    $this->tkeys['local_hidden_link'] = "<a href=\"feature_page_hidden_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_hidden_link'] .= $tr->trans('hidden');
    $this->tkeys['local_hidden_link'] .= '</a>';

    $display_options = [
      BLURB_DISPLAY_OPTION_AUTO => 'auto',
      BLURB_DISPLAY_OPTION_AUTO_LONG => 'long (auto)',
      BLURB_DISPLAY_OPTION_AUTO_SHORT => 'short (auto)',
      BLURB_DISPLAY_OPTION_NOIMAGE_LONG => 'long (no image)',
      BLURB_DISPLAY_OPTION_LEFT_LONG => 'long (left img)',
      BLURB_DISPLAY_OPTION_RIGHT_LONG => 'long (right img)',
      BLURB_DISPLAY_OPTION_NOIMAGE_SHORT => 'short (no image)',
      BLURB_DISPLAY_OPTION_LEFT_SHORT => 'short (left img)',
      BLURB_DISPLAY_OPTION_RIGHT_SHORT => 'short (right img)',
      BLURB_DISPLAY_OPTION_HTML_LONG => 'long (html only)',
      BLURB_DISPLAY_OPTION_HTML_SHORT => 'short (html only)',
    ];

    if (!is_array($blurb_list) || count($blurb_list) < 1) {
      // Colspan to 9 (i added the test column :)
      $gridhtml = '<div class="grid--row-full">';
      $gridhtml .= $tr->trans('no_features_to_edit');
      $gridhtml .= '</div>';

    }
    else {

      $i = 0;
      foreach ($blurb_list as $next_blurb) {
        $display_type =
                     $feature_page_renderer_class->makeSelectForm('next_display_option_id' . $next_blurb['news_item_id'],
        $display_options, $next_blurb['display_option_id']);
        ;

        $i = $i + 1;
        if (!is_int($i / 2)) {
          $addclass = ' bg-grey';
        }
        else {
          $addclass = '';
        }
        $next_blurb_date_obj = new Date();
        $next_blurb_date_obj->setTimeFromSqlTime($next_blurb['version_creation_date']);
        if ($next_blurb_date_obj->getUnixTime() > $last_update_date_obj->getUnixTime()) {
          $modifiedclass = 'modified';
        }
        else {
          $modifiedclass = '';
        }

        $gridhtml .= '<div class="attribute ' . $modifiedclass . $addclass . '" data-attr="Subtitle:">';
        $gridhtml .= '<a href="blurb_edit.php?id=';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '" class="' . $modifiedclass . '" title="Edit This Feature">';
        $gridhtml .= $next_blurb['title2'];
        $gridhtml .= '</a>';
        // Wanting to add live blurb link here.
        // $gridhtml .= '<br><span class="small">';
        // $gridhtml .= '<a href="#">view live blurb</a>';
        // $gridhtml .= '</span>';
        // end live blurb link.
        $gridhtml .= '</div>';
        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr="ID:">';
        $gridhtml .= '<input type="hidden" name="news_item_id' . $i;
        $gridhtml .= '" value="';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '">';
        $gridhtml .= $next_blurb['news_item_id'] . '</div>';
        $gridhtml .= '<div class="attribute' . $addclass . '" data-attr="Created:">';
        $gridhtml .= $next_blurb['created'];
        $gridhtml .= '</div><div class="attribute ' . $modifiedclass . $addclass . '" data-attr="Modified:">';
        $gridhtml .= $next_blurb['modified'];
        $gridhtml .= '</div><div class="attribute' . $addclass . '" data-attr="Order #:"><INPUT type="text" size="4" name="blurb_order';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '" value="';
        $gridhtml .= $next_blurb['order_num'];
        $gridhtml .= '"></div>';

        $gridhtml .= '<div class="attribute' . $addclass . '" data-attr="Template:">' . $display_type . '</div>';
        $gridhtml .= '<div class="attribute feature-display' . $addclass . '" data-attr="Status:">';
        $gridhtml .= '<select id="display_' . $next_blurb['news_item_id'] . '" name="display_' .
          $next_blurb['news_item_id'] . '">';
        $gridhtml .= '<option value="display">Display</option>';
        $gridhtml .= '<option value="archive">Archive</option>';
        $gridhtml .= '<option value="hide">Hide</option>';
        $gridhtml .= '</select><br>';
        $gridhtml .= '<span class="linklike admin-select-control" title="archive this blurb" data-select-id="display_' .
          $next_blurb['news_item_id'] . '" data-select-set="archive">Archive</span></div>';
        $gridhtml .= '';
        // $gridhtml .= "<div><a href=\"feature_page_archived_blurb_list.php?page_id=$page_id&remove_blurb_ordering=";
        // $gridhtml .= $next_blurb['news_item_id'];
        // $gridhtml .= "\">" . $tr->trans('action_archive') . "</a></div>";
        // $gridhtml .= "<div><a href=\"feature_page_hidden_blurb_list.php?page_id=$page_id&make_blurb_hidden=";
        // $gridhtml .= $next_blurb['news_item_id']."\"";
        // $gridhtml .= ">" . $tr->trans('action_hide') . "</a></div>";
      }
    }

    $this->tkeys['local_table_rows'] = $gridhtml;

    return 1;

  }

}
