<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\Cache\EventListCache;
use Indybay\Cache\FeaturePageCache;
use Indybay\DB\FeaturePageDB;
use Indybay\Page;
use Indybay\Renderer\FeaturePageRenderer;
use Indybay\Translate;

/**
 * Feature page preview.
 */
class FeaturePagePreview extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $feature_page_db_class = new FeaturePageDB();
    $feature_page_renderer_class = new FeaturePageRenderer();
    $page_id = $_GET['page_id'];
    $tr = new Translate();

    $page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $this->tkeys['local_feature_page_name'] = $page_info['long_display_name'];
    $this->tkeys['local_page_id'] = $page_id;
    $blurb_list = $feature_page_db_class->getCurrentBlurbList($page_id);
    $html_for_page = $feature_page_renderer_class->renderPage($blurb_list, $page_info);
    if (isset($_GET['push_page_live'])) {
      $feature_page_cache_class = new FeaturePageCache();
      $feature_page_cache_class->cacheCenterColumn($page_info, $html_for_page);
      $feature_page_cache_class->cacheAllBlurbsOnPage($page_id);
      $feature_page_db_class->updatePagePushedLiveDate($page_id);
      $tr = new Translate();
      $this->addStatusMessage($tr->trans('page_push_success') . '<br><br>');
    }
    if (strpos($html_for_page, 'INDYBAY_HIGHLIGHTED_EVENTS') > 0) {
      $event_list_cache_class = new EventListCache();
      $cached_events = $event_list_cache_class->loadCachedHighlightedEventsCacheForPage($page_id);
      $html_for_page = str_replace('INDYBAY_HIGHLIGHTED_EVENTS', $cached_events, $html_for_page);
    }
    $this->tkeys['local_preview_feature_page'] = $html_for_page;

    return 1;

  }

}
