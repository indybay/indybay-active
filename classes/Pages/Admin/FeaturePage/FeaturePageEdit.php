<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\Cache\FeaturePageCache;
use Indybay\DB\FeaturePageDB;
use Indybay\Page;
use Indybay\Renderer\FeaturePageRenderer;
use Indybay\Translate;

/**
 * Feature page edit class.
 */
class FeaturePageEdit extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $tr = new Translate();

    if (!empty($_REQUEST['page_id'])) {
      $is_edit = TRUE;
      $this->tkeys['subtitle'] = $tr->trans('feature_page_edit');
      $this->tkeys['local_form'] = 'feature_page_update.php';
    }
    else {
      $is_edit                   = FALSE;
      $this->tkeys['local_form'] = 'feature_page_add.php';
      $this->tkeys['subtitle']   = $tr->trans('feature_page_add');
    }

    $feature_page_db_class = new FeaturePageDB();
    $feature_page_renderer_class = new FeaturePageRenderer();

    if (isset($_POST['save'])) {
      $feature_page_db_class->updateFeaturePageInfo($_POST);
      $page_info = $feature_page_db_class->getFeaturePageInfo($_POST['page_id']);
      $feature_page_cache_class = new FeaturePageCache();
      $feature_page_cache_class->cacheNewswireTemplateForPage($page_info);
    }
    if ($is_edit == TRUE) {
      $feature_page = $feature_page_db_class->getFeaturePageInfo($_REQUEST['page_id']);

      $this->tkeys['local_short_display_name'] = $feature_page['short_display_name'];
      $this->tkeys['local_page_id'] = $feature_page['page_id'];
      $this->tkeys['local_long_display_name'] = $feature_page['long_display_name'];
      $this->tkeys['local_items_per_newswire_section'] = $feature_page['items_per_newswire_section'];
      $this->tkeys['local_relative_path'] = $feature_page['relative_path'];
      $this->tkeys['local_include_in_readmore_links'] = '';
      if ($feature_page['include_in_readmore_links'] == 1) {
        $this->tkeys['local_include_in_readmore_links'] = ' checked ';
      }
      $this->tkeys['LOCAL_MAX_EVENTS_IN_HIGHLIGHTED_LIST'] = $feature_page['max_events_in_highlighted_list'];
      $this->tkeys['LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_HIGHLIGHTED_EVENT_LIST'] = $feature_page['max_timespan_in_days_for_highlighted_event_list'];
      $this->tkeys['LOCAL_MAX_EVENTS_IN_FULL_EVENT_LIST'] = $feature_page['max_events_in_full_event_list'];
      $this->tkeys['LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_FULL_EVENT_LIST'] = $feature_page['max_timespan_in_days_for_full_event_list'];

    }

    $center_column_select_list = $feature_page_renderer_class->makeSelectForm(
                'center_column_type_id',
                $feature_page_db_class->getCenterColumnTypeList(),
                $feature_page['center_column_type_id']);

    $news_select_list = $feature_page_renderer_class->makeSelectForm('newswire_type_id',
    $feature_page_db_class->getNewswireTypeList(),
    $feature_page['newswire_type_id']);

    $eventlist_select_list = $feature_page_renderer_class->makeSelectForm('event_list_type_id',
    $feature_page_db_class->getEventListTypeList(),
    $feature_page['event_list_type_id']);

    $this->tkeys['local_select_newswire'] = $news_select_list;
    $this->tkeys['local_select_centercolumn'] = $center_column_select_list;
    $this->tkeys['local_select_eventlist'] = $eventlist_select_list;

    return 1;
  }

}
