<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\DB\BlurbDB;
use Indybay\DB\FeaturePageDB;
use Indybay\Page;
use Indybay\Translate;

/**
 * Class for feature_display_list page.
 */
class FeaturePageHiddenBlurbList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    if (isset($_GET['page_id'])) {
      $page_id = $_GET['page_id'];
    }
    else {
      $page_id = 0;
    }

    if (isset($_GET['page_number'])) {
      $page_number = $_GET['page_number'] + 0;
    }
    else {
      $page_number = 0;
    }

    $gridhtml = '';
    $tr = new Translate();
    if (isset($_GET['make_blurb_hidden'])) {
      $news_item_id = $_GET['make_blurb_hidden'];
      if ($news_item_id > 0) {
        $blurb_db_class = new BlurbDB();
        $blurb_db_class->updateNewsItemStatusId($news_item_id, NEWS_ITEM_STATUS_ID_HIDDEN);
        $blurb_db_class->removeBlurbFromPage($news_item_id, $page_id);
      }
    }

    $this->tkeys['local_subtitle'] = $tr->trans('features_edit');

    $feature_page_db_class = new FeaturePageDB();
    $feature_page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $feature_page_long_name = $feature_page_info['long_display_name'];
    $this->tkeys['local_feature_page_name'] = $feature_page_long_name;

    $page_size = 20;
    $start_limit = ($page_number) * $page_size;
    $blurb_list = $feature_page_db_class->getHiddenBlurbListLimitedInfo($page_id, $start_limit, $page_size + 1);

    $this->tkeys['local_current_link'] = "<a href=\"feature_page_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_current_link'] .= $tr->trans('current');
    $this->tkeys['local_current_link'] .= '</a>';

    $this->tkeys['local_archived_link'] = "<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_archived_link'] .= $tr->trans('archived');
    $this->tkeys['local_archived_link'] .= '</a>';
    $this->tkeys['local_hidden_link'] = $tr->trans('hidden');

    if (!is_array($blurb_list) || count($blurb_list) < 1) {
      $gridhtml = '<div class="grid--row-full">';
      $gridhtml .= $tr->trans('no_features_to_edit');
      $gridhtml .= '</div>';
      $this->tkeys['nav'] = '';

    }
    else {
      $i = 0;

      $this->tkeys['nav'] = '<span class="pagination featurepagehiddenblurblist-pagination">';
      if ($page_number > 0) {
        $this->tkeys['nav'] .= '<a href="/admin/feature_page/feature_page_archived_blurb_list.php?page_id=' .
                $page_id . '&amp;page_number=' . ($page_number - 1) .
                '"><img src="/im/arrow-back.svg" alt="back" title="back page" class="mediaicon pageicon" width="12" height="12"></a>';

      }
      $this->tkeys['nav'] .= $page_number + 1;
      if (count($blurb_list) > $page_size) {
        $this->tkeys['nav'] .= '<a href="/admin/feature_page/feature_page_archived_blurb_list.php?page_id=' .
                    $page_id . '&amp;page_number=' . ($page_number + 1) .
                    '"><img src="/im/arrow-next.svg" alt="next" title="next page" class="mediaicon pageicon" width="12" height="12"></a>';
      }
      $this->tkeys['nav'] .= '</span>';

      $blurb_list = array_reverse($blurb_list);
      while ($next_blurb = array_pop($blurb_list)) {
        $i = $i + 1;
        if (!is_int($i / 2)) {
          $addclass = ' bg-grey';
        }
        else {
          $addclass = '';
        }

        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr="Subtitle:"><a href="blurb_edit.php?id=';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '">';
        $gridhtml .= $next_blurb['title2'];
        $gridhtml .= '</a></div>';
        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr="ID:">';
        $gridhtml .= '<a href="blurb_edit.php?id=';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '">';
        $gridhtml .= $next_blurb['news_item_id'] . '</a></div>';
        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr="Created:">';
        $gridhtml .= $next_blurb['created'];
        $gridhtml .= '</div><div class="attribute ' . $addclass . '" data-attr="Modified:">';
        $gridhtml .= $next_blurb['modified'];
        $gridhtml .= '</div>';
        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr=" ">';
        $gridhtml .= "<a href=\"feature_page_blurb_list.php?page_id=$page_id&amp;make_blurb_current=";
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '&amp;make_blurb_unhidden=';
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '">' . $tr->trans('action_display') . '</a></div>';
        $gridhtml .= '<div class="attribute ' . $addclass . '" data-attr=" ">';
        $gridhtml .= "<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id&amp;make_blurb_unhidden=";
        $gridhtml .= $next_blurb['news_item_id'];
        $gridhtml .= '">' . $tr->trans('action_archive') . '</a></div>';
        if ($i == $page_size) {
          break;
        }
      }

    }

    $this->tkeys['local_table_rows'] = $gridhtml;

    return 1;

  }

}
