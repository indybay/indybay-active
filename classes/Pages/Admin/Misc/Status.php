<?php

namespace Indybay\Pages\Admin\Misc;

use Indybay\Page;

/**
 * Class for admin_index page.
 */
class Status extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $this->tkeys['start'] = time() - 604800;
    $this->tkeys['end'] = time();
  }

}
