<?php

namespace Indybay\Pages\Admin\Misc;

use Indybay\Page;

/**
 * Class for Lumen submission page.
 */
class Lumen extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $this->tkeys['lumen_public_key'] = LUMEN_PUBLIC_KEY;
    $this->tkeys['lumen_script_src'] = LUMEN_SCRIPT_SRC;
  }

}
