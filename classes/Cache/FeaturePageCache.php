<?php

namespace Indybay\Cache;

use Indybay\DB\FeaturePageDB;
use Indybay\DB\NewsItemDB;
use Indybay\MediaAndFileUtil\FileUtil;
use Indybay\Renderer\FeaturePageRenderer;

/**
 * Caches center columns for pages.
 *
 * As well as data used in rendering of left column and headers for pages.
 *
 * Written January 2006.
 * Modification Log:
 * 12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class FeaturePageCache extends Cache {

  /**
   * Caches newswire template and regenerates the newswire caches.
   */
  public function cacheNewswireTemplateForPage($page_info) {

    $newswire_type_id = $page_info['newswire_type_id'];
    if ($newswire_type_id == NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_CAT ||
     $newswire_type_id == NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_NOCAT) {
      $template_name = 'newswire/newswire_lgo.tpl';
    }
    elseif ($newswire_type_id == NEWSWIRE_TYPE_ALL_CAT) {
      $template_name = 'newswire/newswire_all.tpl';
    }
    elseif ($newswire_type_id == NEWSWIRE_TYPE_CLASSIFIED_CAT) {
      $template_name = 'newswire/newswire_classified.tpl';
    }
    if (isset($template_name)) {
      $temp_array = ['TPL_LOCAL_PAGE_ID' => $page_info['page_id']];
      $result_html = $this->render($template_name, $temp_array);
      $file_path = CACHE_PATH . '/newswires/newswire_page' . $page_info['page_id'];
      $cache_class = new Cache();
      $cache_class->cacheFile($file_path, $result_html);
    }
    $newswire_cache_class = new NewswireCache();
    $newswire_cache_class->forceRegenerateNewswireForPage($page_info['page_id']);

  }

  /**
   * Given HTML for center column caches it for use in includes.
   */
  public function cacheCenterColumn($page_info, $html_for_center_column) {

    $file_path = CACHE_PATH . '/feature_page/center_columns/center_column_page' . $page_info['page_id'] . '.html';
    $cache_class = new Cache();
    $cache_class->cacheFile($file_path, $html_for_center_column);
  }

  /**
   * Cache or recache all blurbs on a page.
   */
  public function cacheAllBlurbsOnPage($page_id) {

    $feature_page_db_class = new FeaturePageDB();
    $article_cache_class = new ArticleCache();
    $current_blurbs = $feature_page_db_class->getCurrentBlurbList($page_id);
    $news_item_db = new NewsItemDB();
    foreach ($current_blurbs as $blurb_info) {
      $syndicate = FALSE;
      if ($page_id == FRONT_PAGE_CATEGORY_ID) {
        if (!$blurb_info['syndicated']) {

          // [news_item_version_id] => 18791015
          // [news_item_id] => 18701483
          // [title1] => Vacant Bank Occupied in Santa Cruz
          // [title2] => 75 River Street Occupied as Community Center for 75+
          // [displayed_author_name] =>
          // [email] =>
          // [phone] =>
          // [address] =>
          // [displayed_date] => 2011-12-4 9:59:00
          // [event_duration] => 0
          // [summary] => On November 30th, more than a hundred activists in
          // [text] => On November 30th, more than a hundred activists in Santa
          // [is_summary_html] => 1
          // [is_text_html] => 1
          // [related_url] => /newsitems/2011/12/01/18701406.php
          // [display_contact_info] => 0
          // [media_attachment_id] => 18639615
          // [thumbnail_media_attachment_id] => 18639614
          // [version_creation_date] => 2011-12-04 09:59:29
          // [version_created_by_id] => 69
          // [modified] => 2011-12-04 09:59 AM
          // [displayed_date_year] => 2011
          // [displayed_date_month] => 12
          // [displayed_date_day] => 04
          // [displayed_date_hour] => 09
          // [displayed_date_minute] => 59
          // [displayed_timestamp] => 1323021540
          // [modified_timestamp] => 1323021569
          // [current_version_id] => 18791015
          // [news_item_type_id] => 6
          // [news_item_status_id] => 3
          // [parent_item_id] => 18701483
          // [creation_timestamp] => 1322740830
          // [created] => Thursday Dec 1st, 2011 4:00 AM
          // [created_by_id] => 69
          // require('HTTP/OAuth/Consumer.php');
          // $request = new HTTP_Request2('https://graph.facebook.com/' .
          // $request->addPostParameter('access_token',
          // $request->addPostParameter('uid', FACEBOOK_PAGE_ID);
          // $request->addPostParameter('message', 'Blurb text');
          // $request->addPostParameter('picture',
          // $request->addPostParameter('link', ...
          $syndicate = TRUE;
          $news_item_db->updateSyndicated($blurb_info['news_item_id']);
        }
      }
      $article_cache_class->cacheEverythingForArticle($blurb_info['news_item_id'], $syndicate);
      $news_item_db->updateNewsItemStatusId($blurb_info['news_item_id'], NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
    }

  }

  /**
   * Recaches a feature page given its id.
   */
  public function cacheFeaturePage($page_id) {

    $feature_page_renderer_class = new FeaturePageRenderer();
    $feature_page_db_class = new FeaturePageDB();
    $page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $blurb_list = $feature_page_db_class->getCurrentBlurbList($page_id);
    $html_for_page = $feature_page_renderer_class->renderPage($blurb_list, $page_info);
    $this->cacheCenterColumn($page_info, $html_for_page);
    $this->cacheAllBlurbsOnPage($page_id);
    $feature_page_db_class->updatePagePushedLiveDate($page_id);

  }

  /**
   * Loads the cache for a feature page given its id.
   */
  public function loadCacheForFeaturePage($page_id) {

    $file_util = new FileUtil();
    $center_column_file = CACHE_PATH . '/feature_page/center_columns/center_column_page' . $page_id . '.html';
    if (!file_exists($center_column_file)) {
      if (isset($page_id) &&  $page_id > 0) {
        $this->cacheFeaturePage($page_id);
      }
    }
    if (file_exists($center_column_file)) {

      $ret = $file_util->loadFileAsString($center_column_file);
      if (strpos($ret, 'INDYBAY_HIGHLIGHTED_EVENTS') > 0) {
        $event_list_cache_class = new EventListCache();
        $cached_events = $event_list_cache_class->loadCachedHighlightedEventsCacheForPage($page_id);
        $ret = str_replace('INDYBAY_HIGHLIGHTED_EVENTS', $cached_events, $ret);
      }
    }

    return $ret;

  }

  /**
   * Loads cached list of recent features for a given page.
   *
   * Intended for bottom of single blurb view and other such places.
   */
  public function loadCachedRecentFeatures($page_id) {

    $file_path = CACHE_PATH . '/feature_page/recent_blurbs/recent_blurbs_for_page_' . $page_id;
    $ret = $this->cacheOrCallBasedOffAge(new FeaturePageRenderer(), 'renderRecentFeatureList', $page_id, $file_path, 600);

    return $ret;
  }

}
