<?php

namespace Indybay\Cache;

/**
 * @file
 * Indybay Cache Class.
 *
 * Written through refactoring of an sfactive class (with little original code
 * left) December 2005.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Common;
use Indybay\MediaAndFileUtil\FileUtil;

/**
 * Main cache class.
 *
 * This file is uses for common caching functionality
 * this mainly includes getting directory names and making
 * sure safe_includes dont allow malicious content.
 */
class Cache extends Common {

  /**
   * This just calls the FileUtils save for now.
   *
   * But it could also do some security checks since only certain folders can be
   * used for caching.
   */
  public function cacheFile($file_path, $contents) {

    $file_util_class = new FileUtil();
    $success = $file_util_class->saveStringAsFile($file_path, $contents);

    return $success;
  }

  /**
   * Returns date portion of directory structure.
   *
   * As used when saving any cache file.
   */
  public function getRelDirFromDate($date) {

    $year = date('Y', $date);
    $month = date('m', $date);
    $day = date('d', $date);
    $date_partial_path = $year . '/' . $month . '/' . $day . '/';

    return $date_partial_path;
  }

  /**
   * Creates a relative directory structure if it doesnt yet exist.
   *
   * Returns the same result as get_rel_dir_from_date.
   */
  public function createDateBasedRelativeDirectory($partial_path, $date) {

    $year = date('Y', $date);
    $month = date('m', $date);
    $day = date('d', $date);
    $date_partial_path = $year . '/';
    $mkdir_return_code = 1;
    if (!file_exists($partial_path . $date_partial_path)) {
      $mkdir_return_code = mkdir($partial_path . $date_partial_path, 0777);
    }
    $date_partial_path .= $month . '/';
    if ($mkdir_return_code == 1 &&  !file_exists($partial_path . $date_partial_path)) {
      $mkdir_return_code = mkdir($partial_path . $date_partial_path, 0777);
    }
    $date_partial_path .= $day . '/';
    if ($mkdir_return_code == 1 && !file_exists($partial_path . $date_partial_path)) {
      $mkdir_return_code = mkdir($partial_path . $date_partial_path, 0777);
    }
    if ($mkdir_return_code == 1) {
      $ret = $date_partial_path;
    }
    else {
      $ret = 0;
    }

    return $ret;

  }

  /**
   * This is the include that shuld be around any user content that is cached.
   */
  public function safeInclude($file_path) {
    readfile($file_path);
  }

  /**
   * Should be around any admin user content that is cached.
   */
  public function safeAdminInclude($file_path) {
    readfile($file_path);
  }

  /**
   * Allows you to have method only called if cached file is too old.
   *
   * If the cached file isnt too old its contents are returned. The method
   * itself doesnt define the name of the cached file based off the paramters;
   * that must be done by the calling method and be part of the cache_file name.
   */
  public function cacheOrCallBasedOffAge($class, $method, $param, $cache_file, $max_age_in_seconds, &$file_m_time = NULL) {
    $html = '';
    $file_util = new FileUtil();

    if ($max_age_in_seconds == 0 || !file_exists($cache_file) || $file_util->getFileAgeInSeconds($cache_file) > $max_age_in_seconds) {
      touch($cache_file);
      $html = $class->$method($param);
      if ($html != '') {
        $file_util->saveStringAsFile($cache_file, $html);
      }
      $file_m_time = time();
    }
    if ($html == '') {
      $html = $file_util->loadFileAsString($cache_file);
    }
    if (!$file_m_time) {
      $file_m_time = filemtime($cache_file);
    }
    return $html;
  }

}
