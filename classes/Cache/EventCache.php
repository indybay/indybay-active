<?php

namespace Indybay\Cache;

use Indybay\MediaAndFileUtil\FileUtil;
use Indybay\Renderer\EventRenderer;

/**
 * Caches events, days and event weeks.
 *
 * Since events act a lot like articles most of the single event caching is
 * inherited from ArticleCache and this class mainly deals with caching days
 * and weeks.
 *
 * Written January 2006.
 * Modification Log:
 * 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class EventCache extends ArticleCache {

  /**
   * Caches the HTML for a week given a day in it.
   *
   * Loops through and recaches the days and
   * also cache the week wrapper around the days.
   */
  public function recacheWeekGivenDate($date) {

    $date->findStartOfWeek();
    $event_renderer = new EventRenderer();
    $html = $event_renderer->renderWeekHtml($date->getUnixTime(), 0, 0);

    $year = $date->getYear();
    $month = $date->getMonth();
    $day = $date->getDay();
    $cache_file_name = 'calendar_week_' . $year . '_' . $month . '_' . $day;

    $week_file_path = CACHE_PATH . '/calendar/week_cache/' . $cache_file_name;

    $this->cacheFile($week_file_path, $html);
  }

  /**
   * Returns the cache file for a week filtered by topic/region and status.
   */
  public function getCachedWeek($date, $topic_id, $region_id, $news_item_status_restriction) {

    $event_renderer = new EventRenderer();

    $year = $date->getYear();
    $month = $date->getMonth();
    $day = $date->getDay();

    $cache_file_name = 'calendar_week_' . $year . '_' . $month . '_' . $day;
    $week_file_path = CACHE_PATH . '/calendar/week_cache/' . $cache_file_name;
    if (!file_exists($week_file_path)) {
      $this->recacheWeekGivenDate($date);
    }

    if (file_exists($week_file_path)) {
      $file_util = new FileUtil();
      $html = $file_util->loadFileAsString($week_file_path);
      $html = $event_renderer->filterCachedHtml(
       $html, $topic_id, $region_id, $news_item_status_restriction);
    }

    return $html;
  }

}
