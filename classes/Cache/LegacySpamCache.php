<?php

namespace Indybay\Cache;

use Indybay\MediaAndFileUtil\FileUtil;

/**
 * Legacy blocking system.
 *
 * Largely replaced a year after it was written but
 * still used for some rare types of blocks not supported by newer code.
 * This system consists of lists of rules that get cached to a file that define
 * what should get blocked and what the result of blocking should be
 * the system was largely replaces since it was hard to use and wasnt easy to
 * integrate with external lists of spam ips.
 *
 * Written May 2006.
 * Modification Log:
 * 5/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class LegacySpamCache extends Cache {

  /**
   * Adds an entry from the older version of the spam blocking system.
   *
   * This code should eventually ger moved to a redirection section of the code
   * since it remains useful for redirecting based off url, browser type or
   * referring url.
   */
  public function addEntryToSpamCache(
    $ip,
    $ip_part1,
    $ip_part2,
    $ip_part3,
    $url,
    $referring_url,
    $http_method,
    $parent_news_item_id,
    $keyword,
    $destination,
    $note,
  ) {
    $str_row = $this->createSavableStringRow($ip, $ip_part1, $ip_part2, $ip_part3,
    $url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note);
    $this->appendSpamBlockRow($str_row);
  }

  /**
   * Removes an entry from the older version of the spam blocking system.
   *
   * This code should eventually ger moved to a redirection section of the code
   * since it remains useful for redirecting based off url, browser type or
   * referring url.
   */
  public function removeEntryFromSpamCache($id) {
    $str = $this->loadCachedSpamBlocksIntoString();
    $rows = explode("\r\n", $str);
    $array2 = [];
    foreach ($rows as $next_row) {
      $parsed_row = $this->parseCacheBlockRow($next_row);
      if ($parsed_row['id'] != $id) {
        array_push($array2, $next_row);
      }
    }
    $str2 = implode("\r\n", $array2);
    $this->saveSpamBlocksFromString($str2);
  }

  /**
   * Removes all entries from the older version of the spam blocking system.
   *
   * This code should eventually get moved to a redirection section of the code
   * since it remains useful for redirecting based off url, browser type or
   * referring url.
   */
  public function clearSpamCache() {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/spam_block.txt', '');
  }

  /**
   * This is the core of the older spam blockng system.
   *
   * That looks for specific redirection urls based off HTTP params.
   */
  public function getAlternateSpamBlockPage() {

    $redirect = '';

    $block_conditions = $this->listBlocks();
    foreach ($block_conditions as $block_condition) {
      if (!$this->compareCurrentRequestToBlockHelper($block_condition, FALSE)) {
        $redirect = $block_condition['destination'];
        break;
      }
    }
    return $redirect;
  }

  /**
   * Compares a block conditions against the current state of an HTTP request.
   *
   * Returns true if request should be blocked (and redirected) and false if it
   * shouldn't. The calling method looks for the redirect url in the block
   * conditions.
   */
  public function compareCurrentRequestToBlockHelper($block_condition, $isdbadd) {

    if (getenv('REMOTE_ADDR')) {
      $current_ip = getenv('REMOTE_ADDR');
    }
    elseif (getenv('HTTP_CLIENT_IP')) {
      $current_ip = getenv('HTTP_CLIENT_IP');
    }
    elseif (getenv('HTTP_X_FORWARDED_FOR')) {
      $current_ip = getenv('HTTP_X_FORWARDED_FOR');
    }
    else {
      $current_ip = 'UNKNOWN';
    }

    // Legacy code to block
    // Set local vars
    // get vars from block_condition_dict.
    $block_url = $block_condition['url'];
    $current_url = $_SERVER['SCRIPT_NAME'] . ($_SERVER['REDIRECT_URL'] ?? '');

    // So you cant block yourself from getting rid of a block.
    if (strpos(' ' . $current_url, '/admin/') > 0) {
      return TRUE;
    }

    if (!$isdbadd && $block_condition['http_method'] != '' && $block_condition['http_method'] != '0') {
      $request_method = $_SERVER['REQUEST_METHOD'] ?? '';
      if ($block_condition['http_method'] != $request_method) {
        return TRUE;
      }
    }

    $block_parent_id = $block_condition['parent_news_item_id'];
    if (!empty($_GET['top_id'])) {
      $current_parent_id = (int) $_GET['top_id'];
    }
    elseif (!empty($_POST['parent_item_id'])) {
      $current_parent_id = (int) $_POST['parent_item_id'];
    }
    else {
      $current_parent_id = 0;
    }

    $block_ip = $block_condition['ip'];
    $block_ip1 = '';
    if ($current_ip != 'UNKNOWN') {
      $ip_pieces = explode('.', $current_ip);
      $block_ip1 = trim($block_condition['ip_part1']);
      $current_ip1 = trim($ip_pieces[0]);
      $block_ip2 = trim($block_condition['ip_part2']);
      $current_ip2 = isset($ip_pieces[1]) ? trim($ip_pieces[1]) : '';
      $block_ip3 = trim($block_condition['ip_part3']);
      $current_ip3 = isset($ip_pieces[2]) ? trim($ip_pieces[2]) : '';
    }

    $block_ref_url = $block_condition['referring_url'];
    $current_ref_url = getenv('HTTP_REFERER') . ($_SERVER['REDIRECT_URL'] ?? '');

    $block_keyword = $block_condition['keyword'];
    $current_keywords = ($_SERVER['REDIRECT_URL'] ?? '') . ' ' . ($_SERVER['QUERY_STRING'] ?? '') . ' ' . ($_SERVER['HTTP_USER_AGENT'] ?? '');
    $current_keywords .= 'POST_TITLE1: ' . ($_POST['title1'] ?? '');
    $current_keywords .= 'POST_AUTHOR: ' . ($_POST['displayed_author_name'] ?? '');
    $current_keywords .= 'POST_EMAIL: ' . ($_POST['email'] ?? '');
    $current_keywords .= 'POST_SUMMARY: ' . ($_POST['summary'] ?? '');

    $ret = FALSE;
    $at_least_one_set = FALSE;

    $block_ip = $block_condition['ip'] ?? '';
    if ($ret == FALSE && $block_ip != '') {
      if ($current_ip == $block_ip) {
        $at_least_one_set = TRUE;
      }
      else {
        $ret = TRUE;
      }
    }

    if ($ret == FALSE && $block_ip1 != '') {
      if ($current_ip1 == $block_ip1) {
        $at_least_one_set = TRUE;
        if ($block_ip2 != '' && $block_ip2 != $current_ip2) {
          $ret = TRUE;
        }
        elseif ($block_ip3 != '' && $block_ip3 != $current_ip3) {
          $ret = TRUE;
        }
      }
      else {
        $ret = TRUE;
      }
    }

    if ($ret == FALSE && $block_url != '') {
      $at_least_one_set = TRUE;
      if (!strpos(' ' . $current_url, $block_url) > 0) {
        $ret = TRUE;
      }
    }
    if ($ret == FALSE && $block_ref_url != '') {
      $at_least_one_set = TRUE;
      if (!strpos(' ' . $current_ref_url, $block_ref_url) > 0) {
        $ret = TRUE;
      }
    }

    if ($ret == FALSE && $block_parent_id != '' && $block_parent_id != 0) {
      $at_least_one_set = TRUE;
      if ($block_parent_id != $current_parent_id) {
        $ret = TRUE;
      }
    }

    if ($ret == FALSE && $block_keyword != '') {
      $at_least_one_set = TRUE;
      if (!strpos(' ' . $current_keywords, $block_keyword) > 0) {
        $ret = TRUE;
      }
    }

    if ($at_least_one_set) {
      return $ret;
    }
    else {
      return TRUE;
    }

  }

  /**
   * Lists all blocks stored in the cache file for blocking.
   */
  public function listBlocks() {

    $cache_file_str = $this->loadCachedSpamBlocksIntoString();
    $string_array = explode("\r\n", $cache_file_str);
    $return_array = [];
    foreach ($string_array as $row) {

      $parsed_row = $this->parseCacheBlockRow($row);

      if (count($parsed_row) == 13) {
        array_push($return_array, $parsed_row);
      }
    }
    $return_array = array_reverse($return_array);

    return $return_array;
  }

  /**
   * Loads cached spam blocks into a string for use by other methods.
   */
  public function loadCachedSpamBlocksIntoString() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/spam_block.txt');
  }

  /**
   * Takes row from cached spam block file and parses it into its components.
   */
  public function parseCacheBlockRow($row_text) {
    $row_dict = [];
    $row_param_array = explode("|\t|", $row_text);
    if (count($row_param_array) == 13) {
      $row_dict['id'] = $row_param_array[0];
      $row_dict['ip'] = $row_param_array[1];
      $row_dict['ip_part1'] = $row_param_array[2];
      $row_dict['ip_part2'] = $row_param_array[3];
      $row_dict['ip_part3'] = $row_param_array[4];
      $row_dict['url'] = $row_param_array[5];
      $row_dict['referring_url'] = $row_param_array[6];
      $row_dict['http_method'] = $row_param_array[7];
      $row_dict['parent_news_item_id'] = $row_param_array[8];
      $row_dict['keyword'] = $row_param_array[9];
      $row_dict['destination'] = $row_param_array[10];
      $row_dict['added_info'] = $row_param_array[11];
      $row_dict['note'] = $row_param_array[12];
    }
    return $row_dict;
  }

  /**
   * Takes block conditions and returns string that can be saved to cache file.
   *
   * For blocking requests that match the given condition.
   */
  public function createSavableStringRow(
    $ip,
    $ip_part1,
    $ip_part2,
    $ip_part3,
    $url,
    $referring_url,
    $http_method,
    $parent_news_item_id,
    $keyword,
    $destination,
    $note,
  ) {

    $user_info_str = 'Added By ' . $_SESSION['session_username'];
    $user_info_str .= ' on ' . date(DATE_RFC822);

    $id = $_SESSION['session_user_id'] . '_' . time();
    $row_param_array = [];
    $row_param_array[0] = $id;
    $row_param_array[1] = $ip;
    $row_param_array[2] = $ip_part1;
    $row_param_array[3] = $ip_part2;
    $row_param_array[4] = $ip_part3;
    $row_param_array[5] = $url;
    $row_param_array[6] = $referring_url;
    $row_param_array[7] = $http_method;
    $row_param_array[8] = $parent_news_item_id;
    $row_param_array[9] = $keyword;
    $row_param_array[10] = $destination;
    $row_param_array[11] = $user_info_str;
    $row_param_array[12] = $note;
    $str_row = implode("|\t|", $row_param_array);
    return $str_row;
  }

  /**
   * Saves the full cache blocing file.
   */
  public function saveSpamBlocksFromString($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/spam_block.txt', $str);
  }

  /**
   * Should just append to file.
   */
  public function appendSpamBlockRow($row_str) {
    $str = $this->loadCachedSpamBlocksIntoString();
    $rows = explode("\r\n", $str);

    array_push($rows, $row_str);
    $str2 = implode("\r\n", $rows);
    $this->saveSpamBlocksFromString($str2);
  }

}
