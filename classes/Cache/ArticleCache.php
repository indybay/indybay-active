<?php

namespace Indybay\Cache;

use Indybay\DB\ArticleDB;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\MediaAndFileUtil\ImageUtil;
use Indybay\Renderer\ArticleRenderer;
use Indybay\Renderer\BlurbRenderer;

/**
 * Class dealing with caching posted newsitems (events & articles).
 *
 * Written December 2005.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class ArticleCache extends NewsItemCache {

  /**
   * Get a rendered version of central section of a posted article or event.
   *
   * This is in the cache class since the rendered template is just a series of
   * includes.
   */
  public function getCachedIncludeArticleHtml($directory, $article_info) {

    if ($article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
      if (isset($article_info['not_duplicate'])) {
        $template_name = 'article/hidden_post_layout_template.tpl';
      }
      else {
        $template_name = 'article/precheck_layout_template.tpl';
      }
    }
    elseif ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $template_name = 'calendar/calendar_layout_template.tpl';
    }
    elseif ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
      $template_name = 'feature_page/single_blurb_layout_template.tpl';
    }
    elseif ($article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED || $article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED) {
      $template_name = 'article/highlighted_post_layout_template.tpl';
    }
    else {
      $template_name = 'article/post_layout_template.tpl';
    }
    $temp_array['TPL_LOCAL_NEWS_ITEM_ID'] = $article_info['news_item_id'];
    $temp_array['TPL_LOCAL_TITLE1'] = str_replace('"', '\\"', $article_info['title1']);
    $temp_array['TPL_LOCAL_PATH'] = $directory;
    $result_html = $this->render($template_name, $temp_array);
    return $result_html;
  }

  /**
   * Returns the full php to store for an article.
   */
  public function getCachedFullArticleFileHtml($directory, $article_info) {
    $feature_page_db_class = new FeaturePageDB();
    $html = "<?php\ninclude_once '" . CLASS_PATH . "/config/indybay.cfg';\n";
    $html .= "header('Last-Modified: " . gmdate(DATE_RFC7231) . "');\n";
    if (isset($article_info['not_duplicate']) || $article_info['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN) {
      $html .= "\$GLOBALS['news_item_id'] = '{$article_info['news_item_id']}';\n";
      if ($article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN) {
        $html .= "\$GLOBALS['page_title'] = 'This post has been unpublished';\n";
      }
      elseif ($article_info['title1'] != '') {
        $html .= "\$GLOBALS['page_title'] = '" . str_replace(['\\', "'"], [
          '',
          "\'",
        ], $article_info['title1']) . "';\n";
      }
      else {
        $html .= "\$GLOBALS['page_title'] = '" . str_replace(['\\', "'"], [
          '',
          "\'",
        ], $article_info['title2']) . "';\n";
      }
      $html .= '$GLOBALS["page_summary"] = ' . "'" . str_replace(['\\', "'"], [
        '',
        "\'",
      ], strip_tags($article_info['summary'])) . "';\n";
      $image_url = '';
      $image_width = 0;
      $image_height = 0;
      $mime_type = '';
      $poster_url = '';
      if ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
        $body_class = 'page-event';
      }
      elseif ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
        $body_class = 'page-blurb';
      }
      else {
        $body_class = 'page-article';
      }
      if (!empty($article_info['media_attachment_id'])) {
        $media_attachment_db_class = new MediaAttachmentDB();
        $media_attachment_info = $media_attachment_db_class->getMediaAttachmentInfo($article_info['media_attachment_id']);
        $media_grouping_id = $media_attachment_info['media_type_grouping_id'];
        if ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_IMAGE) {
          $body_class .= ' content-image';
          $image_width = (int) $media_attachment_info['image_width'];
          $image_height = (int) $media_attachment_info['image_height'];
          $image_alt = $media_attachment_info['alt_tag'];
        }
        elseif ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_AUDIO) {
          $body_class .= ' content-audio';
        }
        elseif ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_VIDEO) {
          $body_class .= ' content-video';
          if ($image = $media_attachment_db_class->getRenderedVideoInfo($article_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM)) {
            $image_width = $image['image_width'];
            $image_height = $image['image_height'];
            $poster_url = SERVER_URL . UPLOAD_URL . $image['relative_path'] . str_replace([
              '\\',
              "'",
            ], [
              '',
              "\'",
            ], $image['file_name']);
          }
          $image_util = new ImageUtil();
          if ($media_attachment_info['mime_type'] == 'video/mp4' && is_null($media_attachment_info['browser_compat'])) {
            $image_util->getVideoBrowserCompat($media_attachment_info);
          }
          if ($media_attachment_info['mime_type'] != 'video/mp4' || !$media_attachment_info['browser_compat']) {
            $preview = $media_attachment_db_class->getRenderedH264((int) $media_attachment_info['media_attachment_id']);
            if (!$preview) {
              if ($id = $image_util->makeH264ForVideo($media_attachment_info)) {
                $preview = $media_attachment_db_class->getMediaAttachmentInfo($id);
              }
            }
            if ($preview) {
              $media_attachment_info = $preview;
            }
          }
        }
        $image_url = SERVER_URL . UPLOAD_URL . $media_attachment_info['relative_path'] . str_replace([
          '\\',
          "'",
        ], [
          '',
          "\'",
        ], $media_attachment_info['file_name']);
        $mime_type = $media_attachment_info['mime_type'];
      }
      $html .= '$GLOBALS["body_class"] = ' . "'$body_class';\n";
      $html .= '$GLOBALS["image_url"] = ' . "'" . $image_url . "';\n";
      $html .= '$GLOBALS["poster_url"] = ' . "'" . $poster_url . "';\n";
      $html .= '$GLOBALS["image_width"] = ' . "'" . $image_width . "';\n";
      $html .= '$GLOBALS["image_height"] = ' . "'" . $image_height . "';\n";
      if (isset($image_alt)) {
        $html .= '$GLOBALS["image_alt"] = ' . "'" . htmlspecialchars($image_alt) . "';\n";
      }
      $html .= '$GLOBALS["mime_type"] = ' . "'" . $mime_type . "';\n";
      if ($article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN || $article_info['news_item_status_id'] == NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN) {
        $html .= '$GLOBALS["page_display"] = "f";' . "\n";
      }
      $html .= '$GLOBALS["indexing_suspended"] = ' . (NEWS_ITEM_STATUS_INDEXING_SUSPENDED % $article_info['news_item_status_id'] ? '0' : '1') . ";\n";
      $html .= '$GLOBALS["syndication_suspended"] = ' . (NEWS_ITEM_STATUS_SYNDICATION_SUSPENDED % $article_info['news_item_status_id'] ? '0' : '1') . ";\n";
      $html .= '$GLOBALS["page_ids"] = array(';
      $i = 0;
      $associated_pages = $feature_page_db_class->getAssociatedPagesInfo($article_info['news_item_id']);
      foreach ($associated_pages as $page_info) {
        if ($i > 0) {
          $html .= ', ';
        }
        $html .= $page_info['page_id'];
        $i = $i + 1;
      }
      $html .= ");\n";
      $html .= 'include INCLUDE_' . "PATH.\"/article/article_header.inc\";\n";
      $html .= 'if (empty($_GET["printable"])): ?>';
      $html .= '<div class="label-page-wrapper">';
      $html .= '<div class="label-page label-pageevent">From the Open-Publishing Calendar</div>';
      $html .= '<div class="label-page label-pagearticle">From the Open-Publishing Newswire</div>';
      $html .= '<div class="label-page label-pageblurb">Indybay Feature</div>';
      $html .= '</div>';
      $html .= "<?php endif; \n";
    }
    $html .= $this->getCachedIncludeArticleHtml($directory, $article_info);
    $html .= "include INCLUDE_PATH . '/common/index_bottom.inc'; \n";
    // $html .= "include INCLUDE_PATH . '/common/center_right.inc'; \n";.
    $html .= "include INCLUDE_PATH . '/article/article_footer.inc';";
    return $html;
  }

  /**
   * Gets the php for the included comments include.
   *
   * This file is intended to be solely a list of safe includes so
   * for optimization adding comments can juts append to this file and
   * not require additional regeneration.
   */
  public function getCachedIncludeCommentHtml($news_item_id_array, $directory) {
    if (count($news_item_id_array) == 0) {
      return '';
    }
    $html = '';
    foreach ($news_item_id_array as $news_item_id) {
      $html .= "readfile('{$directory}{$news_item_id}_content.html');\n";
    }
    $ret = "\n<" . "?php\n" . $html . "\n?" . ">\n";
    return $ret;
  }

  /**
   * Gets the php for the included attachments include.
   *
   * This works the same as the comments be is mainly to keep the main
   * layout template simple so it only has one include even if it has 20
   * attachments.
   */
  public function getCachedIncludeAttachmentHtml($news_item_id_array, $directory) {
    if (count($news_item_id_array) == 0) {
      return '';
    }
    $html = '';
    foreach ($news_item_id_array as $news_item_id) {
      $html .= "readfile('{$directory}{$news_item_id}_content.html');\n";
    }
    $ret = "\n<" . "?php\n" . $html . "\n?" . ">\n";
    return $ret;
  }

  /**
   * Gets the include php for comments and writes it to the correct file.
   *
   * See function that renders the php for more info on the purpose behind this.
   */
  public function cacheCommentsIncludeForArticle($directory, $news_item_id, $news_item_id_array) {
    $file_path = $directory . $news_item_id . '_comments.inc';
    $html = $this->getCachedIncludeCommentHtml($news_item_id_array, $directory);
    $this->cacheFile($file_path, $html);
  }

  /**
   * Saves the latest comment box at that is included at the bottom of posts.
   */
  public function cacheCommentBoxForArticleIfNeeded($directory, $news_item_id) {
    $article_renderer_class = new ArticleRenderer();
    $file_path = $directory . $news_item_id . '_commentbox.html';
    $html = $article_renderer_class->getRenderedCommentBoxHtml($news_item_id, $directory);
    $this->cacheFile($file_path, $html);
  }

  /**
   * Gets the include php for attachments and writes it to the correct file.
   *
   * See function that renders the php for more info on the purpose behind this.
   */
  public function cacheAttachmentIncludeForArticle($directory, $news_item_id, $news_item_id_array) {
    $file_path = $directory . $news_item_id . '_attachments.inc';
    $html = $this->getCachedIncludeAttachmentHtml($news_item_id_array, $directory);
    $this->cacheFile($file_path, $html);
  }

  /**
   * Gets the php for the main include for an article and caches it.
   */
  public function cacheMainIncludeForArticle($directory, $article_info) {
    $file_path = $directory . $article_info['news_item_id'] . '.php';
    $html = $this->getCachedFullArticleFileHtml($directory, $article_info);
    $this->cacheFile($file_path, $html);
    $this->cacheCommentBoxForArticleIfNeeded($directory, $article_info['news_item_id']);
  }

  /**
   * Saves the content of an article or event to a file.
   */
  public function cacheContentForArticle($directory, $article_info) {
    $article_renderer_class = new ArticleRenderer();
    $file_path = $directory . $article_info['news_item_id'] . '_content.html';
    if ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
      $blurb_renderer_class = new BlurbRenderer();
      $html = $blurb_renderer_class->getRenderedBlurbHtmlFromNewsItemId($article_info['news_item_id']);
    }
    else {
      $html = $article_renderer_class->getRenderedArticleHtml($article_info);
    }
    $this->cacheFile($file_path, $html);
    if ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $html = $article_renderer_class->getRenderedArticleHtml($article_info);
    }
    return $html;
  }

  /**
   * Called when an article changes.
   *
   * The reason the main include is also refreshed is that we want to make sure
   * users get the new timestamp and dont still see the old file because of
   * browser caching.
   */
  public function recacheJustArticle($news_item_id) {
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    if ($article_info['parent_item_id'] + 0 != 0 && $article_info['parent_item_id'] != $news_item_id) {
      $parent_info = $article_db_class->getArticleInfoFromNewsItemId($article_info['parent_item_id']);
    }
    else {
      $parent_info = $article_info;
    }
    $directory = NEWS_PATH . '/' . $this->createDateBasedRelativeDirectory(NEWS_PATH . '/', $parent_info['creation_timestamp']);
    $this->cacheContentForArticle($directory, $article_info);
    $this->cacheMainIncludeForArticle($directory, $parent_info);
  }

  /**
   * Recaches or initially caches everything for article and returns preview.
   *
   * Includes the central section of the page but not the headers and footers.
   */
  public function cacheEverythingForArticle($news_item_id, $syndicate = FALSE) {
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    if ($article_info['parent_item_id'] != 0 && $article_info['parent_item_id'] != $news_item_id) {
      $news_item_id = $article_info['parent_item_id'];
      $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    }
    $directory = NEWS_PATH . '/' . $this->createDateBasedRelativeDirectory(NEWS_PATH . '/', $article_info['creation_timestamp']);
    $preview_html = $this->cacheContentForArticle($directory, $article_info);
    $attachment_ids_for_post = $article_db_class->getAttachmentIds($news_item_id);
    foreach ($attachment_ids_for_post as $child_id) {
      $child_info = $article_db_class->getArticleInfoFromNewsItemId($child_id);
      $preview_html .= $this->cacheContentForArticle($directory, $child_info);
    }
    $this->cacheAttachmentIncludeForArticle($directory, $news_item_id, $attachment_ids_for_post);
    $comment_ids_for_post = [];
    if ($article_info['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN) {
      $comment_ids_for_post = $article_db_class->getCommentIds($news_item_id);
      foreach ($comment_ids_for_post as $child_id) {
        $child_info = $article_db_class->getArticleInfoFromNewsItemId($child_id);
        $this->cacheContentForArticle($directory, $child_info);
      }
    }
    $this->cacheCommentsIncludeForArticle($directory, $news_item_id, $comment_ids_for_post);
    $this->cacheMainIncludeForArticle($directory, $article_info);
    return $preview_html;
  }

  /**
   * Called if attachment changes and editor chooses full recache.
   *
   * It will likely always call the comment regenerate but is kept seperate in
   * case there are ever differences.
   */
  public function cacheEverythingForAttachment($news_item_id, $parent_item_info) {
    $ret = $this->cacheEverythingForComment($news_item_id, $parent_item_info);
    return $ret;
  }

  /**
   * Called if comment changes and editor chooses full recache.
   *
   * Usually only the comment include itself need change since the parent
   * safe_include will just include  an empty string if the comment is hidden
   * but if things ever get messed up with post this lets editor force recache.
   */
  public function cacheEverythingForComment($news_item_id, $parent_item_info) {
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    $parent_info = $article_db_class->getArticleInfoFromNewsItemId($parent_item_info);
    $directory = NEWS_PATH . '/' . $this->createDateBasedRelativeDirectory(NEWS_PATH . '/', $parent_info['creation_timestamp']);
    $preview_html = $this->cacheContentForArticle($directory, $article_info);
    $comment_ids_for_post = $article_db_class->getCommentIds($parent_item_info);
    $this->cacheCommentsIncludeForArticle($directory, $parent_item_info, $comment_ids_for_post);
    $this->cacheMainIncludeForArticle($directory, $parent_info);
    return $preview_html;
  }

  /**
   * Returns the relative url for an artcile or event.
   *
   * This can be used either to access the file from the root file directory or
   * to create a url based off the root web path.
   */
  public function getRelativeUrlForArticle($news_item_id) {
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    $directory = '/newsitems' . '/' . $this->createDateBasedRelativeDirectory(NEWS_PATH . '/', $article_info['creation_timestamp'] ?? NULL);
    $rel_url = $directory . $news_item_id . '.php';
    return $rel_url;
  }

  /**
   * Makes normal cache file for news item redirect to another item.
   *
   * It is mainly useful for hidden posts and posts turned into attachments.
   */
  public function cacheRedirectForArticle($news_item_id, $news_item_id2) {
    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item_id);
    if (is_array($article_info)) {
      $new_url = $this->getRelativeUrlForArticle($news_item_id2);
      $directory = NEWS_PATH . '/' . $this->createDateBasedRelativeDirectory(NEWS_PATH . '/', $article_info['creation_timestamp']);
      $file_path = $directory . $article_info['news_item_id'] . '.php';
      $html = "<?php\nheader(\"HTTP/1.0 301 Moved Permanently\");\n";
      $html .= 'header("Location: ' . SERVER_URL . $new_url . "\");\n";
      $html .= '?>';
      $this->cacheFile($file_path, $html);
    }
  }

}
