<?php

namespace Indybay\Cache;

use Indybay\DB\FeaturePageDB;
use Indybay\DB\NewsItemDB;
use Indybay\DB\NewswireDB;
use Indybay\MediaAndFileUtil\FileUtil;
use Indybay\Renderer\NewswireRenderer;

/**
 * Generates the newswires included on the feature pages.
 *
 * Most of the code relates to only updating the needed newswires when events
 * are classified or change category.
 *
 * Written January 2006.
 * Modification Log:
 * 12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class NewswireCache extends Cache {

  /**
   * Regenerate newswires for a newsitem that just came in.
   */
  public function regenerateNewswiresForNewNewsitem($news_item_id) {

    $old_cats = [];
    $this->regenerateNewswiresForNewsitemHelper($news_item_id, $old_cats, NEWS_ITEM_STATUS_ID_NEW, NEWS_ITEM_STATUS_ID_NEW);

  }

  /**
   * Regenerate newswires for a changed newsitem.
   */
  public function regenerateNewswiresForNewsitem($news_item_id, $old_cats, $old_display_status, $new_display_status) {

    $this->regenerateNewswiresForNewsitemHelper($news_item_id, $old_cats, $old_display_status, $new_display_status);

  }

  /**
   * Regenerate newswires for a changed newsitem or new item.
   */
  public function regenerateNewswiresForNewsitemHelper($news_item_id, $old_cats, $old_display_status, $new_display_status) {

    $news_item_db_class = new NewsItemDB();
    $page_db_class = new FeaturePageDB();
    $new_categories = $news_item_db_class->getNewsItemCategoryIds($news_item_id);
    $page_id_list = $page_db_class->getAssociatedPageIds(array_merge($new_categories, $old_cats));
    $page_id_list = array_merge($page_id_list, $page_db_class->getPagesWithAllCategoryNewswires());
    foreach ($page_id_list as $page_id) {
      $this->regenerateNewswireForPageHelper($page_id, $old_display_status, $new_display_status);
    }

  }

  /**
   * Force full regeneration of all the newswire sections on a specific page.
   */
  public function forceRegenerateNewswireForPage($page_id) {

    $this->regenerateNewswireForPageHelper($page_id, 0, 0);

  }

  /**
   * Helper method used in regeneration of newswire sections on a page.
   */
  public function regenerateNewswireForPageHelper($page_id, $old_display_status, $new_display_status) {

    $page_db_class = new FeaturePageDB();
    $page_info = $page_db_class->getFeaturePageInfo($page_id);
    $page_name = 'page' . $page_info['page_id'];
    $newswire_type_id = $page_info['newswire_type_id'];
    $items_per_newswire_section = $page_info['items_per_newswire_section'];
    $associated_category_array = [];

    if ($old_display_status == 0 && $new_display_status == 0) {
      $force_regenerate = 1;
    }
    if ($newswire_type_id != NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_NOCAT) {
      $associated_category_array = $page_db_class->getCategoryIdsForPage($page_id);
    }

    if ($newswire_type_id == NEWSWIRE_TYPE_ALL_CAT) {
      $this->regenerateAllNewswireForCategories($page_name, $associated_category_array, $items_per_newswire_section);
    }
    elseif ($newswire_type_id == NEWSWIRE_TYPE_CLASSIFIED_CAT) {
      if (!empty($force_regenerate) || !($old_display_status == NEWS_ITEM_STATUS_ID_HIDDEN || $old_display_status == NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN
               || $new_display_status == NEWS_ITEM_STATUS_ID_HIDDEN || $new_display_status == NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN)) {
        $this->regenerateClassifiedNewswire($page_name, $associated_category_array, $items_per_newswire_section);
      }
    }
    else {
      if (isset($force_regenerate) || $old_display_status == NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED ||
       $new_display_status == NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED) {
        $this->regenerateLocalNewswire($page_name, $associated_category_array, $items_per_newswire_section);

      }
      if (isset($force_regenerate) ||$old_display_status == NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED ||
       $new_display_status == NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED) {
        $this->regenerateNonlocalNewswire($page_name, $associated_category_array, $items_per_newswire_section);
      }
      $this->regenerateOtherNewswire($page_name, $associated_category_array, $items_per_newswire_section);
    }

  }

  /**
   * Recache the local newswire on a page.
   */
  public function regenerateLocalNewswire($page_name, $category_list, $items_per_newswire_section) {

    $newswire_db_class = new NewswireDB();
    $newswire_cache_file_name = "newswire_{$page_name}_local";
    if (isset($GLOBALS['part_of_bulk_operation']) && $GLOBALS['part_of_bulk_operation'] == 1 && count($category_list) != 0) {
      $this->markNewswireFileForRegenerateOnView($newswire_cache_file_name);
    }
    else {
      $newswire_list = $newswire_db_class->getLocalListForCategories($category_list, $items_per_newswire_section);
      $this->regenerateNewswireGivenResultData($newswire_cache_file_name, $newswire_list);
    }
  }

  /**
   * Recache the nonlocal newswire on a page.
   */
  public function regenerateNonlocalNewswire($page_name, $category_list, $items_per_newswire_section) {

    $newswire_db_class = new NewswireDB();
    $newswire_cache_file_name = "newswire_{$page_name}_nonlocal";
    if (array_key_exists('part_of_bulk_operation', $GLOBALS) && $GLOBALS['part_of_bulk_operation'] == 1 && count($category_list) != 0) {
      $this->markNewswireFileForRegenerateOnView($newswire_cache_file_name);
    }
    else {
      $newswire_list = $newswire_db_class->getNonlocalListForCategories($category_list, $items_per_newswire_section);
      $this->regenerateNewswireGivenResultData($newswire_cache_file_name, $newswire_list);
    }
  }

  /**
   * Recache the other newswire on a page.
   */
  public function regenerateOtherNewswire($page_name, $category_list, $items_per_newswire_section) {

    $newswire_db_class = new NewswireDB();
    $newswire_cache_file_name = "newswire_{$page_name}_other";

    if (array_key_exists('part_of_bulk_operation', $GLOBALS) && $GLOBALS['part_of_bulk_operation'] == 1 && count($category_list) != 0) {
      $this->markNewswireFileForRegenerateOnView($newswire_cache_file_name);
    }
    else {
      $newswire_list = $newswire_db_class->getOtherListForCategories($category_list, $items_per_newswire_section);
      $this->regenerateNewswireGivenResultData($newswire_cache_file_name, $newswire_list);
    }

  }

  /**
   * Recache the classified newswire on a page.
   */
  public function regenerateClassifiedNewswire($page_name, $category_list, $items_per_newswire_section) {

    $newswire_db_class = new NewswireDB();
    $newswire_cache_file_name = "newswire_{$page_name}_classified";
    if (isset($GLOBALS['part_of_bulk_operation']) && $GLOBALS['part_of_bulk_operation'] == 1 && count($category_list) != 0) {
      $this->markNewswireFileForRegenerateOnView($newswire_cache_file_name);
    }
    else {
      $newswire_list = $newswire_db_class->getClassifiedListForCategories($category_list, $items_per_newswire_section);
      $this->regenerateNewswireGivenResultData($newswire_cache_file_name, $newswire_list);
    }
  }

  /**
   * Recache the all newswire on a page.
   */
  public function regenerateAllNewswireForCategories($page_name, $category_list, $items_per_newswire_section) {

    $newswire_db_class = new NewswireDB();
    $newswire_cache_file_name = "newswire_{$page_name}_all";
    if (!empty($GLOBALS['part_of_bulk_operation']) && !count($category_list)) {
      $this->markNewswireFileForRegenerateOnView($newswire_cache_file_name);
    }
    else {
      $newswire_list = $newswire_db_class->getAllListForCategories($category_list, $items_per_newswire_section);
      $this->regenerateNewswireGivenResultData($newswire_cache_file_name, $newswire_list);
    }
  }

  /**
   * Helper method used in saving of any newswire.
   */
  public function regenerateNewswireGivenResultData($newswire_cache_file_name, $result_data) {

    $file_path = CACHE_PATH . '/newswires/' . $newswire_cache_file_name;
    $newswire_renderer_class = new NewswireRenderer();
    $html = $newswire_renderer_class->getNewswireRowsHtml($result_data);
    $cache_class = new Cache();
    if ($html != '') {
      $cache_class->cacheFile($file_path, $html);
    }

  }

  /**
   * Adds hidden field to newswire section forcing it to be regenerated.
   *
   * This is mainly needed so updates to posts that are on a lot of pages
   * dont take forever to save. Instead of regenerating the newswires when an
   * item in them changes this just sets the newswire itself to have a tag in it
   * telling the cache code that loads it to rerun the newswire section on next
   * viewing.
   */
  public function markNewswireFileForRegenerateOnView($cachefile_name) {
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString(CACHE_PATH . '/newswires/' . $cachefile_name);
    $str = ' <!--regen-->' . $str;
    $file_util->saveStringAsFile(CACHE_PATH . '/newswires/' . $cachefile_name, $str);
  }

  /**
   * Loads the highlighted local section of the newswire for a given page.
   *
   * The code first tries to load from cache but if it is marked dirty it will
   * rerun the newswire.
   */
  public function loadLocalNewswireForPage($page_id) {

    $cachefile_name = "newswire_page{$page_id}_local";
    $file_path = CACHE_PATH . '/newswires/' . $cachefile_name;
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString($file_path);
    if (strpos($str, '<!--regen-->') > 0) {
      $this->regenerateNewswireForPageHelper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      $str = $file_util->loadFileAsString($file_path);
    }

    return $str;
  }

  /**
   * Loads the highlighted non-local (ie global) section of the newswire.
   *
   * The code first tries to load from cache but if it is marked dirty it will
   * rerun the newswire.
   */
  public function loadNonlocalNewswireForPage($page_id) {

    $cachefile_name = "newswire_page{$page_id}_nonlocal";
    $file_path = CACHE_PATH . '/newswires/' . $cachefile_name;
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString($file_path);
    if (strpos($str, '<!--regen-->') > 0) {
      $this->regenerateNewswireForPageHelper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      $str = $file_util->loadFileAsString($file_path);
    }

    return $str;
  }

  /**
   * Loads the "other/breaking" section of the newswire for a given page.
   *
   * The code first tries to load from cache but if it is marked dirty it will
   * rerun the newswire.
   */
  public function loadOtherNewswireForPage($page_id) {

    $cachefile_name = "newswire_page{$page_id}_other";
    $file_path = CACHE_PATH . '/newswires/' . $cachefile_name;
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString($file_path);
    if (strpos($str, '<!--regen-->') > 0) {
      $this->regenerateNewswireForPageHelper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      $str = $file_util->loadFileAsString($file_path);
    }

    return $str;
  }

  /**
   * Loads the classified newswire for a given page.
   *
   * This newswire is mainly used by international pages on Indybay
   * it has no sections and displays anything that has been classified as
   * editors to new anything but OTHER, NEW, HIDDEN or QUESTIONABLE (hidden or
   * unhidden).
   */
  public function loadClassifiedNewswireForPage($page_id) {

    $cachefile_name = "newswire_page{$page_id}_classified";
    $file_path = CACHE_PATH . '/newswires/' . $cachefile_name;
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString($file_path);
    if (strpos($str, '<!--regen-->') > 0) {
      $this->regenerateNewswireForPageHelper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      $str = $file_util->loadFileAsString($file_path);
    }

    return $str;
  }

  /**
   * Loads the full newswire for a given page.
   *
   * This newswire is not a part of Indybay it has no sections and displays
   * anything that has not been classified as editors as HIDDEN or QUESTIONABLE
   * HIDDEN.
   */
  public function loadAllNewswireForPage($page_id) {

    $cachefile_name = "newswire_page{$page_id}_all";
    $file_path = CACHE_PATH . '/newswires/' . $cachefile_name;
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString($file_path);
    if (strpos($str, '<!--regen-->') > 0) {
      $this->regenerateNewswireForPageHelper($page_id, NEWS_ITEM_STATUS_ID_OTHER, NEWS_ITEM_STATUS_ID_OTHER);
      $str = $file_util->loadFileAsString($file_path);
    }

    return $str;
  }

}
