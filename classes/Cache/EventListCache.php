<?php

namespace Indybay\Cache;

use Indybay\DB\FeaturePageDB;
use Indybay\Date;
use Indybay\Renderer\EventListRenderer;

/**
 * Generates eventlinks and upcoming events lists on the feature pages.
 *
 * Written January 2006.
 * Modification Log:
 * 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class EventListCache extends DependentListCache {

  /**
   * Code to figure out what to update cache-wise when an event is updated.
   */
  public function updateCachesOnChange($old_article_info, $new_article_info) {

    $old_timestamp = $old_article_info['displayed_timestamp'];
    $new_timestamp = $new_article_info['displayed_timestamp'];
    $old_date = new Date();
    $new_date = new Date();
    $old_date->setTimeFromUnixTime($old_timestamp);
    $new_date->setTimeFromUnixTime($new_timestamp);

    $old_date->findStartOfWeek();
    $new_date->findStartOfWeek();

    $event_cache_class = new EventCache();
    $event_cache_class->recacheWeekGivenDate($old_date);
    if ($old_date->getFormattedDate() !=
    $new_date->getFormattedDate()) {
      $event_cache_class->recacheWeekGivenDate($new_date);
    }

    $feature_page_db_class = new FeaturePageDB();
    $page_list = $feature_page_db_class->getAssociatedPagesInfo($old_article_info['news_item_id']);
    foreach ($page_list as $page_info) {
      $this->cacheHighlightedEventsCacheForPage($page_info['page_id']);
      $this->cacheEventsForPage($page_info['page_id']);
    }

    $this->cacheHighlightedEventsCacheForPage(FRONT_PAGE_CATEGORY_ID);

  }

  /**
   * Code to load highlighted events (eventlinks) for a given page.
   */
  public function loadCachedHighlightedEventsCacheForPage($page_id) {

    $file_path = CACHE_PATH . '/calendar/highlighted_cache/highlighted_list_for_page_' . $page_id;

    $ret = $this->cacheOrCallBasedOffAge(new EventListRenderer(), 'renderHighlightedEventList', $page_id, $file_path, 600);

    return $ret;

  }

  /**
   * Code to recache highlighted events (eventlinks) for a given page.
   */
  public function cacheHighlightedEventsCacheForPage($page_id) {

    $file_path = CACHE_PATH . '/calendar/highlighted_cache/highlighted_list_for_page_' . $page_id;

    $this->cacheOrCallBasedOffAge(new EventListRenderer(), 'renderHighlightedEventList', $page_id, $file_path, 0);

  }

  /**
   * Code to load full events for a given page.
   *
   * Tthis is the list at the bottom of the feature page.
   */
  public function loadCachedEventsForPage($page_id) {

    $file_path = CACHE_PATH . '/calendar/upcoming_cache/event_list_cache_for_page_' . $page_id;
    $ret = $this->cacheOrCallBasedOffAge(new EventListRenderer(), 'renderEventList', $page_id, $file_path, 600);

    return $ret;
  }

  /**
   * Code to recache full events list for a given page.
   *
   * This is the list at the bottom of the feature page.
   */
  public function cacheEventsForPage($page_id) {

    $file_path = CACHE_PATH . '/calendar/upcoming_cache/event_list_cache_for_page_' . $page_id;
    $this->cacheOrCallBasedOffAge(new EventListRenderer(), 'renderEventList', $page_id, $file_path, 0);

  }

}
