<?php

namespace Indybay\Cache;

/**
 * The newsitem cache class included common caching methods.
 *
 * Used by articles, events, blurbs and breaking news items.
 *
 * Written January 2006.
 * Modification Log:
 * 12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class NewsItemCache extends Cache {

  /**
   * Returns the file name of the main files saved for a news item.
   */
  public function getFileNameForNewsItem($news_item_id) {

    $file_name = $news_item_id . '.php';

    return $file_name;
  }

  /**
   * Returns the relative web path to a newsitem.
   *
   * Given its id and creation timstamp.
   */
  public function getWebCachePathForNewsItemGivenDate($news_item_id, $creation_date) {

    $cache_class = new Cache();
    $additional_date_path = $cache_class->getRelDirFromDate($creation_date);
    $rel_link = NEWS_REL_PATH . '/' . $additional_date_path . $this->getFileNameForNewsItem($news_item_id);

    return $rel_link;
  }

  /**
   * Purges cache for all news items.
   */
  public function purgeAll() {
    foreach (glob(WEB_PATH . NEWS_REL_PATH . '/[0-9][0-9][0-9][0-9]', GLOB_ONLYDIR) as $year) {
      foreach (glob("$year/[0-9][0-9]", GLOB_ONLYDIR) as $month) {
        foreach (glob("$month/[0-9][0-9]", GLOB_ONLYDIR) as $day) {
          foreach (glob("$day/[0-9]*.*") as $file) {
            unlink($file);
          }
        }
      }
    }
  }

}
