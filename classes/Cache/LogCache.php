<?php

namespace Indybay\Cache;

use Indybay\MediaAndFileUtil\FileUtil;

/**
 * Caches and loads lists rules about what to log to the DB.
 *
 * Written May 2006.
 * Modification Log:
 * 5/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class LogCache extends Cache {

  /**
   * Should logging record anything aside from DB adds.
   */
  public function logAnyNonDb() {
    $log_setting_array = $this->loadCachedLogsIntoArray();
    if ($log_setting_array[0] == '1' || $log_setting_array[2] == 1) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Determines if logging should take place for given HTTP method.
   *
   * Or search since that overrides GET.
   */
  public function shouldRecord($http_method) {
    $log_setting_array = $this->loadCachedLogsIntoArray();
    if ($http_method == 'GET') {
      if ($log_setting_array[0] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'SEARCH') {
      if ($log_setting_array[6] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'POST') {
      if ($log_setting_array[2] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'DB') {
      if ($log_setting_array[4] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Determines if IP logging is on for the given HTTP method.
   *
   * Or search since that overrides GET.
   */
  public function shouldRecordIps($http_method) {
    $log_setting_array = $this->loadCachedLogsIntoArray();
    if ($http_method == 'GET') {
      if ($log_setting_array[1] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'SEARCH') {
      if ($log_setting_array[7] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'POST') {
      if ($log_setting_array[3] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    if ($http_method == 'DB') {
      if ($log_setting_array[5] == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Changes log settings and then calls save.
   */
  public function changeLogSettings(
    $log_get,
    $log_ip_get,
    $log_search,
    $log_ip_search,
    $log_post,
    $log_ip_post,
    $log_db,
    $log_ip_db,
  ) {
    $log_seting_array = [];
    if ($log_get != '') {
      $log_seting_array[0] = 1;
    }
    else {
      $log_seting_array[0] = 0;
    }
    if ($log_ip_get != '') {
      $log_seting_array[1] = 1;
    }
    else {
      $log_seting_array[1] = 0;
    }
    if ($log_post != '') {
      $log_seting_array[2] = 1;
    }
    else {
      $log_seting_array[2] = 0;
    }
    if ($log_ip_post != '') {
      $log_seting_array[3] = 1;
    }
    else {
      $log_seting_array[3] = 0;
    }
    if ($log_db != '') {
      $log_seting_array[4] = 1;
    }
    else {
      $log_seting_array[4] = 0;
    }
    if ($log_ip_db != '') {
      $log_seting_array[5] = 1;
    }
    else {
      $log_seting_array[5] = 0;
    }
    if ($log_search != '') {
      $log_seting_array[6] = 1;
    }
    else {
      $log_seting_array[6] = 0;
    }
    if ($log_ip_search != '') {
      $log_seting_array[7] = 1;
    }
    else {
      $log_seting_array[7] = 0;
    }
    $str = implode('|', $log_seting_array);
    $this->saveLogSettingsFromString($str);
  }

  /**
   * Private functions for use in this class.
   */
  public function loadCachedLogsIntoArray() {
    if (!file_exists(CACHE_PATH . '/log_settings.txt')) {
      return [0, 0, 0, 0, 0, 0, 0];
    }
    $file_util = new FileUtil();
    $str = $file_util->loadFileAsString(CACHE_PATH . '/log_settings.txt');
    $ret_array = explode('|', $str);
    if (!is_array($ret_array) || count($ret_array) < 7) {
      $ret_array = [0, 0, 0, 0, 0, 0, 0, 0];
    }
    return $ret_array;
  }

  /**
   * Saves log settings.
   */
  public function saveLogSettingsFromString($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/log_settings.txt', $str);
  }

  /**
   * Should just append to file.
   */
  public function appendSpamBlockRow($row_str) {
    $str = $this->loadCachedSpamBlocksIntoString();
    $rows = explode("\r\n", $str);

    array_push($rows, $row_str);
    $str2 = implode("\r\n", $rows);
    $this->saveSpamBlocksFromString($str2);
  }

}
