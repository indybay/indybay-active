<?php

namespace Indybay;

/**
 * A basic translate class.
 */
class Translate {

  /**
   * Local dictionary.
   */
  protected ?array $local;

  /**
   * Class constructor.
   */
  public function __construct() {
    // Constructor creates common language table.
    $global_filename = INDYBAY_BASE_PATH . '/dictionary/' . $GLOBALS['lang'] . '/common.dict';

    if (file_exists($global_filename)) {
      include $global_filename;
    }
    else {
      return 0;
    }

    return 1;
  }

  /**
   * Translates a string.
   */
  public function trans($key) {
    if (array_key_exists($key, $GLOBALS['dict'])) {
      return $GLOBALS['dict'][$key];
    }
  }

  /**
   * Builds translation table.
   */
  public function createTranslateTable($keyword) {
    $lang = $GLOBALS['lang'] ?? 'en';
    if (strlen($keyword) > 0) {
      $template_filename = INDYBAY_BASE_PATH . '/dictionary/' . $lang . '/' . $keyword . '.dict';
      $this->local = NULL;

      // Give local dictionary precedence over global dictionary.
      if (file_exists($template_filename)) {
        $this->local = [];
        include $template_filename;
      }

      if (isset($this->local)) {
        $GLOBALS['dict'] = array_merge($GLOBALS['dict'], $this->local);
      }

    }
    else {
      return 0;
    }
    return 1;
  }

}
