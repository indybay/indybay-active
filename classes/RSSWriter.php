<?php

namespace Indybay;

/**
 * Start of a convenience library to help RSS1.0 creation.
 *
 * Edd Dumbill <mailto:edd+rsswriter@usefulinc.com>
 * Revision 1.1  2001/05/17 18:17:46  edmundd.
 */
class RSSWriter {

  /**
   * Channel info array.
   */
  protected array $chaninfo;

  /**
   * Image array.
   */
  protected array $image;

  /**
   * Items array.
   */
  protected array $items;

  /**
   * Modules array.
   */
  protected array $modules;

  /**
   * Class constructor.
   */
  public function __construct(protected string $website, $title, $description, protected string $channelURI, $meta = []) {
    // Constructor.
    $this->chaninfo = [];
    $this->chaninfo['link'] = $website;
    $this->chaninfo['description'] = $description;
    $this->chaninfo['title'] = $title;
    $this->items = [];
    $this->modules = ['dc' => 'http://purl.org/dc/elements/1.1/'];
    foreach ($meta as $key => $value) {
      $this->chaninfo[$key] = $value;
    }
  }

  /**
   * Use module.
   */
  public function useModule($prefix, $uri) {
    $this->modules[$prefix] = $uri;
  }

  /**
   * Set image.
   */
  public function setImage($imgURI, $imgAlt, $imgWidth = 88, $imgHeight = 31) {
    $this->image = [
      'uri' => $imgURI,
      'title' => $imgAlt,
      'width' => $imgWidth,
      'height' => $imgHeight,
    ];
  }

  /**
   * Add item.
   */
  public function addItem($uri, $title, $meta = []) {
    $item = [
      'uri' => $uri,
      'link' => $uri,
      'title' => $this->deTag($title),
    ];
    foreach ($meta as $key => $value) {
      if ($key == 'dcterms:alternative') {
        $value = $this->deTag($value);
      }
      $item[$key] = $value;
    }
    $this->items[] = $item;
  }

  /**
   * Serialize.
   */
  public function serialize() {
    $this->preamble();
    $this->channelinfo();
    $this->image();
    $this->items();
    $this->postamble();
    return $GLOBALS['print_rss'];
  }

  /**
   * Detag.
   */
  public function deTag($in) {
    while (preg_match('/<[^>]+>/', $in)) {
      $in = preg_replace('/<[^>]+>/', '', $in);
    }
    return $in;
  }

  /**
   * Preamble.
   */
  public function preamble() {

    $GLOBALS['print_rss'] .= '<?xml version="1.0" ?>
<rdf:RDF 
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns="http://purl.org/rss/1.0/"
';
    foreach ($this->modules as $prefix => $uri) {
      $GLOBALS['print_rss'] .= "         xmlns:{$prefix}=\"{$uri}\"\n";
    }
    $GLOBALS['print_rss'] .= ">\n\n";
  }

  /**
   * Channel info.
   */
  public function channelinfo() {

    $GLOBALS['print_rss'] .= '  <channel rdf:about="' . $this->channelURI . '">
';
    $i = $this->chaninfo;
    foreach (['title', 'link', 'dc:source', 'description', 'dc:language', 'dc:publisher',
      'dc:creator', 'dc:rights',
    ] as $f) {
      if (isset($i[$f])) {
        $GLOBALS['print_rss'] .= "    <{$f}>" . htmlspecialchars($i[$f]) . "</{$f}>\n";
      }
    }
    if (isset($this->image)) {
      $GLOBALS['print_rss'] .= '    <image rdf:resource="' . htmlspecialchars($this->image['uri']) . "\" />\n";
    }
    $GLOBALS['print_rss'] .= "    <items>\n";
    $GLOBALS['print_rss'] .= "      <rdf:Seq>\n";
    foreach ($this->items as $i) {
      $GLOBALS['print_rss'] .= '        <rdf:li rdf:resource="' . htmlspecialchars($i['uri']) . "\" />\n";
    }
    $GLOBALS['print_rss'] .= "      </rdf:Seq>\n";
    $GLOBALS['print_rss'] .= "    </items>\n";
    $GLOBALS['print_rss'] .= "  </channel>\n\n";
  }

  /**
   * Image.
   */
  public function image() {

    if (isset($this->image)) {
      $GLOBALS['print_rss'] .= '  <image rdf:about="' . htmlspecialchars($this->image['uri']) . "\">\n";
      $GLOBALS['print_rss'] .= '     <title>' . htmlspecialchars($this->image['title']) . "</title>\n";
      $GLOBALS['print_rss'] .= '     <url>' . htmlspecialchars($this->image['uri']) . "</url>\n";
      $GLOBALS['print_rss'] .= '     <link>' . htmlspecialchars($this->website) . "</link>\n";
      if ($this->chaninfo['description']) {
        $GLOBALS['print_rss'] .= '     <dc:description>' . htmlspecialchars($this->chaninfo['description']) .
            "</dc:description>\n";
      }
      $GLOBALS['print_rss'] .= "  </image>\n\n";
    }
  }

  /**
   * Postamble.
   */
  public function postamble() {

    $GLOBALS['print_rss'] .= '
</rdf:RDF>
';
  }

  /**
   * Items.
   */
  public function items() {

    foreach ($this->items as $item) {
      $GLOBALS['print_rss'] .= '  <item rdf:about="' . htmlspecialchars($item['uri']) . "\">\n";
      foreach ($item as $key => $value) {
        if ($key !== 'uri' && $key !== 'dcterms:hasPart' && $key !== 'content:encoded' && $key !== 'description') {
          if (is_array($value)) {
            foreach ($value as $v1) {
              $GLOBALS['print_rss'] .= "    <{$key}>" . htmlspecialchars($v1) . "</{$key}>\n";
            }
          }
          else {
            $GLOBALS['print_rss'] .= "    <{$key}>" . htmlspecialchars($value) . "</{$key}>\n";
          }
        }
        if ($key == 'content:encoded' or $key == 'description') {
          $GLOBALS['print_rss'] .= "    <{$key}><![CDATA[" . $value . "]]></{$key}>\n";
        }
        if ($key == 'dcterms:hasPart' and strlen($value) > 0) {
          $GLOBALS['print_rss'] .= "    <{$key} rdf:resource=\"" . $value . "\" />\n";
        }
      }
      $GLOBALS['print_rss'] .= "  </item>\n\n";
    }
  }

}
