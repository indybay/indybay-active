<?php

namespace Indybay\Renderer;

/**
 * End class.
 */
class Captcha {

  /**
   * Array of captchas to be solved.
   */
  protected array $captchas;

  /**
   * Validation token.
   */
  protected string $token;

  /**
   * Validation result.
   */
  public bool $valid;

  /**
   * Makes form.
   */
  public function makeForm() {
    if ($this->validateCaptcha()) {
      return 'You appear to be human.
        <input id="captcha_math" name="captcha_math" type="hidden" value="'
        . htmlspecialchars($this->captchas['math']['answer'])
        . '"><input type="hidden" name="token" value="'
        . htmlspecialchars($this->token) . '">';
    }
    else {
      $this->makeCaptcha();
      $this->makeToken();
      return $this->captchas['math']['form'] . '<input type="hidden" name="token" value="' . htmlspecialchars($this->token) . '">';
    }
  }

  /**
   * Make captcha.
   */
  public function makeCaptcha() {

    $questions[] = '<div>The sun rises in which direction every morning?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'east';
    $questions[] = '<div>The Earth revolves around which large fiery object once every year?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'sun';
    $questions[] = '<div>Which star is closest to the Earth?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'sun';
    $questions[] = '<div>Which form of electrical power is generated from the sun: geothermal or solar?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'solar';
    $questions[] = '<div>Is the sun a star or planet?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'star';
    $questions[] = '<div>Unscramble the following word to reveal the name of a planet: svneu.</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'venus';
    $questions[] = '<div>What is the name of the third planet from the sun?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'earth';
    $questions[] = '<div> What is water called when it is frozen solid?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'ice';
    $questions[] = '<div>Which is the tallest mountain in the world: Tamalpais or Everest?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'everest';
    $questions[] = '<div>Which ocean is off the coast of California: Pacific or Indian?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'pacific';
    $questions[] = '<div>How many sides does a triangle have: three or four?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'three';
    $questions[] = '<div>How many sides does an octagon have: seven or eight?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'eight';
    $questions[] = '<div>Due to global climate change, sea levels are rising or falling?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'rising';
    $questions[] = '<div>What is the name of the organ that pumps blood through the human body?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'heart';
    $questions[] = '<div>How many horns did a Triceratops have: three or four?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'three';
    $questions[] = '<div>Do spiders have six or eight legs?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'eight';
    $questions[] = '<div>A doe is a female of what kind of animal?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'deer';
    $questions[] = '<div>Which domesticated animal purrs and chases mice: cat or cow?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'cat';
    $questions[] = '<div>What is the adult form of a caterpillar called?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'butterfly';
    $questions[] = '<div>The speed of sound faster than the speed of light: true or false?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'false';
    $questions[] = '<div>What fruit is used as the primary ingredient in guacamole?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'avocado';
    $questions[] = '<div>Acorns come from which type of tree?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'oak';
    $questions[] = '<div>What is the vehicle called that runs on a track and blows a whistle?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'train';
    $questions[] = '<div>How many corners does a square have: three or four?</div> <input id="captcha_math" name="captcha_math" type="text" class="captcha" size="8" maxlength="32">';
    $answers[] = 'four';

    $delta = random_int(0, count($answers) - 1);
    $captcha['answer'] = $answers[$delta];
    $captcha['form'] = "<div class=\"captcha-wrapper\">{$questions[$delta]}</div>";
    $this->captchas['math'] = $captcha;
  }

  /**
   * Makes token.
   */
  public function makeToken() {
    $this->token = hash_hmac('sha256', mktime(date('H'), 0, 0) . mb_strtolower($this->captchas['math']['answer']), INDYBAY_PRIVATE_KEY);
  }

  /**
   * Validates captcha.
   */
  public function validateCaptcha() {
    $math = $_POST['captcha_math'] ?? '';
    $token = $_POST['token'] ?? '';
    $time = mktime(date('H'), 0, 0);
    $tokens = range(0, 5);
    foreach ($tokens as $value) {
      if (hash_equals($token, hash_hmac('sha256', ($time - (3600 * $value)) . mb_strtolower($math), INDYBAY_PRIVATE_KEY))) {
        $this->token = $token;
        $this->captchas = ['math' => ['answer' => $math]];
        return $this->valid = TRUE;
      }
    }
    return $this->valid = FALSE;
  }

}
