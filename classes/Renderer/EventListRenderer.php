<?php

namespace Indybay\Renderer;

/**
 * Written January 2006.
 *
 * Modification Log:
 * 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Cache\NewsItemCache;
use Indybay\DB\EventListDB;

/**
 * Used to render the newswires on the feature pages.
 */
class EventListRenderer extends Renderer {

  /**
   * Renders the hrml for a newswire section.
   */
  public function renderHighlightedEventList($page_id) {
    $event_list_db = new EventListDB();
    $result_data = $event_list_db->getHighlightedEventListForPage($page_id);
    if (!is_array($result_data)) {
      return '';
    }
    $html = '';

    if (count($result_data) == 0) {
      $html = '';
    }
    else {

      $num_rows = 0;
      $last_formatted_day = '';
      foreach ($result_data as $row) {
        $event_start_date = $row['displayed_timestamp'];
        $formatted_day = date('D M j', $event_start_date);
        if ($formatted_day != $last_formatted_day) {
          $num_rows = $num_rows + 1;
        }
        $last_formatted_day = $formatted_day;
        $num_rows = $num_rows + 1;
      }
      $current_row = 0;
      $last_formatted_day = '';
      $added_column = 0;
      $html .= '<div class="feature-eventlinks grid grid--2-cols"><div class="calendarwrapper-1"><div class="breakbug-event">';
      foreach ($result_data as $row) {
        $event_start_date = $row['displayed_timestamp'];
        $formatted_day = date('l M j', $event_start_date);
        if ($formatted_day != $last_formatted_day) {
          $html .= '<div class="breakbug-event-date">' . $formatted_day . '</div>';
          $current_row = $current_row + 1;
        }
        $last_formatted_day = $formatted_day;
        $html .= $this->renderHighlightedEventLinkRow($row);
        $current_row = $current_row + 1;
        if ($current_row >= round($num_rows / 2) && $added_column == 0) {
          $html .= '</div></div><div class="calendarwrapper-1"><div class="breakbug-event">';
          $added_column = 1;
        }
      }

      $html .= '
        <div class="moreevents"><a href="/calendar/?page_id=' . $page_id . '">More Events...</a> </div></div><div class="closenav" title="close">[ <span>&times;</span> close ]</div></div><!-- END .calendarwrapper-1 --></div>
	    	';
    }

    return $html;
  }

  /**
   * Renders the hrml for a newswire section.
   */
  public function renderEventList($page_id) {

    $event_list_db = new EventListDB();
    $result_data = $event_list_db->getListForPage($page_id);
    $html = '';

    if (count($result_data) != 0) {
      $current_row = 0;
      $html .= '';
      foreach ($result_data as $row) {
        $event_start_date = $row['displayed_timestamp'];
        $formatted_day = date('D M jS', $event_start_date);
        $row['formatted_day'] = $formatted_day;
        $html .= $this->renderEventLinkRow($row);
        $current_row = $current_row + 1;
      }
    }
    $html .= '';

    return $html;
  }

  /**
   * Renders a row in the cached newswire file.
   */
  public function renderHighlightedEventLinkRow($row) {

    $news_item_cache_class = new NewsItemCache();
    $news_item_link = $news_item_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);

    if ($row['title1'] == strtoupper($row['title1'])) {
      $row['title1'] = ucwords(strtolower($row['title1']));
    }

    $title = $row['title1'];
    $test = wordwrap($row['title1'], 49);
    $i = strpos($test, "\n");

    if ($i > 0) {
      $row['title1'] = substr($test, 0, $i) . '...';
    }

    $event_start_date = $row['displayed_timestamp'];
    if (date('i', $event_start_date) == '00') {
      $row['formatted_time'] = date('ga', $event_start_date);
    }
    else {
      $row['formatted_time'] = date('g:ia', $event_start_date);
    }

    $result_html = '<div class="breakbug-event-inner">
      <span class="date">
			' . $row['formatted_time'] . '
      </span>
			<a title="' . htmlspecialchars($title) . '" href="' . $news_item_link . '">
			' . htmlspecialchars($row['title1']) . '</a>
			</div>
    	';

    return $result_html;

  }

  /**
   * Renders a row in the cached newswire file.
   */
  public function renderEventLinkRow($row) {

    $news_item_cache_class = new NewsItemCache();
    $news_item_link = $news_item_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);

    if ($row['title1'] == strtoupper($row['title1'])) {
      $row['title1'] = ucwords(strtolower($row['title1']));
    }

    $test = wordwrap($row['title1'], 55);
    $i = strpos($test, "\n");

    if ($i > 0) {
      $row['title1'] = substr($test, 0, $i) . '...';
    }

    $event_start_date = $row['displayed_timestamp'];
    if (date('i', $event_start_date) == '00') {
      $row['formatted_time'] = date('ga', $event_start_date);
    }
    else {
      $row['formatted_time'] = date('g:ia', $event_start_date);
    }

    $result_html = '<div class="module-inner-horiz">
      <img src="/im/imc_event.svg" alt="event" class="mediaicon" width="12" height="12">
      <a href="' . $news_item_link . '">
			' . $row['title1'] . '</a>
      <span class="date">
			' . $row['formatted_day'] . ' ' . $row['formatted_time'] . '
			</span>
			</div>
    	';

    return $result_html;

  }

}
