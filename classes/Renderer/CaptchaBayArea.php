<?php

namespace Indybay\Renderer;

/**
 * End class.
 */
class CaptchaBayArea extends Captcha {

  /**
   * Make captcha.
   */
  public function makeCaptcha() {

    $questions[] = '<div>Fill in the missing word: <code>yerba <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"> island</code></div>';
    $answers[] = 'buena';
    $questions[] = '<div>Fill in the missing word: <code>Sittin&#039; on the dock of the <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"></code></div>';
    $answers[] = 'bay';
    $questions[] = '<div>What county is north of San Francisco? <input name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Marin';
    $questions[] = '<div>Which East Bay city has the largest population? <input name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Oakland';
    $questions[] = '<div>Which city, 50 miles south of San Francisco, has a larger population than San Francisco? <input name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'San Jose';
    $questions[] = '<div>What is the name of the native people who created the shellmounds around the San Francisco Bay? <input name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Ohlone';
    $questions[] = '<div>Fill in the missing word: <code>If we don\'t get it? <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"> It Down!</code></div>';
    $answers[] = 'Shut';
    $questions[] = '<div>Fill in the missing word: <code>Ain\'t no power like the <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"> of the people!</code></div>';
    $answers[] = 'power';
    $questions[] = '<div>Fill in the missing word: <code>When our communities are under attack. What do we do? <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"> Up, Fight Back!</code></div>';
    $answers[] = 'Stand';
    $questions[] = '<div>In what city was the Black Panther Party for Self-Defense first established? <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Oakland';
    $questions[] = '<div>At what University of California campus did Mario Savio\'s speeches inspire the Free Speech Movement? <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Berkeley';
    $questions[] = '<div>Whose murder by BART police at Fruitvale station set off a movement for justice in 2009? Oscar <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"></div>';
    $answers[] = 'Grant';
    $questions[] = '<div>Which island in the San Francisco Bay was occupied by the Indians of All Tribes (IOAT) in 1969? <input style="font-family: monospace; margin: 0;" name="captcha_math" type="text" size="8" maxlength="32"> Island</div>';
    $answers[] = 'Alcatraz';

    $delta = random_int(0, count($answers) - 1);
    $captcha['answer'] = $answers[$delta];
    $captcha['form'] = "<div>{$questions[$delta]}</div>";
    $this->captchas['math'] = $captcha;
  }

}
