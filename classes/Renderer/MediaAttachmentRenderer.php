<?php

namespace Indybay\Renderer;

/**
 * Indybay MediaAttachmentRenderer Class.
 *
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\DB\MediaAttachmentDB;
use Indybay\DB\NewsItemVersionDB;
use Indybay\MediaAndFileUtil\ImageUtil;
use Indybay\MediaAndFileUtil\UploadUtil;

/**
 * Renders media attachments on posts and center column items.
 *
 * Also makes calls off to the legacy migration code to move
 * uploads into date specific folders when they are
 * not found during rendering.
 */
class MediaAttachmentRenderer extends Renderer {

  /**
   * Renders small thumbnail from news item info.
   */
  public function renderSmallThumbnailFromNewsItemInfo($news_item_info, $additional_image_tag_info) {

    if (is_array($news_item_info)) {
      $upload_util_class = new UploadUtil();
      $news_item_version_db_class = new NewsItemVersionDB();
      $thumbnail_media_attachment_id = $news_item_info['thumbnail_media_attachment_id'];
      $media_attachment_id = $news_item_info['media_attachment_id'];
      $news_item_version_id = $news_item_info['news_item_version_id'];

      if ($thumbnail_media_attachment_id + 0 != 0) {
        $media_attachment_db = new MediaAttachmentDB();
        $thumbnail_info = $media_attachment_db->getMediaAttachmentInfo($thumbnail_media_attachment_id);
        if (!is_array($thumbnail_info) || $thumbnail_info['media_attachment_id'] != $thumbnail_media_attachment_id) {
          $thumbnail_media_attachment_id = 0;
        }
      }
      if ($thumbnail_media_attachment_id + 0 != 0) {
        $ret = $this->renderSmallThumbnailForAttachmentFromMediaAttachmentId($thumbnail_media_attachment_id, $additional_image_tag_info);
      }
      elseif ($media_attachment_id + 0 != 0) {
        $thumbnail_info = $upload_util_class->generateThumbnailsForMediaAttachmentFromId($media_attachment_id);
        $thumbnail_media_attachment_id = is_array($thumbnail_info) ? $thumbnail_info['small_thumbnail_media_attachment_id'] : 0;
        if ($thumbnail_media_attachment_id + 0 > 0) {
          if ($news_item_info['news_item_type_id'] != NEWS_ITEM_TYPE_ID_BLURB) {
            $news_item_version_db_class->updateThumbnailAttachmentIdForVersion($news_item_version_id, $thumbnail_media_attachment_id);
          }
          $ret = $this->renderSmallThumbnailForAttachmentFromMediaAttachmentId($thumbnail_media_attachment_id, $additional_image_tag_info);
        }
        else {
          $ret = $this->renderSmallThumbnailForAttachmentFromMediaAttachmentId($media_attachment_id, $additional_image_tag_info);
        }
      }
      else {
        $ret = '';
      }
    }
    else {
      echo 'error: renderSmallThumbnailFromNewsItemInfo called with null info<br>';
      $ret = '';
    }

    return $ret;
  }

  /**
   * Renders medium thumbnail from news item info.
   */
  public function renderMediumThumbnailFromNewsItemInfo($news_item_info, $additional_image_tag_info) {

    $ret = '';
    if (is_array($news_item_info)) {
      $upload_util_class = new UploadUtil();
      $media_attachment_id = $news_item_info['media_attachment_id'];
      if ($media_attachment_id + 0 != 0) {
        $thumbnail_info = $upload_util_class->generateThumbnailsForMediaAttachmentFromId($media_attachment_id);
        if (is_array($thumbnail_info) && count($thumbnail_info) > 0) {
          $thumbnail_media_attachment_id = $thumbnail_info['medium_thumbnail_media_attachment_id'];
          if ($thumbnail_media_attachment_id + 0 > 0) {
            $ret = $this->renderMediumThumbnailForAttachmentFromMediaAttachmentId($thumbnail_media_attachment_id, $additional_image_tag_info);
          }
          else {
            $ret = $this->renderMediumThumbnailForAttachmentFromMediaAttachmentId($media_attachment_id, $additional_image_tag_info);
          }
        }
      }
      else {
        $ret = '';
      }
    }
    else {
      echo 'error: renderMediumThumbnailFromNewsItemInfo called with null info<br>';
    }

    return $ret;
  }

  /**
   * Renders small thumbnail from media attachment ID.
   */
  public function renderSmallThumbnailForAttachmentFromMediaAttachmentId($media_attachment_id, $additional_image_tag_info) {

    $media_attachment_db = new MediaAttachmentDB();
    $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
    if (!is_array($media_attachment_info) || $media_attachment_info['media_attachment_id'] != $media_attachment_id) {
      echo 'renderSmallThumbnailForAttachmentFromMediaAttachmentId called with bad media_attachment_id ' . $media_attachment_id . '<br>';
      $ret = '';
    }
    else {
      $media_attachment_info['is_thumbnail'] = 1;
      $ret = $this->renderAttachment($media_attachment_info, 0, THUMBNAIL_WIDTH_SMALL, THUMBNAIL_HEIGHT_SMALL, $additional_image_tag_info);
    }

    return $ret;
  }

  /**
   * Renders medium thumbnail from media attachment ID.
   */
  public function renderMediumThumbnailForAttachmentFromMediaAttachmentId($media_attachment_id, $additional_image_tag_info) {

    $media_attachment_db = new MediaAttachmentDB();
    $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
    if (!is_array($media_attachment_info) || $media_attachment_info['media_attachment_id'] != $media_attachment_id) {
      echo 'renderMediumThumbnailForAttachmentFromMediaAttachmentId called with bad media_attachment_id ' . $media_attachment_id . '<br>';
      $ret = '';
    }
    else {
      $media_attachment_info['is_thumbnail'] = 1;
      $ret = $this->renderAttachment($media_attachment_info, 1, THUMBNAIL_WIDTH_MEDIUM, THUMBNAIL_HEIGHT_MEDIUM, $additional_image_tag_info);
    }

    return $ret;
  }

  /**
   * Renders small thumbnail from media attachment info.
   */
  public function renderSmallThumbnailForAttachment($media_attachment_info) {

    $ret = $this->renderAttachment($media_attachment_info);

    return $ret;
  }

  /**
   * Renders attachment.
   */
  public function renderAttachment($media_attachment_info, $include_name_info = 1, $restrict_width_in_tag = 0, $restrict_height_in_tag = 0, $additional_tag_params = '') {

    if (!is_array($media_attachment_info) || count($media_attachment_info) == 0) {
      echo 'renderAttachment called with empty media attachment info<br>';
    }
    else {

      $media = '';
      $upload_util_class = new UploadUtil();
      $image_util_class = new ImageUtil();

      if ($media_attachment_info['upload_status_id'] != UPLOAD_STATUS_VALIDATED) {
        $media_attachment_info = $upload_util_class->verifyAttachment($media_attachment_info);
      }

      $file_name = $media_attachment_info['file_name'];
      $relative_path = $media_attachment_info['relative_path'];
      $file_dir = UPLOAD_PATH . '/' . $media_attachment_info['relative_path'];
      $full_file_path = $file_dir . $file_name;
      $relative_url = '/uploads/' . $relative_path . $file_name;

      if ($media_attachment_info['upload_status_id'] != UPLOAD_STATUS_FAILED) {

        if (file_exists($full_file_path)) {
          $file_size = $this->renderFileSize(filesize($full_file_path));
        }

        switch ($media_attachment_info['mime_type']) {
          case 'image/avif':
          case 'image/gif':
          case 'image/heic':
          case 'image/jpeg':
          case 'image/png':
          case 'image/webp':

            if (file_exists($full_file_path)) {

              if (str_ends_with($full_file_path, '.heic')) {
                $imagick = new \Imagick("{$full_file_path}[0]");
                $imagick->autoOrient();
                ['width' => $width, 'height' => $height] = $imagick->getImageGeometry();
                $type = NULL;
                $imagick->clear();
              }
              else {
                [$width, $height, $type] = getimagesize($full_file_path);
              }

              if ($restrict_width_in_tag != 0 && $restrict_width_in_tag < $width) {
                $new_width = $restrict_width_in_tag;
                $new_height = $height * ($restrict_width_in_tag / $width);
              }
              else {
                $new_width = $width;
                $new_height = $height;
              }
              if ($restrict_height_in_tag != 0 && $restrict_height_in_tag < $new_height) {
                $new_width = $new_width * ($restrict_height_in_tag / $new_height);
                $new_height = $restrict_height_in_tag;
              }

              $additional_tag_params .= ' style="max-width: ' . $new_width . 'px;" ';

              if ($type == 4 || $type == 13) {
                $media = '<embed allowScriptAccess="never" allowNetworking="none" allowFullScreen="false" $additional_tag_params ';
              }
              else {
                $media = '<img loading="lazy" ' . $additional_tag_params . ' ';
              }
              $media .= 'src="' . $relative_url . '" ';

              $alt_tag = $media_attachment_info['alt_tag'] ?: $file_name;
              if ($width >= POSTED_IMAGE_MAX_WIDTH - 210 || $height >= POSTED_IMAGE_MAX_HEIGHT - 160 || ($restrict_width_in_tag != 0)) {
                $media_attachment_db_class = new MediaAttachmentDB();
                $original_info = $media_attachment_db_class->getOriginalAttachmentInfoFromAttachmentId($media_attachment_info['media_attachment_id']);
                if (is_array($original_info) && $original_info['media_attachment_id'] != $media_attachment_info['media_attachment_id']) {
                  $original_file_name = $original_info['file_name'];
                  $original_relative_path = $original_info['relative_path'];
                  $original_file_dir = UPLOAD_PATH . '/' . $original_info['relative_path'];
                  $original_full_file_path = $original_file_dir . $original_file_name;
                  $original_relative_url = '/uploads/' . $original_relative_path . $original_file_name;
                  if (str_ends_with($original_full_file_path, '.heic')) {
                    $imagick = new \Imagick("{$original_full_file_path}[0]");
                    $imagick->autoOrient();
                    ['width' => $original_width, 'height' => $original_height] = $imagick->getImageGeometry();
                    $imagick->clear();
                  }
                  else {
                    [$original_width, $original_height] = getimagesize($original_full_file_path);
                  }
                  if (strlen($original_file_name) > 27) {
                    $original_file_name = substr($original_file_name, 0, 25) . '...';
                  }
                  // If this is not a thumbnail the link should go to the
                  // original large upload. If it is a thumbnail the image
                  // will usually already be in a link to the news
                  // article.
                  if (!array_key_exists('is_thumbnail', $media_attachment_info)) {
                    $media = '<div class="image-file"><a href="' . $original_relative_url . '" title="original image: ' . $original_file_name . '(' . $original_width . 'x' . $original_height . ')">' . $media;
                    $is_link = TRUE;
                  }
                }
              }
              $class = (!$include_name_info && $restrict_width_in_tag && $restrict_height_in_tag) ? 'list-image' : 'article-image';
              $media .= ' alt="' . htmlspecialchars($alt_tag) . '" class="' . $class . '">';
              if (!empty($is_link)) {
                $media .= '</a></div>';
              }
              if ((!isset($original_info) || $original_info['media_attachment_id'] == $media_attachment_info['media_attachment_id']) &&
              (($restrict_width_in_tag > 0  && $width > $restrict_width_in_tag) || ($restrict_height_in_tag > 0 && $height > $restrict_height_in_tag))
              ) {
                $media = '<div class="image-file"><a class="mediaattachmentrenderer" href="' . $relative_url . '" title="view image by itself">' . $media . '</a></div>';
              }
              if ($include_name_info == 1) {
                if (strlen($file_name) > 27) {
                  $file_name = substr($file_name, 0, 25) . '...';
                }
                $media .= '<div class="media-options">';
                if (isset($original_info) && $original_info['media_attachment_id'] != $media_attachment_info['media_attachment_id']) {
                  if (array_key_exists('is_thumbnail', $media_attachment_info)) {
                    $media .= '</a>';
                  }
                  $media .= '<div class="media-links"><span class="nowrap"><a href="'
                    . $original_relative_url . '" title="original image: ' . $original_file_name . ' (' . $original_width . 'x' . $original_height . ')">original image</a></span> ';
                  $media .= '<span class="filesize nowrap">(' . $original_width . 'x' . $original_height . ')</span></div>';
                }
                $media .= '</div>';
              }
            }
            break;

          case 'application/pdf':

            if (strlen($file_name) > 27) {
              $file_name = substr($file_name, 0, 25) . '...';
            }
            $media = '<div class="image-file">';
            $class = (!$include_name_info && $restrict_width_in_tag && $restrict_height_in_tag) ? 'pdf-link' : '';
            $media .= '<a href="' . $relative_url . '" title="download pdf: ' . $file_name . ' (' . $file_size . ')">';
            $media_attachment_db_class = new MediaAttachmentDB();
            $rendered_pdf_info = $media_attachment_db_class->getRenderedPdfInfo($media_attachment_info['media_attachment_id']);
            if (!is_array($rendered_pdf_info)) {
              $image_util_class->makeImageForPdf($media_attachment_info['media_attachment_id'], $media_attachment_info);
              $rendered_pdf_info = $media_attachment_db_class->getRenderedPdfInfo($media_attachment_info['media_attachment_id']);
            }
            if (is_array($rendered_pdf_info)) {
              $rendered_pdf_file_name = $rendered_pdf_info['file_name'];
              $alt_tag = $rendered_pdf_info['alt_tag'] ?: $rendered_pdf_file_name;
              $rendered_pdf_relative_path = $rendered_pdf_info['relative_path'];
              $rendered_pdf_file_dir = UPLOAD_PATH . '/' . $rendered_pdf_info['relative_path'];
              $rendered_pdf_relative_url = '/uploads/' . $rendered_pdf_relative_path . $rendered_pdf_file_name;
              [$width, $height] = getimagesize($rendered_pdf_file_dir . $rendered_pdf_file_name);
              if ($restrict_width_in_tag != 0 && $restrict_width_in_tag < $width) {
                $new_width = $restrict_width_in_tag;
                $new_height = $height * ($restrict_width_in_tag / $width);
              }
              else {
                $new_width = $width;
                $new_height = $height;
              }
              if ($restrict_height_in_tag != 0 && $restrict_height_in_tag < $new_height) {
                $new_width = $new_width * ($restrict_height_in_tag / $new_height);
                $new_height = $restrict_height_in_tag;
              }
              $additional_tag_params .= ' style="max-width: ' . $new_width . 'px;" ';
              $class = (!$include_name_info && $restrict_width_in_tag && $restrict_height_in_tag) ? 'list-image' : 'article-image';
              $media .= '<img loading="lazy" class="' . $class . '" src="' . $rendered_pdf_relative_url . '" alt="' . htmlspecialchars($alt_tag) . '" ' . $additional_tag_params . '>';
            }

            $media .= '</a></div>';
            $media .= '<div class="media-options">';
            $media .= '<div class="media-links"><span class="nowrap">';
            $media .= '<a href="' . $relative_url . '" title="download pdf: ' . $file_name . ' (' . $file_size . ')">Download PDF</a></span> ';
            $media .= '<span class="filesize nowrap">(' .
              $file_size . ')</span></div>';
            $media .= '</div>';

            break;

          case 'application/smil':
            $media .= 'multimedia: <a href="' . $relative_url . '">SMIL at ';
            $media .= $file_size . '</a>';
            break;

          case 'audio/amr':
          case 'audio/x-m4a':
          case 'audio/mpeg':
          case 'audio/ogg':
          case 'audio/x-mpegurl':
          case 'audio/mpegurl':
          case 'audio/x-scpls':
          case 'audio/x-pn-realaudio':
          case 'audio/x-pn-realaudio-meta':
          case 'audio/x-ms-wma':
          case 'audio/x-wav':
          case 'audio/flac':
            if (strlen($file_name) > 27) {
              $file_name = substr($file_name, 0, 25) . '...';
            }
            $media .= '';
            if ($media_attachment_info['mime_type'] != 'audio/x-mpegurl') {
              $audio = '<audio preload="none" src="' . SERVER_URL . $relative_url . '" controls="controls"></audio>';
              $media .= '<div class="audio-listen">Listen now:</div><div class="audio-file">' . $audio . '</div><div class="media-options"><div class="media-embed">Copy the code below to embed this audio into a web page: <textarea onclick="this.focus();this.select();" rows="1" cols="1">' . htmlspecialchars($audio) . '</textarea></div>
							<div class="media-links"><span class="nowrap"><a class="audio" href="' . $relative_url . '" title="download audio: ' . $file_name . ' (' . $file_size . ')"><!-- <img src="/im/audio-icon_160x24.png" alt="Download audio" width="160" height="24"><br> -->Download Audio</a> (' . $file_size . ')</span> | <span class="nowrap"><a class="embed">Embed Audio</a></span></div></div>';
            }
            $media .= '';
            break;

          case 'video/ogg':
          case 'video/x-ms-wmv':
          case 'video/x-pn-realvideo':
          case 'video/quicktime':
          case 'video/x-msvideo':
          case 'video/avi':
          case 'video/mp2':
          case 'video/mpeg':
          case 'video/x-flv':
          case 'video/3gpp':
          case 'video/mp4':
          case 'video/webm':
            $media_attachment_db_class = new MediaAttachmentDB();
            $image_util = new ImageUtil();
            if ($media_attachment_info['mime_type'] == 'video/mp4' && is_null($media_attachment_info['browser_compat'])) {
              $image_util->getVideoBrowserCompat($media_attachment_info);
            }
            if ($media_attachment_info['mime_type'] != 'video/mp4' || !$media_attachment_info['browser_compat']) {
              $preview = $media_attachment_db_class->getRenderedH264((int) $media_attachment_info['media_attachment_id']);
              if (!$preview) {
                if ($id = $image_util_class->makeH264ForVideo($media_attachment_info)) {
                  $preview = $media_attachment_db_class->getMediaAttachmentInfo($id);
                }
              }
              if ($preview) {
                $original_url = $relative_url;
                $relative_url = '/uploads/' . $preview['relative_path'] . $preview['file_name'];
              }
            }
            $rendered_video_info = $media_attachment_db_class->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
            if (!is_array($rendered_video_info)) {
              $image_util->makeImageForVideo($media_attachment_info['media_attachment_id'], $media_attachment_info);
              $rendered_video_info = $media_attachment_db_class->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
            }
            $thumbnail = ' ';
            // Default video max width is 740px, current content max-width.
            $new_width = 740;
            $poster = '';
            if (is_array($rendered_video_info)) {
              if (!$rendered_video_info['image_width']) {
                // Try to make the thumbnail again if width is empty.
                $media_attachment_db_class->deleteThumbnails($media_attachment_info['media_attachment_id']);
                $image_util->makeImageForVideo($media_attachment_info['media_attachment_id'], $media_attachment_info);
                $rendered_video_info = $media_attachment_db_class->getRenderedVideoInfo($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
              }
              // Video width calculating incorrectly (often at 1.6x actual).
              $new_width = $rendered_video_info['image_width'];
              $new_height = $rendered_video_info['image_height'];
              $aspect_ratio = $rendered_video_info['image_width'] / $rendered_video_info['image_height'];
              $rendered_video_file_name = $rendered_video_info['file_name'];
              $rendered_video_relative_path = $rendered_video_info['relative_path'];
              $poster = SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
              $thumbnail = '<br><span class="video-thumbnail"><img loading="lazy" src="' . $poster . '"></span><br>';
            }

            if (strlen($file_name) > 27) {
              $file_name = substr($file_name, 0, 25) . '...';
            }
            if (empty($media_attachment_info['image_width']) || empty($media_attachment_info['image_height'])) {
              $image_util->getVideoDimensions($media_attachment_info);
            }
            if ($media_attachment_info['image_width'] > $media_attachment_info['image_height']) {
              $new_width = $media_attachment_info['image_width'] >= 370 ? 740 : 444;
            }
            elseif ($media_attachment_info['image_width'] < $media_attachment_info['image_height'] && $media_attachment_info['image_height'] < 370) {
              $new_width *= 0.74;
            }
            $download_url = $original_url ?? $relative_url;
            $video = '<video style="width: ' . $new_width . 'px;" data-aspect-ratio="' . $aspect_ratio . '" preload="none" poster="' . $poster . '" controls><source src="' . SERVER_URL . $relative_url . '" type="video/mp4"><a class="video" href="' . SERVER_URL . $download_url . '" title="download video: ' . $file_name . '">' . $thumbnail . '</a></video>';
            $media .= '<div class="video-file mp4">' . $video . '</div>';
            $media .= '<div class="media-options"><div class="media-embed">
							Copy the code below to embed this movie into a web page:<textarea onclick="this.focus();this.select();">' . htmlspecialchars($video) . '</textarea></div>
							<div class="media-links"><span class="nowrap"><a href="' . SERVER_URL . $download_url . '" title="download video: ' . $file_name . ' (' . $file_size . ')">Download Video</a></span> <span class="filesize nowrap">(' . $file_size . ')</span> | <span class="nowrap"><a class="embed" title="embed this video in your website">Embed Video</a></span></div></div>';
            break;

          case 'video/x-pn-realvideo-meta':
            $media .= 'video: <a href="' . $relative_url . '">
		                 ' . $file_name . '<br>
		                RealVideo ';
            $media .= 'metafile</a>';
            break;

          case 'application/x-bittorrent':
            $media .= 'download: <a href="' . $relative_url . '">
		                ' . $file_name . '<br>
		                BitTorrent ';
            $media .= 'at ' . $file_size . '</a>';
            break;
        }
      }
    }

    return $media;
  }

  /**
   * Gets restrict image size select.
   */
  public function getRestrictImageSizeSelect($select_name) {

    // 225 was 120, 360 was 320.
    $dropdown_array = $this->customRangeByN(225, 360, 40);
    $dropdown_array['0'] = "don't resize";
    // Third argument below sets "selected".
    $select_list = $this->makeSelectForm($select_name, $dropdown_array, 0);

    return $select_list;
  }

  /**
   * Renders file size.
   */
  public function renderFileSize($size) {
    // Returns HTML for a filesize.
    if ($size < 1048576) {
      $size = $size / 1024;
      $size = sprintf('%.1f', $size);
      $size = $size . 'KB';
    }
    else {
      $size = $size / 1048576;
      $size = sprintf('%.1f', $size);
      $size = $size . 'MB';
    }

    return $size;
  }

}
