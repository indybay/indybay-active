<?php

namespace Indybay\Renderer;

/**
 * Written December 2005.
 *
 * Modification Log:
 * 12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

/**
 * Render information related to users.
 */
class UserRenderer extends Renderer {

  /**
   * Text block on edit pages for information on who has been changing things.
   */
  public function renderUserDetailInfo(?array $user_info = NULL) {
    if (!$user_info) {
      return '';
    }

    $result = '<br><span class="user-detail-info">';
    $result .= $user_info['username'];
    // $result .= $user_info['first_name'] . ' ';
    // $result .= $user_info['last_name'] . '<br>';
    // $result .= $user_info['email'] . '<br>';
    // $result .= $user_info['phone'] . '<br>';
    $result .= '</span>';

    return $result;
  }

}
