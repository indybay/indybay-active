<?php

namespace Indybay\Renderer;

/**
 * @file
 * Indybay EventRenderer Class.
 *
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Cache\Cache;
use Indybay\DB\CalendarDB;
use Indybay\DB\FeaturePageDB;
use Indybay\Renderer\Calendar\IndyCalendar;

/**
 * Used to render event details and week views for events.
 *
 * As well as the upcomming events sections on the feature pages.
 */
class EventRenderer extends ArticleRenderer {

  /**
   * Renders HTML for day in week list.
   */
  public function renderHtmlForDayInWeekList($day_datetime) {

    $html = ' ';
    $calendar_db_class = new CalendarDB();
    $start_of_day = date('Y-m-d', $day_datetime) . ' 0:00';
    $end_of_day = date('Y-m-d', $day_datetime) . ' 23:59:59';
    $events_for_day = $calendar_db_class->getEventsBetweenDates($start_of_day, $end_of_day);
    foreach ($events_for_day as $event_info) {
      $html .= $this->renderRowInWeekview($event_info);
    }
    $html .= "\r\n";
    return $html;
  }

  /**
   * Renders row in week view.
   */
  public function renderRowInWeekview($event_info) {

    $calendar_db_class = new CalendarDB();
    $feature_page_db_class = new FeaturePageDB();

    $type_info = $calendar_db_class->getEventTypeInfoForNewsitem($event_info['news_item_id']);
    $keyword = $type_info['keyword'];

    $associated_pages = $feature_page_db_class->getAssociatedPagesInfo($event_info['news_item_id']);

    $html = "\r\n<event class=\"event event-listing\" ";
    $i = 0;
    $topics_and_regions = '';
    foreach ($associated_pages as $page_info) {
      if ($i > 0) {
        $topics_and_regions .= ' | ';
      }
      $topics_and_regions .= $page_info['long_display_name'];

      $cat_ids = $feature_page_db_class->getCategoryIdsForPage($page_info['page_id']);
      foreach ($cat_ids as $cat_id) {
        $html .= ' category_id="' . $cat_id . '"';
      }
      $i = $i + 1;
    }

    $cache_class = new Cache();
    $html .= '>';
    $html .= '<div class="event-listing-title"><a href="';
    $html .= '/newsitems/' . $cache_class->getRelDirFromDate($event_info['creation_timestamp']);
    $html .= $event_info['news_item_id'] . '.php">';
    $html .= htmlspecialchars($event_info['title1']);
    $html .= '</a></div><div class="event-listing-type">';
    if ($keyword != '') {
      $html .= $keyword;
      $html .= '</div><div class="event-listing-categories">';
    }
    if ($topics_and_regions != '') {
      $html .= $topics_and_regions;
      $html .= '</div><div class="event-listing-time">';
    }
    $start_long = $event_info['event_date_timestamp'];
    // $duration = $event_info['event_duration'] + 0;
    // $end_long = $start_long + ($duration * 60 * 60);
    $html .= date('g:i a', $start_long);
    // $html .= ' - ';
    // $html .= date('g:i A', $end_long);
    $html .= '</div></event>';

    return $html;
  }

  /**
   * Filters cached HTML.
   */
  public function filterCachedHtml($html, $topic_id, $region_id, $news_item_status_restriction) {

    $tokenized_string = explode("\r\n", $html);
    $filtered_html = '';
    foreach ($tokenized_string as $line) {
      if (strpos(' ' . $line, '<event') == 1) {
        if ($this->verifyLineInCachedFile($line, $topic_id, $region_id, $news_item_status_restriction) == 1) {
          $filtered_html .= $line;
        }
      }
      else {
        $filtered_html .= $line;
      }
    }

    return $filtered_html;
  }

  /**
   * Verifies line in cached file.
   */
  public function verifyLineInCachedFile($line, $topic_id, $region_id, $news_item_status_restriction) {

    $ret = 1;
    if ($topic_id != 0) {
      if (strpos($line, 'category_id="' . $topic_id . '"') == 0) {
        $ret = 0;
      }
    }

    if ($region_id != 0) {
      if (strpos($line, 'category_id="' . $region_id . '"') == 0) {
        $ret = 0;
      }
    }

    return $ret;
  }

  /**
   * Render Functions.
   */
  public function renderShortMonthViewPrev($date, $datenow, int $topic_id, int $region_id, int $news_item_status_restriction) {

    $calendar = new IndyCalendar($topic_id, $region_id, $news_item_status_restriction);
    if ($date->getMonth() == $datenow->getMonth() ||
    $date->getMonth() == $datenow->getMonth() + 1) {
      $date_to_use = $datenow;
    }
    else {
      $date_to_use = $date;
    }

    $month = $date_to_use->getMonth();
    $year = $date_to_use->getYear();

    $mv = $calendar->getMonthView($month, $year, $date);

    return $mv;

  }

  /**
   * Renders short month view next.
   */
  public function renderShortMonthViewNext($date, $datenow, int $topic_id, int $region_id, int $news_item_status_restriction) {

    $calendar = new IndyCalendar((int) $topic_id, (int) $region_id, (int) $news_item_status_restriction);
    if ($date->getMonth() == $datenow->getMonth() ||
    $date->getMonth() == $datenow->getMonth() + 1) {
      $date_to_use = $datenow;
    }
    else {
      $date_to_use = $date;
    }

    $month = $date_to_use->getMonth();
    $year = $date_to_use->getYear();
    $month = $month + 1;

    $mv = $calendar->getMonthView($month, $year, $date, TRUE);

    return $mv;

  }

  /**
   * Renders week HTML.
   */
  public function renderWeekHtml($date_as_long, $topic_id, $region_id) {

    $sunday = 0;
    $saturday = 6;

    $event_renderer = new EventRenderer();

    $html_for_week = '';
    $tmpday = $sunday;
    $day_datetime = $date_as_long;
    while ($tmpday < $saturday + 1) {
      $html_for_week = $html_for_week . '<div id="week-day-' . $tmpday . '" class="day-events">';
      $html_for_week = $html_for_week . '<div class="day-title">';
      $html_for_week = $html_for_week . '<span class="day-title-day">' . date('D, ', $day_datetime) . '</span>';
      $html_for_week = $html_for_week . '<span class="day-title-dayfull">' . date('l, ', $day_datetime) . '</span>';
      $html_for_week = $html_for_week . '<span class="day-title-date-text">' . date('M d', $day_datetime) . '</span>';
      $html_for_week = $html_for_week . '<span class="day-title-date-textfull">' . date('F d', $day_datetime) . '</span>';
      $html_for_week = $html_for_week . '<span class="day-title-date-num">' . date('n/j', $day_datetime) . '</span>';
      $html_for_week = $html_for_week . '</div>';
      $html_for_week = $html_for_week . $event_renderer->renderHtmlForDayInWeekList($day_datetime, $topic_id, $region_id);
      $html_for_week = $html_for_week . '</div>';
      $day_datetime = $this->addDay($day_datetime);
      $tmpday = $tmpday + 1;
    }

    return $html_for_week;
  }

  /**
   * Adds a day to the timstamp submitted through $timeStamp.
   */
  public function addDay($timeStamp) {
    $tS = $timeStamp;
    $timeStamp = mktime(date('H', $tS),
                             date('i', $tS),
                             date('s', $tS),
                             date('n', $tS),
                             date('j', $tS) + 1,
                             date('Y', $tS));

    return $timeStamp;
  }

}
