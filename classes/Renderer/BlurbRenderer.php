<?php

namespace Indybay\Renderer;

use Indybay\DB\ArticleDB;
use Indybay\DB\BlurbDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\MediaAndFileUtil\UploadUtil;

/**
 * Renders the HTML for a blurb.
 *
 * As it appears in the center column, a single item view, the archive list
 * or the preview page.
 *
 * Written December 2005 - January 2006.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class BlurbRenderer extends NewsItemVersionRenderer {

  /**
   * Used when trying to get a single blurb view for single item view.
   */
  public function getRenderedBlurbHtmlFromNewsItemId($news_item_id) {

    $blurb_db_class = new BlurbDB();
    $feature_page_renderer_class = new FeaturePageRenderer();

    $blurb_info = $blurb_db_class->getBlurbInfoFromNewsItemId($news_item_id);
    $template_name = $feature_page_renderer_class->determineSingleItemTemplate($blurb_info);
    $html = $this->getRenderedBlurbHtml($blurb_info, $template_name);

    return $html;
  }

  /**
   * Used when trying to get a single blurb view for archives.
   */
  public function getRenderedBlurbHtmlForListFromNewsItemId($news_item_id) {

    $blurb_db_class = new BlurbDB();
    $feature_page_renderer_class = new FeaturePageRenderer();

    $blurb_info = $blurb_db_class->getBlurbInfoFromNewsItemId($news_item_id);
    $template_name = $feature_page_renderer_class->determineSingleItemTemplateForList($blurb_info);
    $html = $this->getRenderedBlurbHtml($blurb_info, $template_name);

    return $html;
  }

  /**
   * Renders the HTML for a blurb.
   *
   * As it appears in the center column, a single item view, the archive list
   * or the preview page.
   */
  public function getRenderedBlurbHtml($article_data, $template_name) {

    $new_array = [];
    $new_array = array_merge($new_array, $article_data);

    $icons = '';
    $icon_names = '';
    if (strpos($new_array['text'], 'imc_photo') > 0) {
      $icons .= '<img alt="photo" src="/im/imc_photo.svg" class="mediaicon" width="12" height="12"> ';
      $icon_names .= ' Photos';
    }
    if (strpos($new_array['text'], 'imc_video') > 0) {
      $icons .= '<img alt="video" src="/im/imc_video.svg" class="mediaicon" width="12" height="12"> ';
      if (strlen($icon_names) > 0) {
        $icon_names .= ' & ';
      }
      $icon_names .= ' Video';
    }
    if (strpos($new_array['text'], 'imc_audio') > 0) {
      $icons .= '<img alt="audio" src="/im/imc_audio.svg" class="mediaicon" width="12" height="12"> ';
      if (strlen($icon_names) > 0) {
        $icon_names .= ' & ';
      }
      $icon_names .= ' Audio';
    }
    if (strpos($new_array['text'], 'imc_pdf') > 0) {
      $icons .= '<img alt="PDF" src="/im/imc_pdf.svg" class="mediaicon" width="12" height="12"> ';
      if (strlen($icon_names) > 0) {
        $icon_names .= ' & ';
      }
      $icon_names .= ' PDF';
    }

    if ($new_array['is_text_html']) {
      $new_array['text'] = $new_array['text'];
    }
    else {
      $new_array['text'] = $this->cleanupText($new_array['text'], TRUE);
    }

    if ($new_array['is_summary_html']) {
      $new_array['summary'] = $new_array['summary'];
    }
    else {
      $new_array['summary'] = $this->cleanupText($new_array['summary'], TRUE);
    }

    $created_timestamp = $new_array['creation_timestamp'];
    $modified_timestamp = $new_array['modified_timestamp'];
    $fcreated = date('D M j Y', $created_timestamp);
    $fmodified = date('m/d/y', $modified_timestamp);
    if (date('Y-m-d', $modified_timestamp) == date('Y-m-d', $created_timestamp)) {
      $formatted_date = $fcreated;
    }
    else {
      $formatted_date = $fcreated . ' <span class="up-date">(Updated ';
      $formatted_date .= $fmodified . ')</span>';
    }
    $new_array['formatted_date'] = $formatted_date;
    if (isset($new_array['related_url'])) {
      $new_array['related_link'] = $new_array['related_url'];
    }
    else {
      $new_array['related_link'] = '';
    }
    $new_array['shortened_related_link'] = $this->shortenLinkForDisplay($new_array['related_link']);
    if (strpos($template_name, 'short') > 0) {
      $media_attachment_id = $new_array['thumbnail_media_attachment_id'];
    }
    else {
      $media_attachment_id = $new_array['media_attachment_id'];
    }

    $media_attachment_db = new MediaAttachmentDB();
    if ($media_attachment_id != 0) {
      $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
      if ($media_attachment_info['upload_status_id'] + 0 == 0) {
        $upload_util = new UploadUtil();
        $media_attachment_info = $upload_util->verifyAttachment($media_attachment_info);
      }
      if ($media_attachment_info['upload_status_id'] + 0 != 0 && $media_attachment_info['upload_status_id'] + 0 != 3) {
        $new_array['image_url'] = UPLOAD_URL . $media_attachment_info['relative_path'] . $media_attachment_info['file_name'];
        $new_array['image_width'] = $media_attachment_info['image_width'] ?: '';
        $new_array['image_height'] = $media_attachment_info['image_height'] ?: '';
        $new_array['image_class'] = ($media_attachment_info['image_width'] < 480 && $media_attachment_info['image_height'] < 320) ? 'image-small' : 'image-large';
        $new_array['alt_tag'] = htmlspecialchars($media_attachment_info['alt_tag'] ?? '');
      }
      else {
        $new_array['media'] = 'DEBUG:MISSING Attachment';
        $new_array['alt_tag'] = 'DEBUG:MISSING Attachment';
      }
    }
    else {
      $new_array['media'] = '';
      $new_array['alt_tag'] = '';
    }

    $new_array['SINGLE_ITEM_VIEW_LINK'] = $this->getRelativeWebPathFromItemInfo($article_data);

    if (strpos($template_name, 'short') >= 0) {
      // $feature_page_db_class = new FeaturePageDB();
      $read_more_link = '<a href="' . $new_array['SINGLE_ITEM_VIEW_LINK'] . '">Read More</a>';
      // $pages_with_long_versions_of_blurb=$feature_page_db_class->getPagesWithPushedAutoVersionsOfBlurb($article_data["news_item_id"]);
      // $i=1;
      // $j=count($pages_with_long_versions_of_blurb);
      // foreach($pages_with_long_versions_of_blurb as $page_info){
      // if ($i==1)
      // $read_more_link .= " on Indybay's ";
      // $read_more_link.="<a href=\"/".$page_info["relative_path"]."\">";
      // $read_more_link.=$page_info["long_display_name"]."</a>";
      // if ($i<$j-1)
      // $read_more_link.=", ";
      // if ($i==$j-1)
      // $read_more_link.=" or ";
      // if ($i==$j && $j>1)
      // $read_more_link.=" pages";
      // if ($j==1 && $i==$j)
      // $read_more_link.=" page";
      // $i=$i+1;
      // }
      // $read_more_link.="";.
      if (strlen($icon_names) > 0) {
        $read_more_link = '<div class="readmorelinks">' . $icons . ' ' . $read_more_link . '</div>';
      }
      else {
        $read_more_link = '<div class="readmorelinks">' . $read_more_link . '</div>';
      }

      $new_array['READ_MORE_LINKS'] = $read_more_link;
    }

    $new_array['breaking_news'] = '';
    rsort($new_array['breaking_items']);
    foreach ($new_array['breaking_items'] as $news_item) {
      $article_db_class = new ArticleDB();
      $article_info = $article_db_class->getArticleInfoFromNewsItemId($news_item['news_item_id']);
      $article_renderer_class = new ArticleRenderer();
      $new_array['breaking_news'] .= $article_renderer_class->getRenderedArticleHtml($article_info);
    }
    unset($new_array['breaking_items']);

    foreach ($new_array as $key => $value) {
      $temp_array['TPL_LOCAL_' . strtoupper($key)] = $value;
    }

    // As an optimization, only render page links for single blurb templates.
    if (strpos($template_name, 'single')) {
      $temp_array['TPL_LOCAL_PAGE_LINKS'] = $this->renderPageLinks($new_array['news_item_id']);
    }
    $result_html = $this->render($template_name, $temp_array);
    return $this->htmlPurify($result_html, NULL, TRUE);

  }

  /**
   * Renders the blurb version list as it appears on the blurb edit page.
   */
  public function renderBlurbVersionList($article_list_info) {

    return $this->renderVersionList($article_list_info, 'blurb_edit.php');

  }

}
