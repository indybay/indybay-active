<?php

namespace Indybay\Renderer\Calendar;

/**
 * PHP Calendar Class Version 1.4 (5th March 2001)
 *
 * Copyright David Wilkinson 2000 - 2001. All Rights reserved.
 *
 * This software may be used, modified and distributed freely
 * providing this copyright notice remains intact at the head
 * of the file.
 *
 * This software is freeware. The author accepts no liability for
 * any loss or damages whatsoever incurred directly or indirectly
 * from the use of this script. The author of this software makes
 * no claims as to its fitness for any purpose whatsoever. If you
 * wish to use this software you should first satisfy yourself that
 * it meets your requirements.
 *
 * URL:   http://www.cascade.org.uk/software/php/calendar/
 * Email: davidw@cascade.org.uk.
 */

/**
 * Renders small monthly calendars at the top of the weekly view of events.
 *
 * The Calendar class is a slight modification of a freeware class developed
 * by David Wilkinson.
 * URL:   http://www.cascade.org.uk/software/php/calendar/
 * Email: davidw@cascade.org.uk.
 */
class Calendar {

  /**
   * Constructor for the Calendar class.
   */
  public function __construct() {
  }

  /**
   * Get the array of strings used to label the days of the week.
   *
   * This array contains seven elements, one for each day of the week. The first
   * entry in this array represents Sunday.
   */
  public function getDayNames() {
    return $this->dayNames;
  }

  /**
   * Set the array of strings used to label the days of the week.
   *
   * This array must contain seven elements, one for each day of the week. The
   * first entry in this array represents Sunday.
   */
  public function setDayNames($names) {
    $this->dayNames = $names;
  }

  /**
   * Get the array of strings used to label the months of the year.
   *
   * This array contains twelve elements, one for each month of the year. The
   * first entry in this array represents January.
   */
  public function getMonthNames() {
    return $this->monthNames;
  }

  /**
   * Set the array of strings used to label the months of the year.
   *
   * This array must contain twelve elements, one for each month of the year.
   * The first entry in this array represents January.
   */
  public function setMonthNames($names) {
    $this->monthNames = $names;
  }

  /**
   * Gets the start day of the week.
   *
   * This is the day that appears in the first column of the calendar.
   * Sunday = 0.
   */
  public function getStartDay() {
    return $this->startDay;
  }

  /**
   * Sets the start day of the week.
   *
   * This is the day that appears in the first column of the calendar.
   * Sunday = 0.
   */
  public function setStartDay($day) {
    $this->startDay = $day;
  }

  /**
   * Gets the start month of the year.
   *
   * This is the month that appears first in the year view. January = 1.
   */
  public function getStartMonth() {
    return $this->startMonth;
  }

  /**
   * Sets the start month of the year.
   *
   * This is the month that appears first in the year view. January = 1.
   */
  public function setStartMonth($month) {
    $this->startMonth = $month;
  }

  /**
   * Returns the URL to display a calendar for a given month/year.
   *
   * You must override this method if you want to activate the "forward" and
   * "back" feature of the calendar.
   *
   * Note: If you return an empty string from this function, no navigation link
   * will be displayed. This is the default behaviour.
   *
   * If the calendar is being displayed in "year" view, $month will be set to
   * zero.
   */
  public function getCalendarLink($month, $year) {
    return '';
  }

  /**
   * Returns the URL to link to for a given date.
   *
   * You must override this method if you want to activate the date linking
   * feature of the calendar.
   *
   * Note: If you return an empty string from this function, no navigation link
   * will be displayed. This is the default behaviour.
   */
  public function getDateLink($day, $month, $year) {
    return '';
  }

  /**
   * Returns the HTML for the current month.
   */
  public function getCurrentMonthView() {
    $d = getdate(time());
    return $this->getMonthView($d['mon'], $d['year']);
  }

  /**
   * Returns the HTML for the current year.
   */
  public function getCurrentYearView() {
    $d = getdate(time());
    return $this->getYearView($d['year']);
  }

  /**
   * Returns the HTML for a specified month.
   */
  public function getMonthView($month, $year, $highlight_week, $is_next = NULL) {
    return $this->getMonthHtml($month, $year, $highlight_week, 1, $is_next);
  }

  /**
   * Returns the HTML for a specified year.
   */
  public function getYearView($year) {
    return $this->getYearHtml($year);
  }

  /**
   * Calculates the number of days in a month, taking into account leap years.
   *
   * Private method (should not be called directly).
   */
  public function getDaysInMonth($month, $year) {
    if ($month < 1 || $month > 12) {
      return 0;
    }

    $d = $this->daysInMonth[$month - 1];

    if ($month == 2) {
      // Check for leap year
      // Forget the 4000 rule, I doubt I'll be around then...
      if ($year % 4 == 0) {
        if ($year % 100 == 0) {
          if ($year % 400 == 0) {
            $d = 29;
          }
        }
        else {
          $d = 29;
        }
      }
    }

    return $d;
  }

  /**
   * Generates the HTML for a given month.
   *
   * Private method (should not be called directly).
   */
  public function getMonthHtml($m, $y, $highlight_week, $showYear = 1, $is_next = NULL) {
    $s = '';

    $a = $this->adjustDate($m, $y);
    $month = $a[0];
    $year = $a[1];

    $daysInMonth = $this->getDaysInMonth($month, $year);
    $date = getdate(mktime(12, 0, 0, $month, 1, $year));

    $first = $date['wday'];
    $monthName = $this->monthNames[$month - 1];

    $prev = [NULL, NULL];
    if ($y > $GLOBALS['site_start_year'] - 1) {
      $prev = $this->adjustDate($month - ($is_next ? 2 : 1), $year);
    }
    if ($y < date('Y') + 3) {
      $next = $this->adjustDate($month + ($is_next ? 0 : 1), $year);
    }

    if ($showYear == 1) {
      $prevMonth = $this->getCalendarLink($prev[0], $prev[1]);
      $nextMonth = isset($next) ? $this->getCalendarLink($next[0], $next[1]) : '';
    }
    else {
      $prevMonth = '';
      $nextMonth = '';
    }

    $header = $monthName . (($showYear > 0) ? ' ' . $year : '');

    $s .= "<div class=\"grid grid--7-cols calendar\">\n";
    $s .= '<div class="calendar">' . (($prevMonth == '') ? '' : "<a href=\"$prevMonth\">&lt;&lt;</a>") . "</div>\n";
    $s .= "<div class=\"grid--item-span-5 calendar calendar-header\"><span>$header</span></div>\n";
    $s .= '<div class="calendar">' . (($nextMonth == '') ? '' : "<a href=\"$nextMonth\">&gt;&gt;</a>") . "</div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 1) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 2) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 3) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 4) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 5) % 7] . "</span></div>\n";
    $s .= '<div class="calendar calendar-header"><span>' . $this->dayNames[($this->startDay + 6) % 7] . "</span></div>\n";

    // We need to work out what date to start at so that the first appears in
    // the correct column.
    $d = $this->startDay + 1 - $first;
    while ($d > 1) {
      $d -= 7;
    }

    // Make sure we know when today is, so that we can use a different CSS
    // style.
    $today = getdate(time());

    $temp_date = $highlight_week->clonedate();
    $temp_date->moveForwardDays(7);

    while ($d <= $daysInMonth) {
      if (($month == $highlight_week->getMonth() && $d < $highlight_week->getDay() + 7 && $d >= $highlight_week->getDay()) || ($highlight_week->getMonth() != $temp_date->getMonth() && $month == $temp_date->getMonth() && $d < $temp_date->getDay())) {
        $classHighlightWeek = ' calendarHighlightWeek';
      }
      else {
        $classHighlightWeek = '';
      }
      for ($i = 0; $i < 7; $i++) {
        $class = ($year == $today['year'] && $month == $today['mon'] && $d == $today['mday']) ? 'calendar calendar-today' : 'calendar';

        $s .= "<div class=\"$class $classHighlightWeek\">";

        if ($d > 0 && $d <= $daysInMonth) {
          $topic_id = 0;
          $region_id = 0;
          $link = $this->getDateLink($d, $month, $year, $topic_id, $region_id);
          $s .= (($link == '') ? $d : "<a href=\"$link\">$d</a>");
        }
        else {
          $s .= '&#160;';
        }
        $s .= "</div>\n";
        $d++;
      }
    }

    $s .= "</div><!-- END grid grid--7-cols calendar -->\n";

    return $s;
  }

  /**
   * Generates the HTML for a given year.
   *
   * Private method (should not be called directly).
   */
  public function getYearHtml($year) {
    $s = '';
    $prev = $this->getCalendarLink(0, $year - 1);
    $next = $this->getCalendarLink(0, $year + 1);

    $s .= "<table class=\"calendar\" border=\"0\">\n";
    $s .= '<tr>';
    $s .= '<td align="center" valign="top" align="left">' . (($prev == '') ? '' : "<a href=\"$prev\">&lt;&lt;</a>") . "</td>\n";
    $s .= '<td class="calendar-header" valign="top" align="center">' . (($this->startMonth > 1) ? $year . ' - ' . ($year + 1) : $year) . "</td>\n";
    $s .= '<td align="center" valign="top" align="right">' . (($next == '') ? '' : "<a href=\"$next\">&gt;&gt;</a>") . "</td>\n";
    $s .= "</tr>\n";
    $s .= '<tr>';
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(0 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(1 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(2 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= "</tr>\n";
    $s .= "<tr>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(3 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(4 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(5 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= "</tr>\n";
    $s .= "<tr>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(6 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(7 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(8 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= "</tr>\n";
    $s .= "<tr>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(9 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(10 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= '<td class="calendar" valign="top">' . $this->getMonthHtml(11 + $this->startMonth, $year, NULL, 0) . "</td>\n";
    $s .= "</tr>\n";
    $s .= "</table>\n";

    return $s;
  }

  /**
   * Adjusts dates to allow months > 12 and < 0.
   *
   * Just adjust the years appropriately. e.g. Month 14 of the year 2001 is
   * actually month 2 of year 2002.
   *
   * Private method (should not be called directly).
   */
  public function adjustDate($month, $year) {
    $a = [];
    $a[0] = $month;
    $a[1] = $year;

    while ($a[0] > 12) {
      $a[0] -= 12;
      $a[1]++;
    }

    while ($a[0] <= 0) {
      $a[0] += 12;
      $a[1]--;
    }

    return $a;
  }

  /**
   * The start day of the week. Sunday = 0.
   *
   * @var int
   */
  public $startDay = 0;

  /**
   * The start month of the year. January = 1.
   *
   * @var int
   */
  public $startMonth = 1;

  /**
   * The labels to display for the days of the week, starting with Sunday.
   *
   * @var array
   */
  public $dayNames = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

  /**
   * The labels to display for the months of the year, starting with January.
   *
   * @var array
   */
  public $monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December',
  ];


  /**
   * The number of days in each month, starting with January.
   *
   * @var array
   */
  public $daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

}
