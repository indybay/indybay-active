<?php

namespace Indybay\Renderer\Calendar;

use Indybay\Date;

/**
 * Specialized Calendar for Indymedia that inherits from the Calendar class.
 */
class IndyCalendar extends Calendar {

  /**
   * {@inheritdoc}
   */
  public function __construct(public int $topicId = 0, public int $regionId = 0, public int $newsItemStatusRestriction = 0) {
  }

  /**
   * Returns a link for a given day.
   */
  public function getDateLink($day, $month, $year) {
    $date = new Date();
    $date->setTime(0, 0, $day, $month, $year);
    $day = $date->getDay();
    $month = $date->getMonth();
    $year = $date->getYear();
    if ($year < $GLOBALS['site_start_year'] || $year > date('Y') + 1) {
      return '';
    }
    $news_item_status_restriction = 0;
    return 'event_week.php?day=' . $day . '&month=' . $month . '&year=' . $year . '&' .
                'topic_id=' . $this->topicId . '&region_id=' . $this->regionId . '&' .
                'news_item_status_restriction=' . $news_item_status_restriction;
  }

  /**
   * Returns link to allow navigation by month.
   */
  public function getCalendarLink($month, $year) {
    static $unusedmonthvalue;
    static $isleft;
    // For now disable link
    // $unusedmonthvalue=$month;.
    if ($unusedmonthvalue == $month) {
      return '';
    }
    else {
      $date = new Date();
      if ($isleft) {
        // If ($month==12){
        // $date->setTime(0,0,1,1,$year+1);
        // }else{.
        $date->setTime(0, 0, 1, $month + 1, $year);
        // }.
        $date->moveForwardDays(-1);
      }
      else {
        // If ($month==1){
        // $date->setTime(0,0,1,1,$year+1);
        // }else{.
        $date->setTime(0, 0, 1, $month, $year);
        // }.
        $date->findStartOfWeek();
        if ($month != $date->getMonth()) {
          $date->moveForwardWeeks(1);
        }
      }
      $day1 = $date->getDay();
      $month1 = $date->getMonth();
      $year1 = $date->getYear();
      if ($year1 < $GLOBALS['site_start_year'] || $year1 > date('Y') + 1) {
        return '';
      }
      // Return "or=".$ormonth."un=".$unusedmonthvalue;.
      return 'event_week.php?day=' . $day1 . '&amp;month=' . $month1 . '&amp;year=' . $year1 . '&amp;topic_id=' . $this->topicId . '&amp;region_id=' . $this->regionId . '&amp;news_item_status_restriction=' . $this->newsItemStatusRestriction;
    }
  }

}
