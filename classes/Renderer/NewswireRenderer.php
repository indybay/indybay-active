<?php

namespace Indybay\Renderer;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Cache\NewsItemCache;

/**
 * Used to render the newswires on the feature pages.
 */
class NewswireRenderer extends Renderer {

  /**
   * Renders the hrml for a newswire section.
   */
  public function getNewswireRowsHtml($result_data) {

    $html = '';
    foreach ($result_data as $row) {
      $html .= $this->renderNewswireRow($row);
    }

    return $html;
  }

  /**
   * Format a string containing a count of items.
   *
   * @param int $count
   *   The item count to display.
   * @param string $singular
   *   The string for the singular case. Please make sure it is clear this is
   *   singular, to ease translation (e.g. use "1 new comment" instead of "1
   *   new"). Do not use @count in the singular string.
   * @param string $plural
   *   The string for the plural case. Please make sure it is clear this is
   *   plural, to ease translation. Use @count in place of the item count, as
   *   in "@count new comments".
   *
   * @return string
   *   A translated string.
   */
  public function formatPlural($count, $singular, $plural) {
    if ($count == 1) {
      return $singular;
    }
    else {
      return strtr($plural, ['@count' => 0 + $count]);
    }
  }

  /**
   * Renders a row in the cached newswire file.
   */
  public function renderNewswireRow($row) {

    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_CALENDAR_ICON;
      $row['image_icon_alt'] = 'event icon';
    }
    elseif ($row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_NONE ||
      $row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_TEXT) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_TEXT_ICON;
      $row['image_icon_alt'] = 'article icon';
    }
    elseif ($row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_IMAGE) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_IMAGE_ICON;
      $row['image_icon_alt'] = 'image icon';
    }
    elseif ($row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_AUDIO) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_AUDIO_ICON;
      $row['image_icon_alt'] = 'audio icon';
    }
    elseif ($row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_VIDEO) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_VIDEO_ICON;
      $row['image_icon_alt'] = 'video icon';
    }
    elseif ($row['media_type_grouping_id'] == MEDIA_TYPE_GROUPING_DOCUMENT) {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_DOCUMENT_ICON;
      $row['image_icon_alt'] = 'document icon';
    }
    else {
      $row['image_icon_link'] = MEDIA_TYPE_GROUPING_OTHER_ICON;
      $row['image_icon_alt'] = 'article icon';
    }

    if (0 + $row['num_comments'] > 0) {
      $row['numcomments_section'] = '(' . $this->formatPlural($row['num_comments'], '1 comment', '@count comments') . ')';
    }
    else {
      $row['numcomments_section'] = '';
    }

    $news_item_cache_class = new NewsItemCache();
    $row['news_item_link'] = $news_item_cache_class->getWebCachePathForNewsItemGivenDate($row['news_item_id'], $row['creation_timestamp']);
    $template_name = 'newswire/newswire_row.tpl';
    foreach ($row as $key => $value) {
      $temp_array['TPL_LOCAL_' . strtoupper($key)] = $value;
    }
    if ($row['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $format = date('i', $row['displayed_date_timestamp']) === '00' ? 'D M jS ga' : 'D M jS g:ia';
      $temp_array['TPL_LOCAL_DATE'] = 'Event: ' . date($format, $row['displayed_date_timestamp']);
    }
    else {
      $temp_array['TPL_LOCAL_DATE'] = 'Posted: ' . date('D M jS g:ia', $row['creation_timestamp']);
    }
    $result_html = $this->render($template_name, $temp_array);

    return $result_html;

  }

}
