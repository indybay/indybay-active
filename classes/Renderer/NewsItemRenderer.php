<?php

namespace Indybay\Renderer;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Cache\Cache;
use Indybay\DB\FeaturePageDB;
use Indybay\DB\NewsItemDB;

/**
 * Used for rendering news items.
 */
class NewsItemRenderer extends Renderer {

  /**
   * Gets relative web path from item info.
   */
  public function getRelativeWebPathFromItemInfo($news_item_info) {

    $cache_class = new Cache();
    $append_anchor = '';
    if (!empty($news_item_info['parent_item_id']) && isset($news_item_info['news_item_id']) && $news_item_info['parent_item_id'] != $news_item_info['news_item_id']) {
      $append_anchor = '#' . $news_item_info['news_item_id'];
      $news_item_db_class = new NewsItemDB();
      $news_item_info = $news_item_db_class->getNewsItemInfo($news_item_info['parent_item_id']);
    }
    $created_date = $news_item_info['creation_timestamp'] ?? time();
    $rel_dir = $cache_class->getRelDirFromDate($created_date);
    $news_item_id = $news_item_info['news_item_id'] ?? 0;
    $rel_url = NEWS_REL_PATH . '/' . $rel_dir . $news_item_id . '.php' . $append_anchor;

    return $rel_url;
  }

  /**
   * Renders associated page links for a news item.
   */
  public function renderPageLinks(int|string $news_item_id): string {
    $feature_page_db_class = new FeaturePageDB();
    return $this->render('article/page_links.tpl', ['pages' => $feature_page_db_class->getAssociatedPagesInfo($news_item_id)]);
  }

}
