<?php

namespace Indybay\Renderer;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

/**
 * Parent class for article and blurb renderers.
 */
class NewsItemVersionRenderer extends NewsItemRenderer {

  /**
   * Renders a version list for the article edit page.
   */
  public function renderVersionList($article_list_info, $url) {

    $return = '<div class="grid grid--5-cols version-list">';
    $return .= '<div class="bg-header">VersionID</div>';
    $return .= '<div class="bg-header">Version Date</div>';
    $return .= '<div class="bg-header">User ID</div>';
    $return .= '<div class="bg-header">Username</div>';
    $return .= '<div class="bg-header">Title</div>';
    foreach ($article_list_info as $version_info) {
      $return .= '<div class="attribute bg-grey" data-attr="VersionID:"><a href="' . $url . '?version_id=' . $version_info['news_item_version_id'] . '">';
      $return .= $version_info['news_item_version_id'] . '</a></div>';
      $return .= '<div class="attribute bg-grey" data-attr="Version Date:">' . $version_info['version_creation_date'] . '</div>';
      $return .= '<div class="attribute bg-grey" data-attr="User ID:">' . $version_info['user_id'] . '</div>';
      $return .= '<div class="attribute bg-grey" data-attr="Username:">' . $version_info['username'] . '</div>';
      $return .= '<div class="attribute bg-grey" data-attr="Title:">' . $version_info['title1'] . '</div>';
    }
    $return .= '</div>';

    return $return;
  }

}
