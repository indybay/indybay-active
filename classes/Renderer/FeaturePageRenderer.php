<?php

namespace Indybay\Renderer;

use Indybay\Cache\ArticleCache;
use Indybay\DB\BlurbDB;
use Indybay\DB\FeaturePageDB;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

/**
 * Used to render who feature pages and previes of those pages.
 *
 * Also used to render options that appear on the page edit page.
 */
class FeaturePageRenderer extends Renderer {

  /**
   * Renders the blurbs on a current feature page.
   */
  public function renderPage($blurb_list, $page_info) {

    $blurb_renderer_class = new BlurbRenderer();
    $html = '';
    $template_name = '';
    $previous_template = '';
    $center_column_type_id = $page_info['center_column_type_id'];
    foreach ($blurb_list as $next_blurb) {
      $template_name = $this->determineTemplate($next_blurb, $next_blurb['display_option_id'], $previous_template, $center_column_type_id);
      if (strpos($template_name, 'left') > 0 || strpos($template_name, 'right') > 0) {
        $previous_template = $template_name;
      }
      $html .= $blurb_renderer_class->getRenderedBlurbHtml($next_blurb, $template_name);
    }

    return $html;
  }

  /**
   * Determines the template to use when rendering a blurb.
   */
  public function determineTemplate($next_blurb, $display_option_id, $previous_template, $center_column_type_id) {

    $has_short_image = 0;
    $has_long_image = 0;

    if ($center_column_type_id == CENTER_COLUMN_TYPE_SHORT_ALTERNATING ||
    $center_column_type_id == CENTER_COLUMN_TYPE_SHORT_LEFT ||
    $center_column_type_id == CENTER_COLUMN_TYPE_SHORT_RIGHT
    ) {
      $page_default_size = 2;
    }
    else {
      $page_default_size = 1;
    }
    if ($center_column_type_id == CENTER_COLUMN_TYPE_LONG_LEFT ||
    $center_column_type_id == CENTER_COLUMN_TYPE_SHORT_LEFT
    ) {
      $page_default = 2;
    }
    elseif ($center_column_type_id == CENTER_COLUMN_TYPE_LONG_RIGHT ||
    $center_column_type_id == CENTER_COLUMN_TYPE_SHORT_RIGHT
    ) {
      $page_default = 3;
    }
    else {
      $page_default = 1;
    }

    $prev_left = 0;

    if ($next_blurb['media_attachment_id'] + 0 != 0) {
      $has_long_image = 1;
    }
    if ($next_blurb['thumbnail_media_attachment_id'] + 0 != 0) {
      $has_short_image = 1;
    }

    if (strpos($previous_template, 'left')) {
      $prev_left = 1;
    }

    if ($display_option_id == BLURB_DISPLAY_OPTION_AUTO) {
      if ($page_default_size == 1) {
        $display_option_id = 1;
      }
      elseif ($page_default_size == 2) {
        $display_option_id = 2;
      }
    }

    if ($display_option_id == BLURB_DISPLAY_OPTION_AUTO_LONG) {
      if ($has_long_image == 0 || $page_default == 4) {
        $template_name = 'feature_page/long_version_no_image.tpl';
      }
      elseif ($page_default == 3 ||    ($page_default == 1 && $prev_left == 1)) {
        $template_name = 'feature_page/long_version_image_on_right.tpl';
      }
      elseif ($page_default == 2 || $page_default == 1) {
        $template_name = 'feature_page/long_version_image_on_left.tpl';
      }
    }

    if ($display_option_id == BLURB_DISPLAY_OPTION_AUTO_SHORT) {
      if ($has_short_image == 0 || $page_default == 4) {
        $template_name = 'feature_page/short_version_no_image.tpl';
      }
      elseif ($page_default == 3 ||    ($page_default == 1 && $prev_left == 1)) {
        $template_name = 'feature_page/short_version_image_on_right.tpl';
      }
      elseif ($page_default == 2 || $page_default == 1) {
        $template_name = 'feature_page/short_version_image_on_left.tpl';
      }
    }

    if ($display_option_id == BLURB_DISPLAY_OPTION_NOIMAGE_LONG) {
      $template_name = 'feature_page/long_version_no_image.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_LEFT_LONG) {
      $template_name = 'feature_page/long_version_image_on_left.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_RIGHT_LONG) {
      $template_name = 'feature_page/long_version_image_on_right.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_NOIMAGE_SHORT) {
      $template_name = 'feature_page/short_version_no_image.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_LEFT_SHORT) {
      $template_name = 'feature_page/short_version_image_on_left.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_RIGHT_SHORT) {
      $template_name = 'feature_page/short_version_image_on_right.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_HTML_LONG) {
      $template_name = 'feature_page/long_version_html_only.tpl';
    }
    elseif ($display_option_id == BLURB_DISPLAY_OPTION_HTML_SHORT) {
      $template_name = 'feature_page/short_version_html_only.tpl';
    }

    return $template_name;
  }

  /**
   * Returns the template to use for blurb in preview of short version.
   */
  public function determineSampleShortTemplate($blurb_info) {

    if ($blurb_info['thumbnail_media_attachment_id'] == 0) {
      $ret = 'feature_page/short_version_no_image.tpl';
    }
    else {
      $ret = 'feature_page/short_version_image_on_left.tpl';
    }

    return $ret;
  }

  /**
   * Returns the template to use for a blurb in the single item view.
   */
  public function determineSingleItemTemplate($blurb_info) {

    if ($blurb_info['media_attachment_id'] == 0) {
      $ret = 'feature_page/single_item_version_no_image.tpl';
    }
    else {
      $ret = 'feature_page/single_item_version_image_on_left.tpl';
    }

    return $ret;
  }

  /**
   * Returns the template to use for a blurb in the single item view.
   */
  public function determineSingleItemTemplateForList($blurb_info) {

    if ($blurb_info['media_attachment_id'] == 0) {
      $ret = 'feature_page/short_version_no_image.tpl';
    }
    else {
      $ret = 'feature_page/short_version_image_on_left.tpl';
    }

    return $ret;
  }

  /**
   * This code displays latest headlines at footer of front page.
   *
   * This function seems duplicative of code in
   * include/feature_page/single_item_feature_list.inc.
   */
  public function renderRecentFeatureList($page_id) {
    $feature_page_db_class = new FeaturePageDB();
    $blurb_db_class = new BlurbDB();
    $article_cache_class = new ArticleCache();
    $results = $feature_page_db_class->getRecentBlurbVersionIds();
    $i = 0;
    $previous_titles = [];
    $ret = '';
    foreach ($results as $version_id) {
      $blurb_info = $blurb_db_class->getBlurbInfoFromVersionId($version_id);

      $title2 = $blurb_info['title2'];
      $title1 = $blurb_info['title1'];
      if (trim($title1) == '' || trim($title2) == '') {
        continue;
      }
      if (!isset($previous_titles[$title2])) {
        $pages = $feature_page_db_class->getPagesWithPushedAutoVersionsOfBlurb($blurb_info['news_item_id']);
        if (count($pages) != 0 && !(count($pages) == 1 && $pages[0]['page_id'] == FRONT_PAGE_CATEGORY_ID)) {

          $create_time_formatted = date('m/d/y', $blurb_info['creation_timestamp']);

          $page_link = '';
          $pi = 0;
          $page_link = '';
          foreach ($pages as $page_info) {
            $pi = $pi + 1;
            if ($pi > 1) {
              $page_link .= ' | ';
            }
            $page_link .= '<a href="/' . $page_info['relative_path'] . '">';
            $page_link .= $page_info['short_display_name'];
            $page_link .= '</a>';
          }

          $previous_titles[$title2] = 1;
          $searchlink = $article_cache_class->getWebCachePathForNewsItemGivenDate($blurb_info['news_item_id'], $blurb_info['creation_timestamp']);
          $row = '<span class="archivelink2-item">';
          $row .= '<span class="archivelink2-date">';
          $row .= $create_time_formatted;
          $row .= '</span>';
          $row .= ' <a href="' . $searchlink;
          $row .= '">';
          $row .= htmlspecialchars($blurb_info['title2']);
          $row .= '</a> <span class="archivelink2-categories">';
          $row .= $page_link;
          $row .= '</span></span>';
          $ret .= $row;
          $i = $i + 1;
          if ($i > 10) {
            break;
          }
        }

      }
    }
    return $ret;
  }

}
