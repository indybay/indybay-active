<?php

namespace Indybay\Renderer;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\DB\ArticleDB;
use Indybay\DB\CalendarDB;
use Indybay\DB\MediaAttachmentDB;
use Indybay\MediaAndFileUtil\UploadUtil;

/**
 * Article renderer renders article info.
 *
 * For when it appears in lists, or in the cached files that are generated by
 * the Cache classes.
 */
class ArticleRenderer extends NewsItemVersionRenderer {

  /**
   * Generates HTML stored in content file for a post, comment or event.
   *
   * If the article has an attachment the renderer for the attachment may
   * try to find the old location of the attachment and move it or download it
   * if it doesnt exist in the expected file location.
   */
  public function getRenderedArticleHtml($article_data, $stripslashes = FALSE) {

    $new_array = [];
    $new_array = array_merge($new_array, $article_data);

    $trusted = (isset($article_data['news_item_status_id']) && $article_data['news_item_status_id'] != NEWS_ITEM_STATUS_ID_NEW && $article_data['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN && $article_data['news_item_status_id'] != NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN && $article_data['news_item_status_id'] != NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN) ? TRUE : FALSE;

    if ($new_array['is_text_html']) {
      $new_array['text'] = $this->htmlPurify($new_array['text']);
    }
    else {
      $new_array['text'] = $this->cleanupText($new_array['text'], $trusted);
    }

    if (isset($new_array['is_summary_html']) && $new_array['is_summary_html'] == 1) {
      $new_array['summary'] = $this->htmlPurify($new_array['summary']);
    }
    else {
      $new_array['summary'] = $this->cleanupText($new_array['summary'], $trusted);
    }

    $new_array['more_info_label'] = empty($new_array['related_url']) ? '' : 'For more information: ';
    $new_array['related_url'] = $this->formatLink($new_array['related_url']);
    $new_array['shortened_related_link'] = $this->shortenLinkForDisplay($new_array['related_url']);

    $new_array['title1'] = htmlspecialchars($new_array['title1']);
    if (isset($new_array['title2'])) {
      $new_array['title2'] = htmlspecialchars($new_array['title2']);
    }
    else {
      $new_array['title2'] = '';
    }
    $new_array['displayed_author_name'] = htmlspecialchars($new_array['displayed_author_name']);
    $new_array['email'] = $this->cleanupText($new_array['email'], $trusted);

    if ($new_array['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $calendar_db_class = new CalendarDB();
      if (isset($new_array['news_item_id'])) {
        $event_type_info = $calendar_db_class->getEventTypeInfoForNewsitem($new_array['news_item_id']);
      }
      if (isset($event_type_info)) {
        $new_array['keyword_name'] = $event_type_info['keyword'];
      }
      else {
        $new_array['keyword_name'] = 'No type given';
        if (!empty($article_data['event_type_id'])) {
          $keyword_list = $calendar_db_class->getEventTypeForSelectList();
          if (isset($keyword_list[$article_data['event_type_id']])) {
            $new_array['keyword_name'] = $keyword_list[$article_data['event_type_id']];
          }
        }
      }
      if ($new_array['display_contact_info'] == 1) {
        $new_array['contact_info_rows'] = '';
        if (!empty($new_array['displayed_author_name'])) {
          // Displayed author name is HTML-encoded above.
          $new_array['contact_info_rows'] .= '<div class="first-col">Organizer/Author:</div><div>' . $new_array['displayed_author_name'] . '</div>';
        }
        if (!empty($new_array['email'])) {
          // Email address is HTML-encoded above.
          $new_array['contact_info_rows'] .= '<div class="first-col">Email:</div><div>' . $new_array['email'] . '</div>';
        }
        if (!empty($new_array['phone'])) {
          $new_array['contact_info_rows'] .= '<div class="first-col">Phone:</div><div>' . htmlspecialchars($new_array['phone']) . '</div>';
        }
        if (!empty($new_array['address'])) {
          $new_array['contact_info_rows'] .= '<div class="first-col">Address:</div><div>' . htmlspecialchars($new_array['address']) . '</td></tr>';
        }
      }
      else {
        $new_array['contact_info_rows'] = '
				<div class="first-col">Organizer/Author:</div><div>' . $new_array['displayed_author_name'] . '</div>';
      }
      if (isset($article_data['is_preview']) && $article_data['is_preview'] + 0 == 1) {
        $new_array['hide_link_for_preview1'] = '<!-- ';
        $new_array['hide_link_for_preview2'] = '-->';
      }
      else {
        $new_array['hide_link_for_preview1'] = '';
        $new_array['hide_link_for_preview2'] = '';
      }
    }
    else {
      if ($new_array['display_contact_info'] == 1 && strlen(trim($new_array['email'])) > 0) {
        $new_array['email'] = '(' . $new_array['email'] . ')';
      }
      else {
        $new_array['email'] = '';
      }
    }

    if (isset($new_array['media_attachment_id'])) {
      $media_attachment_id = $new_array['media_attachment_id'] + 0;
    }
    else {
      $media_attachment_id = 0;
    }

    $media_attachment_db = new MediaAttachmentDB();
    $media_attachment_renderer = new MediaAttachmentRenderer();
    if ($media_attachment_id != 0) {
      $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
      if (isset($media_attachment_info) && $media_attachment_info['media_attachment_id'] > 0) {
        $new_array['media'] = $media_attachment_renderer->renderAttachment($media_attachment_info);
      }
      else {
        $new_array['media'] = '';
      }
      if (isset($article_data['news_item_status_id']) && ($article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED ||
      $article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED)) {
        $upload_util = new UploadUtil();
        $upload_util->generateSecondaryFilesOnChange($article_data);
      }
      $new_array['nomedia1'] = '';
      $new_array['nomedia2'] = '';
    }
    else {
      $new_array['media'] = '';
      $new_array['nomedia1'] = '<!--';
      $new_array['nomedia2'] = '-->';
    }
    if (!array_key_exists('is_preview', $article_data)) {
      $article_data['is_preview'] = 0;
    }
    if (isset($article_data['news_item_type_id']) && $article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_POST) {
      if (isset($article_data['news_item_status_id']) && $article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN && $article_data['is_preview'] + 0 != 1) {
        $template_name = 'article/hidden_post_main_section.tpl';
      }
      else {
        $template_name = 'article/post_main_section.tpl';
      }
    }
    elseif ($article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_ATTACHMENT) {
      if ($article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN  && $article_data['is_preview'] + 0 != 1) {
        $template_name = 'article/post_hidden_child_section.tpl';
      }
      else {
        $template_name = 'article/post_attachment_section.tpl';
      }
    }
    elseif ($article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_COMMENT) {
      if (isset($article_data['news_item_status_id']) && $article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN && $article_data['is_preview'] + 0 != 1) {
        $template_name = 'article/post_hidden_child_section.tpl';
      }
      else {
        $template_name = 'article/post_comment_section.tpl';
      }
    }
    elseif ($article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      if (isset($article_data['news_item_status_id']) && $article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN  && $article_data['is_preview'] + 0 != 1) {
        $template_name = 'article/hidden_post_main_section.tpl';
      }
      else {
        $template_name = 'calendar/calendar_main_section.tpl';
      }
    }
    elseif ($article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BREAKING) {
      if ($article_data['news_item_status_id'] == NEWS_ITEM_STATUS_ID_HIDDEN  && $article_data['is_preview'] + 0 != 1) {
        $template_name = 'article/post_hidden_breaking_section.tpl';
      }
      else {
        $template_name = 'article/post_breaking_section.tpl';
      }
    }
    elseif ($article_data['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT_LOCATION) {
      $template_name = 'calendar/calendar_additional_location_section.tpl';
    }

    $temp_array = [];
    foreach ($new_array as $key => $value) {
      $temp_array['TPL_LOCAL_' . strtoupper($key)] = $value;
    }
    if (array_key_exists('is_preview', $article_data) && $article_data['is_preview'] == 1
    && isset($article_data['displayed_date_day'])) {
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_DATE'] =
        $article_data['displayed_date_month'] . '/' . $article_data['displayed_date_day'] . '/' . $article_data['displayed_date_year'];
      $temp_array['TPL_LOCAL_DISPLAYED_DAY'] = $article_data['displayed_date_day'];
      $temp_array['TPL_LOCAL_DISPLAYED_MONTH'] = $article_data['displayed_date_month'];
      $temp_array['TPL_LOCAL_DISPLAYED_YEAR'] = $article_data['displayed_date_year'];
      $starthour = $article_data['displayed_date_hour'];
      if (isset($article_data['displayed_date_ampm'])) {
        if ($article_data['displayed_date_ampm'] == 'PM') {
          $starthour = $starthour + 12;
        }
        elseif ($starthour == 12) {
          $starthour = 0;
        }
        if ($starthour == 24) {
          $starthour = 12;
        }
      }
      $stime = mktime($starthour, $article_data['displayed_date_minute'], 0, $temp_array['TPL_LOCAL_DISPLAYED_DAY'], $temp_array['TPL_LOCAL_DISPLAYED_MONTH'], $temp_array['TPL_LOCAL_DISPLAYED_YEAR']);
      $temp_array['TPL_LOCAL_ISO_START_TIME'] = date(\DateTime::ISO8601, $stime);
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_START_TIME'] =
                   date('g:i A', $stime);
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_END_TIME'] =
                   date('g:i A', $stime + 60 * 60 * $article_data['event_duration']);
    }
    elseif (isset($temp_array['TPL_LOCAL_DISPLAYED_TIMESTAMP'])) {
      $displayed_date_timestamp = $temp_array['TPL_LOCAL_DISPLAYED_TIMESTAMP'];
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_DATE'] =
                   date('l, F d, Y', $displayed_date_timestamp);
      $temp_array['TPL_LOCAL_DISPLAYED_DAY'] =
                   date('j', $displayed_date_timestamp);
      $temp_array['TPL_LOCAL_DISPLAYED_MONTH'] =
                   date('n', $displayed_date_timestamp);
      $temp_array['TPL_LOCAL_DISPLAYED_YEAR'] =
                   date('Y', $displayed_date_timestamp);
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_START_TIME'] =
                   date('g:i A', $displayed_date_timestamp);
      $temp_array['TPL_LOCAL_ISO_START_TIME'] = date(\DateTime::ISO8601, $displayed_date_timestamp);
      $end_timestamp = $displayed_date_timestamp + 60 * 60 * $temp_array['TPL_LOCAL_EVENT_DURATION'];
      $temp_array['TPL_LOCAL_FORMATTED_DISPLAYED_END_TIME'] =
                   date('g:i A', $end_timestamp);
      $temp_array['TPL_LOCAL_ISO_END_TIME'] = date(\DateTime::ISO8601, $end_timestamp);
    }
    $temp_array['TPL_LOCAL_ARTICLE_URL'] = SERVER_URL . $this->getRelativeWebPathFromItemInfo($article_data);
    if (isset($new_array['news_item_id'])) {
      $temp_array['TPL_LOCAL_PAGE_LINKS'] = $this->renderPageLinks($new_array['news_item_id']);
    }

    $result_html = $this->render($template_name, $temp_array);

    return $result_html;

  }

  /**
   * Renders HTML for comment box that appears at bottom of classified posts.
   */
  public function getRenderedCommentBoxHtml($news_item_id, $directory) {

    $article_db_class = new ArticleDB();
    $comment_id_list = $article_db_class->getCommentIds($news_item_id);
    $comment_id_list = array_reverse($comment_id_list);
    $i = 0;
    $latest_comments_rows = '';
    foreach ($comment_id_list as $comment_id) {

      $row_info = $article_db_class->getArticleInfoFromNewsItemId($comment_id);
      if ($row_info['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN &&
      $row_info['news_item_status_id'] != NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN) {
        $i = $i + 1;
        $latest_comments_rows .= $this->formatLatestCommentRow($directory, $news_item_id, $row_info);
        if ($i == 10) {
          break;
        }
      }
    }
    if ($i == 0) {
      $result_html = '';
    }
    else {
      $template_name = 'article/comment_box.tpl';
      $temp_array = [];
      $temp_array['TPL_LOCAL_LATEST_COMMENT_LINKS'] = $latest_comments_rows;
      $temp_array['TPL_LOCAL_NEWS_ITEM_ID'] = $news_item_id;
      $result_html = $this->render($template_name, $temp_array);

    }
    return $result_html;
  }

  /**
   * Renders latest comment row.
   */
  public function formatLatestCommentRow($directory, $news_item_id, $row_info) {

    $ret = '<div class="comment_list_item comment_list_title"><a href="';
    $ret .= $this->formatCommentLink($news_item_id, $row_info['news_item_id']);
    $ret .= '">' . $row_info['title1'];
    // Original with linking author and date
    // $ret.="</a></td><td><a href=\"";
    // $ret.=$this->formatCommentLink($news_item_id,
    // $row_info["news_item_id"]);
    // $ret.="\">".$row_info["displayed_author_name"]."</a></td><td><a href=\"";
    // $ret.=$this->formatCommentLink($news_item_id,
    // $row_info["news_item_id"]);
    // $ret.="\">".$row_info["created"]."</a></td></tr>";
    // below, new without linking author and date.
    $ret .= '</a></div><div class="comment_list_item comment_list_author">';
    $ret .= $row_info['displayed_author_name'] . '</div><div class="comment_list_item comment_list_date">';
    $ret .= $row_info['created'] . '</div>';

    return $ret;
  }

  /**
   * Returns comment URL.
   */
  public function formatCommentLink($parent_item_id, $comment_id) {
    return $parent_item_id . '.php?show_comments=1#' . $comment_id;
  }

  /**
   * Finds recent duplicate nonhidden URLs by title.
   */
  public function findRecentDuplicateNonhiddenUrlsByTitle($title, $old_id) {

    $article_db_class = new ArticleDB();
    $article_info = $article_db_class->findRecentDuplicateNonhiddenVersionsByTitle($title, $old_id);

    if (!$article_info || $article_info['news_item_status_id'] == 17) {
      return '';
    }

    return $this->getRelativeWebPathFromItemInfo($article_info);
  }

}
