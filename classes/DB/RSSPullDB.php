<?php

namespace Indybay\DB;

/**
 * Provides DB access related to pulling RSS feeds onto the site.
 *
 * Written July 2007.
 */
class RSSPullDB extends DB {

  /**
   * Get list of feeds.
   */
  public function getFeedList() {

    $query = 'select * from inbound_rss_feeds order by name';
    $result = $this->query($query);

    return $result;
  }

  /**
   * Gets feed list for default status.
   */
  public function getFeedListForDefaultStatus($status_id) {

    $status_id = (int) $status_id;
    if ($status_id != NEWS_ITEM_STATUS_ID_OTHER && $status_id != NEWS_ITEM_STATUS_ID_NEW && $status_id != 0) {
      $query = 'select * from inbound_rss_feeds where default_status_id=' . $status_id . ' order by name';
    }
    else {
      $query = 'select * from inbound_rss_feeds where default_status_id=0 or default_status_id=' . NEWS_ITEM_STATUS_ID_OTHER . ' or default_status_id=' . NEWS_ITEM_STATUS_ID_NEW . ' order by name';
    }
    $result = $this->query($query);

    return $result;
  }

  /**
   * Get list of feed items from temp table.
   */
  public function getItemList() {

    $sql = 'select * from inbound_rss_feed_items';
    $result = $this->query($sql);

    return $result;
  }

  /**
   * Get list of feed items from temp table.
   */
  public function getItemListForFeed($feed_id) {

    $feed_id = (int) $feed_id;
    $query = 'select *, unix_timestamp(creation_date) as creation_date_timestamp  from inbound_rss_feed_items ';
    if ($feed_id != 0) {
      $query .= 'where rss_feed_id=' . $feed_id;
    }
    $query .= ' order by creation_date desc , rss_item_id desc';
    $result = $this->query($query);

    return $result;
  }

  /**
   * Get list of feed items from temp table.
   */
  public function getItemListByFeedStatus($status_id) {

    $status_id = (int) $status_id;
    $query = 'select fi.*, unix_timestamp(fi.creation_date) as creation_date_timestamp  from inbound_rss_feed_items fi, ';
    $query .= ' inbound_rss_feeds f where f.rss_feed_id=fi.rss_feed_id and f.default_status_id=' . $status_id;
    $query .= ' order by creation_date desc , rss_item_id desc';
    $result = $this->query($query);

    return $result;
  }

  /**
   * Get full info for a feed item.
   */
  public function getItemInfo($feed_item_id) {

    $feed_item_id = (int) $feed_item_id;
    $query = 'select *, unix_timestamp(creation_date) as creation_date_timestamp from inbound_rss_feed_items where rss_item_id=' . $feed_item_id;
    $result_list = $this->query($query);
    if (!is_array($result_list) || count($result_list) == 0) {
      echo 'get_item_info:unable to find feed item # ' . $feed_item_id . '<br>';
      $result = '';
    }
    else {
      $result = array_pop($result_list);
    }

    return $result;
  }

  /**
   * Get full info for a feed item.
   */
  public function getFeedInfo($feed_id) {

    $feed_id = (int) $feed_id;
    $query = 'select * from inbound_rss_feeds where rss_feed_id=' . $feed_id;
    $result_list = $this->query($query);
    if (!is_array($result_list) || count($result_list) == 0) {
      echo 'get_feed_info:unable to find feed # ' . $feed_id . '<br>';
      $result = '';
    }
    else {
      $result = array_pop($result_list);
    }

    return $result;
  }

  /**
   * Gets RSS item ID from related URL and feed ID.
   */
  public function getRssItemIdFromRelatedUrlAndFeedId($related_url, $feed_id) {
    $feed_id = (int) $feed_id;
    $query = "select rss_item_id from inbound_rss_feed_items where related_url='" .
    $this->prepareString($related_url);
    $query .= "' and rss_feed_id=" . $feed_id;

    $rss_item_ids = $this->singleColumnQuery($query);
    if (is_array($rss_item_ids)) {
      return array_pop($rss_item_ids);
    }
    return 0;
  }

  /**
   * Add item.
   */
  public function addItem($item_info) {
    if (!isset($item_info['external_unique_id'])) {
      $item_info['external_unique_id'] = '';
    }

    $query = 'insert into inbound_rss_feed_items (is_published,rss_feed_id, external_unique_id, title, summary, author, text,categories,news_item_status_id,related_url,creation_date, pull_date) values ' .
            '(0,' . (int) $item_info['rss_feed_id'] . ", '"
            . $this->prepareString($item_info['external_unique_id']) . "', '"
            . $this->prepareString($item_info['title']) . "', '"
            . $this->prepareString($item_info['summary']) . "', '"
            . $this->prepareString($item_info['author']) . "', '"
            . $this->prepareString($item_info['text']) . "', '"
            . $this->prepareString($item_info['categories']) . "', "
            . (int) $item_info['news_item_status_id'] . ", '"
            . $this->prepareString($item_info['related_url']) . "', '"
            . $this->prepareString($item_info['creation_date']) . "', Now()) ";
    $rss_item_id = $this->executeStatementReturnAutokey($query);

    return $rss_item_id;
  }

  /**
   * Add feed.
   */
  public function addFeed($feed_info) {

    $query = 'insert into inbound_rss_feeds (name, url, feed_format_id, author_template,
    	 	summary_template, text_template,  text_scrape_start, text_scrape_end,summary_scrape_start,
    	  	summary_scrape_end,author_scrape_start, author_scrape_end,date_scrape_start,
    	   	date_scrape_end,title_scrape_start, title_scrape_end,  replace_url,
    	    replace_url_with,  restrict_urls, default_region_id, default_topic_id, default_status_id) values ' .
            "('" . $this->prepareString($feed_info['name']) . "', '"
    . $this->prepareString($feed_info['url']) . "', "
            . (int) $feed_info['feed_format_id'] . ", '"
            . $this->prepareString($feed_info['author_template']) . "', '"
            . $this->prepareString($feed_info['summary_template']) . "', '"
            . $this->prepareString($feed_info['text_template']) . "', '"
            . $this->prepareString($feed_info['text_scrape_start']) . "', '"
            . $this->prepareString($feed_info['text_scrape_end']) . "', '"
            . $this->prepareString($feed_info['summary_scrape_start']) . "', '"
            . $this->prepareString($feed_info['summary_scrape_end']) . "', '"
            . $this->prepareString($feed_info['author_scrape_start']) . "', '"
            . $this->prepareString($feed_info['author_scrape_end']) . "', '"
            . $this->prepareString($feed_info['date_scrape_start']) . "', '"
            . $this->prepareString($feed_info['date_scrape_end']) . "', '"
            . $this->prepareString($feed_info['title_scrape_start']) . "', '"
            . $this->prepareString($feed_info['title_scrape_end']) . "', '"
            . $this->prepareString($feed_info['replace_url']) . "', '"
            . $this->prepareString($feed_info['replace_url_with']) . "', '"
            . $this->prepareString($feed_info['restrict_urls']) . "', "
            . (int) $feed_info['default_region_id'] . ', '
            . (int) $feed_info['default_topic_id'] . ', '
            . (int) $feed_info['default_status_id'] . ') ';
    $rss_feed_id = $this->executeStatementReturnAutokey($query);

    return $rss_feed_id;
  }

  /**
   * Add feed.
   */
  public function updateFeed($feed_info) {

    $query = "update inbound_rss_feeds set name='" . $this->prepareString($feed_info['name']) . "', " .
            "url='" . $this->prepareString($feed_info['url']) . "', " .
            "author_template='" . $this->prepareString($feed_info['author_template']) . "', " .
            'feed_format_id=' . $this->prepareString($feed_info['feed_format_id']) . ', ' .
            "summary_template='" . $this->prepareString($feed_info['summary_template']) . "', " .
            "text_template='" . $this->prepareString($feed_info['text_template']) . "', " .
            "text_scrape_start='" . $this->prepareString($feed_info['text_scrape_start']) . "', " .
            "text_scrape_end='" . $this->prepareString($feed_info['text_scrape_end']) . "', " .
            "summary_scrape_start='" . $this->prepareString($feed_info['summary_scrape_start']) . "', " .
            "summary_scrape_end='" . $this->prepareString($feed_info['summary_scrape_end']) . "', " .
            "author_scrape_start='" . $this->prepareString($feed_info['author_scrape_start']) . "', " .
            "author_scrape_end='" . $this->prepareString($feed_info['author_scrape_end']) . "', " .
            "date_scrape_start='" . $this->prepareString($feed_info['date_scrape_start']) . "', " .
            "date_scrape_end='" . $this->prepareString($feed_info['date_scrape_end']) . "', " .
            "title_scrape_start='" . $this->prepareString($feed_info['title_scrape_start']) . "', " .
            "title_scrape_end='" . $this->prepareString($feed_info['title_scrape_end']) . "', " .
            "replace_url='" . $this->prepareString($feed_info['replace_url']) . "', " .
            "replace_url_with='" . $this->prepareString($feed_info['replace_url_with']) . "', " .
            "restrict_urls='" . $this->prepareString($feed_info['restrict_urls']) . "', " .
            'default_region_id=' . (int) $feed_info['default_region_id'] . ', ' .
            'default_topic_id=' . (int) $feed_info['default_topic_id'] . ', ' .
            'default_status_id=' . (int) $feed_info['default_status_id'] .
            ' where rss_feed_id=' . (int) $feed_info['rss_feed_id'];
    $this->executeStatement($query);

  }

  /**
   * Add item.
   */
  public function updateItem($item_info, $is_not_from_post = FALSE) {

    if ($is_not_from_post) {
      $item_info['title'] = $this->prepareString($item_info['title']);
      $item_info['summary'] = $this->prepareString($item_info['summary']);
      $item_info['author'] = $this->prepareString($item_info['author']);
      $item_info['text'] = $this->prepareString($item_info['text']);
      $item_info['related_url'] = $this->prepareString($item_info['related_url']);
    }
    else {
      $item_info['title'] = $this->prepareString($item_info['title']);
      $item_info['summary'] = $this->prepareString($item_info['summary']);
      $item_info['author'] = $this->prepareString($item_info['author']);
      $item_info['text'] = $this->prepareString($item_info['text']);
      $item_info['related_url'] = $this->prepareString($item_info['related_url']);
    }

    $query = "update inbound_rss_feed_items set title='" . $item_info['title'] . "', " .
            "summary='" . $item_info['summary'] . "', author='" . $item_info['author'] . "', ";
    if (isset($item_info['creation_date'])) {
      $creation_date_timestamp = strtotime($item_info['creation_date']);
      if ($creation_date_timestamp > time() - 60 * 60 * 24 * 1000) {
        $query .= "creation_date='" . date('Y-m-d H:i', $creation_date_timestamp) . "', ";
      }
    }
    $query .= "text='" . $item_info['text'] . "', categories='" . $this->prepareString($item_info['categories']) . "', " .
            'news_item_status_id=' . (int) $item_info['news_item_status_id'] . ', ' .
            "related_url='" . $this->prepareString($item_info['related_url']) . "' " .
            'where rss_item_id=' . (int) $item_info['rss_item_id'];
    $this->executeStatement($query);

  }

  /**
   * Sets item to published.
   */
  public function setItemToPublished($rss_item_id, $published_news_item_id) {

    $sql = 'update inbound_rss_feed_items set is_published=1, published_news_item_id=' . (int) $published_news_item_id
    . ' where rss_item_id=' . (int) $rss_item_id;
    $this->executeStatement($sql);

  }

  /**
   * Update feed pull time feed.
   */
  public function updateFeedPullTimeAndDuration($feed_id, $last_pull_duration_secs) {

    $query = 'update inbound_rss_feeds set last_pull_date=Now(), ' .
            'last_pull_duration_usecs=' . (int) $last_pull_duration_secs .
            ' where rss_feed_id=' . (int) $feed_id;
    $this->executeStatement($query);

  }

  /**
   * Update item.
   */
  public function deleteFeed($feed_id) {

    $query = 'delete from inbound_rss_feeds where rss_feed_id=' . (int) $feed_id;
    $this->executeStatement($query);

  }

  /**
   * Add feed.
   */
  public function deleteItem($item_id) {

    $query = 'delete from inbound_rss_feed_items where rss_item_id=' . (int) $item_id;
    return $this->executeStatement($query);
  }

  /**
   * Add feed.
   */
  public function deleteAllItemsForFeed($feed_id) {

    $query = 'delete from inbound_rss_feed_items where rss_feed_id=' . (int) $feed_id;
    $this->executeStatement($query);

  }

  /**
   * Add feed.
   */
  public function deleteOldItems($feed_id) {

    $query = 'delete from inbound_rss_feed_items where rss_feed_id=' . (int) $feed_id;
    $query .= ' and DATEDIFF(Now(),creation_date)>5 and DATEDIFF(Now(),creation_date)<4000';
    $this->executeStatement($query);

  }

  /**
   * Updates item if duplicate.
   */
  public function updateIfDuplicate($rss_item_info) {

    if ($rss_item_info['related_url'] != '' && strlen($rss_item_info['title']) > 7) {
      if (!isset($GLOBALS['old_max_news_item_id']) || $GLOBALS['old_max_news_item_id'] + 0 == 0) {
        $query = 'select max(news_item_id) from news_item';
        $rss_item_ids = $this->singleColumnQuery($query);
        $max_id = array_pop($rss_item_ids);
        $GLOBALS['old_max_news_item_id'] = $max_id;
      }
      else {
        $max_id = $GLOBALS['old_max_news_item_id'] + 0;
      }

      $query = "select news_item_id from news_item_version where (related_url='" .
                    $this->prepareString($rss_item_info['related_url']) . "' or title1='" . $this->prepareString($rss_item_info['title']) . "' or " .
                    " summary like '%" . $this->prepareString($rss_item_info['related_url']) . "%' or text like '%" . $this->prepareString($rss_item_info['related_url']) . "%') and news_item_id>" . ($max_id - 6000);

      $result_list = $this->singleColumnQuery($query);
      if (!is_array($result_list) || count($result_list) == 0) {
        return;
      }
      else {
        $news_item_id = array_pop($result_list);
        $this->setItemToPublished($rss_item_info['rss_item_id'], $news_item_id);
      }
    }

  }

}
