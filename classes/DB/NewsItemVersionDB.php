<?php

namespace Indybay\DB;

/**
 * Written December 2005.
 *
 * DB class that deals with the news item version table
 * that contains most data related to posts, blurbs, events....
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class NewsItemVersionDB extends NewsItemDB {

  /**
   * Checks to see if a news item version has changed and if it has update it.
   */
  public function addNewsItemVersionIfChanged($news_item_id, $news_item_version_map) {

    $news_item_id = (int) $news_item_id;
    $title1 = $news_item_version_map['title1'];
    if (isset($news_item_version_map['title2'])) {
      $title2 = $news_item_version_map['title2'];
    }
    else {
      $title2 = '';
    }
    if (isset($news_item_version_map['displayed_author_name'])) {
      $displayed_author_name = $news_item_version_map['displayed_author_name'];
    }
    else {
      $displayed_author_name = '';
    }
    if (isset($news_item_version_map['email'])) {
      $email = $news_item_version_map['email'];
    }
    else {
      $email = '';
    }
    if (isset($news_item_version_map['phone'])) {
      $phone = $news_item_version_map['phone'];
    }
    else {
      $phone = '';
    }
    if (isset($news_item_version_map['address'])) {
      $address = $news_item_version_map['address'];
    }
    else {
      $address = '';
    }
    if (isset($news_item_version_map['summary'])) {
      $summary = $news_item_version_map['summary'];
    }
    else {
      $summary = '';
    }
    if (isset($news_item_version_map['is_summary_html'])) {
      $is_summary_html = $news_item_version_map['is_summary_html'] + 0;
    }
    else {
      $is_summary_html = 0;
    }
    if (isset($news_item_version_map['media_attachment_id'])) {
      $media_attachment_id = $news_item_version_map['media_attachment_id'] + 0;
    }
    else {
      $media_attachment_id = 0;
    }

    if (isset($news_item_version_map['text'])) {
      $text = $news_item_version_map['text'];
    }
    else {
      $text = '';
    }
    if (isset($news_item_version_map['is_text_html'])) {
      $is_text_html = $news_item_version_map['is_text_html'] + 0;
    }
    else {
      $is_text_html = 0;
    }
    if (isset($news_item_version_map['related_url'])) {
      $related_url = $news_item_version_map['related_url'];
    }
    else {
      $related_url = '';
    }
    if (isset($news_item_version_map['display_contact_info'])) {
      $display_contact_info = $news_item_version_map['display_contact_info'] + 0;
    }
    else {
      $display_contact_info = 0;
    }
    if (isset($news_item_version_map['thumbnail_media_attachment_id'])) {
      $thumbnail_media_attachment_id = $news_item_version_map['thumbnail_media_attachment_id'];
    }
    else {
      $thumbnail_media_attachment_id = 0;
    }
    if (isset($_SESSION['session_user_id'])) {
      $version_created_by_id = $_SESSION['session_user_id'] + 0;
    }
    else {
      echo 'Error: You Must Be Logged In To Change News Items';
      exit;
    }
    if (isset($news_item_version_map['media_type_grouping_id'])) {
      $media_type_grouping_id = (int) $news_item_version_map['media_type_grouping_id'];
    }
    else {
      $media_type_grouping_id = 0;
    }
    if (isset($news_item_version_map['thumbnail_media_attachment_id'])) {
      $thumbnail_media_attachment_id = (int) $news_item_version_map['thumbnail_media_attachment_id'];
    }
    else {
      $thumbnail_media_attachment_id = 0;
    }

    if (isset($news_item_version_map['event_duration'])) {
      $event_duration = $news_item_version_map['event_duration'] + 0;
    }
    else {
      $event_duration = 0;
    }

    if (isset($news_item_version_map['displayed_date_day'])) {
      $display_day = $news_item_version_map['displayed_date_day'] + 0;
    }
    else {
      $display_day = date('d') + 0;
    }
    if (isset($news_item_version_map['displayed_date_month'])) {
      $display_month = $news_item_version_map['displayed_date_month'] + 0;
    }
    else {
      $display_month = date('m') + 0;
    }
    if (isset($news_item_version_map['displayed_date_year'])) {
      $display_year = $news_item_version_map['displayed_date_year'] + 0;
    }
    else {
      $display_year = date('Y') + 0;
    }
    if (isset($news_item_version_map['displayed_date_hour'])) {
      $display_hour = $news_item_version_map['displayed_date_hour'] + 0;
    }
    else {
      $display_hour = date('h');
    }
    if (isset($news_item_version_map['displayed_date_minute'])) {
      $display_minute = $news_item_version_map['displayed_date_minute'] + 0;
    }
    else {
      $display_minute = date('i');
    }

    if (isset($news_item_version_map['displayed_date_ampm']) && $news_item_version_map['displayed_date_ampm'] != '24Hour') {
      if (isset($news_item_version_map['displayed_date_ampm'])) {
        $display_ampm = strtoupper($news_item_version_map['displayed_date_ampm']);
      }
      else {
        $display_ampm = date('A');
      }

      if ($display_ampm == 'AM' && $display_hour == 12) {
        $display_hour = $display_hour - 12;
      }
      elseif ($display_ampm == 'PM' && $display_hour != 12) {
        $display_hour = $display_hour + 12;
      }
    }

    $displayed_date = $display_year . '-' . $display_month . '-' . $display_day . ' ' . $display_hour . ':' . $display_minute . ':00';

    $old_version_id = $this->getCurrentVersionId($news_item_id);
    $old_version_info = $this->getNewsItemVersionInfoFromVersionId($old_version_id);
    $has_anything_changed = 0;

    $title1 = $this->prepareString($title1);
    if ($title1 != $this->prepareString($old_version_info['title1'])) {
      $has_anything_changed = 1;
    }

    $title2 = $this->prepareString($title2);
    if ($title2 != $this->prepareString($old_version_info['title2'])) {
      $has_anything_changed = 1;
    }

    $displayed_author_name = $this->prepareString($displayed_author_name);
    if ($displayed_author_name != $this->prepareString($old_version_info['displayed_author_name'])) {
      $has_anything_changed = 1;
    }

    $email = $this->prepareString($email);
    if ($email != $this->prepareString($old_version_info['email'])) {
      $has_anything_changed = 1;
    }

    $phone = $this->prepareString($phone);
    if ($phone != $this->prepareString($old_version_info['phone'])) {
      $has_anything_changed = 1;
    }

    $address = $this->prepareString($address);
    if ($address != $this->prepareString($old_version_info['address'])) {
      $has_anything_changed = 1;
    }

    $related_url = $this->prepareString($related_url);
    if ($related_url != $this->prepareString($old_version_info['related_url'])) {
      $has_anything_changed = 1;
    }

    if ($displayed_date != $old_version_info['displayed_date']) {
      $has_anything_changed = 1;
    }

    if ($event_duration != $old_version_info['event_duration']) {
      $has_anything_changed = 1;
    }

    if ($display_contact_info != $old_version_info['display_contact_info']) {
      $has_anything_changed = 1;
    }

    if ($display_contact_info != $old_version_info['display_contact_info']) {
      $has_anything_changed = 1;
    }

    if ($is_text_html != $old_version_info['is_text_html']) {
      $has_anything_changed = 1;
    }

    if ($is_summary_html != $old_version_info['is_summary_html']) {
      $has_anything_changed = 1;
    }

    $summary = $this->prepareString($summary);
    if ($summary != $this->prepareString($old_version_info['summary'])) {
      $has_anything_changed = 1;
    }

    $text = $this->prepareString($text);
    if ($text != $this->prepareString($old_version_info['text'])) {
      $has_anything_changed = 1;
    }

    if ($media_attachment_id != $old_version_info['media_attachment_id']) {
      $has_anything_changed = 1;
    }

    if ($thumbnail_media_attachment_id != $old_version_info['thumbnail_media_attachment_id']) {
      $has_anything_changed = 1;
    }

    // Fix mime type if it may have changed.
    if ($media_attachment_id != 0) {
      $media_attachment_db_class = new MediaAttachmentDB();
      $media_attachment_info = $media_attachment_db_class->getMediaAttachmentInfo($media_attachment_id);
      $media_type_grouping_id = $media_attachment_info['media_type_grouping_id'];
    }

    if ($has_anything_changed == 1) {

      $query = 'insert into news_item_version (news_item_id, title1,title2,displayed_author_name, email, phone, address, ' .
                'summary, is_summary_html, media_attachment_id,  text, is_text_html,' .
                'related_url, thumbnail_media_attachment_id, version_created_by_id, display_contact_info, ' .
                "event_duration, displayed_date, version_creation_date  ) values ($news_item_id, '$title1','$title2','$displayed_author_name', '$email', '$phone', '$address', " .
                "'$summary', $is_summary_html, $media_attachment_id, " .
                "'$text', $is_text_html, '$related_url', $thumbnail_media_attachment_id, " .
                "'$version_created_by_id', $display_contact_info, $event_duration, '$displayed_date', Now())";

      $news_item_version_id = $this->executeStatementReturnAutokey($query);
      if ($news_item_version_id + 0 > 0) {
        $this->updateCurrentVersionIdAndMediaGroupingId($news_item_id, $news_item_version_id, $media_type_grouping_id);
        $query = 'update news_item_fulltext nift, news_item_version niv ' .
        "set nift.normalized_text=Concat(niv.title1,' ',niv.title2,' ',niv.displayed_author_name,' '," .
        " niv.email,' ',niv.summary,' ',niv.text)" .
        'where niv.news_item_version_id=' . $news_item_version_id . ' and nift.news_item_id=niv.news_item_id';
      }
      $this->executeStatement($query);
    }
    else {
      $news_item_version_id = $old_version_id;
    }

    return $news_item_version_id;
  }

  /**
   * Add news item version class.
   */
  public function addNewsItemVersion($news_item_id, $news_item_version_map) {

    $news_item_id = (int) $news_item_id;
    $title1 = $this->prepareString($news_item_version_map['title1']);
    $title2 = $this->prepareString($news_item_version_map['title2']);
    $displayed_author_name = $this->prepareString($news_item_version_map['displayed_author_name']);
    if (array_key_exists('email', $news_item_version_map)) {
      $email = $this->prepareString($news_item_version_map['email']);
    }
    else {
      $email = '';
    }
    if (array_key_exists('phone', $news_item_version_map)) {
      $phone = $this->prepareString($news_item_version_map['phone']);
    }
    else {
      $phone = '';
    }
    if (array_key_exists('address', $news_item_version_map)) {
      $address = $this->prepareString($news_item_version_map['address']);
    }
    else {
      $address = '';
    }
    if (array_key_exists('related_url', $news_item_version_map)) {
      $related_url = $this->prepareString($news_item_version_map['related_url']);
    }
    else {
      $related_url = $this->prepareString($news_item_version_map['related_url']);
    }
    if (array_key_exists('summary', $news_item_version_map)) {
      $summary = $this->prepareString($news_item_version_map['summary']);
    }
    else {
      $summary = '';
    }
    if (array_key_exists('is_summary_html', $news_item_version_map)) {
      $is_summary_html = (int) $news_item_version_map['is_summary_html'];
    }
    else {
      $is_summary_html = 0;
    }
    if (array_key_exists('media_attachment_id', $news_item_version_map)) {
      $media_attachment_id = (int) $news_item_version_map['media_attachment_id'];
    }
    else {
      $media_attachment_id = 0;
    }
    $text = $this->prepareString($news_item_version_map['text']);
    $is_text_html = (int) $news_item_version_map['is_text_html'];
    $display_contact_info = (int) $news_item_version_map['display_contact_info'];

    if (array_key_exists('thumbnail_media_attachment_id', $news_item_version_map)) {
      $thumbnail_media_attachment_id = (int) $news_item_version_map['thumbnail_media_attachment_id'];
    }
    else {
      $thumbnail_media_attachment_id = 0;
    }
    $version_created_by_id = (int) $news_item_version_map['version_created_by_id'];

    $media_type_grouping_id = (int) $news_item_version_map['media_type_grouping_id'];

    $event_duration = 0;
    if (array_key_exists('event_duration', $news_item_version_map)) {
      $event_duration = (float) $news_item_version_map['event_duration'];
    }

    if (array_key_exists('displayed_date_day', $news_item_version_map)) {
      $display_day = $news_item_version_map['displayed_date_day'] + 0;
      $display_month = $news_item_version_map['displayed_date_month'] + 0;
      $display_year = $news_item_version_map['displayed_date_year'] + 0;
      $display_hour = $news_item_version_map['displayed_date_hour'] + 0;
      $display_minute = $news_item_version_map['displayed_date_minute'] + 0;
      if (isset($news_item_version_map['displayed_date_ampm'])) {
        $display_ampm = $news_item_version_map['displayed_date_ampm'];
      }
      else {
        $display_ampm = '24Hour';
      }
    }
    else {
      $display_day = date('d') + 0;
      $display_month = date('m') + 0;
      $display_year = date('Y') + 0;
      $display_hour = date('h') + 0;
      $display_minute = date('i') + 0;
      $display_ampm = date('A');
    }

    if ($display_year > 1900 && $display_year < 2100) {
      if ($display_ampm == 'PM' && $display_hour != 12) {
        $display_hour = $display_hour + 12;
      }
      elseif ($display_ampm == 'AM' && $display_hour == 12) {
        $display_hour = 0;
      }
      $displayed_date = $display_year . '-' . $display_month . '-' . $display_day . ' ' . $display_hour . ':' . $display_minute;

    }

    $query = 'insert into news_item_version (news_item_id, title1,title2,displayed_author_name, email, phone, address, ' .
                'summary, is_summary_html, media_attachment_id,  text, is_text_html,' .
                'related_url, thumbnail_media_attachment_id, version_created_by_id, display_contact_info, ' .
                "event_duration, displayed_date,version_creation_date  ) values ($news_item_id, '$title1','$title2','$displayed_author_name', '$email', '$phone', '$address', " .
                "'$summary', $is_summary_html, $media_attachment_id, " .
                "'$text', $is_text_html, '$related_url', $thumbnail_media_attachment_id, " .
                " '$version_created_by_id', $display_contact_info, $event_duration, '$displayed_date', Now())";
    $news_item_version_id = $this->executeStatementReturnAutokey($query);
    $this->updateCurrentVersionIdAndMediaGroupingId($news_item_id, $news_item_version_id, $media_type_grouping_id);

    $query = 'insert into news_item_fulltext (news_item_id, normalized_text) ' .
                " select niv.news_item_id, Concat(niv.title1,' ',niv.title2,' ',niv.displayed_author_name,' ',niv.email,' ',niv.summary,' ',niv.text)" .
                'from news_item_version niv where news_item_version_id=' . $news_item_version_id;

    $this->executeStatement($query);

    return $news_item_version_id;
  }

  /**
   * Returns version info for a newsitem.
   */
  public function getNewsItemVersionInfoFromVersionId($news_item_version_id) {

    $news_item_version_id = (int) $news_item_version_id;
    $query = "select *, date_format(version_creation_date,'%W, %b %e, %Y %l:%i%p') as modified,
  		date_format(displayed_date,'%Y') as displayed_date_year,
  		date_format(displayed_date,'%m') as displayed_date_month,
  		date_format(displayed_date,'%d') as displayed_date_day,
		date_format(displayed_date,'%H') as displayed_date_hour,
  		date_format(displayed_date,'%i') as displayed_date_minute,
  		unix_timestamp(displayed_date) as displayed_timestamp,
  		unix_timestamp(version_creation_date) as modified_timestamp
  		 from news_item_version where news_item_version_id=$news_item_version_id";
    $list = $this->query($query);
    // Initialize the return value.
    $version_info = NULL;
    if (is_array($list) && count($list) > 0) {
      $version_info = array_pop($list);
      $display_day = $version_info['displayed_date_day'] + 0;
      $display_month = $version_info['displayed_date_month'] + 0;
      $display_year = $version_info['displayed_date_year'] + 0;
      $display_hour = $version_info['displayed_date_hour'] + 0;
      $display_minute = $version_info['displayed_date_minute'] + 0;
      $display_date = $display_year . '-' . $display_month . '-' . $display_day . ' ' . $display_hour . ':' . $display_minute . ':00';
      $version_info['displayed_date'] = $display_date;
    }

    return $version_info;
  }

  /**
   * Given a news item id returns info from the current version.
   */
  public function getCurrentNewsItemVersionInfo($news_item_id) {

    $result = '';
    $current_version_id = $this->getCurrentVersionId($news_item_id);
    $query = "select * from news_item_version niv where news_item_version_id=$current_version_id";
    $result_list = $this->query($query);
    if (count($result_list) > 0) {
      $result = $result_list[0];
    }

    return $result;
  }

  /**
   * Given a news item id returns info from the latest version.
   */
  public function getLatestNewsItemVersionInfo($news_item_id) {

    $result = '';
    $latest_version_id = $this->getLatestVersionId($news_item_id);
    $query = "select * from news_item_version niv where news_item_version_id=$latest_version_id";
    $result_list = $this->query($query);
    if (count($result_list) > 0) {
      $result = $result_list[0];
    }

    return $result;
  }

  /**
   * Returns info used by version lists.
   */
  public function getVersionListInfo($news_item_id, $max_num_items) {

    $news_item_id = (int) $news_item_id;
    $max_num_items = (int) $max_num_items;
    $query = 'select niv.news_item_version_id, niv.title1,niv.displayed_author_name,niv.version_creation_date, niv.version_created_by_id, ' .
          ' u.username, u.user_id from news_item_version niv left join  user u on niv.version_created_by_id=u.user_id where ' .
          " niv.news_item_id=$news_item_id order by niv.news_item_version_id desc";
    if ($max_num_items > 0) {
      $query .= ' limit 0, ' . $max_num_items;
    }
    $result = $this->query($query);

    return $result;
  }

  /**
   * Updates the thumbnail attachment id on a version.
   *
   * Useful when a thumbnail is generated for an existing item.
   */
  public function updateThumbnailAttachmentIdForVersion($news_item_version_id, $thumbnail_media_attachment_id) {

    $news_item_version_id = (int) $news_item_version_id;
    $thumbnail_media_attachment_id = (int) $thumbnail_media_attachment_id;
    $sql = 'update news_item_version set thumbnail_media_attachment_id=' . $thumbnail_media_attachment_id;
    $sql .= ' where news_item_version_id=' . $news_item_version_id;

    return $this->executeStatement($sql);
  }

}
