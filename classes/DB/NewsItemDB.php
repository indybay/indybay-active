<?php

namespace Indybay\DB;

use Indybay\Cache\NewswireCache;
use Indybay\Cache\SpamCache;

/**
 * Loads information that is used by all newsitem classes.
 *
 * Written December 2005.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class NewsItemDB extends DB {

  /**
   * Adds an entry to the news_item table with 0 for the user id that added it.
   */
  public function addNewsItem($news_item_status_id, $news_item_type_id, $parent_item_id) {

    $news_item_status_id = (int) $news_item_status_id;
    $news_item_type_id = (int) $news_item_type_id;
    $parent_item_id = (int) $parent_item_id;
    $spam_cache = new SpamCache();
    if ($spam_cache->shouldBlockDbAdd()) {
      $news_item_status_id = NEWS_ITEM_STATUS_ID_HIDDEN;
    }

    $query = 'insert into news_item (news_item_status_id, news_item_type_id, parent_item_id, creation_date) values ' .
            "($news_item_status_id, $news_item_type_id, $parent_item_id, Now())";
    $news_item_id = $this->executeStatementReturnAutokey($query);
    $GLOBALS['just_added_id'] = $news_item_id;
    if ($parent_item_id == 0 && $news_item_id > 0) {
      $this->updateParentId($news_item_id, $news_item_id);
    }
    elseif ($news_item_type_id == NEWS_ITEM_TYPE_ID_COMMENT) {
      $this->updateNumcomments($parent_item_id);
      if ($news_item_id - $parent_item_id < 1000) {

        $newswire_cache = new NewswireCache();
        // Trying to reuse method meant for something else hence weird args.
        $parent_info = $this->getNewsItemInfo($parent_item_id);
        $newswire_cache->regenerateNewswiresForNewsitemHelper($parent_item_id,
        [], NEWS_ITEM_STATUS_ID_HIDDEN, $parent_info['news_item_status_id']);
      }
    }

    return $news_item_id;
  }

  /**
   * Adds news item with a user id.
   */
  public function addNewsItemForRegisteredUser($news_item_status_id, $news_item_type_id, $parent_item_id) {

    $news_item_status_id = (int) $news_item_status_id;
    $news_item_type_id = (int) $news_item_type_id;
    $parent_item_id = (int) $parent_item_id;
    $query = 'insert into news_item (news_item_status_id, news_item_type_id, parent_item_id, created_by_id, creation_Date) values ' .
            "($news_item_status_id, $news_item_type_id, $parent_item_id, " . $_SESSION['session_user_id'] . ', Now())';
    $news_item_id = $this->executeStatementReturnAutokey($query);
    if ($parent_item_id == 0 && $news_item_id > 0) {
      $this->updateParentId($news_item_id, $news_item_id);
    }

    return $news_item_id;
  }

  /**
   * Removes the association with a news item and a category.
   */
  public function removeNewsItemCategory($news_item_id, $category_id) {

    $news_item_id = (int) $news_item_id;
    $category_id = (int) $category_id;
    $query = "delete from news_item_category where news_item_id=$news_item_id and category_id=$category_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Adds an association between a newsitem and a category.
   */
  public function addNewsItemCategory($news_item_id, $category_id) {

    $news_item_id = (int) $news_item_id;
    $category_id = (int) $category_id;
    $query = "insert into news_item_category (news_item_id, category_id) values ($news_item_id, $category_id)";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Updates the current version if on a newsitem.
   */
  public function isNewsItemInCategory($news_item_id, $category_id) {

    $news_item_id = (int) $news_item_id;
    $category_id = (int) $category_id;
    $query = 'select * from news_item_category where news_item_id=' . $news_item_id . ' and category_id=' . $category_id;
    $result = $this->query($query);
    $ret = 0;
    if (is_array($result) && count($result) > 0) {
      $ret = 1;
    }

    return $ret;
  }

  /**
   * Updates the latest version on a newsitem.
   */
  public function updateCurrentVersionIdAndMediaGroupingId($news_item_id, $current_version_id, $media_type_grouping_id) {

    $news_item_id = (int) $news_item_id;
    $current_version_id = (int) $current_version_id;
    $media_type_grouping_id = (int) $media_type_grouping_id;
    $query = "update news_item set current_version_id=$current_version_id, media_type_grouping_id=$media_type_grouping_id
          where news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Updates the latest version on a newsitem.
   */
  public function updateLatestVersionId($news_item_id, $latest_version_id) {

    $news_item_id = (int) $news_item_id;
    $latest_version_id = (int) $latest_version_id;
    $query = "update news_item set latest_version_id=$latest_version_id where news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Updates the parent id of a news item.
   */
  public function updateParentId($news_item_id, $new_parent_id) {

    $news_item_id = (int) $news_item_id;
    $new_parent_id = (int) $new_parent_id;
    $query = "update news_item set parent_item_id=$new_parent_id where news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Updates the status id on a news item.
   */
  public function updateNewsItemStatusId($news_item_id, $new_status_id) {

    $news_item_id = (int) $news_item_id;
    $new_status_id = (int) $new_status_id;
    $this->updateStatusHelper($news_item_id, $new_status_id);

    $query = "update news_item set news_item_status_id=$new_status_id where news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Updates both status and type on a newsitem.
   */
  public function updateNewsItemStatusAndType($news_item_id, $news_item_status_id, $news_item_type_id) {

    $news_item_id = (int) $news_item_id;
    $news_item_status_id = (int) $news_item_status_id;
    $news_item_type_id = (int) $news_item_type_id;
    $this->updateStatusHelper($news_item_id, $news_item_status_id);

    $query = "update news_item set news_item_type_id=$news_item_type_id, news_item_status_id=$news_item_status_id " .
            " where news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Update status helper function.
   */
  public function updateStatusHelper($news_item_id, $new_status_id) {

    $news_item_id = (int) $news_item_id;
    $new_status_id = (int) $new_status_id;
    $news_item_info = $this->getNewsItemInfo($news_item_id);
    $parent_item_id = $news_item_info['parent_item_id'];
    $news_item_type_id = $news_item_info['news_item_type_id'];
    if ($news_item_type_id == NEWS_ITEM_TYPE_ID_COMMENT) {
      $this->updateNumcomments($parent_item_id);
      if ($news_item_id - $parent_item_id < 1000) {

        $newswire_cache = new NewswireCache();
        // Trying to reuse method meant for something else hence weird args.
        $parent_info = $this->getNewsItemInfo($parent_item_id);
        $newswire_cache->regenerateNewswiresForNewsitemHelper($parent_item_id,
        [], NEWS_ITEM_STATUS_ID_HIDDEN, $parent_info['news_item_status_id']);
      }
    }

    if ($news_item_type_id == NEWS_ITEM_TYPE_ID_POST || $news_item_type_id == NEWS_ITEM_TYPE_ID_EVENT) {
      if ($news_item_info['news_item_status_id'] != NEWS_ITEM_STATUS_ID_HIDDEN) {
        $this->updateNumcomments($news_item_id);
        $query = 'update news_item set news_item_status_id=' . $new_status_id . ' 
        	where parent_item_id=' . $news_item_id . ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_ATTACHMENT . ' and
        	news_item_status_id<>' . NEWS_ITEM_STATUS_ID_HIDDEN . ' and news_item_id>' . $news_item_id;
        $this->executeStatement($query);
      }
      else {
        $query = 'update news_item set news_item_status_id=' . $new_status_id . ' 
        	where parent_item_id=' . $news_item_id . ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_ATTACHMENT . ' and news_item_id>' . $news_item_id;
        ;
        $this->executeStatement($query);
      }
    }

  }

  /**
   * Gets category information for a newsitem.
   */
  public function getNewsItemCategoryInfo($news_item_id, $category_type_id) {

    $news_item_id = (int) $news_item_id;
    $category_type_id = (int) $category_type_id;
    $query = 'select c.category_id, c.name, c.category_type_id from news_item_category nic, ' .
            "category c where nic.news_item_id=$news_item_id and " .
            'c.category_id=nic.category_id';
    if ($category_type_id != 0) {
      $query .= " and c.category_type_id=$category_type_id";
    }
    $query .= ' order by c.name';
    $result = $this->query($query);

    return $result;
  }

  /**
   * Gets lists of ids for categories associated with a newsitem.
   */
  public function getNewsItemCategoryIds($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select category_id from news_item_category where news_item_id=$news_item_id";
    $result = $this->singleColumnQuery($query);

    return $result;
  }

  /**
   * Updates categories on a newsitem and returns list of old categories.
   *
   * (so newswires and the link can get updated)
   */
  public function updateNewsItemCategoriesReturnOldCategories($news_item_id, $new_category_array) {

    $old_category_array = $this->getNewsItemCategoryIds($news_item_id);

    $temp_array = [];
    if (!isset($new_category_array)) {
      $new_category_array = [];
    }
    $temp_array = array_merge($new_category_array, $temp_array);

    if (is_array($old_category_array)) {
      foreach ($old_category_array as $next_cat_id) {
        if (!isset($temp_array[$next_cat_id])) {
          $this->removeNewsItemCategory($news_item_id, $next_cat_id);
        }
        else {
          unset($temp_array, $next_cat_id);
        }
      }
    }
    if (is_array($temp_array)) {
      foreach ($temp_array as $next_cat_id) {
        $this->addNewsItemCategory($news_item_id, $next_cat_id);
      }
    }

    return $old_category_array;
  }

  /**
   * Gets news item info from a news item id.
   */
  public function getNewsItemInfo($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select news_item_id, current_version_id, news_item_type_id, news_item_status_id,
        	 parent_item_id, unix_timestamp(creation_date) as creation_timestamp, 
        	 	date_format(creation_date,'%a, %b %e, %Y %l:%i%p ') as created,
        	 	created_by_id
        	 from news_item where news_item_id=" . $news_item_id;
    $result_list = $this->query($query);
    if (!is_array($result_list) || count($result_list) == 0) {
      $result = '';
    }
    else {
      $result = array_pop($result_list);
    }

    return $result;
  }

  /**
   * Returns the current version id for a news item.
   */
  public function getCurrentVersionId($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select current_version_id from news_item where news_item_id=$news_item_id";
    $result_list = $this->singleColumnQuery($query);
    $version_id = 0;
    if (!is_array($result_list) || count($result_list) == 0) {
      return '';
    }
    else {
      $version_id = array_pop($result_list);
    }

    return $version_id;
  }

  /**
   * Returns the latest version id for a news item.
   */
  public function getLatestVersionId($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select current_version_id from news_item where news_item_id=$news_item_id";
    $result = $this->query($query);
    $version_id = 0;
    if (count($result) == 0) {
      return '';
    }
    else {
      $list = $this->query($query);
      if (count($list) > 0) {
        $row = array_pop($list);
        $version_id = array_pop($row);
      }
    }

    return $version_id;
  }

  /**
   * Returns a list of type names and ids.
   */
  public function getNewsItemTypesSelectList() {

    $query = 'select news_item_type_id, name from news_item_type where news_item_type_id!=5 and news_item_type_id!=6';
    $types = $this->query($query);
    $type_select_list = [];
    foreach ($types as $row) {
      $type_select_list[$row['news_item_type_id']] = $row['name'];
    }

    return $type_select_list;
  }

  /**
   * Returns the status type names and ids for newsitems.
   */
  public function getNewsItemStatusSelectList() {

    $query = 'select news_item_status_id, name from news_item_status order by sort_order desc';
    $types = $this->query($query);
    if (is_array($types)) {
      $type_select_list = [];
      foreach ($types as $row) {
        $type_select_list[$row['news_item_status_id']] = $row['name'];
      }
    }

    return $type_select_list;
  }

  /**
   * Updates the number of nonhidden comments.
   */
  public function updateNumcomments($news_item_id) {
    $news_item_id = (int) $news_item_id;
    $query = 'select count(*) from news_item where parent_item_id=' . $news_item_id . '
			and news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN . ' and news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN . ' and
			news_item_type_id=' . NEWS_ITEM_TYPE_ID_COMMENT . ' and current_version_id>0 and current_version_id is not null';
    $result = $this->singleColumnQuery($query);
    $numcomments = array_pop($result);
    $query = 'update news_item set num_comments=' . $numcomments . ' where news_item_id=' . $news_item_id;
    $this->executeStatement($query);
  }

  /**
   * Updates syndicated status.
   */
  public function updateSyndicated($news_item_id) {
    $news_item_id = (int) $news_item_id;
    $query = 'update news_item set syndicated = 1 where news_item_id = ' . $news_item_id;
    $this->executeStatement($query);
  }

}
