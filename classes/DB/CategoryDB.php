<?php

namespace Indybay\DB;

/**
 * Category class.
 *
 * Categories are like pages but not all pages have categories (for example
 * the front page) and the code is written in a way that multipke categories
 * could eventually be associated to a single page (for example perhaps at
 * some point there could be a bay area page that includes all local
 * categories... the main reason there isnt support for this yet is that it
 * could be that people want the list to be or based like a page for all bay
 * area regions or and based like an antiwar page for a specific region).
 *
 * Written December 2005 - January 2006
 * somewhat extended from something in sf-active.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class CategoryDB extends DB {

  /**
   * Returns a list of category by type and/or parent id.
   */
  public function getCategoryInfoListByType($category_type_id, $parent_category_id, $is_publish_dropdown = 0) {

    $category_type_id = (int) $category_type_id;
    $parent_category_id = (int) $parent_category_id;
    // Returns an array with category ID as key, category name as value.
    $db_obj = new DB();
    $category_list = [];
    $query = "SELECT category_id,name FROM category where category_type_id=$category_type_id AND status = 1 ";
    if ($is_publish_dropdown == 1) {
      $query .= ' and include_in_dropdowns=1 ';
    }
    if ($parent_category_id == 0) {
      $query .= ' and parent_category_id is null';
    }
    else {
      $query .= " and parent_category_id=$parent_category_id";
    }
    $query .= ' ORDER BY
                 CASE
                  WHEN name="California" THEN 1
                  WHEN name="U.S." THEN 2
                 END,
                 name asc';
    // Echo $query;.
    $resultset = $db_obj->query($query);
    if (is_array($resultset)) {
      foreach ($resultset as $row) {
        $category_list["$row[category_id]"] = $row['name'];
      }
    }
    return $category_list;
  }

  /**
   * Given a category id returns the category name.
   */
  public function getCategoryNameFromId($category_id) {

    $category_id = (int) $category_id;
    // Returns an array with category ID as key, category name as value.
    $db_obj = new DB();
    $query = 'SELECT name FROM category where category_id=' . $category_id;

    $resultset = $db_obj->singleColumnQuery($query);
    $ret = 'No Category Found';
    if (is_array($resultset)) {
      $ret = array_pop($resultset);
    }

    return $ret;
  }

}
