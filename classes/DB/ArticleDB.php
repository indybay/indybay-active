<?php

namespace Indybay\DB;

/**
 * Class for article specific DB functionality.
 *
 * Most of the actual SQL is in the NewsItemDB and NewsItemVersionDB classes.
 *
 * Written December 2005 - January 2006.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class ArticleDB extends NewsItemVersionDB {

  /**
   * Returns news_item and news_item_version info for an article.
   */
  public function getArticleInfoFromNewsItemId($news_item_id) {

    $version_id = $this->getCurrentVersionId($news_item_id);

    $article_info = $this->getArticleInfoFromVersionId($version_id);

    return $article_info;
  }

  /**
   * Returns news_item and news_item_version info for an article.
   */
  public function getArticleInfoFromVersionId($version_id) {

    $version_info = $this->getNewsItemVersionInfoFromVersionId($version_id);
    if (!$version_info) {
      return NULL;
    }
    $item_info = $this->getNewsItemInfo($version_info['news_item_id']);
    $article_info = (is_array($version_info) && is_array($item_info)) ? array_merge($version_info, $item_info) : NULL;

    return $article_info;
  }

  /**
   * Adds a new version to the DB from posted fields.
   */
  public function createNewVersion($news_item_id, $post_array) {

    $post_array['version_created_by_id'] = $_SESSION['session_user_id'];
    $post_array['media_type_grouping_id'] = 0;
    $post_array['thumbnail_media_attachment_id'] = 0;
    if (!isset($post_array['is_summary_html'])) {
      $post_array['is_summary_html'] = 0;
    }
    if (!isset($post_array['is_text_html'])) {
      $post_array['is_text_html'] = 0;
    }
    if (!isset($post_array['display_contact_info'])) {
      $post_array['display_contact_info'] = 0;
    }
    if (trim($post_array['media_attachment_id']) == '') {
      $post_array['media_attachment_id'] = 0;
    }
    if (!isset($post_array['event_duration']) || trim($post_array['event_duration']) == '') {
      $post_array['event_duration'] = 0;
    }
    $news_item_version_id = $this->addNewsItemVersionIfChanged($news_item_id, $post_array);

    return $news_item_version_id;
  }

  /**
   * Creates a new news item as well as the version from posted fields.
   */
  public function createNewArticle($item_type_id, $post_array) {

    $post_array['version_created_by_id'] = 0;

    if (!array_key_exists('media_type_grouping_id', $post_array)) {
      $post_array['media_type_grouping_id'] = 0;
    }
    if (!array_key_exists('media_type_grouping_id', $post_array)) {
      $post_array['thumbnail_media_attachment_id'] = 0;
    }
    if (!array_key_exists('is_summary_html', $post_array)) {
      $post_array['is_summary_html'] = 0;
    }
    if (!array_key_exists('display_contact_info', $post_array)) {
      $post_array['display_contact_info'] = 0;
    }
    if (!array_key_exists('is_text_html', $post_array)) {
      $post_array['is_text_html'] = 0;
    }
    if (!array_key_exists('display_contact_info', $post_array)) {
      $post_array['display_contact_info'] = 0;
    }
    if (!array_key_exists('title2', $post_array)) {
      $post_array['title2'] = $post_array['title1'];
    }
    if (!array_key_exists('title1', $post_array)) {
      $post_array['title1'] = $post_array['title2'];
    }

    if (!array_key_exists('event_duration', $post_array)) {
      $post_array['event_duration'] = 0;
    }
    else {
      $post_array['event_duration'] = $post_array['event_duration'] + 0;
    }

    if (!array_key_exists('media_attachment_id', $post_array) || trim($post_array['media_attachment_id']) == '') {
      $post_array['media_attachment_id'] = 0;
    }
    $parent_id = 0;
    if (array_key_exists('parent_item_id', $post_array)) {
      $parent_id = $post_array['parent_item_id'] + 0;
    }
    $news_item_id = $this->addNewsItem(NEWS_ITEM_STATUS_ID_NEW, $item_type_id, $parent_id);
    $this->addNewsItemVersion($news_item_id, $post_array);

    return $news_item_id;
  }

  /**
   * Returns a list of ids for all comments on an article.
   */
  public function getCommentIds($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select news_item_id from news_item where parent_item_id=$news_item_id
	    	and news_item_type_id=" . NEWS_ITEM_TYPE_ID_COMMENT . ' and current_version_id>0 and current_version_id is not null order by news_item_id desc';
    $result = $this->singleColumnQuery($query);
    $result = array_reverse($result);

    return $result;
  }

  /**
   * Returns a list of ids for all attachments on an article.
   */
  public function getAttachmentIds($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select news_item_id from news_item where parent_item_id=$news_item_id
	    	and news_item_type_id=" . NEWS_ITEM_TYPE_ID_ATTACHMENT . ' and current_version_id>0 and current_version_id is not null order by news_item_id desc';
    $result = $this->singleColumnQuery($query);
    $result = array_reverse($result);

    return $result;
  }

  /**
   * Given title and id, finds possible duplicates posted around the same time.
   */
  public function findRecentDuplicateNonhiddenVersionsByTitle($title, $old_id) {

    $title = $this->prepareString($title);
    $old_id = (int) $old_id;
    $query = 'select ni.news_item_id from news_item ni, news_item_version niv ' .
            " where ni.news_item_id=niv.news_item_id and niv.title1 like '%" . $title . "%' " .
            ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN . ' and  ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN . ' ' .
            ' AND ni.parent_item_id <> ' . $old_id .
            ' and ni.news_item_id>' . ($old_id - 10000) . ' and ni.news_item_id<' . ($old_id + 10000) . ' and ni.news_item_id!=' . $old_id . ' order by news_item_id desc';

    $result = $this->singleColumnQuery($query);
    $dup_info = '';
    if (is_array($result) && count($result) > 0) {
      $dup_id = $result[0];
      $dup_info = $this->getArticleInfoFromNewsItemId($dup_id);
    }

    return $dup_info;
  }

  /**
   * Finds all recent nonhidden duplicates by title.
   */
  public function findAllRecentDuplicateNonhiddenVersionsByTitle($title) {

    if (!array_key_exists('old_max_news_item_id', $GLOBALS) || $GLOBALS['old_max_news_item_id'] + 0 == 0) {
      $query = 'select max(news_item_id) from news_item';
      $rss_item_ids = $this->singleColumnQuery($query);
      $max_id = array_pop($rss_item_ids);
      $GLOBALS['old_max_news_item_id'] = $max_id;
    }
    else {
      $max_id = $GLOBALS['old_max_news_item_id'] + 0;
    }

    $title = $this->prepareString($title);
    $query = 'select niv.*, ni.parent_item_id, unix_timestamp(niv.displayed_date) as displayed_timestamp from news_item ni, news_item_version niv ' .
            " where ni.news_item_id=niv.news_item_id and niv.title1='" . $title . "' " .
            ' and ni.news_item_id>' . ($max_id - 5000) . '  order by news_item_id desc';

    $result = $this->query($query);

    return $result;
  }

  /**
   * Function to change all ids for children of a post.
   *
   * This is mainly useful if you are making one post and its attachments an
   * attachment on another post.
   */
  public function changeAllParentIdsForChildren($news_item_id, $parent_item_id) {

    $news_item_id = (int) $news_item_id;
    $parent_item_id = (int) $parent_item_id;
    $query = "update news_item set parent_item_id=$parent_item_id " .
            " where parent_item_id=$news_item_id";
    $this->executeStatement($query);

  }

}
