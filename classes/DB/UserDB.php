<?php

namespace Indybay\DB;

/**
 * User data class.
 *
 * Used for getting user information to show edit histories as
 * well as the user admin pages.
 *
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class UserDB extends DB {

  /**
   * Gets user info.
   */
  public function getUserInfo($user_id) {
    $user_detail_query = "SELECT u.user_id, u.username, u.has_login_rights, date_format(u.creation_date, '%b %e %Y %l:%i%p') as created, date_format(u.last_login_time, '%b %e %Y %l:%i%p') as last_login,  c.* FROM user u, contact_info c WHERE 
            	u.user_id=" . (int) $user_id . ' and c.contact_info_id=u.contact_info_id';
    $resultset = $this->query($user_detail_query);
    $user = array_pop($resultset);

    return $user;
  }

  /**
   * Gets a list of users.
   */
  public function getList($is_enabled) {
    $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%c %e, %Y') as last_activity, u.username, date_format(u.creation_date, '%c %e, %Y') as created, date_format(u.last_login_time, '%b %e, %Y') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=" . (int) $is_enabled . ' order by u.username';
    $result = $this->query($user_list_query);

    return $result;
  }

  /**
   * Gets a list of recently-logged-in users.
   */
  public function getRecentList($num_days) {
    $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%a %c/%e %l:%i%p') as last_activity, u.username, date_format(u.creation_date, '%a %c/%e %l:%i%p') as created, date_format(u.last_login_time, '%a %c/%e %l:%i%p') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=1
            	and
            		(UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_admin_activity) <=" . (int) $num_days . '*60*60*24
            	or
            	UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_login_time) <=' . (int) $num_days . '*60*60*24
            	)
            	 order by u.last_admin_activity desc';
    $result = $this->query($user_list_query);

    return $result;
  }

  /**
   * Gets non-recent list.
   */
  public function getNonrecentList($num_days) {
    $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%c/%e %l:%i%p') as last_activity, u.username, date_format(u.creation_date, '%c/%e %l:%i%p') as created, date_format(u.last_login_time, '%c/%e %l:%i%p') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=1 and
            	(	(UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_admin_activity) > " . (int) $num_days . '*60*60*24
            	and
            	UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_login_time) >' . (int) $num_days . '*60*60*24
            	)
            	or (last_login_time is null and last_admin_activity is null)
            	)
            	 order by u.username';
    $result = $this->query($user_list_query);

    return $result;
  }

  /**
   * Inserts a user.
   */
  public function add($userfields) {

    $user_id = -1;

    if (strlen($userfields['username']) != 0 && strlen($userfields['password']) != 0) {
      $email = $this->prepareString($userfields['email']);
      $phone = $this->prepareString($userfields['phone']);
      $last_name = $this->prepareString($userfields['last_name']);
      $first_name = $this->prepareString($userfields['first_name']);

      $user_add_query = 'INSERT INTO contact_info (';
      $user_add_query .= 'email, phone, last_name, first_name';
      $user_add_query .= ") VALUES ('";
      $user_add_query .= $email . "', '";
      $user_add_query .= $phone . "', '";
      $user_add_query .= $last_name . "', '";
      $user_add_query .= $first_name . "') ";
      $contact_info_id = $this->executeStatementReturnAutokey($user_add_query);

      if ($contact_info_id > 0) {
        $user_add_query  = 'INSERT INTO user (';
        $user_add_query .= 'username, has_login_rights, contact_info_id';
        $user_add_query .= ") VALUES ('";
        $user_add_query .= $this->prepareString($userfields['username']) . "',
	            " . (int) $userfields['has_login_rights'] . ', ' . $contact_info_id . '
	            )';
        $user_id         = $this->executeStatementReturnAutokey($user_add_query);
        $error           = 0;
        if ($user_id + 0 > 0) {
          $hash = password_hash($userfields['password'], PASSWORD_DEFAULT);
          $sql = "INSERT INTO user_password (user_id, password) VALUES ($user_id, '{$this->prepareString($hash)}')";
          $error = $this->executeStatement($sql);
        }
        else {
          echo 'couldnt set password';
          $user_id = -1;
        }
        if ($error + 0 > 0) {
          echo 'couldnt set password';
          $user_id = -1;
        }
      }
    }

    return $user_id;
  }

  /**
   * Updates a user.
   */
  public function update($user_id, $userfields) {

    $email = $this->prepareString($userfields['email']);
    $phone = $this->prepareString($userfields['phone']);
    $last_name = $this->prepareString($userfields['last_name']);
    $first_name = $this->prepareString($userfields['first_name']);

    $user_update_query = 'UPDATE contact_info ci, user u SET ';
    $user_update_query .= 'has_login_rights=';
    $user_update_query .= (int) $userfields['has_login_rights'];
    $user_update_query .= ", email='";
    $user_update_query .= $email;
    $user_update_query .= "', phone='";
    $user_update_query .= $phone;
    $user_update_query .= "', first_name='";
    $user_update_query .= $first_name;
    $user_update_query .= "', last_name='";
    $user_update_query .= $last_name;
    $user_update_query .= "' WHERE ci.contact_info_id=u.contact_info_id and u.user_id=" . (int) $user_id;
    $this->executeStatement($user_update_query);

  }

  /**
   * Changes user password.
   */
  public function changeUserPassword($user_id, $new_password) {

    $hash = password_hash($new_password, PASSWORD_DEFAULT);
    $sql = "UPDATE user_password SET password = '{$this->prepareString($hash)}' WHERE user_id = " . (int) $user_id;
    $this->executeStatement($sql);

  }

  /**
   * Updates last activity date.
   */
  public function updateLastActivityDate() {
    $user_id = $_SESSION['session_user_id'];
    if ($user_id + 0 > 0) {
      $update_last_activity_date = 'update user set last_admin_activity=Now() where user_id=' . (int) $user_id;
      $this->executeStatement($update_last_activity_date);
    }
  }

  /**
   * Authenticates a user.
   */
  public function authenticate($username, $password) {
    $user_id = 0;
    $result = $this->query("SELECT u.user_id, up.password FROM user u
      INNER JOIN user_password up ON u.user_id = up.user_id
      WHERE u.has_login_rights = 1
      AND u.username = '{$this->prepareString($username)}'");
    if (is_array($result) && ($user_row = array_pop($result)) && password_verify($password, $user_row['password'])) {
      $user_id = $user_row['user_id'];
      $this->executeStatement("UPDATE user SET last_login_time = NOW() WHERE user_id = $user_id");
    }
    return $user_id;
  }

  /**
   * Validates old password.
   */
  public function validateOldPassword($user_id, $password) {
    $result = $this->query('SELECT password FROM user_password WHERE user_id = ' . (int) $user_id);
    if (is_array($result) && ($user_row = array_pop($result)) && password_verify($password, $user_row['password'])) {
      return 1;
    }
    return 0;
  }

  /**
   * Updates user temporary password.
   */
  public function updateUpdateUserTempPassword($user_id, $temp_password) {

    $user = array_pop($resultset);

    return $user;
  }

}
