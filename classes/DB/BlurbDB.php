<?php

namespace Indybay\DB;

/**
 * Class for saving and loading info for center column blurbs.
 *
 * Written December 2005 - January 2006.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class BlurbDB extends NewsItemVersionDB {

  /**
   * Gets info for the current blurbs on a page.
   */
  public function getCurrentPageInfoForBlurb($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select p.* from page p,
    		news_item_page_order nipo where 
    		nipo.page_id=p.page_id and nipo.news_item_id=$news_item_id";
    $page_info_list = $this->query($query);

    return $page_info_list;
  }

  /**
   * Gets info for the archived blurbs on a page.
   */
  public function getArchivedPageInfoForBlurb($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select p.* from page p,
    		page_category pc, news_item_category nic, news_item ni  where 
    		nic.news_item_id=$news_item_id and nic.category_id=pc.category_id and
    		ni.news_item_id=nic.news_item_id and ni.news_item_status_id!=" . NEWS_ITEM_STATUS_ID_HIDDEN . " and
    		pc.page_id=p.page_id and p.page_id not in ( select page_id from 
    		news_item_page_order where news_item_id=$news_item_id)
    		";
    // Get rid of duplicates.
    $page_info_list = $this->query($query);
    $new_list = [];
    foreach ($page_info_list as $key => $value) {
      $new_list[$key] = $value;
    }

    return $new_list;
  }

  /**
   * Gets info for the hidden blurbs on a page.
   */
  public function getHiddenPageInfoForBlurb($news_item_id) {

    $news_item_id = (int) $news_item_id;
    $query = "select p.* from page p,
    		page_category pc, news_item_category nic, news_item ni  where 
    		nic.news_item_id=$news_item_id and nic.category_id=pc.category_id and
    		ni.news_item_id=nic.news_item_id and ni.news_item_status_id=" . NEWS_ITEM_STATUS_ID_HIDDEN . " and
    		pc.page_id=p.page_id and p.page_id not in ( select page_id from 
    		news_item_page_order where news_item_id=$news_item_id)
    		";
    // Get rid of duplicates.
    $page_info_list = $this->query($query);
    $new_list = [];
    foreach ($page_info_list as $key => $value) {
      $new_list[$key] = $value;
    }

    return $new_list;
  }

  /**
   * Gets blurb info given a blurbs news_item_id.
   */
  public function getBlurbInfoFromNewsItemId($news_item_id) {

    $news_item_db_class = new NewsItemDB();
    $version_id = $news_item_db_class->getCurrentVersionId($news_item_id);

    $version_info = $this->getBlurbInfoFromVersionId($version_id);

    return $version_info;
  }

  /**
   * Gets blurb info given a blurbs version_id.
   */
  public function getBlurbInfoFromVersionId($version_id) {

    $news_item_version_db_class = new NewsItemVersionDB();
    $news_item_db_class = new NewsItemDB();
    $version_info = $news_item_version_db_class->getNewsItemVersionInfoFromVersionId($version_id);
    $item_info = $news_item_db_class->getNewsItemInfo($version_info['news_item_id']);
    $blurb_info = array_merge($version_info, $item_info);

    // Get attached breaking news items. An index should make this query
    // pretty fast. For speed, do not sort and only get "Other" items.
    $query = 'SELECT news_item_id FROM news_item WHERE parent_item_id = '
          . $version_info['news_item_id'] . ' AND news_item_type_id = '
          . NEWS_ITEM_TYPE_ID_BREAKING . ' AND news_item_status_id = '
          . NEWS_ITEM_STATUS_ID_OTHER;
    $blurb_info['breaking_items'] = $this->query($query);

    return $blurb_info;
  }

  /**
   * Adds a blurb to a page and its associated categories.
   */
  public function addBlurbToCategoriesAndPage($news_item_id, $page_id) {

    $feature_page_db_class = new FeaturePageDB();
    $this->addBlurbToPage($news_item_id, $page_id);
    $category_list = $feature_page_db_class->getCategoryIdsForPage($page_id);
    foreach ($category_list as $next_category_id) {
      if (!$this->isNewsItemInCategory($news_item_id, $next_category_id)) {
        $this->addNewsItemCategory($news_item_id, $next_category_id);
      }
    }

  }

  /**
   * Adds blubs to pages associated with a category.
   *
   * As well as adding the category if it isn't there.
   */
  public function addBlurbToPagesForGivenCategory($news_item_id, $category_id) {

    $feature_page_db_class = new FeaturePageDB();
    if (!$this->isNewsItemInCategory($news_item_id, $category_id)) {
      $this->addNewsItemCategory($news_item_id, $category_id);
    }
    $page_ids = $feature_page_db_class->getAssociatedPageIds([$category_id]);
    if (count($page_ids) > 0) {
      foreach ($page_ids as $page_id) {
        $this->addBlurbToPage($news_item_id, $page_id);
      }
    }

  }

  /**
   * Adds blurb to a page.
   */
  public function addBlurbToPage($news_item_id, $page_id) {

    // Make sure blurb isnt already added
    // Make sure this really is a blurb
    // get max_ordernum on page < 10000.
    $news_item_id = (int) $news_item_id;
    $page_id = (int) $page_id;
    $order_num = 10;
    $query = 'SELECT MAX(order_num) FROM news_item_page_order WHERE page_id = ' . $page_id . ' AND order_num < 10000';
    $numlist = $this->singleColumnQuery($query);
    if ($numlist && $numlist > 0) {
      $order_num = array_pop($numlist) + 10;
    }

    $query = 'insert into news_item_page_order (news_item_id, page_id, order_num, display_option_id)';
    $query .= " values ($news_item_id, $page_id, $order_num,0)";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Removes a blurb from the current page (archiving it)
   */
  public function removeBlurbFromPage($news_item_id, $page_id) {

    $news_item_id = (int) $news_item_id;
    $page_id = (int) $page_id;
    $query = "delete from  news_item_page_order  where news_item_id=$news_item_id
        	and page_id=$page_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Adds a new blurb.
   */
  public function createNewBlurb($post_array) {

    $post_array['version_created_by_id'] = $_SESSION['session_user_id'];
    $post_array['media_type_grouping_id'] = 0;
    $post_array['thumbnail_media_attachment_id'] = $post_array['thumbnail_media_attachment_id'] + 0;
    if (!isset($post_array['is_summary_html'])) {
      $post_array['is_summary_html'] = 0;
    }
    if (!isset($post_array['is_text_html'])) {
      $post_array['is_text_html'] = 0;
    }

    $post_array['display_contact_info'] = 0;
    $post_array['displayed_author_name'] = '';
    $post_array['event_duration'] = 0;

    if (trim($post_array['media_attachment_id']) == '') {
      $post_array['media_attachment_id'] = 0;
    }
    $news_item_db_class = new NewsItemDB();
    $news_item_id = $news_item_db_class->addNewsItemForRegisteredUser(NEWS_ITEM_STATUS_ID_NEW, NEWS_ITEM_TYPE_ID_BLURB, 0);
    $news_item_version_db_class = new NewsItemVersionDB();
    $news_item_version_db_class->addNewsItemVersion($news_item_id, $post_array);

    return $news_item_id;
  }

  /**
   * Updates an order entry (reordering and changing template)
   */
  public function updateOrderEntry($page_id, $news_item_id, $order_num, $display_option_id) {

    $page_id = (int) $page_id;
    $news_item_id = (int) $news_item_id;
    $order_num = (int) $order_num;
    $display_option_id = (int) $display_option_id;
    if ($order_num == 0) {
      return -1;
    }
    $query = "update news_item_page_order set order_num=$order_num,
        	display_option_id=$display_option_id where page_id=$page_id and
        	news_item_id=$news_item_id";
    $this->executeStatement($query);

    return 1;
  }

  /**
   * Adds a new version entry for a blurb.
   */
  public function createNewVersion($news_item_id, $post_array) {

    $post_array['version_created_by_id'] = $_SESSION['session_user_id'];
    $post_array['media_type_grouping_id'] = 0;

    $post_array['thumbnail_media_attachment_id'] = $post_array['thumbnail_media_attachment_id'] + 0;
    $post_array['event_duration'] = 0;
    if (!isset($post_array['is_summary_html'])) {
      $post_array['is_summary_html'] = 0;
    }
    if (!isset($post_array['is_text_html'])) {
      $post_array['is_text_html'] = 0;
    }
    $post_array['display_contact_info'] = 0;

    if (trim($post_array['media_attachment_id']) == '') {
      $post_array['media_attachment_id'] = 0;
    }
    $news_item_version_db_class = new NewsItemVersionDB();
    $news_item_version_id = $news_item_version_db_class->addNewsItemVersionIfChanged($news_item_id, $post_array);

    return $news_item_version_id;
  }

  /**
   * Removes a blurb from all categories associated with a page.
   */
  public function removeBlurbFromCategory($news_item_id, $page_id) {

    $this->removeBlurbFromPage($news_item_id, $page_id);
    $feature_page_db_class = new FeaturePageDB();
    $category_list = $feature_page_db_class->getCategoryIdsForPage($page_id);
    foreach ($category_list as $category_id) {
      $this->removeNewsItemCategory($news_item_id, $category_id);
    }

  }

}
