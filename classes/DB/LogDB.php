<?php

namespace Indybay\DB;

use Indybay\Cache\LogCache;

/**
 * Manages logging of ips to the DB if they match the current settings.
 *
 * Written December 2005.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class LogDB extends DB {

  /**
   * Adds log entry to the DB.
   *
   * If the HTTP headers match the current settings for what should be
   * logged.
   */
  public function addLogEntry() {

    $referring_url = getenv('HTTP_REFERER');

    $request_method = $_SERVER['REQUEST_METHOD'];

    $log_cache = new LogCache();
    if (!$log_cache->shouldRecord($request_method)) {
      return;
    }

    if ($log_cache->shouldRecordIps($request_method)) {
      if (getenv('REMOTE_ADDR')) {
        $ip = getenv('REMOTE_ADDR');
      }
      elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      }
      elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      }
      else {
        $ip = 'UNKNOWN';
      }
      if ($ip != 'UNKNOWN') {
        $ip_pieces = explode('.', $ip);
        $ip_part1 = $ip_pieces[0] + 0;
        $ip_part2 = $ip_pieces[1] + 0;
        $ip_part3 = $ip_pieces[2] + 0;
        $ip_part4 = $ip_pieces[3] + 0;
      }
    }
    else {
      $ip = 0;
      $ip_part1 = 0;
      $ip_part2 = 0;
      $ip_part3 = 0;
      $ip_part4 = 0;
    }

    $query_string = '';
    if ($_SERVER['QUERY_STRING'] != '') {
      $query_string = '?' . $_SERVER['QUERY_STRING'];
    }
    $url = $_SERVER['SCRIPT_NAME'] . $query_string;

    $news_item_id = 0;
    $parent_news_item_id = 0;

    if (isset($_GET['news_item_id'])) {
      $news_item_id = $_GET['news_item_id'] + 0;
    }
    if (strrpos($url, 'newsitems') > 0) {
      $i = strrpos($url, '/');

      $j = strrpos($url, '.');
      $parent_news_item_id = substr($url, $i + 1, $j - $i - 1) + 0;
    }
    elseif (isset($_GET['top_id'])) {
      $parent_news_item_id = $_GET['top_id'] + 0;
    }
    elseif (isset($_POST['parent_item_id'])) {
      $parent_news_item_id = $_POST['parent_item_id'] + 0;
    }

    $full_headers = '';

    if (isset($_SERVER['REDIRECT_URL'])) {
      $full_headers .= 'REDIRECT_URL: ' . $_SERVER['REDIRECT_URL'] . "\r\n";
    }
    $full_headers .= 'HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";

    $ip = $this->prepareString($ip);
    $ip_part1 = (int) $ip_part1;
    $ip_part2 = (int) $ip_part2;
    $ip_part3 = (int) $ip_part3;
    $ip_part4 = (int) $ip_part4;
    $request_method = $this->prepareString($request_method);
    $url = $this->prepareString($url);
    $referring_url = $this->prepareString($referring_url);
    $parent_news_item_id = (int) $parent_news_item_id;
    $full_headers = $this->prepareString($full_headers);

    $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',$parent_news_item_id,
			'$full_headers',null)";

    $log_id = $this->executeStatementReturnAutokey($query);
    $GLOBALS['log_id'] = $log_id;

    return $news_item_id;
  }

  /**
   * This update happens at the end of when pages run.
   *
   * So it can be determined how long searches and uploads are taking.
   */
  public function updateLogIdEndTime() {

    if (isset($GLOBALS['log_id'])) {
      $sql = 'update logs set end_time=Now() where log_id=' . $GLOBALS['log_id'];
      $this->executeStatement($sql);
    }
  }

  /**
   * Adds log entry at publish time.
   *
   * Logging to the DB at publish time is slightly different than at other
   * times since more data is available.
   */
  public function addLogEntryBeforePublish() {

    $log_cache = new LogCache();
    if (!$log_cache->shouldRecord('DB')) {
      return;
    }

    $referring_url = getenv('HTTP_REFERER');

    if ($log_cache->shouldRecordIps('DB')) {
      if (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      }
      elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      }
      elseif (getenv('REMOTE_ADDR')) {
        $ip = getenv('REMOTE_ADDR');
      }
      else {
        $ip = 'UNKNOWN';
      }
      $ip_pieces = explode('.', $ip);
      $ip_part1 = $ip_pieces[0] + 0;
      $ip_part2 = $ip_pieces[1] + 0;
      $ip_part3 = $ip_pieces[2] + 0;
      $ip_part4 = $ip_pieces[3] + 0;
    }
    else {
      $ip = 0;
      $ip_part1 = 0;
      $ip_part2 = 0;
      $ip_part3 = 0;
      $ip_part4 = 0;
    }

    $query_string = '';
    if ($_SERVER['QUERY_STRING'] != '') {
      $query_string = '?' . $_SERVER['QUERY_STRING'];
    }
    $url = $_SERVER['SCRIPT_NAME'] . $query_string;

    $parent_news_item_id = 0;
    if (isset($_POST['parent_item_id'])) {
      $parent_news_item_id = $_POST['parent_item_id'];
    }

    $request_method = 'DB';

    $full_headers = '';
    if (isset($_SERVER['REDIRECT_URL'])) {
      $full_headers .= 'REDIRECT_URL: ' . $_SERVER['REDIRECT_URL'] . "\r\n";
    }
    $full_headers .= 'HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
    $full_headers .= 'POST_TITLE1: ' . $_POST['title1'] . "\r\n";
    $full_headers .= 'POST_AUTHOR: ' . $_POST['displayed_author_name'] . "\r\n";
    $full_headers .= 'POST_EMAIL: ' . $_POST['email'] . "\r\n";

    $ip = $this->prepareString($ip);
    $ip_part1 = (int) $ip_part1;
    $ip_part2 = (int) $ip_part2;
    $ip_part3 = (int) $ip_part3;
    $ip_part4 = (int) $ip_part4;
    $request_method = $this->prepareString($request_method);
    $url = $this->prepareString($url);
    $referring_url = $this->prepareString($referring_url);
    $parent_news_item_id = (int) $parent_news_item_id;
    $full_headers = $this->prepareString($full_headers);

    $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id, news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',$parent_news_item_id,0,
			'$full_headers', null)";

    $log_id = $this->executeStatementReturnAutokey($query);
    $GLOBALS['db_log_id'] = $log_id;

  }

  /**
   * Updates end time.
   *
   * So it can be determined how long publishing took (as compared to file
   * uploading).
   */
  public function updateLogEntryAfterPublish() {
    if (isset($GLOBALS['just_added_id']) && array_key_exists('db_log_id', $GLOBALS)) {
      $sql = 'update logs set end_time=Now(), news_item_id=' . $GLOBALS['just_added_id'] . ' where log_id=' . $GLOBALS['db_log_id'];
      $this->executeStatement($sql);
    }

  }

  /**
   * Code to add log entry before a search is run.
   */
  public function addLogEntryBeforeSearch() {

    $log_cache = new LogCache();
    if (!$log_cache->shouldRecord('SEARCH')) {
      return;
    }

    $referring_url = getenv('HTTP_REFERER');

    if ($log_cache->shouldRecordIps('SEARCH')) {
      if (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      }
      elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      }
      elseif (getenv('REMOTE_ADDR')) {
        $ip = getenv('REMOTE_ADDR');
      }
      else {
        $ip = 'UNKNOWN';
      }
      $ip_pieces = explode('.', $ip);
      $ip_part1 = $ip_pieces[0] + 0;
      $ip_part2 = $ip_pieces[1] + 0;
      $ip_part3 = $ip_pieces[2] + 0;
      $ip_part4 = $ip_pieces[3] + 0;
    }
    else {
      $ip = 0;
      $ip_part1 = 0;
      $ip_part2 = 0;
      $ip_part3 = 0;
      $ip_part4 = 0;
    }
    $query_string = '';
    if ($_SERVER['QUERY_STRING'] != '') {
      $query_string = '?' . $_SERVER['QUERY_STRING'];
    }
    $url = $_SERVER['SCRIPT_NAME'] . $query_string;

    $request_method = 'SEARCH';

    $full_headers = '';
    if (isset($_SERVER['REDIRECT_URL'])) {
      $full_headers .= 'REDIRECT_URL: ' . $_SERVER['REDIRECT_URL'] . "\r\n";
    }
    $full_headers .= 'HTTP_USER_AGENT: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";

    $ip = $this->prepareString($ip);
    $ip_part1 = (int) $ip_part1;
    $ip_part2 = (int) $ip_part2;
    $ip_part3 = (int) $ip_part3;
    $ip_part4 = (int) $ip_part4;
    $request_method = $this->prepareString($request_method);
    $url = $this->prepareString($url);
    $referring_url = $this->prepareString($referring_url);
    $full_headers = $this->prepareString($full_headers);

    $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id, news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',0,0,
			'$full_headers', null)";

    $log_id = $this->executeStatementReturnAutokey($query);
    $GLOBALS['search_log_id'] = $log_id;

    return $log_id;
  }

  /**
   * Code to update log entry at end of searching.
   */
  public function updateLogIdAfterSearch() {

    if (array_key_exists('search_log_id', $GLOBALS)) {
      $sql = 'update logs set end_time=Now() where log_id=' . $GLOBALS['search_log_id'];
      $this->executeStatement($sql);
    }
  }

  /**
   * Returns the log list for the admin log list page.
   */
  public function getLogList(
    $page_number,
    $page_size,
    $ip,
    $ip_part1,
    $ip_part2,
    $ip_part3,
    $method_type,
    $url,
    $referring_url,
    $news_item_id,
    $parent_news_item_id,
    $keyword,
    $sort_by,
  ) {

    $page_number = (int) $page_number;
    $page_size = (int) $page_size;
    $logs_query = 'select *,UNIX_TIMESTAMP(end_time)-UNIX_TIMESTAMP(start_time) as time_diff ';
    $logs_query .= $this->getLogListQuery($ip, $ip_part1, $ip_part2, $ip_part3,
    $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword);
    if ($sort_by == 'DURATION') {
      $logs_query .= ' order by UNIX_TIMESTAMP(end_time)-UNIX_TIMESTAMP(start_time) desc';
    }
    else {
      $logs_query .= ' order by log_id desc';
    }
    $logs_query .= ' limit ' . (0 + $page_size * ($page_number - 1)) . ', ' . $page_size;

    $result = $this->query($logs_query);

    return $result;
  }

  /**
   * Helper function used for getting log list for admin log list page.
   */
  public function getLogListQuery(
    $ip,
    $ip_part1,
    $ip_part2,
    $ip_part3,
    $method_type,
    $url,
    $referring_url,
    $news_item_id,
    $parent_news_item_id,
    $keyword,
  ) {

    $needs_and = FALSE;
    $needs_where = TRUE;
    $logs_query = ' from logs ';

    if ($method_type != '' and $method_type != '0') {
      $logs_query .= "where http_method='" . $method_type . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;

    }

    if ($ip != '') {
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " ip='" . $this->prepareString($ip) . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($ip_part1 != '') {
      $ip_part1 = (int) $ip_part1;
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " ip_part1='" . $ip_part1 . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($ip_part2 != '') {
      $ip_part2 = (int) $ip_part2;
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " ip_part2='" . $ip_part2 . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($ip_part3 != '') {
      $ip_part3 = (int) $ip_part3;
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " ip_part3='" . $ip_part3 . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($url != '') {
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " url like '%" . $this->prepareString($url) . "%' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($referring_url != '') {
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " referring_url like '%" . $this->prepareString($referring_url) . "%' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($news_item_id != '') {
      $news_item_id = (int) $news_item_id;
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " news_item_id='" . $news_item_id . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($parent_news_item_id != '') {
      $parent_news_item_id = (int) $parent_news_item_id;
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " parent_news_item_id='" . $parent_news_item_id . "' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    if ($keyword != '') {
      if ($needs_where) {
        $logs_query .= ' where ';
      }
      if ($needs_and) {
        $logs_query .= ' and ';
      }
      $logs_query .= " full_headers like '%" . $this->prepareString($keyword) . "%' ";
      $needs_and = TRUE;
      $needs_where = FALSE;
    }

    return $logs_query;

  }

  /**
   * Needed for paging link to last page on log list admin page.
   */
  public function getLogListMaxPage(
    $page_size,
    $ip,
    $ip_part1,
    $ip_part2,
    $ip_part3,
    $method_type,
    $url,
    $referring_url,
    $news_item_id,
    $parent_news_item_id,
    $keyword,
  ) {

    $logs_query = 'select count(*) ';
    $logs_query .= $this->getLogListQuery($ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword);

    $result = $this->query($logs_query);

    $length = array_pop(array_pop($result));

    $page_max = ceil($length / $page_size);

    return $page_max;
  }

  /**
   * In admin there are several ways logs can be cleared.
   *
   * This clears GET logs.
   */
  public function clearGetLogs() {
    $query = "delete from logs where http_method='GET'";
    $this->executeStatement($query);
  }

  /**
   * In admin there are several ways logs can be cleared.
   *
   * This clears all logs.
   */
  public function clearAllLogs() {

    $query = 'truncate logs';
    $this->executeStatement($query);
  }

  /**
   * Clears all ips from logs.
   */
  public function clearIps() {

    $query = "update logs set ip_part3=0,ip_part2=0, ip_part1=0, ip=''";
    $this->executeStatement($query);
  }

}
