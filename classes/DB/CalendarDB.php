<?php

namespace Indybay\DB;

/**
 * Calendar database class.
 *
 * The event DB class is responsible for all Calendar related DB access
 * for individual events most of the sql is inherited from ArticleDB
 * the main code in this class relates to finding events between dates and
 * saving and loading event type associations.
 *
 * Written December 2005 - January 2006
 * somewhat extended from something in sf-active.
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class CalendarDB extends ArticleDB {

  /**
   * Gets events between dates.
   */
  public function getEventsBetweenDates($date1, $date2) {
    $sql = "select ni.news_item_id, niv.title1, news_item_status_id, 
			 unix_timestamp(creation_date) as creation_timestamp, event_duration,
	        	 	date_format(creation_date,'%W %b %D %l:%i %p') as created,
	        	 	unix_timestamp(displayed_date) as event_date_timestamp
	        	 from news_item ni, news_item_version niv 
	        	 where ni.current_version_id=niv.news_item_version_id
	        	 and displayed_date >= '" . $this->prepareString($date1) . "'
	        	 and displayed_date <= '" . $this->prepareString($date2) . "'
	        	 and news_item_type_id=" . NEWS_ITEM_TYPE_ID_EVENT . '
	        	 and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN . '
	        	 and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN . '
	        	 and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN . '
	        	 order by displayed_date';
    $result = $this->query($sql);

    return $result;
  }

  /**
   * Gets the event type for an event.
   */
  public function getEventTypeInfoForNewsitem($news_item_id) {

    $event_type_info = NULL;

    $news_item_id = (int) $news_item_id;
    $sql = 'select k.keyword_id, k.keyword from keyword k, news_item_keyword nik
		where nik.keyword_id=k.keyword_id and nik.news_item_id=' . $news_item_id .
    ' and k.keyword_type_id=1';
    $result = $this->query($sql);
    if (is_array($result) &&count($result) > 0) {
      $event_type_info = array_pop($result);
    }

    return $event_type_info;
  }

  /**
   * Gets list of event types for a dropdown list.
   */
  public function getEventTypeForSelectList() {

    $event_type_select_list = [];

    $sql = 'SELECT keyword, keyword_id FROM keyword WHERE keyword_type_id = 1 ORDER BY keyword';
    $result = $this->query($sql);

    if (is_array($result) &&count($result) > 0) {
      foreach ($result as $row) {
        $event_type_select_list[$row['keyword_id']] = $row['keyword'];
      }
    }

    return $event_type_select_list;
  }

  /**
   * Adds or changes the event type of an event.
   */
  public function setOrChangeEventType($news_item_id, $keyword_id) {

    $news_item_id = (int) $news_item_id;
    $keyword_id = (int) $keyword_id;
    $sql = 'delete from news_item_keyword where news_item_id=' . $news_item_id . '
		and keyword_id in (select keyword_id from keyword where keyword_type_id=1)';

    $this->executeStatement($sql);
    $sql = 'insert into news_item_keyword (news_item_id, keyword_id) values (';
    $sql .= $news_item_id . ', ' . $keyword_id . ' )';

    $this->executeStatement($sql);

  }

}
