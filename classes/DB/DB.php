<?php

namespace Indybay\DB;

use Indybay\Common;

/**
 * Common DB classes used as a wrapper around all database access.
 *
 * Written December 2005 - January 2006
 * modified from an sf-active class.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class DB extends Common {

  /**
   * Get conection.
   */
  public function getConnection() {

    ini_set('mssql.timeout', '120');

    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      @$GLOBALS['db_conn'] = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);

      $i = 0;
      while ($i < 7 && $GLOBALS['db_conn'] == '') {
        sleep(5 * $i);
        @$GLOBALS['db_conn'] = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        $i = $i + 1;
      }

      if ($GLOBALS['db_conn'] == '') {
        $GLOBALS['db_down'] = '1';
      }
      else {
        @$GLOBALS['db_conn']->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
        @mysqli_select_db($GLOBALS['db_conn'], DB_DATABASE);
        @mysqli_set_charset($GLOBALS['db_conn'], 'utf8mb4');
      }
    }
  }

  /**
   * Runs a sql query.
   */
  public function query($sql) {

    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      $this->getConnection();

      @$result_prt = mysqli_query($GLOBALS['db_conn'], $sql);

      $error_num = $this->errorNum($GLOBALS['db_conn']);

      if ($error_num < 0) {
        $result = $error_num;
      }
      else {
        $result_array = [];
        $i = 0;
        while (@$next_row = mysqli_fetch_array($result_prt)) {
          $result_array[$i] = $next_row;
          $i = $i + 1;
        }
        $result = $result_array;
      }

      if ($error_num < 0) {
      }
      else {
      }
    }
    return $result;
  }

  /**
   * Single column query.
   */
  public function singleColumnQuery($sql) {

    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      if (!array_key_exists('db_conn', $GLOBALS) || !$GLOBALS['db_conn']) {
        $this->getConnection();
      }
      @$result_prt = mysqli_query($GLOBALS['db_conn'], $sql);
      $error_num = $this->errorNum($GLOBALS['db_conn']);

      if ($error_num < 0) {
        $result = $error_num;
      }
      else {
        $result_array = [];
        while (@$next_row = mysqli_fetch_array($result_prt)) {
          array_push($result_array, array_pop($next_row));
        }
        $result = $result_array;
      }

      if ($error_num < 0) {
      }
      else {
      }
    }
    return $result;
  }

  /**
   * Executes statement.
   */
  public function executeStatement($sql) {
    $error_num = 0;
    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      if (!array_key_exists('db_conn', $GLOBALS)) {
        $this->getConnection();
      }
      @mysqli_query($GLOBALS['db_conn'], $sql);
      $error_num = $this->errorNum($GLOBALS['db_conn']);
      if ($error_num < 0) {
      }
      else {
      }
    }
    return $error_num;
  }

  /**
   * Executes statement and returns key.
   */
  public function executeStatementReturnAutokey($sql) {
    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {
      if (!isset($GLOBALS['db_conn'])) {
        $this->getConnection();
      }
      @mysqli_query($GLOBALS['db_conn'], $sql);
      $error_num = $this->errorNum($GLOBALS['db_conn']);
      if ($error_num == 0) {
        $result_num = mysqli_insert_id($GLOBALS['db_conn']);
      }
      else {
        $result_num = $error_num;
      }
      if ($error_num < 0) {
      }
      else {
      }
    }
    return $result_num;
  }

  /**
   * Prepares string.
   */
  public function prepareString($str) {
    if (empty($GLOBALS['db_conn'])) {
      $this->getConnection();
    }
    $str = $this->convertcharsets($str);
    $str = mysqli_real_escape_string($GLOBALS['db_conn'], $str);
    return $str;
  }

  /**
   * Prepares reloaded string.
   */
  public function prepareReloadedString($str) {
    return $this->prepareString($str);
  }

  /**
   * Prepares integer.
   */
  public function prepareInt($str) {
    return (int) $str;
  }

  /**
   * Returns an error number as a negative number.
   *
   * To distinguish it from an id (in the case of inserts).
   */
  public function errorNum() {
    $ret = -1;
    @$ret = -1 * mysqli_errno($GLOBALS['db_conn']);
    return $ret;
  }

  /**
   * Closes connection.
   */
  public function close() {

    if ($GLOBALS['db_down'] != '1') {
      mysqli_close($GLOBALS['db_conn']);
    }
  }

  /**
   * Converts character sets.
   */
  public function convertcharsets($html_string) {

    if (strlen($html_string) && preg_match('/^./us', $html_string) != 1) {

      $html_string2 = $html_string;
      @$html_string2 = iconv('Windows-1252', 'UTF-8', $html_string2);
      if (mb_strlen($html_string2) < mb_strlen($html_string) * 0.75  || mb_strpos(' ' . $html_string2, 'Â') > 0) {
        $html_string2 = mb_convert_encoding($html_string, 'UTF-8');
      }
      if (mb_strlen($html_string2) < mb_strlen($html_string) * 0.75 || mb_strpos($html_string2, 'Â')) {
        $html_string2 = $html_string;
      }
      $html_string = $html_string2;
    }

    return $html_string;
  }

}
