<?php

namespace Indybay\DB;

/**
 * Written December 2005 - January 2006.
 *
 * Deals with the DB queries on the search pages which appear in
 * sections on the site. The "newswire" at /news/ and the
 * article_list page in admin.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class SearchDB extends NewsItemVersionDB {

  /**
   * Used by the create_status_restrictions_or method to.
   *
   * Figure out how to create the proper SQL to query the correct statuses.
   *
   * @var array
   */
  public $newsItemStatusIds = [2, 3, 5, 7, 11, 13, 17, 19, 23];

  /**
   * Turned a restriction number into a sequence of ors.
   */
  public function createStatusRestrictionOrs($news_item_status_restriction, $prefix) {

    $news_item_status_restriction = (int) $news_item_status_restriction;

    if ($news_item_status_restriction == 0) {
      return '';
    }
    $query = '';
    $i = 0;
    foreach ($this->newsItemStatusIds as $next_id) {
      if (is_int($news_item_status_restriction / $next_id)) {
        if ($i > 0) {
          $query .= ', ';
        }
        $query .= (int) $next_id;
        $i = $i + 1;
      }
    }

    if ($i == 0) {
      // No results.
      $query = ' and ' . $prefix . 'news_item_status_id=0 ';
    }
    elseif ($i == 1) {
      $query = ' and ' . $prefix . 'news_item_status_id=' . $news_item_status_restriction;
    }
    elseif ($i == count($this->newsItemStatusIds)) {
      $query = '';
    }
    elseif ($i > count($this->newsItemStatusIds) - 3) {
      $query = '';
      foreach ($this->newsItemStatusIds as $next_id) {
        if (!is_int($news_item_status_restriction / $next_id)) {
          $query .= ' and ' . $prefix . 'news_item_status_id!=' . (int) $next_id;
        }
      }
    }
    else {
      $query = ' and ' . $prefix . 'news_item_status_id in (' . $query . ')';
    }

    return $query;
  }

  /**
   * Standard search.
   */
  public function standardSearch(
    $page_size,
    $page_number,
    $search,
    $news_item_status_restriction,
    $include_posts,
    $include_comments,
    $include_attachments,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $parent_item_id,
    $search_date_type = '',
    $date_range_start = '',
    $date_range_end = '',
    $just_count = 0,
    $min_item_id = 0,
    $max_item_id = 0,
  ) {

    if ($page_number < 0) {
      $page_number = 0;
    }
    $start_limit = $page_number * $page_size;

    if ($include_comments == 1 || $include_attachments == 1) {
      $result = $this->commentAttachmentSearch($start_limit, $page_size, $search, $news_item_status_restriction,
                    $include_posts, $include_comments, $include_attachments, $include_events, $include_blurbs, $media_type_grouping_id, $topic_id, $region_id, $parent_item_id,
                    $search_date_type, $date_range_start, $date_range_end, $just_count, $min_item_id, $max_item_id);
    }
    else {
      if ($just_count == 0) {
        $query = 'select nv.news_item_version_id, nv.media_attachment_id,nv.thumbnail_media_attachment_id, ni.news_item_status_id,ni.news_item_type_id, ni.news_item_id as news_item_id, ' .
                        'unix_timestamp(ni.creation_date) as creation_timestamp,  unix_timestamp(nv.displayed_date) as displayed_timestamp, ' .
                        'nv.displayed_author_name as displayed_author_name, nv.title1,nv.title2, ' .
                        'ni.parent_item_id as parent_item_id, ni.media_type_grouping_id, ' .
                        'nv.summary as summary,nv.text, ni.num_comments as num_comments, ' .
        'ni.news_item_type_id as news_item_type_id, nv.event_duration, nv.is_text_html, nv.is_summary_html ';
      }
      else {
        $query = 'select count(*) ';
      }
      $query .= ' from news_item ni ';
      if ($just_count == 0 || $search_date_type == 'displayed_date') {
        $query .= ', news_item_version nv';
      }
      if ($region_id) {
        $query .= ', news_item_category nc1 ';
      }
      if ($topic_id) {
        $query .= ', news_item_category nc2 ';
      }
      if ($search) {
        $query .= ', news_item_fulltext nift ';
      }
      $query .= ' where ';
      if ($just_count == 0 || $search_date_type == 'displayed_date') {
        $query .= ' ni.current_version_id=nv.news_item_version_id ';
      }
      else {
        $query .= ' ni.current_version_id!=0  ';
      }
      $query .= $this->createStatusRestrictionOrs($news_item_status_restriction, 'ni.');
      if ($parent_item_id != 0) {
        $query .= " and ni.parent_item_id= (int) $parent_item_id";
      }
      if ($region_id) {
        $query .= ' and ni.news_item_id=nc1.news_item_id and nc1.category_id= ' . (int) $region_id . ' ';
      }
      if ($topic_id) {
        $query .= ' and ni.news_item_id=nc2.news_item_id and nc2.category_id= ' . (int) $topic_id . ' ';
      }

      // Added or section to speed things up a bit.
      if ($include_posts == 1 && $include_comments != 1 && $include_attachments != 1 && $include_events != 1 && $include_blurbs != 1) {
        $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_POST;
      }
      elseif ($include_posts == 0 && $include_comments != 1 && $include_attachments != 1 && $include_events == 1 && $include_blurbs != 1) {
        $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
      }
      elseif ($include_posts == 0 && $include_comments != 1 && $include_attachments != 1 && $include_events == 0 && $include_blurbs == 1) {
        $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_BLURB;
      }
      elseif ($include_posts == 1 && $include_comments != 1 && $include_attachments != 1 && $include_events == 1 && $include_blurbs != 1) {
        $query .= ' and  news_item_type_id in (' . NEWS_ITEM_TYPE_ID_POST . ', ' . NEWS_ITEM_TYPE_ID_EVENT . ' )';
      }
      elseif ($include_posts == 1 && $include_comments != 1 && $include_attachments == 1 && $include_events != 1 && $include_blurbs != 1) {
        $query .= ' and  news_item_type_id in ( ' . NEWS_ITEM_TYPE_ID_POST . ', ' . NEWS_ITEM_TYPE_ID_ATTACHMENT . ' )';
      }
      else {
        if ($include_posts != 1) {
          $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_POST;
        }
        if ($include_comments != 1) {
          $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_COMMENT;
        }
        if ($include_attachments != 1) {
          $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_ATTACHMENT;
        }
        if ($include_events != 1) {
          $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_EVENT .
                               ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_EVENT_LOCATION;
        }
        if ($include_blurbs != 1) {
          $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_BLURB;
        }
      }

      if ($search) {
        $query .= ' and nift.news_item_id=ni.news_item_id';
      }

      if (isset($search_date_type) && $search_date_type != '') {
        if ($search_date_type == 'displayed_date') {
          $query .= " and nv.displayed_date>'" . $date_range_start->getSqlDate() . "'";
          $query .= " and nv.displayed_date<'" . $date_range_end->getSqlDate() . "'";
        }
        if ($search_date_type == 'creation_date') {
          $query .= " and ni.creation_date>'" . $date_range_start->getSqlDate() . "'";
          $query .= " and ni.creation_date<'" . $date_range_end->getSqlDate() . "'";
        }
      }

      if ($media_type_grouping_id + 0 == 1) {
        $query .= ' and (ni.media_type_grouping_id is null or
       		 						ni.media_type_grouping_id=1 or ni.media_type_grouping_id=0) ';
      }
      elseif ($media_type_grouping_id + 0 > 0) {
        $query .= ' and ni.media_type_grouping_id= ' . (int) $media_type_grouping_id . ' ';
      }
      if ($search != '') {
        $query .= ' AND MATCH (nift.normalized_text) AGAINST ';
        $query .= " ('" . $this->prepareString($search) . "' IN BOOLEAN MODE) ";
      }

      if ($just_count == 0) {
        if ($search_date_type == 'displayed_date') {
          $query .= ' order by nv.displayed_date';
        }
        else {
          $query .= ' order by ' . $this->orderByTable($region_id, $topic_id, $search) . '.news_item_id desc';
        }
      }

      if ($just_count == 0) {
        $query .= ' limit ' . (int) $start_limit . ',' . (int) $page_size;
      }

      // Catch exception due to invalid fulltext query.
      try {
        $result = $this->query($query);
      }
      catch (\mysqli_sql_exception $e) {
        $result = $just_count ? [[0]] : [];
      }
    }
    if ($just_count == 1 && is_array($result) && count($result) == 1) {
      $result = array_pop($result);
      $result = array_pop($result);
    }
    elseif ($just_count == 1 && is_array($result) && count($result) == 0) {
      $result = -1;
    }
    elseif ($just_count == 1) {
      $result = -2;
    }

    return $result;

  }

  /**
   * Used for searchs that include comments.
   */
  public function commentAttachmentSearch(
    $start_limit,
    $page_size,
    $search,
    $news_item_status_restriction,
    $include_posts,
    $include_comments,
    $include_attachments,
    $include_events,
    $include_blurbs,
    $media_type_grouping_id,
    $topic_id,
    $region_id,
    $parent_item_id,
    $search_date_type = '',
    $date_range_start = '',
    $date_range_end = '',
    $just_count = 0,
    $min_item_id = 0,
    $max_item_id = 0,
  ) {

    if ($just_count == 0) {
      $query = 'select nv.news_item_version_id, nv.media_attachment_id,nv.thumbnail_media_attachment_id, ni.news_item_status_id,ni.news_item_type_id,  ni.news_item_id as news_item_id, ' .
                    'unix_timestamp(ni.creation_date) as creation_timestamp, unix_timestamp(nv.displayed_date) as displayed_timestamp,  ' .
                    'nv.text, nv.displayed_author_name as displayed_author_name, nv.title1,nv.title2, ' .
                    'ni.parent_item_id as parent_item_id, ni.media_type_grouping_id, ' .
                    'nv.summary as summary, ni.num_comments as num_comments, ' .
      'ni.news_item_type_id as news_item_type_id ';
    }
    else {
      $query = 'select count(*) ';
    }

    $query .= ' from news_item ni ';
    if ($just_count == 0 || $search_date_type == 'displayed_date') {
      $query .= ', news_item_version nv';
    }

    if ($region_id) {
      $query .= ', news_item_category nc1 ';
    }
    if ($topic_id) {
      $query .= ', news_item_category nc2 ';
    }
    if ($search != '') {
      $query .= ', news_item_fulltext nift ';
    }
    $query .= ' where ';

    if ($just_count == 0 || $search_date_type == 'displayed_date') {
      $query .= ' ni.current_version_id=nv.news_item_version_id ';
    }
    else {
      $query .= ' ni.current_version_id!=0   ';
    }
    $query .= $this->createStatusRestrictionOrs($news_item_status_restriction, 'ni.');
    if ($region_id) {
      $query .= ' and ni.parent_item_id=nc1.news_item_id and nc1.category_id= ' . (int) $region_id . ' ';
    }
    if ($topic_id) {
      $query .= ' and ni.parent_item_id=nc2.news_item_id and nc2.category_id= ' . (int) $topic_id . ' ';
    }

    // Added or section to speed things up a bit.
    if ($include_posts == 1 && $include_comments != 1 && $include_attachments != 1 && $include_events != 1 && $include_blurbs != 1) {
      $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_POST;
    }
    elseif ($include_posts == 0 && $include_comments != 1 && $include_attachments != 1 && $include_events == 1 && $include_blurbs != 1) {
      $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
    }
    elseif ($include_posts == 0 && $include_comments != 1 && $include_attachments != 1 && $include_events == 0 && $include_blurbs == 1) {
      $query .= ' and news_item_type_id=' . NEWS_ITEM_TYPE_ID_BLURB;
    }
    elseif ($include_posts == 1 && $include_comments != 1 && $include_attachments != 1 && $include_events == 1 && $include_blurbs != 1) {
      $query .= ' and  news_item_type_id in (' . NEWS_ITEM_TYPE_ID_POST . ', ' . NEWS_ITEM_TYPE_ID_EVENT . ' )';
    }
    elseif ($include_posts == 1 && $include_comments != 1 && $include_attachments == 1 && $include_events != 1 && $include_blurbs != 1) {
      $query .= ' and  news_item_type_id in ( ' . NEWS_ITEM_TYPE_ID_POST . ', ' . NEWS_ITEM_TYPE_ID_ATTACHMENT . ' )';
    }
    else {
      if ($include_posts != 1) {
        $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_POST;
      }
      if ($include_comments != 1) {
        $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_COMMENT;
      }
      if ($include_attachments != 1) {
        $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_ATTACHMENT;
      }
      if ($include_events != 1) {
        $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_EVENT .
                               ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_EVENT_LOCATION;
      }
      if ($include_blurbs != 1) {
        $query .= ' and news_item_type_id!=' . NEWS_ITEM_TYPE_ID_BLURB;
      }
    }
    if ($parent_item_id != 0) {
      $query .= ' and ni.parent_item_id=' . (int) $parent_item_id;
    }

    if ($media_type_grouping_id + 0 == 11) {
      $query .= ' and (
       		 						ni.media_type_grouping_id>2) ';
    }
    elseif ($media_type_grouping_id + 0 == 1) {
      $query .= ' and (ni.media_type_grouping_id is null or
       		 						ni.media_type_grouping_id=1 or ni.media_type_grouping_id=0) ';
    }
    elseif ($media_type_grouping_id + 0 > 0) {
      $query .= ' and ni.media_type_grouping_id= ' . (int) $media_type_grouping_id . ' ';
    }
    if ($search != '') {
      $query .= ' and nift.news_item_id=ni.news_item_id';
    }
    if (isset($search_date_type) && $search_date_type != '') {
      if ($search_date_type == 'displayed_date') {
        $query .= " and displayed_date>'" . $date_range_start->getSqlDate() . "'";
        $query .= " and displayed_date<'" . $date_range_end->getSqlDate() . "'";
      }
      if ($search_date_type == 'creation_date') {
        $query .= " and ni.creation_date>'" . $date_range_start->getSqlDate() . "'";
        $query .= " and ni.creation_date<'" . $date_range_end->getSqlDate() . "'";
      }
    }

    if ($search != '') {
      $query .= ' AND MATCH (nift.normalized_text) AGAINST ';
      $query .= " ('" . $this->prepareString($search) . "' IN BOOLEAN MODE) ";
    }

    if ($just_count == 0) {
      if ($parent_item_id != 0) {
        $query .= ' order by ' . $this->orderByTable($region_id, $topic_id, $search) . '.news_item_id desc';
      }
      elseif ($search_date_type == 'displayed_date') {
        $query .= ' order by nv.displayed_date';
      }
      else {
        $query .= ' order by ' . $this->orderByTable($region_id, $topic_id, $search) . '.news_item_id desc';
      }
    }

    if ($just_count == 0) {
      $query .= ' limit ' . (int) $start_limit . ',' . (int) $page_size;
    }

    // Catch exception due to invalid fulltext query.
    try {
      $result = $this->query($query);
    }
    catch (\mysqli_sql_exception $e) {
      $result = $just_count ? [[0]] : [];
    }

    return $result;

  }

  /**
   * Helper function to determine which table to sort on.
   *
   * Use whichever table is faster: this can be determined
   * by running EXPLAIN on the query; "Using temporary" and
   * "Using filesort" should be avoided at all costs.
   */
  public function orderByTable($region_id = NULL, $topic_id = NULL, $search = NULL) {
    if ($region_id) {
      return 'nc1';
    }
    elseif ($topic_id) {
      return 'nc2';
    }
    elseif ($search) {
      return 'nift';
    }
    else {
      return 'ni';
    }
  }

}
