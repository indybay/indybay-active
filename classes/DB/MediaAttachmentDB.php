<?php

namespace Indybay\DB;

/**
 * Indybay MediaAttachmentDB Class.
 *
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class MediaAttachmentDB extends DB {

  /**
   * Adds a media attachment to the DB table.
   *
   * Used for posts and admin uploads.
   */
  public function addMediaAttachment(
    $upload_name,
    $file_name,
    $relative_path,
    $alt_tag,
    $original_file_name,
    $media_type_id,
    $created_by_id,
    $upload_type_id,
    $upload_status_id,
    $parent_item_id = 0,
    $file_size = 0,
    $image_width = 0,
    $image_height = 0,
  ) {

    $media_type_id = (int) $media_type_id;
    $created_by_id = (int) $created_by_id;
    $upload_type_id = (int) $upload_type_id;
    $upload_status_id = (int) $upload_status_id;
    $parent_item_id = (int) $parent_item_id;
    $file_size = (int) $file_size;
    $image_width = (int) $image_width;
    $image_height = (int) $image_height;
    $query = "insert into media_attachment (upload_name, file_name, relative_path, alt_tag, original_file_name,   media_type_id, created_by_id,
     			upload_type_id, upload_status_id, creation_date,
			parent_attachment_id, file_size, image_width, image_height) values (
     			'" . $this->prepareString($upload_name) . "',
     			 '" . $this->prepareString($file_name) . "',
     			 '" . $this->prepareString($relative_path) . "',
     			 '" . $this->prepareString(mb_substr($alt_tag, 0, 200, 'UTF-8')) . "',
     			  '" . $this->prepareString($original_file_name) . "',
     			$media_type_id, $created_by_id,
     			$upload_type_id, $upload_status_id, Now(),
			$parent_item_id, $file_size, $image_width, $image_height )";

    $media_attachment_id = $this->executeStatementReturnAutokey($query);

    return $media_attachment_id;
  }

  /**
   * Returns info about a media attachment.
   *
   * For use on admin pages or when rendering.
   */
  public function getMediaAttachmentInfo($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $return = [];

    $query = 'select ma.*,
     	 		unix_timestamp(creation_date) as creation_timestamp, mat.mime_type, mat.media_type_grouping_id 
     	 		from media_attachment ma,
     	 		 ' .
                 'media_type mat where ma.media_type_id=mat.media_type_id and ' .
                  "ma.media_attachment_id=$media_attachment_id";
    $ret = $this->query($query);
    if (is_array($ret) && count($ret) > 0) {
      $return = array_pop($ret);
      $query = 'select name from upload_type where upload_type_id=' . $return['upload_type_id'];
      $ret = $this->singleColumnQuery($query);
      if (is_array($ret) && count($ret) > 0) {
        $upload_type_name = array_pop($ret);
        $return['upload_type_name'] = $upload_type_name;
      }
    }

    return $return;
  }

  /**
   * Returns a list of media attachments.
   *
   * For section of admin that shows recent uploads.
   */
  public function getMediaAttachmentList($upload_type_id, $media_type_grouping_id, $creator_id, $page_size, $limit_start, $keyword = '') {

    $query = 'select ma.*,  unix_timestamp(creation_date) as creation_timestamp, mt.* from media_attachment ma, media_type mt ' .
         'where ma.media_type_id=mt.media_type_id ';

    if (trim($keyword) != '') {
      $query .= " and (file_name like '%" . $this->prepareString($keyword) . "%' or original_file_name like '%" . $this->prepareString($keyword) . "%') ";
    }

    if ($upload_type_id != 0) {
      $upload_type_id = (int) $upload_type_id;
      $query .= ' and upload_type_id=' . $upload_type_id;
    }

    if ($media_type_grouping_id != 0) {
      $media_type_grouping_id = (int) $media_type_grouping_id;
      $query .= ' and media_type_grouping_id=' . $media_type_grouping_id;
    }

    if ($creator_id != 0) {
      $creator_id = (int) $creator_id;
      $query .= ' and creator_id=' . $creator_id;
    }
    $limit_start = (int) $limit_start;
    $page_size = (int) $page_size;
    $query .= " order by ma.creation_date desc limit $limit_start, $page_size";

    $ret = $this->query($query);

    return $ret;
  }

  /**
   * Gets list of recent admin uploads.
   *
   * For use on blurb pages when someone is trying to chose a photo for a
   * blurb.
   */
  public function getRecentAdminUploadSelectList() {
    $upload_type_id = UPLOAD_TYPE_ADMIN_UPLOAD;
    $media_type_grouping_id = MEDIA_TYPE_GROUPING_IMAGE;
    $page_size = 100;
    $limit_start = 0;
    $creator_id = 0;
    $list = $this->getMediaAttachmentList($upload_type_id, $media_type_grouping_id, $creator_id, $page_size, $limit_start);
    $return_options = [];
    if (is_array($list)) {
      foreach ($list as $list_item) {
        $media_attachment_id = $list_item['media_attachment_id'];
        $name = $media_attachment_id . ') ' . $list_item['file_name'];
        $name .= ' (' . $list_item['image_width'] . ' x ' . $list_item['image_height'] . ')';
        $return_options[$media_attachment_id] = $name;
      }
    }
    return $return_options;
  }

  /**
   * Returns list of media type groupings for search screens.
   */
  public function getMediumOptions() {

    $query = 'select media_type_grouping_id, name from media_type_grouping where media_type_grouping_id>0';
    $res = $this->query($query);
    $options = [];
    if (is_array($res)) {
      foreach ($res as $row) {
        $options[$row['media_type_grouping_id']] = $row['name'];
      }
    }

    return $options;
  }

  /**
   * Gets a list of upload types.
   *
   * For use in searching options in media attachment section of admin.
   */
  public function getUploadTypes() {

    $query = 'select upload_type_id, name from upload_type';
    $res = $this->query($query);
    $options = [];
    if (is_array($res)) {
      foreach ($res as $row) {
        $options[$row['upload_type_id']] = $row['name'];
      }
    }

    return $options;
  }

  /**
   * When an item is posted this determines its type in the DB.
   *
   * Based off the name of the file and any available MIME info.
   */
  public function getMediaTypeIdFromFileExtensionAndMime($file_ext, $mime) {

    $media_type_id = 0;
    $query = "select media_type_id from media_type where extension = '" . $this->prepareString(strtolower(trim($file_ext))) . "' and mime_type = '" . $this->prepareString($mime) . "'";
    $result = $this->singleColumnQuery($query);
    if (count($result) == 0) {
      $query = "select media_type_id from media_type where extension = '" . $this->prepareString(strtolower(trim($file_ext))) . "'";
      $result = $this->singleColumnQuery($query);
    }
    if (!is_array($result) || count($result) == 0) {
      $media_type_id = 0;
    }
    else {
      $media_type_id = array_pop($result);
    }

    return $media_type_id;
  }

  /**
   * Given type (such as jpeg, gif or mov file) returns grouping type.
   *
   * (image, movie etc...)
   */
  public function getMediaTypeGroupingIdFromTypeId($media_type_id) {

    $media_type_grouping_id = 0;

    $media_type_id = (int) $media_type_id;
    $query = 'select media_type_grouping_id from media_type where  media_type_id=' . $media_type_id;
    $result = $this->singleColumnQuery($query);
    if (!is_array($result) || count($result) == 0) {
      return 0;
    }

    if (is_array($result) && count($result) > 0) {
      $media_type_grouping_id = array_pop($result);
    }

    return $media_type_grouping_id;
  }

  /**
   * Updates the upload status for an image.
   *
   * So the DB can record if the item really was uploaded sucessfully. This
   * is mainly useful when the site is being setup as a mirror or dev site
   * or when an Indymedia is upgraded from a different code based (as when
   * Indybay got converted from sf-active).
   */
  public function updateUploadStatus($media_attachment_id, $new_status_id) {

    $new_status_id = (int) $new_status_id;
    $media_attachment_id = (int) $media_attachment_id;
    $sql = 'update media_attachment set upload_status_id=' . $new_status_id . '
     		where media_attachment_id=' . $media_attachment_id;
    $this->executeStatement($sql);

  }

  /**
   * Updates the upload status and file location for an image.
   *
   * So the DB can record if the item really was uploaded sucessfully. This
   *  is mainly useful when the site is being setup as a mirror or dev site
   * or when an Indymedia is upgraded from a different code based (as when
   * Indybay got converted from sf-active).
   */
  public function updateFileLocationAndStatus($media_attachment_id, $new_file_rel_path, $new_file_name, $new_status) {

    $new_status = (int) $new_status;
    $media_attachment_id = (int) $media_attachment_id;
    $sql = 'update media_attachment set upload_status_id=' . $new_status . ", relative_path='" . $this->prepareString($new_file_rel_path) . "',
     		file_name='" . $this->prepareString($new_file_name) . "'
     		where media_attachment_id=" . $media_attachment_id;
    $this->executeStatement($sql);

  }

  /**
   * Updates the file size stored for an upload.
   *
   * Mainly useful for legacy uploads that dont yet have this stored.
   */
  public function updateFileSize($media_attachment_id, $file_size) {

    $file_size = (int) $file_size;
    $media_attachment_id = (int) $media_attachment_id;
    $sql = 'update media_attachment set file_size=' . $file_size . '
     		where media_attachment_id=' . $media_attachment_id;

    $this->executeStatement($sql);

  }

  /**
   * When images are rendered, more info becomes available.
   *
   * Like width and height. This method is needed so that can be stored in
   * the DB so lists of images can show size (as when you are tying to
   * choose one for a blurb).
   */
  public function updateImageAndFileSize($media_attachment_id, $file_size, $image_width, $image_height) {

    $file_size = (int) $file_size;
    $media_attachment_id = (int) $media_attachment_id;
    $image_width = (int) $image_width;
    $image_height = (int) $image_height;
    $sql = 'update media_attachment set file_size=' . $file_size . ', image_width=' . $image_width . ',
        	image_height=' . $image_height . '
     		where media_attachment_id=' . $media_attachment_id;

    $this->executeStatement($sql);

  }

  /**
   * Returns the news item or blurbs that use this image.
   *
   * This is currently mainly used to navigate back and forth between the
   * media upload detail pages and news items that use them but it could
   * also be used to delete old unused items from the DB or delete images
   * for hidden posts...
   */
  public function getCurrentRelatedNewsItemIds($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = "select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id)";

    $result = $this->singleColumnQuery($sql);

    return $result;
  }

  /**
   * Returns the news item or blurbs that use this image.
   *
   * This is currently mainly used to navigate back and forth between the
   * media upload detail pages and news items that use them but it could
   * also be used to delete old unused items from the DB or delete images
   * for hidden posts...
   */
  public function getCurrentNonhiddenRelatedNewsItemIds($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = "select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id) and ni.news_item_status_id<>" . NEWS_ITEM_STATUS_ID_HIDDEN;

    $result = $this->singleColumnQuery($sql);

    return $result;
  }

  /**
   * Returns the news item or blurbs that use this image.
   *
   * This is currently mainly used to navigate back and forth between the
   * media upload detail pages and news items that use them but it could
   * also be used to delete old unused items from the DB or delete images
   * for hidden posts...
   */
  public function getCurrentHiddenRelatedNewsItemIds($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = "select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id) and ni.news_item_status_id=" . NEWS_ITEM_STATUS_ID_HIDDEN;

    $result = $this->singleColumnQuery($sql);

    return $result;
  }

  /**
   * Also lists news items that used to use the upload.
   */
  public function getAllRelatedNewsItemIds($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = "select distinct(niv.news_item_id) from news_item_version niv
        	where 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id)";

    $result = $this->singleColumnQuery($sql);

    return $result;
  }

  /**
   * Updates the parent id and type of an image.
   *
   * This is mainly useful when the original image uploaded is not the one
   * associated with the post because it was too large.
   */
  public function updateTypeAndParent($media_attachment_id, $upload_type_id, $parent_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $upload_type_id = (int) $upload_type_id;
    $parent_attachment_id = (int) $parent_attachment_id;
    $sql = 'update media_attachment set parent_attachment_id=' . $parent_attachment_id . ', upload_type_id=' . $upload_type_id . '
     		where media_attachment_id=' . $media_attachment_id;

    $this->executeStatement($sql);

  }

  /**
   * Returns the parent attachment info for a media attachment.
   *
   * This is the media attachment that shows up with posts as opposed to
   * thumbnails whose ids may be handed into this.
   */
  public function getParentAttachmentInfo($media_attachment_id) {

    $info = $this->getMediaAttachmentInfo($media_attachment_id);
    $sql = 'select * from media_attachment where media_attachment_id=' . $info['parent_attachment_id'];

    $result = $this->query($sql);

    if (is_array($result) && count($result) > 0) {
      $res = array_pop($result);
    }
    else {
      $res = '';
    }

    return $res;
  }

  /**
   * Returns lists of child attachments (normally thumbnails).
   */
  public function getChildAttachmentInfoList($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = 'select ma.*, ut.name as type_name from media_attachment ma, upload_type ut
         where parent_attachment_id=' . $media_attachment_id . ' and ut.upload_type_id=ma.upload_type_id';

    $result = $this->query($sql);

    return $result;
  }

  /**
   * Deletes DB row for media attachment.
   */
  public function deleteMediaAttachment($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $sql = 'delete from media_attachment where media_attachment_id=' . $media_attachment_id;
    $this->executeStatement($sql);

    $sql = 'update news_item_version set media_attachment_id=0 where media_attachment_id=' . $media_attachment_id;
    $this->executeStatement($sql);

    $sql = 'update  news_item_version set thumbnail_media_attachment_id=0 where thumbnail_media_attachment_id=' . $media_attachment_id;
    $this->executeStatement($sql);

  }

  /**
   * Given media attachment id and aupload type returns children.
   *
   * An example would be looking for an image that is a child of a PDF.
   */
  public function getThumbnailGivenParentIdAndType($media_attachment_id, $upload_type_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $upload_type_id = (int) $upload_type_id;
    $sql = 'select media_attachment_id from media_attachment
         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . $upload_type_id;

    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
    }

    return $id;
  }

  /**
   * Returns info on original upload if current item isnt original upload.
   *
   * This will be the movie or large image associated with a displayed image
   * or thumbnail.
   */
  public function getOriginalAttachmentInfoFromAttachmentId($media_attachment_id) {
    $media_attachment_id = (int) $media_attachment_id;
    $attachment_info = '';

    // First get_parent if this isnt the parent attachment.
    $parent_info = $this->getParentAttachmentInfo($media_attachment_id);
    if (is_array($parent_info) && count($parent_info) > 0) {
      if ($parent_info['media_attachment_id'] != $media_attachment_id) {
        $media_attachment_id = $parent_info['media_attachment_id'];
      }
    }

    $sql = 'select media_attachment_id from media_attachment
         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . UPLOAD_TYPE_POST_ORIGINAL_BEFORE_RESIZE;

    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }
    else {
      $attachment_info = $this->getMediaAttachmentInfo($media_attachment_id);
    }

    return $attachment_info;
  }

  /**
   * Returns torrents associated with an upload.
   */
  public function getAssociatedTorrentInfo($media_attachment_id) {
    $media_attachment_id = (int) $media_attachment_id;
    $attachment_info = '';

    // First get_parent if this isnt the parent attachment
    // I fixed a typo here but wasn't really sure what this does so
    // commented out for now --mark
    // $parent_info=$this->getParentAttachmentInfo($media_attachment_id);
    // if (is_array($parent_info) && count($parent_info)>0){
    // if ($parent_info["media_attachment_id"]!=$media_attachment_id)
    // $media_attachment_id=$parent_info["media_attachment_id"];
    // }.
    $sql = 'select media_attachment_id from media_attachment
	         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . UPLOAD_TYPE_TORRENT;

    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }

    return $attachment_info;
  }

  /**
   * Gets images associated with a pdf.
   */
  public function getRenderedPdfInfo($media_attachment_id) {

    $media_attachment_id = (int) $media_attachment_id;
    $attachment_info = '';

    // First get_parent if this isnt the parent attachment.
    $parent_info = $this->getParentAttachmentInfo($media_attachment_id);
    if (is_array($parent_info) && count($parent_info) > 0) {
      if ($parent_info['media_attachment_id'] != $media_attachment_id) {
        $media_attachment_id = $parent_info['media_attachment_id'];
      }
    }

    $sql = 'select media_attachment_id from media_attachment
	         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . UPLOAD_TYPE_THUMBNAIL_MEDIUM;

    $id = 0;
    $result = $this->singleColumnQuery($sql);

    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }

    return $attachment_info;
  }

  /**
   * Gets images associated with a video.
   */
  public function getRenderedVideoInfo($media_attachment_id, $type = UPLOAD_TYPE_THUMBNAIL_SMALL) {
    $media_attachment_id = (int) $media_attachment_id;
    $type = (int) $type;
    $attachment_info = '';

    // First get_parent if this isnt the parent attachment
    // I fixed a typo here but wasn't really sure what this does so
    // commented out for now --mark
    // $parent_info=$this->getParentAttachmentInfo($media_attachment_id);
    // if (is_array($parent_info) && count($parent_info)>0){
    // if ($parent_info["media_attachment_id"]!=$media_attachment_id)
    // $media_attachment_id=$parent_info["media_attachment_id"];
    // }.
    $sql = 'select media_attachment_id from media_attachment
	         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . $type;

    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }

    return $attachment_info;
  }

  /**
   * Gets images associated with a flv.
   */
  public function getRenderedFlv($media_attachment_id) {
    $media_attachment_id = (int) $media_attachment_id;
    $attachment_info = '';

    // First get_parent if this isnt the parent attachment
    // I fixed a typo here but wasn't really sure what this does so
    // commented out for now --mark
    // $parent_info=$this->getParentAttachmentInfo($media_attachment_id);
    // if (is_array($parent_info) && count($parent_info)>0){
    // if ($parent_info["media_attachment_id"]!=$media_attachment_id)
    // $media_attachment_id=$parent_info["media_attachment_id"];
    // }.
    $sql = 'select media_attachment_id from media_attachment
	         where parent_attachment_id=' . $media_attachment_id . ' and upload_type_id=' . UPLOAD_TYPE_FLV;

    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }

    return $attachment_info;
  }

  /**
   * Gets H264 derivative info array for a video.
   */
  public function getRenderedH264(int $media_attachment_id): array {
    $attachment_info = [];
    $sql = "SELECT media_attachment_id FROM media_attachment WHERE parent_attachment_id = $media_attachment_id AND upload_type_id = " . UPLOAD_TYPE_H264;
    $id = 0;
    $result = $this->singleColumnQuery($sql);
    if (is_array($result) && count($result) > 0) {
      $id = array_pop($result);
      $attachment_info = $this->getMediaAttachmentInfo($id);
    }
    return $attachment_info;
  }

  /**
   * Deletes thumbnails associated with a video.
   */
  public function deleteThumbnails(int $media_attachment_id) {
    $upload_types = UPLOAD_TYPE_THUMBNAIL_SMALL . ', ' . UPLOAD_TYPE_THUMBNAIL_MEDIUM;
    $sql = "DELETE FROM media_attachment WHERE parent_attachment_id = $media_attachment_id AND upload_type_id IN ($upload_types)";
    $this->executeStatement($sql);
  }

  /**
   * Updates a media attachment, adding the width and height.
   */
  public function updateVideoDimensions(int $media_attachment_id, int $width, int $height) {
    $sql = "UPDATE media_attachment SET image_width = $width, image_height = $height WHERE media_attachment_id = $media_attachment_id";
    $this->executeStatement($sql);
  }

  /**
   * Updates a media attachment, adding browser compatibility.
   */
  public function updateVideoBrowserCompat(int $media_attachment_id, int $browser_compat) {
    $sql = "UPDATE media_attachment SET browser_compat = $browser_compat WHERE media_attachment_id = $media_attachment_id";
    $this->executeStatement($sql);
  }

  /**
   * Updates media attachment alt text (image description).
   */
  public function updateAltTag(int $media_attachment_id, string $alt_tag) {
    $this->executeStatement("UPDATE media_attachment SET alt_tag = '{$this->prepareString($alt_tag)}' WHERE media_attachment_id = $media_attachment_id");
  }

}
