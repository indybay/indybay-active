<?php

namespace Indybay\DB;

/**
 * Written December 2005 - January 2006.
 *
 * Class which loads the separate sections for the newswires on the various
 * feature pages. The "newswire" that has the search options at the top is dealt
 * with in the SearchDB class.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class NewswireDB extends DB {

  /**
   * Returns the local newswire data given a list of categories.
   */
  public function getLocalListForCategories($category_list, $items_per_newswire_section) {

    $additional_and = ' and ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED . ' ';

    $ret = $this->getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, 1);

    return $ret;

  }

  /**
   * Returns the nolocal/global newswire data given a list of categories.
   */
  public function getNonlocalListForCategories($category_list, $items_per_newswire_section) {

    $additional_and = ' and ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED . ' ';
    $ret = $this->getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, 0);

    return $ret;
  }

  /**
   * Returns the other/breaking newswire data given a list of categories.
   */
  public function getOtherListForCategories($category_list, $items_per_newswire_section) {

    $additional_and = ' and (ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED . '
   		 or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED . '
   		  or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_OTHER . '
   		  or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN . '
   		  or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_NEW . ' )
   		  ';
    $ret = $this->getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, 0);

    return $ret;
  }

  /**
   * Returns  open newswire data given a list of categories.
   */
  public function getAllListForCategories($category_list, $items_per_newswire_section) {

    $additional_and = ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN . '  ';
    $additional_and .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN . '  ';
    $ret = $this->getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, 1);

    return $ret;
  }

  /**
   * Returns  classified newswire data given a list of categories.
   */
  public function getClassifiedListForCategories($category_list, $items_per_newswire_section) {

    $additional_and = ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN . ' ';
    $additional_and .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN . ' ';
    $additional_and .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_OTHER . ' ';
    $additional_and .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_NEW . ' ';
    $ret = $this->getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, 1);

    return $ret;
  }

  /**
   * Helper function that returns newswires given portions of where clause.
   */
  public function getListForCategoriesHelper($category_list, $items_per_newswire_section, $additional_and, $include_events) {

    $items_per_newswire_section = (int) $items_per_newswire_section;
    $num_cats = count($category_list);
    $temp_num_items = $items_per_newswire_section;
    if ($num_cats > 1) {
      $temp_num_items = $num_cats * items_per_newswire_section;
    }
    if (count($category_list) == 0) {
      $query = 'select niv.title1, niv.news_item_id, niv.displayed_author_name, ni.creation_date, ';
      $query .= ' unix_timestamp(ni.creation_date) as creation_timestamp, ni.news_item_type_id, ni.num_comments, ';
      $query .= ' unix_timestamp(niv.displayed_date) as displayed_date_timestamp, ';
      $query .= " date_format(ni.creation_date,'%W %b %D %l:%i %p') as created, ";
      $query .= ' ni.media_type_grouping_id from news_item_version niv, news_item ni ';
      $query .= ' where ni.current_version_id=niv.news_item_version_id and ( news_item_type_id=' . NEWS_ITEM_TYPE_ID_POST;
      if ($include_events == 1) {
        $query .= ' or news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
      }
      $query .= ' ) and (ni.parent_item_id=ni.news_item_id)';
      $query .= $additional_and;
      $query .= " order by ni.news_item_id desc limit 0,$items_per_newswire_section";
    }
    else {

      $query = 'select niv.title1, niv.news_item_id, niv.displayed_author_name, ni.creation_date, ni.news_item_type_id, ni.num_comments,  ';
      $query .= ' unix_timestamp(ni.creation_date) as creation_timestamp, ';
      $query .= ' unix_timestamp(niv.displayed_date) as displayed_date_timestamp, ';
      $query .= " date_format(ni.creation_date,'%W %b %D %l:%i %p') as created, ";
      $query .= ' ni.media_type_grouping_id from news_item_version niv, news_item ni, news_item_category nic ';
      $query .= ' where ni.current_version_id=niv.news_item_version_id and ( news_item_type_id=' . NEWS_ITEM_TYPE_ID_POST;

      if ($include_events == 1) {
        $query .= ' or news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
      }

      $query .= ' ) and (ni.parent_item_id=ni.news_item_id) ';
      $query .= ' and  nic.news_item_id=ni.news_item_id and ' . $this->getCategoryOrList($category_list);
      $query .= $additional_and;
      $query .= ' order by nic.news_item_id desc limit 0 , ' . $temp_num_items;
    }
    $db_obj = new DB();
    $result = $db_obj->query($query);

    if ($num_cats > 1) {
      $result = $this->removeDuplicatesAndRestrictSize($result, $items_per_newswire_section);
    }
    $result = $result;

    return $result;

  }

  /**
   * Removes duplicates that could be caused by how where clause is setup.
   *
   * These are duplicates in the results with same id not duplicates in terms of
   * contents)
   */
  public function removeDuplicatesAndRestrictSize($result, $items_per_newswire_section) {

    $new_results = [];
    $prev_news_item_id = 0;
    $i = 1;
    foreach ($result as $row) {
      $next_news_item_id = $row[news_item_id];
      if ($next_news_item_id != $prev_news_item_id && $i < $items_per_newswire_section) {
        array_push($new_results, $row);
      }
      $prev_news_item_id = $next_news_item_id;
      $i = $i + 1;
    }
    array_reverse($new_results);

    return $new_results;
  }

  /**
   * Sets up a list of ors in the where clauses for newswire queries.
   */
  public function getCategoryOrList($category_list) {
    $or_list = '';
    if (count($category_list) > 1) {
      $or_list = ' (';
    }
    $i = 0;
    foreach ($category_list as $cat_id) {
      if ($i > 0) {
        $or_list .= ' or ';
      }
      $or_list .= ' nic.category_id=' . (int) $cat_id . ' ';
      $i = $i + 1;
    }
    if (count($category_list) > 1) {
      $or_list .= ')';
    }

    return $or_list;
  }

}
