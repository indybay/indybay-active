<?php

namespace Indybay\DB;

/**
 * Written December 2005 - January 2006.
 *
 * Class which loads the seperate sections for the newswires on the various
 * feature pages. The "newswire" that has the search options at the top is dealt
 * with in the SearchDB class.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class EventListDB extends CalendarDB {

  /**
   * Returns all events related to a given page.
   *
   * For the list at the bottom of feature pages.
   */
  public function getListForPage($page_id) {

    $feature_page_db_class = new FeaturePageDB();
    $page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $max_num_days = 0;
    if (isset($page_info['max_timespan_in_days_for_full_event_list'])) {
      $max_num_days = $page_info['max_timespan_in_days_for_full_event_list'];
    }
    if ($max_num_days == 0) {
      $max_num_days = 100;
    }
    $max_date_timestamp = time() + (60 * 60 * 24) * $max_num_days;
    $max_num_items = $page_info['max_events_in_full_event_list'];
    if ($max_num_items == 0) {
      $max_num_items = 30;
    }
    $category_list = $feature_page_db_class->getCategoryIdsForPage($page_id);
    $ret = $this->getListForCategoriesHelper($category_list, $max_num_items, $max_date_timestamp);

    return $ret;
  }

  /**
   * Returns events related to a given page that are marked as highlighted.
   *
   * For the list at the top of feature pages (eventlinks).
   */
  public function getHighlightedEventListForPage($page_id) {

    $feature_page_db_class = new FeaturePageDB();
    $page_info = $feature_page_db_class->getFeaturePageInfo($page_id);

    $max_num_days = $page_info['max_timespan_in_days_for_highlighted_event_list'];
    if ($max_num_days == 0) {
      $max_num_days = 30;
    }
    $max_date_timestamp = time() + (60 * 60 * 24) * $max_num_days;
    $max_num_items = $page_info['max_events_in_highlighted_list'];
    if ($max_num_items == 0) {
      $max_num_items = 12;
    }
    if ($page_info['event_list_type_id'] == 3) {
      $ret = $this->getHighlightedListHelper($max_num_items, $max_date_timestamp);
    }
    else {
      $category_list = $feature_page_db_class->getCategoryIdsForPage($page_id);
      $ret = $this->getHighightedListForCategories($category_list, $max_num_items, $max_date_timestamp);
    }

    return $ret;
  }

  /**
   * Creates the where clause for lists of categories.
   *
   * This assumes that the relationship is "or" which no pages yet are (see the
   * comment at the top of this file for the issues with this).
   */
  public function getCategoryOrList($category_list) {
    $or_list = '';
    if (is_array($category_list)) {
      if (count($category_list) > 1) {
        $or_list = ' (';
      }
      $i = 0;

      foreach ($category_list as $cat_id) {
        $cat_id = (int) $cat_id;
        if ($i > 0) {
          $or_list .= ' or ';
        }
        $or_list .= ' nic.category_id=' . $cat_id . ' ';
        $i = $i + 1;
      }
      if (count($category_list) > 1) {
        $or_list .= ')';
      }

    }
    return $or_list;
  }

  /**
   * Returns a list of highlighted events given a category list.
   */
  public function getHighightedListForCategories($category_list, $max_num_items, $max_date_timestamp) {

    $ret = $this->getHighlightedListForCategoriesHelper($category_list, $max_num_items, $max_date_timestamp);

    return $ret;

  }

  /**
   * Helper class used for getting lists of highlighted lists (eventlinks).
   */
  public function getHighlightedListHelper($max_num_items, $max_date_timestamp) {

    $max_num_items = (int) $max_num_items;
    $query = 'select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ';
    $query .= ' unix_timestamp(niv.displayed_date ) as displayed_timestamp ';
    $query .= ' from news_item_version niv, news_item ni ';
    $query .= ' where ni.current_version_id=niv.news_item_version_id and news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
    $query .= ' and (ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED . ' or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED . ') ';
    $query .= " and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60 and unix_timestamp(niv.displayed_date) < '" . $this->prepareString($max_date_timestamp) . "'";
    $query .= ' order by niv.displayed_date limit 0 , ' . $max_num_items;
    $db_obj = new DB();

    $result = $db_obj->query($query);

    return $result;

  }

  /**
   * Helper class used for getting lists of highlighted lists (eventlinks).
   */
  public function getHighlightedListForCategoriesHelper($category_list, $max_num_items, $max_date_timestamp) {

    if (!array_key_exists('db_down', $GLOBALS) || $GLOBALS['db_down'] != '1') {

      $max_num_items = (int) $max_num_items;
      $query = 'select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ';
      $query .= ' unix_timestamp(niv.displayed_date ) as displayed_timestamp ';
      $query .= ' from news_item_version niv, news_item ni, news_item_category nic ';
      $query .= ' where ni.current_version_id=niv.news_item_version_id and news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
      $query .= ' and  nic.news_item_id=ni.news_item_id and ' . $this->getCategoryOrList($category_list);
      $query .= ' and (ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED . ' or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED . ' or ni.news_item_status_id=' . NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED . ') ';
      $query .= ' and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*24 ';
      $query .= " and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*niv.event_duration and unix_timestamp(niv.displayed_date) < '" . $this->prepareString($max_date_timestamp) . "'";
      $query .= ' order by niv.displayed_date limit 0 , ' . $max_num_items;

      $db_obj = new DB();

      $result = $db_obj->query($query);
    }

    return $result;

  }

  /**
   * Helper class used for getting lists of highlighted lists (eventlinks).
   */
  public function getListForCategoriesHelper($category_list, $max_num_items, $max_date_timestamp) {

    $max_num_items = (int) $max_num_items;
    $query = 'select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ';
    $query .= ' unix_timestamp(niv.displayed_date ) as displayed_timestamp ';
    $query .= ' from news_item_version niv, news_item ni, news_item_category nic ';
    $query .= ' where ni.current_version_id=niv.news_item_version_id and news_item_type_id=' . NEWS_ITEM_TYPE_ID_EVENT;
    $query .= ' and  nic.news_item_id=ni.news_item_id and ' . $this->getCategoryOrList($category_list);
    $query .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_HIDDEN;
    $query .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN;
    $query .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_NEW;
    $query .= ' and ni.news_item_status_id!=' . NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;
    $query .= ' and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*24 ';
    $query .= " and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*niv.event_duration and unix_timestamp(niv.displayed_date) < '" . $this->prepareString($max_date_timestamp) . "'";
    $query .= ' order by niv.displayed_date limit 0 , ' . $max_num_items;

    $db_obj = new DB();

    $result = $db_obj->query($query);

    return $result;

  }

}
