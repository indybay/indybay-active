<?php

namespace Indybay\LegacySupport;

use Indybay\Common;
use Indybay\DB\MediaAttachmentDB;
use Indybay\MediaAndFileUtil\FileUtil;

/**
 * Helps migrate media from old sites.
 *
 * Indybay LegacyAttachmentMigration Class
 * Written December 2005 - January 2006
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development
 */
class LegacyAttachmentMigration extends Common {

  /**
   * Old center column locations.
   *
   * @var array
   */
  public $oldCenterColumnLocations = [
    'https://www.indybay.org/imcenter/',
    'https://www.indybay.org/uploads/',
    'https://www.indybay.org/im/',
    'https://www.indybay.org/images/',
  ];

  /**
   * Attemps to migrate legacy post attachment.
   */
  public function attemptToMigrateLegacyPostAttachment($media_attachment_info, $correct_new_location) {

    $media_attachment_db_class = new MediaAttachmentDB();
    $original_info = $media_attachment_db_class->getOriginalAttachmentInfoFromAttachmentId($media_attachment_info['media_attachment_id']);
    if (is_array($original_info) && $original_info['media_attachment_id'] != $media_attachment_info['media_attachment_id']) {
      $this->attemptToMigrateLegacyPostAttachment($original_info, $correct_new_location);
    }

    $success = 0;
    $file_util_class = new FileUtil();
    $file_name = $media_attachment_info['file_name'];

    $correct_full_path = $correct_new_location . $file_name;

    $i = strripos($file_name, '/');
    if ($i > 0) {
      $file_name = substr($file_name, $i + 1);
    }

    if (trim($file_name) != '') {

      if (file_exists($correct_full_path)) {

        $success = 1;
      }
      elseif (file_exists(LEGACY_UPLOAD_DIR . $file_name)) {

        $success = $file_util_class->copyFile(LEGACY_UPLOAD_DIR . $file_name, $correct_new_location . $file_name);
      }
      elseif (!empty($GLOBALS['is_mirror_site'])) {
        $url = $GLOBALS['originating_site_url'] . '/uploads/' . $file_name;
        $success = $file_util_class->downloadUrlToFile($url, $correct_new_location . $file_name);
        if (!$success) {
          $j = strrpos($correct_new_location, UPLOAD_ROOT);
          if ($j == 0) {
            $partpath = substr($correct_new_location, $j + strlen(UPLOAD_ROOT), strlen($correct_new_location));
            $url = 'https://www.indybay.org/uploads/' . $partpath . $file_name;
            $success = $file_util_class->downloadUrlToFile($url, $correct_new_location . $file_name);
          }
        }

      }
    }
    else {
      echo 'couldnt get file_name for media_attachment_id ' . $media_attachment_info['media_attachment_id'] . '<br>';
      $success = 0;
    }

    return $success;
  }

  /**
   * Attempts to migrate legacy blurb attachment.
   */
  public function attemptToMigrateLegacyBlurbAttachment($media_attachment_info, $correct_new_location) {

    $success = 0;
    $file_util_class = new FileUtil();
    $file_name = $media_attachment_info['file_name'];

    $i = strripos($file_name, '/');
    if ($i > 0) {
      $file_name = substr($file_name, $i + 1);
    }

    $correct_full_path = $correct_new_location . $file_name;

    if (file_exists($correct_full_path)) {
      $success = 1;
    }
    elseif (file_exists(LEGACY_CENTERCOL_UPLOAD_DIR . $file_name)) {
      $success = $file_util_class->copyFile(LEGACY_CENTERCOL_UPLOAD_DIR . $file_name, $correct_full_path);
    }
    elseif (!empty($GLOBALS['is_mirror_site'])) {
      $url = $GLOBALS['originating_site_url'] . '/uploads/' . $file_name;
      $success = $file_util_class->downloadUrlToFile($url, $correct_new_location . $file_name);
      if (!$success) {
        $j = strrpos($correct_new_location, UPLOAD_ROOT);
        if ($j == 0) {
          $partpath = substr($correct_new_location, $j + strlen(UPLOAD_ROOT), strlen($correct_new_location));
          $url = 'http://indybay.org/uploads/' . $partpath . $file_name;
          $success = $file_util_class->downloadUrlToFile($url, $correct_new_location . $file_name);
        }
      }
    }

    return $success;
  }

}
