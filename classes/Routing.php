<?php

namespace Indybay;

/**
 * Provides a list of routes.
 */
class Routing {

  const ROUTES = [
    '/about' => [
      'news_item_id' => 16643971,
      'cache_file' => './newsitems/2003/12/08/16643971.php',
    ],
    '/contact' => [
      'news_item_id' => 16658991,
      'cache_file' => './newsitems/2003/12/15/16658991.php',
    ],
    '/copyright' => [
      'news_item_id' => 18762449,
      'cache_file' => './newsitems/2014/10/05/18762449.php',
    ],
    '/disclaimer' => [
      'news_item_id' => 16659051,
      'cache_file' => './newsitems/2003/12/15/16659051.php',
    ],
    '/donate' => [
      'news_item_id' => 16659041,
      'cache_file' => './newsitems/2003/12/15/16659041.php',
    ],
    '/editorial-policy' => [
      'news_item_id' => 1395001,
      'cache_file' => './newsitems/2002/08/04/1395001.php',
    ],
    '/privacy' => [
      'news_item_id' => 16659061,
      'cache_file' => './newsitems/2003/12/15/16659061.php',
    ],
  ];

  /**
   * Gets current route.
   */
  public static function getCurrentRoute(): ?array {
    $request_path = strtok($_SERVER['REQUEST_URI'], '?');
    if (isset(static::ROUTES[$request_path])) {
      return static::ROUTES[$request_path];
    }
    return NULL;
  }

}
