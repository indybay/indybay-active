<?php

namespace Indybay;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Written January 2005.
 *
 * Modification Log:
 * 12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development
 * --------------------------------------------
 * this class is responsible for common functions used by any class
 * in the system.
 */

/**
 * Indybay base class.
 */
class Common {

  /**
   * Preferred long date format.
   */
  const DATE_LONG = 'D, M j, Y g:ia T';

  /**
   * Renders a template.
   */
  public function render($template, $variables) {
    static $twig;
    if (!isset($twig)) {
      $loader = new FilesystemLoader(TEMPLATE_PATH);
      $twig = new Environment($loader, [
        'cache' => CACHE_PATH . '/templates',
      ]);
    }
    return $twig->render($template, $variables);
  }

}
