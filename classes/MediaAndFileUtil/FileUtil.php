<?php

namespace Indybay\MediaAndFileUtil;

use Indybay\Common;

/**
 * Main class for file management.
 *
 * This is mainly just a series of wrappers around php functions that may be
 * useful if we ever want to change which functions are used.
 *
 * Written December 2005
 *  * Modification Log:
 * 12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class FileUtil extends Common {

  /**
   * Saves a string to a file and shuld also cleanly handle errors.
   */
  public function saveStringAsFile($file_path, $string) {

    $put = file_put_contents("$file_path~", $string, LOCK_EX);
    return $put === FALSE ? $put : rename("$file_path~", $file_path);

  }

  /**
   * This function loads a file as a string.
   *
   * It should have error handling to cleanly deal with missing files
   * setting a global variable and returning "" without actually
   * showing the error on the screen.
   */
  public function loadFileAsString($file_path) {

    if (file_exists($file_path)) {
      $fh = fopen($file_path, 'r');
      $str = '';
      $i = 0;
      while (!feof($fh)) {
        $str .= fread($fh, 1048576);
        $i = $i + 1;
        if ($i > 3) {
          exit;
        }
      }
    }
    else {
      $str = 'File Not Found: ' . $file_path;
    }

    return $str;
  }

  /**
   * Creates directory to store files.
   */
  public function createDirectoryStructureIfDoesntExist($dir_path) {
  }

  /**
   * Gets file time stamp.
   */
  public function getFileTimeStamp($file_path) {
  }

  /**
   * Moves file.
   */
  public function moveFile($old_file_path, $new_file_path) {
  }

  /**
   * Deletes file.
   */
  public function deleteFile($file_path) {
    if (file_exists($file_path) && is_file($file_path)) {
      unlink($file_path);
    }
  }

  /**
   * Downloads URL to file.
   */
  public function downloadUrlToFile($url, $file) {
    $fail = 0;
    @$fh1 = fopen($url, 'rb') or $fail = 1;
    if ($fail == 0) {
      $fh2 = fopen($file, 'wb') or $fail = 1;
      if ($fail == 1) {
        echo 'error writing to ' . $file . '<br>';
      }
      else {
        while (!feof($fh1)) {
          fwrite($fh2, fread($fh1, 1048576));
        }
        fclose($fh1);
        fclose($fh2);
      }
    }
    return !$fail;
  }

  /**
   * Copies file.
   */
  public function copyFile($old_location, $file) {
    $fail = 0;
    $fh1 = fopen($old_location, 'rb') or $fail = 1;
    if ($fail == 0) {
      $fh2 = fopen($file, 'wb') or $fail = 1;
      if ($fail == 1) {
        echo 'error writing to ' . $file . '<br>';
      }
      else {
        while (!feof($fh1)) {
          fwrite($fh2, fread($fh1, 1048576));
        }
        fclose($fh1);
        fclose($fh2);
      }
    }
    return !$fail;
  }

  /**
   * Returns file age in seconds.
   */
  public function getFileAgeInSeconds($file_path) {

    clearstatcache();
    $file_m_time = filemtime($file_path);
    $c_time = time();
    $diff = $c_time - $file_m_time;

    return $diff;

  }

}
