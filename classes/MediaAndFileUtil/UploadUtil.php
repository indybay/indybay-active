<?php

namespace Indybay\MediaAndFileUtil;

use Indybay\Cache\Cache;
use Indybay\Common;
use Indybay\DB\MediaAttachmentDB;
use Indybay\LegacySupport\LegacyAttachmentMigration;

/**
 * Class which handles moving and managing uploadced files.
 *
 * Written December 2005 - January 2006
 *  * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class UploadUtil extends Common {

  /**
   * Processes file uploaded with a post, attachment or comment.
   */
  public function processAnonymousUpload($tmp_file_path, $original_file_name, $mime_type_from_post, $file_isnt_in_tmp_location = 0) {

    $file_info = [];
    $cache_class = new Cache();

    if (!isset($original_file_name) || trim($original_file_name) == '') {
      echo 'original file name was null';
      exit;
    }

    $original_file_name = preg_replace('/[^a-zA-Z0-9._-]/', '_', $original_file_name);
    $original_file_name = strtolower($original_file_name);
    preg_match('/([a-z0-9._-]+)\.([a-z0-9]+)/', $original_file_name, $regs);

    $upload_ext = $regs[2];
    if (strlen($upload_ext) == 0) {
      echo '<strong>FATAL ERROR</strong>: Unable To Determine File Type Of Upload
        		 (Please make sure it ends in an extension like .jpg, mp3 etc...)';
      exit;
    }
    // Get media_type_id and grouping id and if not found reject.
    $media_attachment_db_class = new MediaAttachmentDB();
    $media_type_id = $media_attachment_db_class->getMediaTypeIdFromFileExtensionAndMime($upload_ext, $mime_type_from_post);

    if ($media_type_id == 0) {
      $file_util_class = new FileUtil();
      $file_util_class->deleteFile($tmp_file_path);
      $file_info = 0;
    }
    else {

      $upload_path = UPLOAD_PATH . '/';
      $news_item_created_stamp = time();
      $date_rel_path = $cache_class->createDateBasedRelativeDirectory($upload_path, $news_item_created_stamp);
      $upload_dir = $upload_path . $date_rel_path;

      $new_file_name = $original_file_name;
      $full_file_path_and_name = $upload_dir . $new_file_name;

      $i = 0;
      while (file_exists($full_file_path_and_name)) {
        if ($i == 200) {
          echo 'problem renaming file' . $full_file_path_and_name;
          exit;
        }
        $new_file_name = $this->makeFileNameOriginal($new_file_name);
        $full_file_path_and_name = $upload_dir . $new_file_name;
        $i = $i + 1;
      }

      if ($file_isnt_in_tmp_location != 0) {
        if (!rename($tmp_file_path, $full_file_path_and_name)
        ) {
          echo "<strong>FATAL ERROR</strong>: Move failed For non-temp File. No disk space? :( \n";
          exit;
        }
      }
      else {
        if (!move_uploaded_file($tmp_file_path, $full_file_path_and_name)) {
          echo "<strong>FATAL ERROR</strong>: Move failed! No disk space? :( \n";
          exit;
        }
        else {
          chmod($full_file_path_and_name, 0644);
        }
      }
      $file_info['media_type_id'] = $media_type_id;
      $file_info['file_extension'] = $upload_ext;
      $file_info['file_name'] = $new_file_name;
      $file_info['original_file_name'] = $original_file_name;
      $file_info['relative_path'] = $date_rel_path;
    }

    return $file_info;
  }

  /**
   * Needed so we can rename images uploaded with same name for same day.
   */
  public function makeFileNameOriginal($new_file_name) {

    if (trim($new_file_name) == '') {
      echo $new_file_name . ' is not a valid file name ';
      exit;
    }
    $i = strrpos($new_file_name, '.');
    $j = strrpos($new_file_name, '_') + 0;
    $k = 0;
    $suffix = substr($new_file_name, $i);
    if ($j > $i) {
      $prefix = substr($new_file_name, 0, $j);
      $k = substr($new_file_name, $j + 1, $i - $j - 1);
      if ("$k" != '' . ($k + 0) . '') {
        $k = 0;
        $prefix = substr($new_file_name, 0, $i);
      }
    }
    else {
      $prefix = substr($new_file_name, 0, $i);
    }
    $k = $k + 1;
    $new_file_name = $prefix . '_' . $k . $suffix;

    return $new_file_name;

  }

  /**
   * Handles secondary files like thumnails, torrents and streaming files.
   */
  public function makeSecondaryFilesAndAssociate($media_attachment_id, $file_info) {

    $media_attachment_db_class = new MediaAttachmentDB();
    $associated_attachment_ids = [];
    $associated_attachment_ids['media_attachment_id'] = $media_attachment_id;

    $media_type_id = $file_info['media_type_id'];
    $media_type_grouping_id = $media_attachment_db_class->getMediaTypeGroupingIdFromTypeId($media_type_id);

    $image_util = new ImageUtil();

    if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_IMAGE) {
      // Rotate image if camera has identified that the image was taken
      // sideways.
      if ($file_info['media_type_id'] == 15 || $file_info['media_type_id'] == 25) {
        exec(EXIFTRAN . ' -i ' . escapeshellarg(UPLOAD_PATH . '/' . $file_info['relative_path'] . $file_info['file_name']));
      }
      $associated_attachment_ids = $this->makeSecondaryImageFiles($media_attachment_id, $file_info);
    }
    elseif ($media_type_id == 2) {
      $image_util->makeImageForPdf($media_attachment_id, $file_info);
    }
    elseif ($media_type_grouping_id == MEDIA_TYPE_GROUPING_VIDEO) {
      $image_util->makeImageForVideo($media_attachment_id, $file_info);
    }
    $associated_attachment_ids['media_type_grouping_id'] = $media_type_grouping_id;

    return $associated_attachment_ids;
  }

  /**
   * Makes secondary image files.
   */
  public function makeSecondaryImageFiles($media_attachment_id, $file_info) {

    if (!isset($file_info['file_name']) || trim($file_info['file_name']) == '') {
      echo 'makeSecondaryImageFiles called ewith null file name';
      exit;
    }

    $upload_path = UPLOAD_PATH . '/';
    $date_rel_path = $file_info['relative_path'];
    $upload_dir = $upload_path . $date_rel_path;
    $existing_full_file_path = $upload_dir . $file_info['file_name'];
    $image_util_class = new ImageUtil();
    $media_type_id = $file_info['media_type_id'];
    $media_attachment_db_class = new MediaAttachmentDB();

    $associated_attachment_ids = [];
    $associated_attachment_ids['media_attachment_id'] = $media_attachment_id;
    $image_info = $image_util_class->getImageInfoFromFullFilePath($existing_full_file_path);

    $media_attachment_db_class->updateImageAndFileSize($media_attachment_id, $image_info['file_size'], $image_info['image_width'], $image_info['image_height']);

    // Always create JPEG version of HEIC images, regardless of size.
    if ($file_info['media_type_id'] == 47 || $image_info['image_width'] > POSTED_IMAGE_MAX_WIDTH || $image_info['image_height'] > POSTED_IMAGE_MAX_HEIGHT) {
      // Changed to "sm_" because some images display smaller than 800w
      // and that filename looks odd.
      $new_file_name = 'sm_' . $file_info['file_name'];
      if (str_ends_with($new_file_name, '.heic')) {
        $pathInfo = pathinfo($new_file_name);
        $new_file_name = "{$pathInfo['filename']}.jpg";
      }
      $full_file_path = $upload_dir . $new_file_name;
      $i = 0;
      while (file_exists($full_file_path)) {
        $new_file_name = $this->makeFileNameOriginal($new_file_name);
        $full_file_path = $upload_dir . $new_file_name;
        $i = $i + 1;
        if ($i == 200) {
          echo 'problem renaming file';
          exit;
        }
      }

      $new_file_info = $image_util_class->scaleImageByWidthIfTooLarge($new_file_name,
      $existing_full_file_path, $full_file_path, POSTED_IMAGE_MAX_WIDTH, POSTED_IMAGE_MAX_HEIGHT);

      if (is_array($new_file_info)) {
        $new_file_name = $new_file_info['file_name'];
        if (!array_key_exists('upload_name', $file_info)) {
          $file_info['upload_name'] = '';
        }
        if (!array_key_exists('alt_tag', $file_info)) {
          $file_info['alt_tag'] = '';
        }
        $resized_media_attachment_id = $media_attachment_db_class->addMediaAttachment($file_info['upload_name'], $new_file_name, $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
        $media_type_id, 0, UPLOAD_TYPE_POST, UPLOAD_STATUS_VALIDATED, 0, $new_file_info['file_size'], $new_file_info['image_width'], $new_file_info['image_height']);
        $media_attachment_db_class->updateTypeAndParent($media_attachment_id, UPLOAD_TYPE_POST_ORIGINAL_BEFORE_RESIZE, $resized_media_attachment_id);
        $associated_attachment_ids['media_attachment_id'] = $resized_media_attachment_id;
        $associated_attachment_ids['original_media_attachment_id'] = $media_attachment_id;
      }
    }
    $thumbnail_info = $this->generateThumbnailsForMediaAttachmentFromInfo($associated_attachment_ids['media_attachment_id'], $image_info, $file_info);
    $associated_attachment_ids['thumbnail_media_attachment_id'] = $thumbnail_info['small_thumbnail_media_attachment_id'];

    return $associated_attachment_ids;
  }

  /**
   * Generates thumbnails for media attachment from ID.
   */
  public function generateThumbnailsForMediaAttachmentFromId($media_attachment_id) {

    $thumbnail_info = [];

    if ($media_attachment_id + 0 == 0) {
      echo 'generateThumbnailsForMediaAttachmentFromId called with media_attachment_id=0<br>';
      $thumbnail_info = 0;
    }
    else {
      $media_attachment_db = new MediaAttachmentDB();
      $upload_path = UPLOAD_PATH . '/';
      $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
      if (!is_array($media_attachment_info)) {
        echo 'error occured getting media_attachment_id ' . $media_attachment_id;
        $thumbnail_info = 0;
      }
      elseif (count($media_attachment_info) == 0) {
        $thumbnail_info = 0;
      }
      else {
        if ($media_attachment_info['upload_status_id'] != UPLOAD_STATUS_VALIDATED) {
          $media_attachment_info = $this->verifyAttachment($media_attachment_info);
        }
        $date_rel_path = $media_attachment_info['relative_path'];
        $upload_dir = $upload_path . $date_rel_path;
        $existing_full_file_path = $upload_dir . $media_attachment_info['file_name'];
        $image_util_class = new ImageUtil();
        $image_info = $image_util_class->getImageInfoFromFullFilePath($existing_full_file_path);
        $thumbnail_info = $this->generateThumbnailsForMediaAttachmentFromInfo($media_attachment_id, $image_info, $media_attachment_info);

      }
    }
    return $thumbnail_info;
  }

  /**
   * Generates thumbnails for media attachment from info.
   */
  public function generateThumbnailsForMediaAttachmentFromInfo($media_attachment_id, $image_info, $file_info) {

    if (!is_array($image_info) || !is_array($file_info) || count($image_info) == 0 || count($file_info) == 0) {
      return '';
    }

    if (!isset($file_info['file_name']) || trim($file_info['file_name']) == '') {
      echo 'generateThumbnailsForMediaAttachmentFromInfo called with null filename';
      exit;
    }

    $media_attachment_db_class = new MediaAttachmentDB();
    $image_util_class = new ImageUtil();
    $upload_path = UPLOAD_PATH . '/';
    $date_rel_path = $file_info['relative_path'];
    $upload_dir = $upload_path . $date_rel_path;
    $existing_full_file_path = $upload_dir . $file_info['file_name'];
    $image_util_class = new ImageUtil();
    $media_type_id = $file_info['media_type_id'];

    $thumbnail_info['medium_thumbnail_media_attachment_id'] = $media_attachment_id;
    $thumbnail_info['small_thumbnail_media_attachment_id'] = $media_attachment_id;

    $medium_thumbnail_id = $media_attachment_db_class->getThumbnailGivenParentIdAndType($media_attachment_id, UPLOAD_TYPE_THUMBNAIL_MEDIUM);

    if ($medium_thumbnail_id != 0) {
      $thumbnail_info['medium_thumbnail_media_attachment_id'] = $medium_thumbnail_id;
    }
    else {
      // Always create JPEG version of HEIC images, regardless of size.
      if ($file_info['media_type_id'] == 47 || $image_info['image_width'] > THUMBNAIL_WIDTH_MEDIUM || $image_info['image_height'] > THUMBNAIL_HEIGHT_MEDIUM) {

        $new_file_name = THUMBNAIL_WIDTH_MEDIUM . '_' . $file_info['file_name'];
        if (str_ends_with($new_file_name, '.heic')) {
          $pathInfo = pathinfo($new_file_name);
          $new_file_name = "{$pathInfo['filename']}.jpg";
        }
        $full_file_path = $upload_dir . $new_file_name;
        $i = 0;
        while (file_exists($full_file_path)) {
          $new_file_name = $this->makeFileNameOriginal($new_file_name);
          $full_file_path = $upload_dir . $new_file_name;
          $i = $i + 1;
          if ($i == 300) {
            echo 'generateThumbnailsForMediaAttachmentFromInfo: problem renaming file' . $full_file_path;
            exit;
          }
        }
        $new_file_info = $image_util_class->scaleImageByWidthIfTooLarge($new_file_name,
                      $existing_full_file_path, $full_file_path, THUMBNAIL_WIDTH_MEDIUM, THUMBNAIL_HEIGHT_MEDIUM, 1);
        if (is_array($new_file_info)) {
          $new_file_name = $new_file_info['file_name'];
          if (!array_key_exists('upload_name', $file_info)) {
            $file_info['upload_name'] = $file_info['file_name'];
          }
          if (!array_key_exists('alt_tag', $file_info)) {
            $file_info['alt_tag'] = $file_info['file_name'];
          }
          $resized_media_attachment_id = $media_attachment_db_class->addMediaAttachment($file_info['upload_name'], $new_file_name, $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
                             $media_type_id, 0, UPLOAD_TYPE_THUMBNAIL_MEDIUM, UPLOAD_STATUS_VALIDATED, $media_attachment_id, $new_file_info['file_size'], $new_file_info['image_width'], $new_file_info['image_height']);
        }
        $thumbnail_info['medium_thumbnail_media_attachment_id'] = $resized_media_attachment_id;
      }
    }

    $small_thumbnail_id = $media_attachment_db_class->getThumbnailGivenParentIdAndType($media_attachment_id, UPLOAD_TYPE_THUMBNAIL_SMALL);
    if ($small_thumbnail_id != 0) {
      $thumbnail_info['small_thumbnail_media_attachment_id'] = $small_thumbnail_id;
    }
    else {
      // Always create JPEG version of HEIC images, regardless of size.
      if ($file_info['media_type_id'] == 47 || $image_info['image_width'] > THUMBNAIL_WIDTH_SMALL || $image_info['image_height'] > THUMBNAIL_HEIGHT_SMALL) {

        $new_file_name = THUMBNAIL_WIDTH_SMALL . '_' . $file_info['file_name'];
        if (str_ends_with($new_file_name, '.heic')) {
          $pathInfo = pathinfo($new_file_name);
          $new_file_name = "{$pathInfo['filename']}.jpg";
        }
        $full_file_path = $upload_dir . $new_file_name;
        $i = 0;
        while (file_exists($full_file_path)) {
          $new_file_name = $this->makeFileNameOriginal($new_file_name);
          $full_file_path = $upload_dir . $new_file_name;
          $i = $i + 1;
          if ($i == 300) {
            echo 'generateThumbnailsForMediaAttachmentFromInfo: problem renaming file' . $full_file_path;
            exit;
          }
        }
        $new_file_info = $image_util_class->scaleImageByWidthIfTooLarge($new_file_name,
                        $existing_full_file_path, $full_file_path, THUMBNAIL_WIDTH_SMALL, THUMBNAIL_HEIGHT_SMALL, 1);
        if (is_array($new_file_info)) {
          $new_file_name = $new_file_info['file_name'];
          if (!array_key_exists('upload_name', $file_info)) {
            $file_info['upload_name'] = '';
          }
          if (!array_key_exists('alt_tag', $file_info)) {
            $file_info['alt_tag'] = '';
          }
          $resized_media_attachment_id = $media_attachment_db_class->addMediaAttachment($file_info['upload_name'], $new_file_name, $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
                            $media_type_id, 0, UPLOAD_TYPE_THUMBNAIL_SMALL, UPLOAD_STATUS_VALIDATED, $media_attachment_id, $new_file_info['file_size'], $new_file_info['image_width'], $new_file_info['image_height']);
          $thumbnail_info['small_thumbnail_media_attachment_id'] = $resized_media_attachment_id;
        }
      }
    }

    return $thumbnail_info;
  }

  /**
   * Determines if an upload that is to be rendered actually exists.
   *
   * Updates info in the DB if it does and calls legacy migration classes to.
   * Get a copy of the file if it does not.
   */
  public function verifyAttachment($media_attachment_info) {

    $sucess = 0;

    if (!is_array($media_attachment_info) || $media_attachment_info['media_attachment_id'] + 0 == 0) {
      print_r($media_attachment_info);
      echo 'invalid media_attachment_info handed to verifyAttachment<br>';
      $success = 0;
    }
    else {
      $success = 1;
      $media_attachment_db_class = new MediaAttachmentDB();
      $cache_class = new Cache();

      // Check to see if the attachment is legacy not loaded status and if
      // so call legacy attachment class to get upload.
      $upload_status_id = $media_attachment_info['upload_status_id'];
      $upload_type_id = $media_attachment_info['upload_type_id'];
      $media_attachment_id = $media_attachment_info['media_attachment_id'];
      $creation_timestamp = $media_attachment_info['creation_timestamp'];

      if ($upload_status_id == UPLOAD_STATUS_NOT_VALIDATED_MIGRATED || DEBUG_MIGRATION) {

        $legacy_attachment_migration_class = new LegacyAttachmentMigration();
        $correct_new_file_name = $media_attachment_info['file_name'];
        $i = strripos($correct_new_file_name, '/');
        if ($i > 0) {
          $correct_new_file_name = substr($correct_new_file_name, $i + 1);
        }

        if ($upload_type_id == UPLOAD_TYPE_ADMIN_UPLOAD) {
          $relative_dir = 'admin_uploads/' . $cache_class->createDateBasedRelativeDirectory(UPLOAD_PATH . '/admin_uploads/', $creation_timestamp);
          $sucess = $legacy_attachment_migration_class->attemptToMigrateLegacyBlurbAttachment($media_attachment_info, UPLOAD_ROOT . $relative_dir);
        }
        else {
          $creation_timestamp = $media_attachment_info['creation_timestamp'];
          $relative_dir = $cache_class->createDateBasedRelativeDirectory(UPLOAD_PATH . '/', $creation_timestamp);
          $success = $legacy_attachment_migration_class->attemptToMigrateLegacyPostAttachment($media_attachment_info, UPLOAD_ROOT . $relative_dir);
        }
        if ($success == 1) {
          $media_attachment_db_class->updateFileLocationAndStatus($media_attachment_id, $relative_dir, $correct_new_file_name, UPLOAD_STATUS_VALIDATED);
          $media_attachment_info['upload_status_id'] = UPLOAD_STATUS_VALIDATED;
          $media_attachment_info['relative_path'] = $relative_dir;
          $media_attachment_info['file_name'] = $correct_new_file_name;
        }
        else {
          $media_attachment_db_class->updateUploadStatus($media_attachment_id, UPLOAD_STATUS_FAILED);
          $media_attachment_info['upload_status_id'] = UPLOAD_STATUS_FAILED;
        }
      }

      if ($sucess == 1) {

        // Check to see if all info is available for file and
        // if not fill in the missing info.
        $relative_path = $media_attachment_info['relative_path'];
        $file_name = $media_attachment_info['file_name'];
        if (strpos($relative_path, 'uploads/') > 0) {
          $intpath = UPLOAD_ROOT . substr($relative_path, 8);
        }
        else {
          $intpath = '/uploads';
        }
        if (substr($relative_path, 0, 1) != '/') {
          $intpath .= '/';
        }
        $full_path = INDYBAY_BASE_PATH . $intpath . $relative_path . $file_name;
        if (file_exists($full_path) && filesize($full_path) > 0) {
          $media_attachment_db_class = new MediaAttachmentDB();
          $media_attachment_db_class
            ->updateUploadStatus($media_attachment_info['media_attachment_id'], 1);
          $media_attachment_info['upload_status_id'] = 1;
        }
      }
    }

    return $media_attachment_info;
  }

  /**
   * Processes admin media uploads.
   */
  public function processAdminMediaUpload(
    $temp_file_name,
    $original_file_name,
    $mime_type,
    $restrict_to_width,
    $restrict_to_height,
    $image_name,
    $alt_text,
  ) {

    $media_attachment_db_class = new MediaAttachmentDB();
    $cache_class = new Cache();
    $copy_from = $temp_file_name;
    $upload_partial_rel_path = '/admin_uploads/';

    $original_file_name = str_replace(' ', '', $original_file_name);

    if (!isset($original_file_name) || trim($original_file_name) == '') {
      echo 'original file name was null';
      exit;
    }
    preg_match('/([a-zA-Z0-9._-]+)\.([a-zA-Z0-9]+)/', $original_file_name, $regs);
    $upload_ext = $regs[2];

    if (!is_uploaded_file($temp_file_name)) {
      echo 'Invalid file.';
      return 0;
    }
    $media_attachment_db_class = new MediaAttachmentDB();
    $media_type_id = $media_attachment_db_class->getMediaTypeIdFromFileExtensionAndMime($upload_ext, $mime_type);
    if (!$media_type_id) {
      unlink($temp_file_name);
      echo 'Unknown file type.';
      return 0;
    }

    $created_by_id = $_SESSION['session_user_id'];
    $new_file_name = $original_file_name;

    $date_rel_path = $cache_class->createDateBasedRelativeDirectory(UPLOAD_PATH . '/' . $upload_partial_rel_path, time());
    if ($date_rel_path == 0) {
      $had_error = 1;
    }
    else {
      $upload_rel_path = $upload_partial_rel_path . $date_rel_path;
      $copy_to = UPLOAD_PATH . $upload_rel_path . $new_file_name;

      while (file_exists($copy_to)) {
        $new_file_name = $this->makeFileNameOriginal($new_file_name);
        $copy_to = UPLOAD_PATH . $upload_rel_path . $new_file_name;
      }
      $image_util_class = new ImageUtil();
      if ($restrict_to_height > 0) {
        $file_info = $image_util_class->scaleImageByWidthIfTooLarge($new_file_name, $copy_from, $copy_to, $restrict_to_width, $restrict_to_height, 1);
      }
      else {
        $file_info = $image_util_class->scaleImageByWidthIfTooLarge($new_file_name, $copy_from, $copy_to, $restrict_to_width, $restrict_to_height);
      }if (is_array($file_info)) {
        $media_attachment_id = $media_attachment_db_class->addMediaAttachment(
        $image_name, $file_info['file_name'],
        $upload_rel_path, $alt_text, $original_file_name, $media_type_id, $created_by_id,
        UPLOAD_TYPE_ADMIN_UPLOAD, UPLOAD_STATUS_NOT_VALIDATED_ADMIN,
        0, $file_info['file_size'], $file_info['image_width'], $file_info['image_height']);
        if ($media_attachment_id <= 0) {
          echo 'an error occured adding image to database';
          $had_error = 1;
        }
      }
      else {
        $had_error = 1;
      }
    }
    if (isset($had_error)) {
      $ret = 0;
    }
    else {
      $ret = $media_attachment_id;
    }

    return $ret;
  }

  /**
   * Generates secondary files on change.
   */
  public function generateSecondaryFilesOnChange($version_info) {
    $media_attachment_id = $version_info['media_attachment_id'];
    if ($media_attachment_id + 0 != 0) {
      $media_attachment_db = new MediaAttachmentDB();
      $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
    }

  }

  /**
   * Deletes an upload.
   */
  public function deleteUpload($media_attachment_id) {
    $media_attachment_db = new MediaAttachmentDB();

    // Delete all child uploads first.
    $child_image_info_list = $media_attachment_db->getChildAttachmentInfoList($media_attachment_id);
    foreach ($child_image_info_list as $next_child) {
      $this->deleteUpload($next_child['media_attachment_id']);
    }

    $media_attachment_info = $media_attachment_db->getMediaAttachmentInfo($media_attachment_id);
    if (is_array($media_attachment_info)) {
      $upload_path = UPLOAD_PATH . '/';
      $date_rel_path = $media_attachment_info['relative_path'];
      $upload_dir = $upload_path . $date_rel_path;
      $existing_full_file_path = $upload_dir . $media_attachment_info['file_name'];

      // Delete actual upload file.
      $file_util = new FileUtil();
      $file_util->deleteFile($existing_full_file_path);
      // Delete DB entry.
      $media_attachment_db->deleteMediaAttachment($media_attachment_id);
    }
  }

}
