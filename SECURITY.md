Thank you for reporting security issues to the Indybay collective! We
would be happy to publicly acknowledge your contribution to Indybay and
its users.

Please report security vulnerabilities to: indybay@lists.riseup.net

Note, as an anti-spam measure, you may receive a confirmation
auto-response when emailing us, to which you must reply, otherwise we
will not receive your message.
