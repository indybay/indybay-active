#!/bin/sh
mysqldump --compact --no-create-info --set-gtid-purged=OFF \
  --skip-extended-insert indybay action action_type category category_type \
  center_column_type group group_action group_relationship keyword \
  keyword_type media_type media_type_grouping news_item_status news_item_type \
  newswire_type page page_category upload_type event_list_type
