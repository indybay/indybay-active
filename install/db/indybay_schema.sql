/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `action_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `action_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `classname` varchar(128) NOT NULL DEFAULT '',
  `methodname` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_type` (
  `action_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`action_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `category_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `parent_category_id` smallint(11) unsigned DEFAULT '0',
  `include_in_dropdowns` tinyint(4) NOT NULL DEFAULT '1',
  `created_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `name` (`name`),
  KEY `category_type_id` (`category_type_id`),
  KEY `include_in_dropdowns` (`include_in_dropdowns`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_type` (
  `category_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `center_column_type` (
  `center_column_type_id` tinyint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`center_column_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(31) DEFAULT NULL,
  `county` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_info` (
  `contact_info_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `cellphone` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `last_modified_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `last_modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_info_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(31) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `county` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(31) DEFAULT NULL,
  `state` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_migration_news_item` (
  `is_migrated` smallint(6) NOT NULL DEFAULT '0',
  `news_item_id` int(11) DEFAULT NULL,
  `legacy_item_type_id` int(11) NOT NULL DEFAULT '0',
  `legacy_item_id` int(11) NOT NULL DEFAULT '0',
  `legacy_item_info_id` int(11) DEFAULT NULL,
  `old_cachefile_location` varchar(128) DEFAULT NULL,
  `title1` varchar(128) NOT NULL DEFAULT '',
  `title2` varchar(128) DEFAULT NULL,
  `display_author` varchar(128) DEFAULT NULL,
  `last_updated_by_user_id` int(11) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `address` varchar(200) NOT NULL DEFAULT '',
  `summary` text,
  `is_summary_html` smallint(6) NOT NULL DEFAULT '0',
  `text` text,
  `is_text_html` smallint(6) NOT NULL DEFAULT '0',
  `upload_path` varchar(64) DEFAULT NULL,
  `file_name` varchar(128) DEFAULT NULL,
  `thumbnail_path` varchar(128) DEFAULT NULL,
  `mime_type` varchar(64) DEFAULT NULL,
  `media_type_id` int(11) DEFAULT NULL,
  `media_attachment_id` int(11) DEFAULT NULL,
  `legacy_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `legacy_updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `legacy_event_type_id` int(11) DEFAULT NULL,
  `event_type_id` int(11) DEFAULT NULL,
  `event_start_time` datetime DEFAULT NULL,
  `event_end_time` datetime DEFAULT NULL,
  `legacy_template_name` varchar(128) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `legacy_display_status` varchar(4) DEFAULT NULL,
  `news_item_status_id` int(11) DEFAULT NULL,
  `news_item_type_id` int(11) DEFAULT NULL,
  `media_attachment_alt_tag` varchar(64) DEFAULT NULL,
  `legacy_parent_item_id` int(11) DEFAULT NULL,
  `parent_news_item_id` int(11) NOT NULL DEFAULT '0',
  `related_url` varchar(128) DEFAULT NULL,
  `alt_tag` varchar(200) NOT NULL DEFAULT '',
  `contact_info_id` int(11) DEFAULT NULL,
  `media_type_grouping_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`legacy_item_id`),
  KEY `file_name` (`file_name`),
  KEY `legacy_item_id` (`legacy_item_id`),
  KEY `news_item_status_id` (`news_item_status_id`),
  KEY `legacy_item_type_id` (`legacy_item_type_id`),
  KEY `mime_type` (`mime_type`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deposit` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `location` smallint(5) unsigned DEFAULT NULL,
  `quantity` smallint(6) DEFAULT NULL,
  `distributor` varchar(31) DEFAULT NULL,
  `issue` tinyint(3) unsigned DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `dropdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=670 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispatch` (
  `sid` char(34) NOT NULL,
  `news_item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `sid` (`sid`,`news_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_list_type` (
  `event_list_type_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`event_list_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `group_id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_action` (
  `group_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `action_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `restrict_to_category_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`action_id`,`restrict_to_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_relationship` (
  `inherit_permission_from_group_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `child_group_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`inherit_permission_from_group_id`,`child_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_rss_feed_items` (
  `rss_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `rss_feed_id` int(11) NOT NULL,
  `author` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `text` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `related_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_text_html` tinyint(1) NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL,
  `pull_date` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `external_unique_id` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `news_item_status_id` int(11) NOT NULL,
  `parent_news_item_id` int(11) NOT NULL DEFAULT '0',
  `published_news_item_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rss_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155469 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_rss_feeds` (
  `rss_feed_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `feed_format_id` int(11) NOT NULL,
  `author_template` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `summary_template` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `text_template` varchar(600) COLLATE utf8_unicode_ci NOT NULL,
  `text_scrape_start` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `text_scrape_end` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `default_region_id` int(11) NOT NULL,
  `default_topic_id` int(11) NOT NULL,
  `default_status_id` int(11) NOT NULL,
  `replace_url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `replace_url_with` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `summary_scrape_start` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `summary_scrape_end` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `author_scrape_start` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `author_scrape_end` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `last_pull_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_pull_duration_usecs` int(11) NOT NULL DEFAULT '0',
  `title_scrape_start` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `title_scrape_end` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `date_scrape_start` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `date_scrape_end` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `exclude_tags` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `required_tags` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `max_to_pull` int(11) NOT NULL DEFAULT '0',
  `max_pull_days` int(11) NOT NULL DEFAULT '0',
  `restrict_urls` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `allow_links` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rss_feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `released` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keyword` (
  `keyword_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `keyword` varchar(64) NOT NULL DEFAULT '',
  `keyword_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`keyword_id`),
  KEY `keyword_type_id` (`keyword_type_id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keyword_type` (
  `keyword_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`keyword_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_category` (
  `name` varchar(200) DEFAULT NULL,
  `order_num` int(5) DEFAULT NULL,
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(200) NOT NULL DEFAULT '0',
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator_id` int(8) NOT NULL DEFAULT '0',
  `default_feature_template_name` varchar(200) NOT NULL DEFAULT '0',
  `shortname` varchar(20) DEFAULT NULL,
  `summarylength` int(5) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `newswire` enum('n','s','a','f') DEFAULT NULL,
  `center` enum('t','f') DEFAULT NULL,
  `description` text,
  `catclass` enum('m','t','l','h','i') DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_catlink` (
  `catid` int(10) DEFAULT NULL,
  `id` int(10) DEFAULT NULL,
  KEY `catid_id` (`catid`,`id`),
  KEY `id_catid` (`id`,`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL DEFAULT '',
  `start_date` datetime NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `location_other` varchar(60) DEFAULT NULL,
  `location_details` text,
  `event_type_id` int(11) DEFAULT NULL,
  `event_type_other` varchar(60) DEFAULT NULL,
  `event_topic_id` int(11) DEFAULT NULL,
  `event_topic_other` varchar(60) DEFAULT NULL,
  `contact_name` varchar(60) DEFAULT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `contact_phone` varchar(60) DEFAULT NULL,
  `description` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` tinyint(4) DEFAULT NULL,
  `confirmation_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_feature` (
  `feature_version_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL DEFAULT '0',
  `summary` text,
  `title1` varchar(200) DEFAULT NULL,
  `title2` varchar(200) DEFAULT NULL,
  `display_date` varchar(100) DEFAULT NULL,
  `order_num` int(5) DEFAULT NULL,
  `category_id` int(5) DEFAULT NULL,
  `template_name` varchar(200) NOT NULL DEFAULT '0',
  `creator_id` int(8) NOT NULL DEFAULT '0',
  `status` char(2) DEFAULT 'c',
  `tag` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `version_num` int(5) DEFAULT '1',
  `is_current_version` int(1) DEFAULT '1',
  `modifier_id` int(11) DEFAULT NULL,
  `image_link` varchar(200) DEFAULT NULL,
  `modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `language_id` tinyint(3) unsigned DEFAULT '1',
  `pushed_live` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`feature_version_id`),
  KEY `status_index` (`status`),
  KEY `is_current_version_index` (`is_current_version`),
  KEY `category_id_index` (`category_id`),
  KEY `feature_id_index` (`feature_id`),
  KEY `pushed_live` (`pushed_live`,`is_current_version`,`status`,`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_user` (
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `middle_name` varchar(20) NOT NULL DEFAULT '',
  `user_id` int(8) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`user_id`),
  KEY `username_index` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_sfactive_webcast` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `heading` varchar(100) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `date_entered` varchar(45) DEFAULT NULL,
  `article` text,
  `contact` varchar(80) DEFAULT NULL,
  `link` varchar(160) DEFAULT NULL,
  `address` varchar(160) DEFAULT NULL,
  `phone` varchar(80) DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `mime_type` varchar(50) DEFAULT NULL,
  `summary` text,
  `numcomment` int(5) DEFAULT NULL,
  `arttype` varchar(50) DEFAULT NULL,
  `html_file` varchar(160) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `linked_file` varchar(160) DEFAULT NULL,
  `mirrored` enum('f','t') DEFAULT NULL,
  `display` enum('f','t','g','l','r') DEFAULT 't',
  `rating` decimal(9,2) DEFAULT NULL,
  `artmime` enum('h','t') DEFAULT NULL,
  `language_id` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id_index` (`parent_id`),
  KEY `display_index` (`display`),
  KEY `mime_type_index` (`mime_type`),
  KEY `display_parent_id_index` (`display`,`parent_id`),
  KEY `date_index` (`created`,`parent_id`),
  KEY `photo_gallery` (`mime_type`,`display`,`arttype`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legacy_users` (
  `uid` int(5) NOT NULL DEFAULT '0',
  `login` varchar(10) DEFAULT NULL,
  `pass` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `host_ip` varchar(15) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address0` varchar(30) DEFAULT NULL,
  `address1` varchar(30) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `post_code` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(40) DEFAULT NULL,
  `description` text,
  `url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `ip_part1` tinyint(3) unsigned NOT NULL,
  `ip_part2` tinyint(3) unsigned NOT NULL,
  `ip_part3` tinyint(3) unsigned NOT NULL,
  `ip_part4` tinyint(3) unsigned NOT NULL,
  `http_method` varchar(9) NOT NULL,
  `url` varchar(512) NOT NULL,
  `referring_url` varchar(512) NOT NULL,
  `news_item_id` int(11) DEFAULT NULL,
  `parent_news_item_id` int(11) DEFAULT NULL,
  `full_headers` text NOT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `ip` (`ip`),
  KEY `ip_part1` (`ip_part1`,`ip_part2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_attachment` (
  `media_attachment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `upload_name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `original_file_name` varchar(255) NOT NULL DEFAULT '',
  `relative_path` varchar(128) NOT NULL DEFAULT '',
  `alt_tag` varchar(200) DEFAULT NULL,
  `media_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `upload_status_id` smallint(6) NOT NULL DEFAULT '0',
  `upload_type_id` smallint(6) NOT NULL DEFAULT '0',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `image_width` smallint(6) NOT NULL DEFAULT '0',
  `image_height` smallint(6) NOT NULL DEFAULT '0',
  `parent_attachment_id` int(11) unsigned NOT NULL DEFAULT '0',
  `external_source` varchar(200) DEFAULT NULL,
  `created_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `browser_compat` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`media_attachment_id`),
  KEY `file_name` (`original_file_name`),
  KEY `upload_path` (`relative_path`),
  KEY `media_type_id` (`media_type_id`),
  KEY `media_attachment_id` (`media_attachment_id`),
  KEY `parent_attachment_id` (`parent_attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18809511 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_type` (
  `media_type_id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `extension` varchar(16) NOT NULL DEFAULT '',
  `description` varchar(128) DEFAULT NULL,
  `mime_type` varchar(64) NOT NULL DEFAULT '',
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`media_type_id`),
  KEY `mime_type` (`mime_type`),
  KEY `media_type_id` (`media_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_type_grouping` (
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`media_type_grouping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item` (
  `news_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_item_id` int(11) unsigned DEFAULT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `current_version_id` int(11) unsigned DEFAULT NULL,
  `news_item_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `news_item_status_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `num_comments` smallint(11) unsigned NOT NULL DEFAULT '0',
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `syndicated` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`news_item_id`),
  KEY `creation_date` (`creation_date`,`created_by_id`),
  KEY `news_item_type_id` (`news_item_type_id`),
  KEY `current_version_id` (`current_version_id`),
  KEY `news_item_status_id` (`news_item_status_id`),
  KEY `parent_item_id` (`parent_item_id`),
  KEY `media_type_grouping_id` (`media_type_grouping_id`),
  KEY `media_type_status_index` (`media_type_grouping_id`,`news_item_type_id`,`news_item_status_id`),
  KEY `newswire_index` (`news_item_status_id`,`news_item_type_id`),
  KEY `mediawire_index` (`news_item_status_id`,`news_item_type_id`,`media_type_grouping_id`),
  KEY `media_type_status_id` (`media_type_grouping_id`,`news_item_type_id`,`news_item_status_id`,`news_item_id`),
  KEY `media_status_type_id` (`media_type_grouping_id`,`news_item_status_id`,`news_item_type_id`,`news_item_id`),
  KEY `id_mediawire_index` (`news_item_id`,`news_item_status_id`,`news_item_type_id`,`media_type_grouping_id`),
  KEY `id_newswire_index` (`news_item_id`,`news_item_status_id`,`news_item_type_id`),
  KEY `news_item_id` (`news_item_id`,`current_version_id`,`news_item_type_id`,`parent_item_id`,`news_item_status_id`,`creation_date`,`num_comments`,`media_type_grouping_id`),
  KEY `news_item_id_2` (`news_item_id`,`current_version_id`,`news_item_status_id`,`news_item_type_id`),
  KEY `breaking` (`news_item_type_id`,`parent_item_id`,`news_item_status_id`),
  KEY `calendar_index` (`news_item_type_id`,`news_item_status_id`,`current_version_id`,`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=18806035 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_category` (
  `news_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `category_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_item_id`,`category_id`),
  KEY `category_id` (`category_id`,`news_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_fulltext` (
  `news_item_id` int(11) NOT NULL,
  `normalized_text` text,
  PRIMARY KEY (`news_item_id`),
  FULLTEXT KEY `normalized_text` (`normalized_text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_keyword` (
  `news_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `keyword_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_item_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_page_order` (
  `news_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `page_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `order_num` smallint(11) unsigned NOT NULL DEFAULT '0',
  `display_option_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_item_id`,`page_id`),
  KEY `ordernum` (`order_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_status` (
  `news_item_status_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_in_user_search` binary(1) NOT NULL DEFAULT '\0',
  `is_hidden` binary(1) NOT NULL DEFAULT '0',
  `is_highlighted` binary(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_type` (
  `news_item_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`news_item_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_item_version` (
  `news_item_version_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title1` varchar(140) NOT NULL DEFAULT '',
  `title2` varchar(200) DEFAULT NULL,
  `displayed_author_name` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `displayed_date` datetime DEFAULT NULL,
  `event_duration` float NOT NULL DEFAULT '0',
  `summary` text,
  `text` text NOT NULL,
  `is_summary_html` tinyint(4) NOT NULL DEFAULT '0',
  `is_text_html` binary(1) NOT NULL DEFAULT '0',
  `related_url` varchar(255) DEFAULT NULL,
  `display_contact_info` binary(1) NOT NULL DEFAULT '0',
  `media_attachment_id` int(11) unsigned DEFAULT NULL,
  `thumbnail_media_attachment_id` int(11) unsigned DEFAULT '0',
  `version_creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version_created_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_item_version_id`),
  KEY `displayed_date` (`displayed_date`),
  KEY `news_item_id` (`news_item_id`),
  KEY `media_attachment_id` (`media_attachment_id`),
  KEY `thumbnail_media_attachment_id` (`thumbnail_media_attachment_id`),
  KEY `news_item_id_3` (`news_item_id`,`news_item_version_id`,`title1`,`displayed_author_name`),
  KEY `displayed_date_2` (`displayed_date`,`news_item_version_id`,`title1`,`news_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18945844 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newswire_type` (
  `newswire_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`newswire_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `page_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `long_display_name` varchar(128) NOT NULL DEFAULT '',
  `short_display_name` varchar(16) NOT NULL DEFAULT '',
  `css_news_item_id` int(11) NOT NULL,
  `banner_section_news_item_id` int(11) DEFAULT NULL,
  `overrided_banner_image_src` varchar(128) NOT NULL,
  `center_column_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `newswire_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `event_list_type_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `max_events_in_highlighted_list` int(11) NOT NULL,
  `max_timespan_in_days_for_highlighted_event_list` int(11) NOT NULL,
  `max_events_in_full_event_list` int(11) NOT NULL,
  `max_timespan_in_days_for_full_event_list` int(11) DEFAULT NULL,
  `header1_news_item_id` int(11) NOT NULL,
  `header2_news_item_id` int(11) NOT NULL,
  `footer1_news_item_id` int(11) NOT NULL,
  `footer2_news_item_id` int(11) NOT NULL,
  `items_per_newswire_section` smallint(11) unsigned NOT NULL DEFAULT '0',
  `relative_path` varchar(64) NOT NULL DEFAULT '',
  `include_in_readmore_links` binary(1) NOT NULL DEFAULT '1',
  `created_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_by_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_pushed_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  KEY `include_in_readmore_links` (`include_in_readmore_links`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_category` (
  `page_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `category_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shorturl` (
  `path` varchar(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`path`),
  KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spot` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `address` varchar(63) DEFAULT NULL,
  `city` tinyint(3) unsigned DEFAULT NULL,
  `zip` mediumint(5) unsigned zerofill DEFAULT NULL,
  `contact` varchar(63) DEFAULT NULL,
  `phone` varchar(31) DEFAULT NULL,
  `email` varchar(63) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `url` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(31) DEFAULT NULL,
  `abbrev` char(3) DEFAULT NULL,
  `country` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `organization` varchar(63) DEFAULT NULL,
  `mailing_address` varchar(63) DEFAULT NULL,
  `mailing_city` varchar(63) DEFAULT NULL,
  `mailing_state` varchar(5) DEFAULT NULL,
  `mailing_zip` varchar(10) DEFAULT NULL,
  `mailing_country` varchar(31) DEFAULT 'USA',
  `billing_address` varchar(63) DEFAULT NULL,
  `billing_city` varchar(63) DEFAULT NULL,
  `billing_state` varchar(5) DEFAULT NULL,
  `billing_zip` varchar(10) DEFAULT NULL,
  `billing_country` varchar(31) DEFAULT NULL,
  `phone` varchar(31) DEFAULT NULL,
  `fax` varchar(31) DEFAULT NULL,
  `email` varchar(63) DEFAULT NULL,
  `website` varchar(63) DEFAULT NULL,
  `status` enum('subscribed','unsubscribed','complimentary') DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `rate` varchar(63) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `cancellation_reason` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `copies` smallint(6) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_variables` (
  `system_var_id` smallint(11) NOT NULL AUTO_INCREMENT,
  `key1` varchar(64) NOT NULL DEFAULT '',
  `key2` varchar(64) DEFAULT NULL,
  `key3` varchar(64) DEFAULT NULL,
  `int_val` int(11) NOT NULL DEFAULT '0',
  `string_val` text NOT NULL,
  `time_val` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`system_var_id`),
  KEY `key1` (`key1`,`key2`,`key3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `template_id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `html` text NOT NULL,
  `template_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_type` (
  `template_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test1` (
  `test1` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_type` (
  `upload_type_id` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`upload_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `last_login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_admin_activity` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_failed_login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `num_failed_tries_since_last_login` int(11) NOT NULL DEFAULT '0',
  `has_login_rights` tinyint(4) NOT NULL DEFAULT '0',
  `contact_info_id` int(11) NOT NULL DEFAULT '0',
  `created_by_id` int(11) NOT NULL DEFAULT '0',
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `user_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `group_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `is_group_admin` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `action_id` smallint(11) unsigned DEFAULT NULL,
  `additional_info` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `associated_object_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `associated_object_id` (`associated_object_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password` (
  `user_id` smallint(11) unsigned NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL DEFAULT '',
  `temp_password` varchar(200) NOT NULL DEFAULT '',
  `temp_password_expiration` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
